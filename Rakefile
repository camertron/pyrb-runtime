$:.unshift File.join(File.dirname(__FILE__), 'lib')

require 'bundler'
require 'etc'
require 'parallel'
require 'pry-byebug'
require 'pyrb'
require 'pyrb/transpiler'
require 'pyrb/rspec/rake_task'
require 'rubygems/package_task'
require 'shellwords'

Bundler::GemHelper.install_tasks

task default: :spec

desc 'Run specs'
Pyrb::RSpec::RakeTask.new

def compile(files, compiler: :ruby)
  if compiler == :java
    system("java -jar ./ext/Pyrb.jar #{files.join(' ')}")
    return
  end

  durations = Parallel.map(files, in_processes: Etc.nprocessors) do |file|
    start = Time.now
    result = Pyrb::Transpiler.transpile(file, File.read(file))
    stop = Time.now
    duration = (stop - start).round(1)
    File.write("#{file.chomp('.py')}.rb", result)
    puts "Transpiled #{file} in #{duration} seconds"
    duration
  end

  total_duration = durations.inject(0) { |sum, dur| sum + dur }
  puts "Transpiled #{files.size} file(s) in #{total_duration.round(1)} seconds"
end

task :compile do |_, args|
  compile(args.to_a.flat_map { |a| Dir.glob(a) })
end

task :java_compile do |_, args|
  compile(args.to_a.flat_map { |a| Dir.glob(a) }, compiler: :java)
end

task :regen do
  files = Dir.glob('lib/pyrb/**/*.py')
  compile(files, compiler: ENV.fetch('COMPILER', 'ruby').to_sym)
end

namespace :spec do
  task :regen do
    files = Dir.glob('spec/**/*.py')
    compile(files, compiler: ENV.fetch('COMPILER', 'ruby').to_sym)
  end
end
