require 'spec_helper'

describe 'logical operators' do
  using Pyrb::Runtime

  describe 'and' do
    it 'true and true' do
      expect(true.and { true }).to eq(true)
    end

    it 'true and false' do
      expect(true.and { false }).to eq(false)
    end

    it 'false and true' do
      expect(false.and { true }).to eq(false)
    end

    it 'false and false' do
      expect(false.and { false }).to eq(false)
    end

    it '0 and false' do
      expect(0.and { false }).to eq(0)
    end

    it '0 and true' do
      expect(0.and { true }).to eq(0)
    end

    it 'false and 0' do
      expect(false.and { 0 }).to eq(false)
    end

    it 'true and 0' do
      expect(true.and { 0 }).to eq(0)
    end

    it "'a' and 'b'" do
      expect('a'.and { 'b' }).to eq('b')
    end

    it "'' and 'b'" do
      expect(''.and { 'b' }).to eq('')
    end

    it "'a' and ''" do
      expect('a'.and { '' }).to eq('')
    end

    it "'' and ''" do
      expect(''.and { '' }).to eq('')
    end

    it "'a' and true" do
      expect('a'.and { true }).to eq(true)
    end

    it "'a' and false" do
      expect('a'.and { false }).to eq(false)
    end

    it "true and 'b'" do
      expect(true.and { 'b' }).to eq('b')
    end

    it "false and 'b'" do
      expect(false.and { 'b' }).to eq(false)
    end
  end

  describe 'or' do
    it 'true or true' do
      expect(true.or { true }).to eq(true)
    end

    it 'true or false' do
      expect(true.or { false }).to eq(true)
    end

    it 'false or true' do
      expect(false.or { true }).to eq(true)
    end

    it 'false or false' do
      expect(false.or { false }).to eq(false)
    end

    it '0 or false' do
      expect(0.or { false }).to eq(false)
    end

    it '0 or true' do
      expect(0.or { true }).to eq(true)
    end

    it 'false or 0' do
      expect(false.or { 0 }).to eq(0)
    end

    it "true or 0" do
      expect(true.or { 0 }).to eq(true)
    end

    it "'a' or 'b'" do
      expect('a'.or { 'b' }).to eq('a')
    end

    it "'' or 'b'" do
      expect(''.or { 'b' }).to eq('b')
    end

    it "'a' or ''" do
      expect('a'.or { '' }).to eq('a')
    end

    it "'' or ''" do
      expect(''.or { '' }).to eq('')
    end

    it "'a' or true" do
      expect('a'.or { true }).to eq('a')
    end

    it "'a' or false" do
      expect('a'.or { false }).to eq('a')
    end

    it "true or 'b'" do
      expect(true.or { 'b' }).to eq(true)
    end

    it "false or 'b'" do
      expect(false.or { 'b' }).to eq('b')
    end
  end
end
