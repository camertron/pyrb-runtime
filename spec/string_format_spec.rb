require 'spec_helper'

describe Pyrb::StringFormat do
  context 'simple implicit positional replacements' do
    it 'replaces curlies with positional arguments' do
      expect(described_class.format('this is {} cool', ['very'])).to eq(
        'this is very cool'
      )
    end
  end

  context 'simple explicit positional replacements' do
    it 'replaces curlies with positional arguments' do
      expect(described_class.format('this is {0} cool', ['very'])).to eq(
        'this is very cool'
      )
    end

    it 'handles out-of-order replacements' do
      expect(described_class.format('this is {1} and {0} cool', ['very', 'awesome'])).to eq(
        'this is awesome and very cool'
      )
    end
  end

  context 'simple named replacements' do
    it 'replaces named arguments' do
      expect(described_class.format('this is {very} cool', 'very' => 'very')).to eq(
        'this is very cool'
      )
    end
  end

  context 'conversions' do
    it 'converts using __repr__' do
      expect(described_class.format('this is {!r} cool', ['very'])).to eq(
        "this is 'very' cool"
      )
    end

    it 'converts using __str__' do
      expect(described_class.format('this is {!s} cool', [['very', 'extremely']])).to eq(
        'this is ["very", "extremely"] cool'
      )
    end
  end

  context 'number formats' do
    context 'binary' do
      it 'emits a binary string' do
        expect(described_class.format('wow, 123 is {:b} in binary', [123])).to eq(
          'wow, 123 is 1111011 in binary'
        )
      end

      it 'emits a binary string with 0b prefix' do
        expect(described_class.format('wow, 123 is {:#b} in binary', [123])).to eq(
          'wow, 123 is 0b1111011 in binary'
        )
      end

      it 'groups every 4 digits' do
        expect(described_class.format('wow, 123 is {:#_b} in binary', [123])).to eq(
          'wow, 123 is 0b111_1011 in binary'
        )
      end

      it 'includes a negative sign' do
        expect(described_class.format('wow, -123 is {:#_b} in binary', [-123])).to eq(
          'wow, -123 is -0b111_1011 in binary'
        )
      end

      it 'includes a positive sign' do
        expect(described_class.format('wow, 123 is {:+#_b} in binary', [123])).to eq(
          'wow, 123 is +0b111_1011 in binary'
        )
      end

      it 'pads and aligns right by default' do
        expect(described_class.format('wow, 123 is {:+#15_b} in binary', [123])).to eq(
          'wow, 123 is     +0b111_1011 in binary'
        )
      end

      it 'pads and aligns left' do
        expect(described_class.format('wow, 123 is {:<+#15_b} in binary', [123])).to eq(
          'wow, 123 is +0b111_1011     in binary'
        )
      end

      it 'pads and aligns center' do
        expect(described_class.format('wow, 123 is {:^+#15_b} in binary', [123])).to eq(
          'wow, 123 is   +0b111_1011   in binary'
        )
      end

      it 'adds padding after the prefix' do
        expect(described_class.format('wow, 123 is {:=+#15_b} in binary', [123])).to eq(
          'wow, 123 is +0b    111_1011 in binary'
        )
      end

      it 'uses the given fill character' do
        expect(described_class.format('wow, 123 is {:*=+#15_b} in binary', [123])).to eq(
          'wow, 123 is +0b****111_1011 in binary'
        )
      end
    end

    context 'octal' do
      it 'emits an octal string' do
        expect(described_class.format('wow, 123 is {:o} in octal', [123])).to eq(
          'wow, 123 is 173 in octal'
        )
      end

      it 'emits an octal string with 0o prefix' do
        expect(described_class.format('wow, 123 is {:#o} in octal', [123])).to eq(
          'wow, 123 is 0o173 in octal'
        )
      end

      it 'groups every 4 digits' do
        expect(described_class.format('wow, 123456 is {:#_o} in octal', [123456])).to eq(
          'wow, 123456 is 0o36_1100 in octal'
        )
      end

      it 'includes a negative sign' do
        expect(described_class.format('wow, -123456 is {:#_o} in octal', [-123456])).to eq(
          'wow, -123456 is -0o36_1100 in octal'
        )
      end

      it 'includes a positive sign' do
        expect(described_class.format('wow, 123456 is {:+#_o} in octal', [123456])).to eq(
          'wow, 123456 is +0o36_1100 in octal'
        )
      end

      it 'pads and aligns right by default' do
        expect(described_class.format('wow, 123 is {:+#15_o} in octal', [123456])).to eq(
          'wow, 123 is      +0o36_1100 in octal'
        )
      end

      it 'pads and aligns left' do
        expect(described_class.format('wow, 123 is {:<+#15_o} in octal', [123456])).to eq(
          'wow, 123 is +0o36_1100      in octal'
        )
      end

      it 'pads and aligns center' do
        expect(described_class.format('wow, 123 is {:^+#15_o} in octal', [123456])).to eq(
          'wow, 123 is   +0o36_1100    in octal'
        )
      end

      it 'adds padding after the prefix' do
        expect(described_class.format('wow, 123 is {:=+#15_o} in octal', [123456])).to eq(
          'wow, 123 is +0o     36_1100 in octal'
        )
      end

      it 'uses the given fill character' do
        expect(described_class.format('wow, 123 is {:*=+#15_o} in octal', [123456])).to eq(
          'wow, 123 is +0o*****36_1100 in octal'
        )
      end
    end

    context 'decimal' do
      it 'emits a decimal string' do
        expect(described_class.format('wow, 123 is {:d} in decimal', [123])).to eq(
          'wow, 123 is 123 in decimal'
        )
      end

      it 'groups every 3 digits' do
        expect(described_class.format('wow, 12345 is {:,d} in decimal', [12345])).to eq(
          'wow, 12345 is 12,345 in decimal'
        )
      end

      it 'includes a negative sign' do
        expect(described_class.format('wow, -12345 is {:,d} in decimal', [-12345])).to eq(
          'wow, -12345 is -12,345 in decimal'
        )
      end

      it 'includes a positive sign' do
        expect(described_class.format('wow, 12345 is {:+,d} in decimal', [12345])).to eq(
          'wow, 12345 is +12,345 in decimal'
        )
      end

      it 'pads and aligns right by default' do
        expect(described_class.format('wow, 12345 is {:+#15,d} in decimal', [12345])).to eq(
          'wow, 12345 is         +12,345 in decimal'
        )
      end

      it 'pads and aligns left' do
        expect(described_class.format('wow, 12345 is {:<+#15,d} in decimal', [12345])).to eq(
          'wow, 12345 is +12,345         in decimal'
        )
      end

      it 'pads and aligns center' do
        expect(described_class.format('wow, 12345 is {:^+#15,d} in decimal', [12345])).to eq(
          'wow, 12345 is     +12,345     in decimal'
        )
      end

      it 'adds padding after the prefix' do
        expect(described_class.format('wow, 12345 is {:=+#15,d} in decimal', [12345])).to eq(
          'wow, 12345 is +        12,345 in decimal'
        )
      end

      it 'uses the given fill character' do
        expect(described_class.format('wow, 12345 is {:*=+#15,d} in decimal', [12345])).to eq(
          'wow, 12345 is +********12,345 in decimal'
        )
      end
    end

    context 'hexadecimal' do
      it 'emits a hexadecimal string' do
        expect(described_class.format('wow, 123 is {:x} in hex', [123])).to eq(
          'wow, 123 is 7b in hex'
        )
      end

      it 'emits an uppercased hexadecimal string' do
        expect(described_class.format('wow, 123 is {:X} in hex', [123])).to eq(
          'wow, 123 is 7B in hex'
        )
      end

      it 'emits a hexadecimal string with 0x prefix' do
        expect(described_class.format('wow, 123 is {:#x} in hex', [123])).to eq(
          'wow, 123 is 0x7b in hex'
        )
      end

      it 'emits an uppercased hexadecimal string with 0x prefix' do
        expect(described_class.format('wow, 123 is {:#X} in hex', [123])).to eq(
          'wow, 123 is 0x7B in hex'
        )
      end

      it 'groups every 4 digits' do
        expect(described_class.format('wow, 123456 is {:#_x} in hex', [123456])).to eq(
          'wow, 123456 is 0x1_e240 in hex'
        )
      end

      it 'includes a negative sign' do
        expect(described_class.format('wow, -123456 is {:#_x} in hex', [-123456])).to eq(
          'wow, -123456 is -0x1_e240 in hex'
        )
      end

      it 'includes a positive sign' do
        expect(described_class.format('wow, 123456 is {:+#_x} in hex', [123456])).to eq(
          'wow, 123456 is +0x1_e240 in hex'
        )
      end

      it 'pads and aligns right by default' do
        expect(described_class.format('wow, 123456 is {:+#15_x} in hex', [123456])).to eq(
          'wow, 123456 is       +0x1_e240 in hex'
        )
      end

      it 'pads and aligns left' do
        expect(described_class.format('wow, 123456 is {:<+#15_x} in hex', [123456])).to eq(
          'wow, 123456 is +0x1_e240       in hex'
        )
      end

      it 'pads and aligns center' do
        expect(described_class.format('wow, 123456 is {:^+#15_x} in hex', [123456])).to eq(
          'wow, 123456 is    +0x1_e240    in hex'
        )
      end

      it 'adds padding after the prefix' do
        expect(described_class.format('wow, 123456 is {:=+#15_x} in hex', [123456])).to eq(
          'wow, 123456 is +0x      1_e240 in hex'
        )
      end

      it 'uses the given fill character' do
        expect(described_class.format('wow, 123456 is {:*=+#15_x} in hex', [123456])).to eq(
          'wow, 123456 is +0x******1_e240 in hex'
        )
      end
    end

    context 'scientific notation' do
      it 'emits a scientific notation string' do
        expect(described_class.format('wow, 123456789123 is {:e} in sci', [123456789123])).to eq(
          'wow, 123456789123 is 1.234568e+11 in sci'
        )
      end

      it 'emits a scientific notation string with a negative exponent' do
        expect(described_class.format('wow, 0.00000000003 is {:e} in sci', [0.00000000003])).to eq(
          'wow, 0.00000000003 is 3.000000e-11 in sci'
        )
      end

      it 'emits a negative scientific notation string with a negative exponent' do
        expect(described_class.format('wow, -0.00000000003 is {:e} in sci', [-0.00000000003])).to eq(
          'wow, -0.00000000003 is -3.000000e-11 in sci'
        )
      end

      it 'emits a positive sign scientific notation string with a negative exponent' do
        expect(described_class.format('wow, 0.00000000003 is {:+e} in sci', [0.00000000003])).to eq(
          'wow, 0.00000000003 is +3.000000e-11 in sci'
        )
      end

      it 'ensures the exponent has a width of 2 (pads with zeroes)' do
        expect(described_class.format('wow, 123456789 is {:e} in sci', [123456789])).to eq(
          'wow, 123456789 is 1.234568e+08 in sci'
        )
      end

      it 'includes a negative sign' do
        expect(described_class.format('wow, -123456789 is {:e} in sci', [-123456789])).to eq(
          'wow, -123456789 is -1.234568e+08 in sci'
        )
      end

      it 'includes a positive sign' do
        expect(described_class.format('wow, 123456789 is {:+e} in sci', [123456789])).to eq(
          'wow, 123456789 is +1.234568e+08 in sci'
        )
      end

      it 'pads and aligns right by default' do
        expect(described_class.format('wow, 123456789 is {:+15e} in sci', [123456789])).to eq(
          'wow, 123456789 is   +1.234568e+08 in sci'
        )
      end

      it 'pads and aligns left' do
        expect(described_class.format('wow, 123456789 is {:<+15e} in sci', [123456789])).to eq(
          'wow, 123456789 is +1.234568e+08   in sci'
        )
      end

      it 'pads and aligns center' do
        expect(described_class.format('wow, 123456789 is {:^+15e} in sci', [123456789])).to eq(
          'wow, 123456789 is  +1.234568e+08  in sci'
        )
      end

      it 'adds padding after the prefix' do
        expect(described_class.format('wow, 123456789 is {:=+15e} in sci', [123456789])).to eq(
          'wow, 123456789 is +  1.234568e+08 in sci'
        )
      end

      it 'uses the given fill character' do
        expect(described_class.format('wow, 123456789 is {:*=+15e} in sci', [123456789])).to eq(
          'wow, 123456789 is +**1.234568e+08 in sci'
        )
      end

      it 'respects the given precision' do
        expect(described_class.format('wow, 123456789 is {:*=+15,.2e} in sci', [123456789])).to eq(
          'wow, 123456789 is +******1.23e+08 in sci'
        )
      end
    end

    context 'fixed (i.e. floating-point)' do
      it 'emits a fixed string' do
        expect(described_class.format('wow, 123.45 is {:f} in fixed-point', [123.45])).to eq(
          'wow, 123.45 is 123.450000 in fixed-point'
        )
      end

      it 'groups every 3 digits' do
        expect(described_class.format('wow, 12345.67 is {:,f} in fixed-point', [12345.67])).to eq(
          'wow, 12345.67 is 12,345.670000 in fixed-point'
        )
      end

      it 'includes a negative sign' do
        expect(described_class.format('wow, -12345.67 is {:,f} in fixed-point', [-12345.67])).to eq(
          'wow, -12345.67 is -12,345.670000 in fixed-point'
        )
      end

      it 'includes a positive sign' do
        expect(described_class.format('wow, 12345.67 is {:+,f} in fixed-point', [12345.67])).to eq(
          'wow, 12345.67 is +12,345.670000 in fixed-point'
        )
      end

      it 'emits "inf" for infinity' do
        expect(described_class.format('wow, 1.0 / 0 is {:f} in fixed-point', [1.0 / 0])).to eq(
          'wow, 1.0 / 0 is inf in fixed-point'
        )
      end

      it 'emits "INF" for uppercased infinity' do
        expect(described_class.format('wow, 1.0 / 0 is {:F} in fixed-point', [1.0 / 0])).to eq(
          'wow, 1.0 / 0 is INF in fixed-point'
        )
      end

      it 'emits "nan" for not-a-number' do
        expect(described_class.format('wow, 0.0 / 0 is {:f} in fixed-point', [0.0 / 0])).to eq(
          'wow, 0.0 / 0 is nan in fixed-point'
        )
      end

      it 'emits "NAN" for uppercased not-a-number' do
        expect(described_class.format('wow, 0.0 / 0 is {:F} in fixed-point', [0.0 / 0])).to eq(
          'wow, 0.0 / 0 is NAN in fixed-point'
        )
      end

      it 'pads and aligns right by default' do
        expect(described_class.format('wow, 12345.67 is {:+20,f} in fixed-point', [12345.67])).to eq(
          'wow, 12345.67 is       +12,345.670000 in fixed-point'
        )
      end

      it 'pads and aligns left' do
        expect(described_class.format('wow, 12345.67 is {:<+20,f} in fixed-point', [12345.67])).to eq(
          'wow, 12345.67 is +12,345.670000       in fixed-point'
        )
      end

      it 'pads and aligns center' do
        expect(described_class.format('wow, 12345.67 is {:^+20,f} in fixed-point', [12345.67])).to eq(
          'wow, 12345.67 is    +12,345.670000    in fixed-point'
        )
      end

      it 'adds padding after the prefix' do
        expect(described_class.format('wow, 12345.67 is {:=+20,f} in fixed-point', [12345.67])).to eq(
          'wow, 12345.67 is +      12,345.670000 in fixed-point'
        )
      end

      it 'uses the given fill character' do
        expect(described_class.format('wow, 12345.67 is {:*=+20,f} in fixed-point', [12345.67])).to eq(
          'wow, 12345.67 is +******12,345.670000 in fixed-point'
        )
      end

      it 'respects the given precision' do
        expect(described_class.format('wow, 1234.567 is {:*=+20,.2f} in fixed-point', [1234.567])).to eq(
          'wow, 1234.567 is +***********1,234.57 in fixed-point'
        )
      end
    end
  end
end
