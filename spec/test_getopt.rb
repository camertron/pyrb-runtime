# encoding: utf-8

require 'pyrb'

module Spec_test_getopt
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  verbose, run_doctest, EnvironmentVarGuard = import('verbose', 'run_doctest', 'EnvironmentVarGuard', from: 'test.support')
  unittest, _ = import('unittest')
  getopt, _ = import('getopt')

  exports['sentinel'] = Pyrb::Object.call()
  exports['GetoptTests'] = Pyrb::PythonClass.new('GetoptTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['setUp'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['env'] = EnvironmentVarGuard.call()
      if !!(("POSIXLY_CORRECT").in?(self_.attr('env')))
        self_.attr('env').del("POSIXLY_CORRECT")
      end
    end

    klass.attrs['tearDown'] = Pyrb.defn(self: nil) do |self_|
      self_.attr('env').fcall('__exit__')
      self_.del('env')
    end

    klass.attrs['assertError'] = Pyrb.defn(self: nil, args: { splat: true }, kwargs: { kwsplat: true }) do |self_, args, kwargs|
      self_.fcall('assertRaises', [getopt.attr('GetoptError'), *Pyrb.splat(args)], { **(kwargs) })
    end

    klass.attrs['test_short_has_arg'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [getopt.fcall('short_has_arg', ["a", "a:"])])
      self_.fcall('assertFalse', [getopt.fcall('short_has_arg', ["a", "a"])])
      self_.fcall('assertError', [getopt.attr('short_has_arg'), "a", "b"])
    end

    klass.attrs['test_long_has_args'] = Pyrb.defn(self: nil) do |self_|
      has_arg, option = Pyrb.deconstruct(2, getopt.fcall('long_has_args', ["abc", ["abc="]]))
      self_.fcall('assertTrue', [has_arg])
      self_.fcall('assertEqual', [option, "abc"])
      has_arg, option = Pyrb.deconstruct(2, getopt.fcall('long_has_args', ["abc", ["abc"]]))
      self_.fcall('assertFalse', [has_arg])
      self_.fcall('assertEqual', [option, "abc"])
      has_arg, option = Pyrb.deconstruct(2, getopt.fcall('long_has_args', ["abc", ["abcd"]]))
      self_.fcall('assertFalse', [has_arg])
      self_.fcall('assertEqual', [option, "abcd"])
      self_.fcall('assertError', [getopt.attr('long_has_args'), "abc", ["def"]])
      self_.fcall('assertError', [getopt.attr('long_has_args'), "abc", []])
      self_.fcall('assertError', [getopt.attr('long_has_args'), "abc", ["abcd", "abcde"]])
    end

    klass.attrs['test_do_shorts'] = Pyrb.defn(self: nil) do |self_|
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_shorts', [[], "a", "a", []]))
      self_.fcall('assertEqual', [opts, [["-a", ""]]])
      self_.fcall('assertEqual', [args, []])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_shorts', [[], "a1", "a:", []]))
      self_.fcall('assertEqual', [opts, [["-a", "1"]]])
      self_.fcall('assertEqual', [args, []])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_shorts', [[], "a", "a:", ["1"]]))
      self_.fcall('assertEqual', [opts, [["-a", "1"]]])
      self_.fcall('assertEqual', [args, []])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_shorts', [[], "a", "a:", ["1", "2"]]))
      self_.fcall('assertEqual', [opts, [["-a", "1"]]])
      self_.fcall('assertEqual', [args, ["2"]])
      self_.fcall('assertError', [getopt.attr('do_shorts'), [], "a1", "a", []])
      self_.fcall('assertError', [getopt.attr('do_shorts'), [], "a", "a:", []])
    end

    klass.attrs['test_do_longs'] = Pyrb.defn(self: nil) do |self_|
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_longs', [[], "abc", ["abc"], []]))
      self_.fcall('assertEqual', [opts, [["--abc", ""]]])
      self_.fcall('assertEqual', [args, []])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_longs', [[], "abc=1", ["abc="], []]))
      self_.fcall('assertEqual', [opts, [["--abc", "1"]]])
      self_.fcall('assertEqual', [args, []])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_longs', [[], "abc=1", ["abcd="], []]))
      self_.fcall('assertEqual', [opts, [["--abcd", "1"]]])
      self_.fcall('assertEqual', [args, []])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_longs', [[], "abc", ["ab", "abc", "abcd"], []]))
      self_.fcall('assertEqual', [opts, [["--abc", ""]]])
      self_.fcall('assertEqual', [args, []])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('do_longs', [[], "foo=42", ["foo-bar", "foo=", ], []]))
      self_.fcall('assertEqual', [opts, [["--foo", "42"]]])
      self_.fcall('assertEqual', [args, []])
      self_.fcall('assertError', [getopt.attr('do_longs'), [], "abc=1", ["abc"], []])
      self_.fcall('assertError', [getopt.attr('do_longs'), [], "abc", ["abc="], []])
    end

    klass.attrs['test_getopt'] = Pyrb.defn(self: nil) do |self_|
      cmdline = ["-a", "1", "-b", "--alpha=2", "--beta", "-a", "3", "-a", "", "--beta", "arg1", "arg2"]
      opts, args = Pyrb.deconstruct(2, getopt.fcall('getopt', [cmdline, "a:b", ["alpha=", "beta"]]))
      self_.fcall('assertEqual', [opts, [["-a", "1"], ["-b", ""], ["--alpha", "2"], ["--beta", ""], ["-a", "3"], ["-a", ""], ["--beta", ""]]])
      self_.fcall('assertEqual', [args, ["arg1", "arg2"]])
      self_.fcall('assertError', [getopt.attr('getopt'), cmdline, "a:b", ["alpha", "beta"]])
    end

    klass.attrs['test_gnu_getopt'] = Pyrb.defn(self: nil) do |self_|
      cmdline = ["-a", "arg1", "-b", "1", "--alpha", "--beta=2"]
      opts, args = Pyrb.deconstruct(2, getopt.fcall('gnu_getopt', [cmdline, "ab:", ["alpha", "beta="]]))
      self_.fcall('assertEqual', [args, ["arg1"]])
      self_.fcall('assertEqual', [opts, [["-a", ""], ["-b", "1"], ["--alpha", ""], ["--beta", "2"]]])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('gnu_getopt', [["-a", "-", "-b", "-"], "ab:", []]))
      self_.fcall('assertEqual', [args, ["-"]])
      self_.fcall('assertEqual', [opts, [["-a", ""], ["-b", "-"]]])
      opts, args = Pyrb.deconstruct(2, getopt.fcall('gnu_getopt', [cmdline, "+ab:", ["alpha", "beta="]]))
      self_.fcall('assertEqual', [opts, [["-a", ""]]])
      self_.fcall('assertEqual', [args, ["arg1", "-b", "1", "--alpha", "--beta=2"]])
      self_.attrs['env']["POSIXLY_CORRECT"] = "1"
      opts, args = Pyrb.deconstruct(2, getopt.fcall('gnu_getopt', [cmdline, "ab:", ["alpha", "beta="]]))
      self_.fcall('assertEqual', [opts, [["-a", ""]]])
      self_.fcall('assertEqual', [args, ["arg1", "-b", "1", "--alpha", "--beta=2"]])
    end

    klass.attrs['test_libref_examples'] = Pyrb.defn(self: nil) do |self_|
      s = <<~__DOC__
      Examples from the Library Reference:  Doc/lib/libgetopt.tex

      An example using only Unix style options:

      >>> import getopt
      >>> args = '-a -b -cfoo -d bar a1 a2'.split()
      >>> args
      ['-a', '-b', '-cfoo', '-d', 'bar', 'a1', 'a2']
      >>> optlist, args = getopt.getopt(args, 'abc:d:')
      >>> optlist
      [('-a', ''), ('-b', ''), ('-c', 'foo'), ('-d', 'bar')]
      >>> args
      ['a1', 'a2']

      Using long option names is equally easy:

      >>> s = '--condition=foo --testing --output-file abc.def -x a1 a2'
      >>> args = s.split()
      >>> args
      ['--condition=foo', '--testing', '--output-file', 'abc.def', '-x', 'a1', 'a2']
      >>> optlist, args = getopt.getopt(args, 'x', [
      ...     'condition=', 'output-file=', 'testing'])
      >>> optlist
      [('--condition', 'foo'), ('--testing', ''), ('--output-file', 'abc.def'), ('-x', '')]
      >>> args
      ['a1', 'a2']
      __DOC__

      types, _ = import('types')
      m = types.fcall('ModuleType', ["libreftest", s])
      run_doctest.call([m, verbose])
    end

    klass.attrs['test_issue4629'] = Pyrb.defn(self: nil) do |self_|
      longopts, shortopts = Pyrb.deconstruct(2, getopt.fcall('getopt', [["--help="], "", ["help="]]))
      self_.fcall('assertEqual', [longopts, [["--help", ""]]])
      longopts, shortopts = Pyrb.deconstruct(2, getopt.fcall('getopt', [["--help=x"], "", ["help="]]))
      self_.fcall('assertEqual', [longopts, [["--help", "x"]]])
      self_.fcall('assertRaises', [getopt.attr('GetoptError'), getopt.attr('getopt'), ["--help="], "", ["help"]])
    end
  end

  GetoptTests = exports['GetoptTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
