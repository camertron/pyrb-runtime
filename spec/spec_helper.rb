$:.push(__dir__)

require 'rspec'
require 'pry-byebug'
require 'pyrb'
require 'pyrb/runtime'

# require 'support/rspec_test_case'
# require 'support/rspec_helpers'

Pyrb::Sys.exports['path'].unshift(__dir__)

RSpec.configure do |config|
  # config.extend(RSpecHelpers)
end
