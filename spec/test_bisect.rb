# encoding: utf-8

require 'pyrb'

module Spec_test_bisect
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  sys, _ = import('sys')
  unittest, _ = import('unittest')
  UserList, _ = import('UserList', from: 'collections')
  py_bisect, _ = import('bisect')

  exports['Range'] = Pyrb::PythonClass.new('Range', [Pyrb::Object]).tap do |klass|
    "A trivial range()-like object that has an insert() method."
    klass.attrs['__init__'] = Pyrb.defn(self: nil, start: nil, stop: nil) do |self_, start, stop|
      self_.attrs['start'] = start
      self_.attrs['stop'] = stop
      self_.attrs['last_insert'] = nil
    end

    klass.attrs['__len__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('stop') - self_.attr('start'))
    end

    klass.attrs['__getitem__'] = Pyrb.defn(self: nil, idx: nil) do |self_, idx|
      n = self_.attr('stop') - self_.attr('start')
      if !!(idx < 0)
        idx += n
      end

      if !!(idx >= n)
        Pyrb.raize(Pyrb::IndexError.call([idx]))
      end

      throw(:return, self_.attr('start') + idx)
    end

    klass.attrs['insert'] = Pyrb.defn(self: nil, idx: nil, item: nil) do |self_, idx, item|
      self_.attrs['last_insert'] = idx, item
    end
  end

  Range = exports['Range']

  exports['TestBisect'] = Pyrb::PythonClass.new('TestBisect', []).tap do |klass|
    klass.attrs['setUp'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['precomputedCases'] = [[self_.attr('module').attr('bisect_right'), [], 1, 0], [self_.attr('module').attr('bisect_right'), [1], 0, 0], [self_.attr('module').attr('bisect_right'), [1], 1, 1], [self_.attr('module').attr('bisect_right'), [1], 2, 1], [self_.attr('module').attr('bisect_right'), [1, 1], 0, 0], [self_.attr('module').attr('bisect_right'), [1, 1], 1, 2], [self_.attr('module').attr('bisect_right'), [1, 1], 2, 2], [self_.attr('module').attr('bisect_right'), [1, 1, 1], 0, 0], [self_.attr('module').attr('bisect_right'), [1, 1, 1], 1, 3], [self_.attr('module').attr('bisect_right'), [1, 1, 1], 2, 3], [self_.attr('module').attr('bisect_right'), [1, 1, 1, 1], 0, 0], [self_.attr('module').attr('bisect_right'), [1, 1, 1, 1], 1, 4], [self_.attr('module').attr('bisect_right'), [1, 1, 1, 1], 2, 4], [self_.attr('module').attr('bisect_right'), [1, 2], 0, 0], [self_.attr('module').attr('bisect_right'), [1, 2], 1, 1], [self_.attr('module').attr('bisect_right'), [1, 2], 1.5, 1], [self_.attr('module').attr('bisect_right'), [1, 2], 2, 2], [self_.attr('module').attr('bisect_right'), [1, 2], 3, 2], [self_.attr('module').attr('bisect_right'), [1, 1, 2, 2], 0, 0], [self_.attr('module').attr('bisect_right'), [1, 1, 2, 2], 1, 2], [self_.attr('module').attr('bisect_right'), [1, 1, 2, 2], 1.5, 2], [self_.attr('module').attr('bisect_right'), [1, 1, 2, 2], 2, 4], [self_.attr('module').attr('bisect_right'), [1, 1, 2, 2], 3, 4], [self_.attr('module').attr('bisect_right'), [1, 2, 3], 0, 0], [self_.attr('module').attr('bisect_right'), [1, 2, 3], 1, 1], [self_.attr('module').attr('bisect_right'), [1, 2, 3], 1.5, 1], [self_.attr('module').attr('bisect_right'), [1, 2, 3], 2, 2], [self_.attr('module').attr('bisect_right'), [1, 2, 3], 2.5, 2], [self_.attr('module').attr('bisect_right'), [1, 2, 3], 3, 3], [self_.attr('module').attr('bisect_right'), [1, 2, 3], 4, 3], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 0, 0], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 1, 1], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 1.5, 1], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 2, 3], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 2.5, 3], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 3, 6], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 3.5, 6], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 4, 10], [self_.attr('module').attr('bisect_right'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 5, 10], [self_.attr('module').attr('bisect_left'), [], 1, 0], [self_.attr('module').attr('bisect_left'), [1], 0, 0], [self_.attr('module').attr('bisect_left'), [1], 1, 0], [self_.attr('module').attr('bisect_left'), [1], 2, 1], [self_.attr('module').attr('bisect_left'), [1, 1], 0, 0], [self_.attr('module').attr('bisect_left'), [1, 1], 1, 0], [self_.attr('module').attr('bisect_left'), [1, 1], 2, 2], [self_.attr('module').attr('bisect_left'), [1, 1, 1], 0, 0], [self_.attr('module').attr('bisect_left'), [1, 1, 1], 1, 0], [self_.attr('module').attr('bisect_left'), [1, 1, 1], 2, 3], [self_.attr('module').attr('bisect_left'), [1, 1, 1, 1], 0, 0], [self_.attr('module').attr('bisect_left'), [1, 1, 1, 1], 1, 0], [self_.attr('module').attr('bisect_left'), [1, 1, 1, 1], 2, 4], [self_.attr('module').attr('bisect_left'), [1, 2], 0, 0], [self_.attr('module').attr('bisect_left'), [1, 2], 1, 0], [self_.attr('module').attr('bisect_left'), [1, 2], 1.5, 1], [self_.attr('module').attr('bisect_left'), [1, 2], 2, 1], [self_.attr('module').attr('bisect_left'), [1, 2], 3, 2], [self_.attr('module').attr('bisect_left'), [1, 1, 2, 2], 0, 0], [self_.attr('module').attr('bisect_left'), [1, 1, 2, 2], 1, 0], [self_.attr('module').attr('bisect_left'), [1, 1, 2, 2], 1.5, 2], [self_.attr('module').attr('bisect_left'), [1, 1, 2, 2], 2, 2], [self_.attr('module').attr('bisect_left'), [1, 1, 2, 2], 3, 4], [self_.attr('module').attr('bisect_left'), [1, 2, 3], 0, 0], [self_.attr('module').attr('bisect_left'), [1, 2, 3], 1, 0], [self_.attr('module').attr('bisect_left'), [1, 2, 3], 1.5, 1], [self_.attr('module').attr('bisect_left'), [1, 2, 3], 2, 1], [self_.attr('module').attr('bisect_left'), [1, 2, 3], 2.5, 2], [self_.attr('module').attr('bisect_left'), [1, 2, 3], 3, 2], [self_.attr('module').attr('bisect_left'), [1, 2, 3], 4, 3], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 0, 0], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 1, 0], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 1.5, 1], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 2, 1], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 2.5, 3], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 3, 3], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 3.5, 6], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 4, 6], [self_.attr('module').attr('bisect_left'), [1, 2, 2, 3, 3, 3, 4, 4, 4, 4], 5, 10]]
    end

    klass.attrs['test_precomputed'] = Pyrb.defn(self: nil) do |self_|
      (self_.attr('precomputedCases')).each do |func, data, elem, expected|
        self_.fcall('assertEqual', [func.call([data, elem]), expected])
        self_.fcall('assertEqual', [func.call([UserList.call([data]), elem]), expected])
      end
    end

    klass.attrs['test_negative_lo'] = Pyrb.defn(self: nil) do |self_|
      mod = self_.attr('module')
      self_.fcall('assertRaises', [Pyrb::ValueError, mod.attr('bisect_left'), [1, 2, 3], 5, -1, 3])
      self_.fcall('assertRaises', [Pyrb::ValueError, mod.attr('bisect_right'), [1, 2, 3], 5, -1, 3])
      self_.fcall('assertRaises', [Pyrb::ValueError, mod.attr('insort_left'), [1, 2, 3], 5, -1, 3])
      self_.fcall('assertRaises', [Pyrb::ValueError, mod.attr('insort_right'), [1, 2, 3], 5, -1, 3])
    end

    klass.attrs['test_large_range'] = Pyrb.defn(self: nil) do |self_|
      mod = self_.attr('module')
      n = sys.attr('maxsize')
      data = Pyrb.range.call([n - 1])
      self_.fcall('assertEqual', [mod.fcall('bisect_left', [data, n - 3]), n - 3])
      self_.fcall('assertEqual', [mod.fcall('bisect_right', [data, n - 3]), n - 2])
      self_.fcall('assertEqual', [mod.fcall('bisect_left', [data, n - 3, n - 10, n]), n - 3])
      self_.fcall('assertEqual', [mod.fcall('bisect_right', [data, n - 3, n - 10, n]), n - 2])
    end

    klass.attrs['test_large_pyrange'] = Pyrb.defn(self: nil) do |self_|
      mod = self_.attr('module')
      n = sys.attr('maxsize')
      data = exports['Range'].call([0, n - 1])
      self_.fcall('assertEqual', [mod.fcall('bisect_left', [data, n - 3]), n - 3])
      self_.fcall('assertEqual', [mod.fcall('bisect_right', [data, n - 3]), n - 2])
      self_.fcall('assertEqual', [mod.fcall('bisect_left', [data, n - 3, n - 10, n]), n - 3])
      self_.fcall('assertEqual', [mod.fcall('bisect_right', [data, n - 3, n - 10, n]), n - 2])
      x = n - 100
      mod.fcall('insort_left', [data, x, x - 50, x + 50])
      self_.fcall('assertEqual', [data.attr('last_insert'), [x, x]])
      x = n - 200
      mod.fcall('insort_right', [data, x, x - 50, x + 50])
      self_.fcall('assertEqual', [data.attr('last_insert'), [x + 1, x]])
    end

    klass.attrs['test_random'] = Pyrb.defn(self: nil, n: { default: 25 }) do |self_, n|
      data, elem, ip = nil

      randrange, _ = import('randrange', from: 'random')
      (Pyrb.range.call([n])).each do |i|
        data = Pyrb.range.call([i]).fcall('__iter__').map do |j|
          randrange.call([0, n, 2])
        end

        data.fcall('sort')
        elem = randrange.call([-1, n + 1])
        ip = self_.attr('module').fcall('bisect_left', [data, elem])
        if !!(ip < Pyrb.len.call([data]))
          self_.fcall('assertTrue', [elem <= data[ip]])
        end

        if !!(ip > 0)
          self_.fcall('assertTrue', [data[ip - 1] < elem])
        end

        ip = self_.attr('module').fcall('bisect_right', [data, elem])
        if !!(ip < Pyrb.len.call([data]))
          self_.fcall('assertTrue', [elem < data[ip]])
        end

        if !!(ip > 0)
          self_.fcall('assertTrue', [data[ip - 1] <= elem])
        end
      end
    end

    klass.attrs['test_optionalSlicing'] = Pyrb.defn(self: nil) do |self_|
      ip = nil

      (self_.attr('precomputedCases')).each do |func, data, elem, expected|
        (Pyrb.range.call([4])).each do |lo|
          lo = Pyrb.min.call([Pyrb.len.call([data]), lo])
          (Pyrb.range.call([3, 8])).each do |hi|
            hi = Pyrb.min.call([Pyrb.len.call([data]), hi])
            ip = func.call([data, elem, lo, hi])
            self_.fcall('assertTrue', [lo <= ip && ip <= hi])
            if !!((func.object_id == self_.attr('module').attr('bisect_left').object_id).and { ip < hi })
              self_.fcall('assertTrue', [elem <= data[ip]])
            end

            if !!((func.object_id == self_.attr('module').attr('bisect_left').object_id).and { ip > lo })
              self_.fcall('assertTrue', [data[ip - 1] < elem])
            end

            if !!((func.object_id == self_.attr('module').attr('bisect_right').object_id).and { ip < hi })
              self_.fcall('assertTrue', [elem < data[ip]])
            end

            if !!((func.object_id == self_.attr('module').attr('bisect_right').object_id).and { ip > lo })
              self_.fcall('assertTrue', [data[ip - 1] <= elem])
            end

            self_.fcall('assertEqual', [ip, Pyrb.max.call([lo, Pyrb.min.call([hi, expected])])])
          end
        end
      end
    end

    klass.attrs['test_backcompatibility'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [self_.attr('module').attr('bisect'), self_.attr('module').attr('bisect_right')])
    end

    klass.attrs['test_keyword_args'] = Pyrb.defn(self: nil) do |self_|
      data = [10, 20, 30, 40, 50]
      self_.fcall('assertEqual', [self_.attr('module').fcall('bisect_left', [], { a: data, x: 25, lo: 1, hi: 3 }), 2])
      self_.fcall('assertEqual', [self_.attr('module').fcall('bisect_right', [], { a: data, x: 25, lo: 1, hi: 3 }), 2])
      self_.fcall('assertEqual', [self_.attr('module').fcall('bisect', [], { a: data, x: 25, lo: 1, hi: 3 }), 2])
      self_.attr('module').fcall('insort_left', [], { a: data, x: 25, lo: 1, hi: 3 })
      self_.attr('module').fcall('insort_right', [], { a: data, x: 25, lo: 1, hi: 3 })
      self_.attr('module').fcall('insort', [], { a: data, x: 25, lo: 1, hi: 3 })
      self_.fcall('assertEqual', [data, [10, 20, 25, 25, 25, 30, 40, 50]])
    end
  end

  TestBisect = exports['TestBisect']

  exports['TestBisectPython'] = Pyrb::PythonClass.new('TestBisectPython', [exports['TestBisect'], unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['module'] = py_bisect
  end

  TestBisectPython = exports['TestBisectPython']

  exports['TestInsort'] = Pyrb::PythonClass.new('TestInsort', []).tap do |klass|
    klass.attrs['test_vsBuiltinSort'] = Pyrb.defn(self: nil, n: { default: 500 }) do |self_, n|
      digit, f = nil

      choice, _ = import('choice', from: 'random')
      ([Pyrb::List.call(), UserList.call()]).each do |insorted|
        (Pyrb.range.call([n])).each do |i|
          digit = choice.call(["0123456789"])
          if !!((digit).in?("02468"))
            f = self_.attr('module').attr('insort_left')
          else
            f = self_.attr('module').attr('insort_right')
          end

          f.call([insorted, digit])
        end
        self_.fcall('assertEqual', [Pyrb.sorted.call([insorted]), insorted])
      end
    end

    klass.attrs['test_backcompatibility'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [self_.attr('module').attr('insort'), self_.attr('module').attr('insort_right')])
    end

    klass.attrs['test_listDerived'] = Pyrb.defn(self: nil) do |self_|
      List ||= Pyrb::PythonClass.new('List', [Pyrb::List]).tap do |klass|
        klass.attrs['data'] = []
        klass.attrs['insert'] = Pyrb.defn(self: nil, index: nil, item: nil) do |self2, index, item|
          self2.attr('data').fcall('insert', [index, item])
        end
      end

      lst = List.call()
      self_.attr('module').fcall('insort_left', [lst, 10])
      self_.attr('module').fcall('insort_right', [lst, 5])
      self_.fcall('assertEqual', [[5, 10], lst.attr('data')])
    end
  end

  TestInsort = exports['TestInsort']

  exports['TestInsortPython'] = Pyrb::PythonClass.new('TestInsortPython', [exports['TestInsort'], unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['module'] = py_bisect
  end

  TestInsortPython = exports['TestInsortPython']

  exports['LenOnly'] = Pyrb::PythonClass.new('LenOnly', []).tap do |klass|
    "Dummy sequence class defining __len__ but not __getitem__."
    klass.attrs['__len__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, 10)
    end
  end

  LenOnly = exports['LenOnly']

  exports['GetOnly'] = Pyrb::PythonClass.new('GetOnly', []).tap do |klass|
    "Dummy sequence class defining __getitem__ but not __len__."
    klass.attrs['__getitem__'] = Pyrb.defn(self: nil, ndx: nil) do |self_, ndx|
      throw(:return, 10)
    end
  end

  GetOnly = exports['GetOnly']

  exports['CmpErr'] = Pyrb::PythonClass.new('CmpErr', []).tap do |klass|
    "Dummy element that always raises an error during comparison"
    klass.attrs['__lt__'] = Pyrb.defn(self: nil, other: nil) do |self_, other|
      Pyrb.raize(Pyrb::ZeroDivisionError)
    end

    klass.attrs['__gt__'] = klass.attrs['__lt__']
    klass.attrs['__le__'] = klass.attrs['__lt__']
    klass.attrs['__ge__'] = klass.attrs['__lt__']
    klass.attrs['__eq__'] = klass.attrs['__lt__']
    klass.attrs['__ne__'] = klass.attrs['__lt__']
  end

  CmpErr = exports['CmpErr']

  exports['TestErrorHandling'] = Pyrb::PythonClass.new('TestErrorHandling', []).tap do |klass|
    klass.attrs['test_non_sequence'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('bisect_left'), self_.attr('module').attr('bisect_right'), self_.attr('module').attr('insort_left'), self_.attr('module').attr('insort_right')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, 10, 10])
      end
    end

    klass.attrs['test_len_only'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('bisect_left'), self_.attr('module').attr('bisect_right'), self_.attr('module').attr('insort_left'), self_.attr('module').attr('insort_right')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, exports['LenOnly'].call(), 10])
      end
    end

    klass.attrs['test_get_only'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('bisect_left'), self_.attr('module').attr('bisect_right'), self_.attr('module').attr('insort_left'), self_.attr('module').attr('insort_right')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, exports['GetOnly'].call(), 10])
      end
    end

    klass.attrs['test_cmp_err'] = Pyrb.defn(self: nil) do |self_|
      seq = [exports['CmpErr'].call(), exports['CmpErr'].call(), exports['CmpErr'].call()]
      ([self_.attr('module').attr('bisect_left'), self_.attr('module').attr('bisect_right'), self_.attr('module').attr('insort_left'), self_.attr('module').attr('insort_right')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::ZeroDivisionError, f, seq, 10])
      end
    end

    klass.attrs['test_arg_parsing'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('bisect_left'), self_.attr('module').attr('bisect_right'), self_.attr('module').attr('insort_left'), self_.attr('module').attr('insort_right')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, 10])
      end
    end
  end

  TestErrorHandling = exports['TestErrorHandling']

  exports['TestErrorHandlingPython'] = Pyrb::PythonClass.new('TestErrorHandlingPython', [exports['TestErrorHandling'], unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['module'] = py_bisect
  end

  TestErrorHandlingPython = exports['TestErrorHandlingPython']

  exports['TestDocExample'] = Pyrb::PythonClass.new('TestDocExample', []).tap do |klass|
    klass.attrs['test_grades'] = Pyrb.defn(self: nil) do |self_|
      grade = Pyrb.defn(score: nil, breakpoints: { default: [60, 70, 80, 90] }, grades: { default: "FDCBA" }) do |score, breakpoints, grades|
        i = self_.attr('module').fcall('bisect', [breakpoints, score])
        throw(:return, grades[i])
      end

      result = [33, 99, 77, 70, 89, 90, 100].fcall('__iter__').map do |score|
        grade.call([score])
      end

      self_.fcall('assertEqual', [result, ["F", "A", "C", "C", "B", "A", "A"]])
    end

    klass.attrs['test_colors'] = Pyrb.defn(self: nil) do |self_|
      data = [["red", 5], ["blue", 1], ["yellow", 8], ["black", 0]]
      data.fcall('sort', [], { key: Pyrb.defn(r: nil) { |r| r[1] } })
      keys = data.fcall('__iter__').map do |r|
        r[1]
      end

      bisect_left = self_.attr('module').attr('bisect_left')
      self_.fcall('assertEqual', [data[bisect_left.call([keys, 0])], ["black", 0]])
      self_.fcall('assertEqual', [data[bisect_left.call([keys, 1])], ["blue", 1]])
      self_.fcall('assertEqual', [data[bisect_left.call([keys, 5])], ["red", 5]])
      self_.fcall('assertEqual', [data[bisect_left.call([keys, 8])], ["yellow", 8]])
    end
  end

  TestDocExample = exports['TestDocExample']

  exports['TestDocExamplePython'] = Pyrb::PythonClass.new('TestDocExamplePython', [exports['TestDocExample'], unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['module'] = py_bisect
  end

  TestDocExamplePython = exports['TestDocExamplePython']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
