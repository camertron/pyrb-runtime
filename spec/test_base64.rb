# encoding: utf-8

require 'pyrb'

module Spec_test_base64
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  support, _ = import('support', from: 'test')
  base64, _ = import('base64')
  binascii, _ = import('binascii')
  os, _ = import('os')
  array, _ = import('array', from: 'array')
  script_helper, _ = import('script_helper', from: 'test.support')

  exports['LegacyBase64TestCase'] = Pyrb::PythonClass.new('LegacyBase64TestCase', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['check_type_errors'] = Pyrb.defn(self: nil, f: nil) do |self_, f|
      self_.fcall('assertRaises', [Pyrb::TypeError, f, ""])
      self_.fcall('assertRaises', [Pyrb::TypeError, f, []])
      multidimensional = Pyrb::Memoryview.call([Pyrb::Bytes.call(["1234"])]).fcall('cast', ["B", [2, 2]])
      self_.fcall('assertRaises', [Pyrb::TypeError, f, multidimensional])
      int_data = Pyrb::Memoryview.call([Pyrb::Bytes.call(["1234"])]).fcall('cast', ["I"])
      self_.fcall('assertRaises', [Pyrb::TypeError, f, int_data])
    end

    klass.attrs['test_encodestring_warns'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertWarns', [Pyrb::DeprecationWarning])]) do
        base64.fcall('encodestring', [Pyrb::Bytes.call(["www.python.org"])])
      end
    end

    klass.attrs['test_decodestring_warns'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertWarns', [Pyrb::DeprecationWarning])]) do
        base64.fcall('decodestring', [Pyrb::Bytes.call(["d3d3LnB5dGhvbi5vcmc=\n"])])
      end
    end

    klass.attrs['test_encodebytes'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('encodebytes', [Pyrb::Bytes.call(["www.python.org"])]), Pyrb::Bytes.call(["d3d3LnB5dGhvbi5vcmc=\n"])])
      eq.call([base64.fcall('encodebytes', [Pyrb::Bytes.call(["a"])]), Pyrb::Bytes.call(["YQ==\n"])])
      eq.call([base64.fcall('encodebytes', [Pyrb::Bytes.call(["ab"])]), Pyrb::Bytes.call(["YWI=\n"])])
      eq.call([base64.fcall('encodebytes', [Pyrb::Bytes.call(["abc"])]), Pyrb::Bytes.call(["YWJj\n"])])
      eq.call([base64.fcall('encodebytes', [Pyrb::Bytes.call([""])]), Pyrb::Bytes.call([""])])
      eq.call([base64.fcall('encodebytes', [(Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz"]) + Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"]))]), (Pyrb::Bytes.call(["YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNE"]) + Pyrb::Bytes.call(["RUZHSElKS0xNTk9QUVJTVFVWV1hZWjAxMjM0\nNT"]) + Pyrb::Bytes.call(["Y3ODkhQCMwXiYqKCk7Ojw+LC4gW117fQ==\n"]))])
      eq.call([base64.fcall('encodebytes', [Pyrb::Bytearray.call([Pyrb::Bytes.call(["abc"])])]), Pyrb::Bytes.call(["YWJj\n"])])
      eq.call([base64.fcall('encodebytes', [Pyrb::Memoryview.call([Pyrb::Bytes.call(["abc"])])]), Pyrb::Bytes.call(["YWJj\n"])])
      eq.call([base64.fcall('encodebytes', [array.call(["B", Pyrb::Bytes.call(["abc"])])]), Pyrb::Bytes.call(["YWJj\n"])])
      self_.fcall('check_type_errors', [base64.attr('encodebytes')])
    end

    klass.attrs['test_decodebytes'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('decodebytes', [Pyrb::Bytes.call(["d3d3LnB5dGhvbi5vcmc=\n"])]), Pyrb::Bytes.call(["www.python.org"])])
      eq.call([base64.fcall('decodebytes', [Pyrb::Bytes.call(["YQ==\n"])]), Pyrb::Bytes.call(["a"])])
      eq.call([base64.fcall('decodebytes', [Pyrb::Bytes.call(["YWI=\n"])]), Pyrb::Bytes.call(["ab"])])
      eq.call([base64.fcall('decodebytes', [Pyrb::Bytes.call(["YWJj\n"])]), Pyrb::Bytes.call(["abc"])])
      eq.call([base64.fcall('decodebytes', [(Pyrb::Bytes.call(["YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNE"]) + Pyrb::Bytes.call(["RUZHSElKS0xNTk9QUVJTVFVWV1hZWjAxMjM0\nNT"]) + Pyrb::Bytes.call(["Y3ODkhQCMwXiYqKCk7Ojw+LC4gW117fQ==\n"]))]), (Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz"]) + Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"]))])
      eq.call([base64.fcall('decodebytes', [Pyrb::Bytes.call([""])]), Pyrb::Bytes.call([""])])
      eq.call([base64.fcall('decodebytes', [Pyrb::Bytearray.call([Pyrb::Bytes.call(["YWJj\n"])])]), Pyrb::Bytes.call(["abc"])])
      eq.call([base64.fcall('decodebytes', [Pyrb::Memoryview.call([Pyrb::Bytes.call(["YWJj\n"])])]), Pyrb::Bytes.call(["abc"])])
      eq.call([base64.fcall('decodebytes', [array.call(["B", Pyrb::Bytes.call(["YWJj\n"])])]), Pyrb::Bytes.call(["abc"])])
      self_.fcall('check_type_errors', [base64.attr('decodebytes')])
    end

    klass.attrs['test_encode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      bytesIO, stringIO = import('BytesIO', 'StringIO', from: 'io')
      infp = bytesIO.call([(Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz"]) + Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"]))])
      outfp = bytesIO.call()
      base64.fcall('encode', [infp, outfp])
      eq.call([outfp.fcall('getvalue'), (Pyrb::Bytes.call(["YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNE"]) + Pyrb::Bytes.call(["RUZHSElKS0xNTk9QUVJTVFVWV1hZWjAxMjM0\nNT"]) + Pyrb::Bytes.call(["Y3ODkhQCMwXiYqKCk7Ojw+LC4gW117fQ==\n"]))])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('encode'), stringIO.call(["abc"]), bytesIO.call()])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('encode'), bytesIO.call([Pyrb::Bytes.call(["abc"])]), stringIO.call()])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('encode'), stringIO.call(["abc"]), stringIO.call()])
    end

    klass.attrs['test_decode'] = Pyrb.defn(self: nil) do |self_|
      bytesIO, stringIO = import('BytesIO', 'StringIO', from: 'io')
      infp = bytesIO.call([Pyrb::Bytes.call(["d3d3LnB5dGhvbi5vcmc="])])
      outfp = bytesIO.call()
      base64.fcall('decode', [infp, outfp])
      self_.fcall('assertEqual', [outfp.fcall('getvalue'), Pyrb::Bytes.call(["www.python.org"])])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('encode'), stringIO.call(["YWJj\n"]), bytesIO.call()])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('encode'), bytesIO.call([Pyrb::Bytes.call(["YWJj\n"])]), stringIO.call()])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('encode'), stringIO.call(["YWJj\n"]), stringIO.call()])
    end
  end

  LegacyBase64TestCase = exports['LegacyBase64TestCase']

  exports['BaseXYTestCase'] = Pyrb::PythonClass.new('BaseXYTestCase', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['check_encode_type_errors'] = Pyrb.defn(self: nil, f: nil) do |self_, f|
      self_.fcall('assertRaises', [Pyrb::TypeError, f, ""])
      self_.fcall('assertRaises', [Pyrb::TypeError, f, []])
    end

    klass.attrs['check_decode_type_errors'] = Pyrb.defn(self: nil, f: nil) do |self_, f|
      self_.fcall('assertRaises', [Pyrb::TypeError, f, []])
    end

    klass.attrs['check_other_types'] = Pyrb.defn(self: nil, f: nil, bytes_data: nil, expected: nil) do |self_, f, bytes_data, expected|
      eq = self_.attr('assertEqual')
      b = Pyrb::Bytearray.call([bytes_data])
      eq.call([f.call([b]), expected])
      eq.call([b, bytes_data])
      eq.call([f.call([Pyrb::Memoryview.call([bytes_data])]), expected])
      eq.call([f.call([array.call(["B", bytes_data])]), expected])
      self_.fcall('check_nonbyte_element_format', [base64.attr('b64encode'), bytes_data])
      self_.fcall('check_multidimensional', [base64.attr('b64encode'), bytes_data])
    end

    klass.attrs['check_multidimensional'] = Pyrb.defn(self: nil, f: nil, data: nil) do |self_, f, data|
      padding = !!((Pyrb.len.call([data]) % 2)) ? Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')]) : Pyrb::Bytes.call([""])
      bytes_data = data + padding
      shape = [Pyrb.int_divide(Pyrb.len.call([bytes_data]), 2), 2]
      multidimensional = Pyrb::Memoryview.call([bytes_data]).fcall('cast', ["B", shape])
      self_.fcall('assertEqual', [f.call([multidimensional]), f.call([bytes_data])])
    end

    klass.attrs['check_nonbyte_element_format'] = Pyrb.defn(self: nil, f: nil, data: nil) do |self_, f, data|
      padding = (Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')]) * (((4 - Pyrb.len.call([data])) % 4)))
      bytes_data = data + padding
      int_data = Pyrb::Memoryview.call([bytes_data]).fcall('cast', ["I"])
      self_.fcall('assertEqual', [f.call([int_data]), f.call([bytes_data])])
    end

    klass.attrs['test_b64encode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call(["www.python.org"])]), Pyrb::Bytes.call(["d3d3LnB5dGhvbi5vcmc="])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')])]), Pyrb::Bytes.call(["AA=="])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call(["a"])]), Pyrb::Bytes.call(["YQ=="])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call(["ab"])]), Pyrb::Bytes.call(["YWI="])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call(["abc"])]), Pyrb::Bytes.call(["YWJj"])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call([""])]), Pyrb::Bytes.call([""])])
      eq.call([base64.fcall('b64encode', [(Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz"]) + Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"]))]), (Pyrb::Bytes.call(["YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNE"]) + Pyrb::Bytes.call(["RUZHSElKS0xNTk9QUVJTVFVWV1hZWjAxMjM0NT"]) + Pyrb::Bytes.call(["Y3ODkhQCMwXiYqKCk7Ojw+LC4gW117fQ=="]))])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')]), ], { altchars: Pyrb::Bytes.call(["*$"]) }), Pyrb::Bytes.call(["01a*b$cd"])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')]), ], { altchars: Pyrb::Bytearray.call([Pyrb::Bytes.call(["*$"])]) }), Pyrb::Bytes.call(["01a*b$cd"])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')]), ], { altchars: Pyrb::Memoryview.call([Pyrb::Bytes.call(["*$"])]) }), Pyrb::Bytes.call(["01a*b$cd"])])
      eq.call([base64.fcall('b64encode', [Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')]), ], { altchars: array.call(["B", Pyrb::Bytes.call(["*$"])]) }), Pyrb::Bytes.call(["01a*b$cd"])])
      self_.fcall('check_other_types', [base64.attr('b64encode'), Pyrb::Bytes.call(["abcd"]), Pyrb::Bytes.call(["YWJjZA=="])])
      self_.fcall('check_encode_type_errors', [base64.attr('b64encode')])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('b64encode'), Pyrb::Bytes.call([""]), ], { altchars: "*$" })
      eq.call([base64.fcall('standard_b64encode', [Pyrb::Bytes.call(["www.python.org"])]), Pyrb::Bytes.call(["d3d3LnB5dGhvbi5vcmc="])])
      eq.call([base64.fcall('standard_b64encode', [Pyrb::Bytes.call(["a"])]), Pyrb::Bytes.call(["YQ=="])])
      eq.call([base64.fcall('standard_b64encode', [Pyrb::Bytes.call(["ab"])]), Pyrb::Bytes.call(["YWI="])])
      eq.call([base64.fcall('standard_b64encode', [Pyrb::Bytes.call(["abc"])]), Pyrb::Bytes.call(["YWJj"])])
      eq.call([base64.fcall('standard_b64encode', [Pyrb::Bytes.call([""])]), Pyrb::Bytes.call([""])])
      eq.call([base64.fcall('standard_b64encode', [(Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz"]) + Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"]))]), (Pyrb::Bytes.call(["YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNE"]) + Pyrb::Bytes.call(["RUZHSElKS0xNTk9QUVJTVFVWV1hZWjAxMjM0NT"]) + Pyrb::Bytes.call(["Y3ODkhQCMwXiYqKCk7Ojw+LC4gW117fQ=="]))])
      self_.fcall('check_other_types', [base64.attr('standard_b64encode'), Pyrb::Bytes.call(["abcd"]), Pyrb::Bytes.call(["YWJjZA=="])])
      self_.fcall('check_encode_type_errors', [base64.attr('standard_b64encode')])
      eq.call([base64.fcall('urlsafe_b64encode', [Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')])]), Pyrb::Bytes.call(["01a-b_cd"])])
      self_.fcall('check_other_types', [base64.attr('urlsafe_b64encode'), Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["01a-b_cd"])])
      self_.fcall('check_encode_type_errors', [base64.attr('urlsafe_b64encode')])
    end

    klass.attrs['test_b64decode'] = Pyrb.defn(self: nil) do |self_|
      data_str, data, altchars_str, altchars = nil

      eq = self_.attr('assertEqual')
      tests = Pyrb::Dict.new([{Pyrb::Bytes.call(["d3d3LnB5dGhvbi5vcmc="]) => Pyrb::Bytes.call(["www.python.org"]), Pyrb::Bytes.call(["AA=="]) => Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["YQ=="]) => Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["YWI="]) => Pyrb::Bytes.call(["ab"]), Pyrb::Bytes.call(["YWJj"]) => Pyrb::Bytes.call(["abc"]), (Pyrb::Bytes.call(["YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNE"]) + Pyrb::Bytes.call(["RUZHSElKS0xNTk9QUVJTVFVWV1hZWjAxMjM0\nNT"]) + Pyrb::Bytes.call(["Y3ODkhQCMwXiYqKCk7Ojw+LC4gW117fQ=="])) => (Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz"]) + Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"])), Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), }])
      (tests.fcall('items')).each do |data, res|
        eq.call([base64.fcall('b64decode', [data]), res])
        eq.call([base64.fcall('b64decode', [data.fcall('decode', ["ascii"])]), res])
      end
      self_.fcall('check_other_types', [base64.attr('b64decode'), Pyrb::Bytes.call(["YWJj"]), Pyrb::Bytes.call(["abc"])])
      self_.fcall('check_decode_type_errors', [base64.attr('b64decode')])
      tests_altchars = Pyrb::Dict.new([{[Pyrb::Bytes.call(["01a*b$cd"]), Pyrb::Bytes.call(["*$"])] => Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')]), }])
      (tests_altchars.fcall('items')).each do |(data,altchars), res|
        data_str = data.fcall('decode', ["ascii"])
        altchars_str = altchars.fcall('decode', ["ascii"])
        eq.call([base64.fcall('b64decode', [data, ], { altchars: altchars }), res])
        eq.call([base64.fcall('b64decode', [data_str, ], { altchars: altchars }), res])
        eq.call([base64.fcall('b64decode', [data, ], { altchars: altchars_str }), res])
        eq.call([base64.fcall('b64decode', [data_str, ], { altchars: altchars_str }), res])
      end
      (tests.fcall('items')).each do |data2, res|
        eq.call([base64.fcall('standard_b64decode', [data2]), res])
        eq.call([base64.fcall('standard_b64decode', [data2.fcall('decode', ["ascii"])]), res])
      end
      self_.fcall('check_other_types', [base64.attr('standard_b64decode'), Pyrb::Bytes.call(["YWJj"]), Pyrb::Bytes.call(["abc"])])
      self_.fcall('check_decode_type_errors', [base64.attr('standard_b64decode')])
      tests_urlsafe = Pyrb::Dict.new([{Pyrb::Bytes.call(["01a-b_cd"]) => Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), }])
      (tests_urlsafe.fcall('items')).each do |data2, res|
        eq.call([base64.fcall('urlsafe_b64decode', [data2]), res])
        eq.call([base64.fcall('urlsafe_b64decode', [data2.fcall('decode', ["ascii"])]), res])
      end
      self_.fcall('check_other_types', [base64.attr('urlsafe_b64decode'), Pyrb::Bytes.call(["01a-b_cd"]), Pyrb::Bytes.call([String.new([0xd3].pack('C*') + "V" + [0xbe].pack('C*') + "o" + [0xf7, 0x1d].pack('C*'), encoding: 'ASCII-8BIT')])])
      self_.fcall('check_decode_type_errors', [base64.attr('urlsafe_b64decode')])
    end

    klass.attrs['test_b64decode_padding_error'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b64decode'), Pyrb::Bytes.call(["abc"])])
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b64decode'), "abc"])
    end

    klass.attrs['test_b64decode_invalid_chars'] = Pyrb.defn(self: nil) do |self_|
      validate = nil

      tests = [[Pyrb::Bytes.call(["%3d=="]), Pyrb::Bytes.call([String.new([0xdd].pack('C*'), encoding: 'ASCII-8BIT')])], [Pyrb::Bytes.call(["$3d=="]), Pyrb::Bytes.call([String.new([0xdd].pack('C*'), encoding: 'ASCII-8BIT')])], [Pyrb::Bytes.call(["[=="]), Pyrb::Bytes.call([""])], [Pyrb::Bytes.call(["YW]3="]), Pyrb::Bytes.call(["am"])], [Pyrb::Bytes.call(["3{d=="]), Pyrb::Bytes.call([String.new([0xdd].pack('C*'), encoding: 'ASCII-8BIT')])], [Pyrb::Bytes.call(["3d}=="]), Pyrb::Bytes.call([String.new([0xdd].pack('C*'), encoding: 'ASCII-8BIT')])], [Pyrb::Bytes.call(["@@"]), Pyrb::Bytes.call([""])], [Pyrb::Bytes.call(["!"]), Pyrb::Bytes.call([""])], [Pyrb::Bytes.call(["YWJj\nYWI="]), Pyrb::Bytes.call(["abcab"])]]
      funcs = [base64.attr('b64decode'), base64.attr('standard_b64decode'), base64.attr('urlsafe_b64decode'), ]
      (tests).each do |bstr, res|
        (funcs).each do |func|
          Pyrb.with.call([self_.fcall('subTest', [], { bstr: bstr, func: func })]) do
            self_.fcall('assertEqual', [func.call([bstr]), res])
            self_.fcall('assertEqual', [func.call([bstr.fcall('decode', ["ascii"])]), res])
          end
        end
        Pyrb.with.call([self_.fcall('assertRaises', [binascii.attr('Error')])]) do
          base64.fcall('b64decode', [bstr, ], { validate: true })
        end
        Pyrb.with.call([self_.fcall('assertRaises', [binascii.attr('Error')])]) do
          base64.fcall('b64decode', [bstr.fcall('decode', ["ascii"]), ], { validate: true })
        end
      end
      res = Pyrb::Bytes.call([String.new([0xFB, 0xEF, 0xBE, 0xFF, 0xFF, 0xFF].pack('C*'), encoding: 'ASCII-8BIT')])
      self_.fcall('assertEqual', [base64.fcall('b64decode', [Pyrb::Bytes.call(["++[[//]]"]), Pyrb::Bytes.call(["[]"])]), res])
      self_.fcall('assertEqual', [base64.fcall('urlsafe_b64decode', [Pyrb::Bytes.call(["++--//__"])]), res])
    end

    klass.attrs['test_b32encode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('b32encode', [Pyrb::Bytes.call([""])]), Pyrb::Bytes.call([""])])
      eq.call([base64.fcall('b32encode', [Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')])]), Pyrb::Bytes.call(["AA======"])])
      eq.call([base64.fcall('b32encode', [Pyrb::Bytes.call(["a"])]), Pyrb::Bytes.call(["ME======"])])
      eq.call([base64.fcall('b32encode', [Pyrb::Bytes.call(["ab"])]), Pyrb::Bytes.call(["MFRA===="])])
      eq.call([base64.fcall('b32encode', [Pyrb::Bytes.call(["abc"])]), Pyrb::Bytes.call(["MFRGG==="])])
      eq.call([base64.fcall('b32encode', [Pyrb::Bytes.call(["abcd"])]), Pyrb::Bytes.call(["MFRGGZA="])])
      eq.call([base64.fcall('b32encode', [Pyrb::Bytes.call(["abcde"])]), Pyrb::Bytes.call(["MFRGGZDF"])])
      self_.fcall('check_other_types', [base64.attr('b32encode'), Pyrb::Bytes.call(["abcd"]), Pyrb::Bytes.call(["MFRGGZA="])])
      self_.fcall('check_encode_type_errors', [base64.attr('b32encode')])
    end

    klass.attrs['test_b32decode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      tests = Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["AA======"]) => Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["ME======"]) => Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["MFRA===="]) => Pyrb::Bytes.call(["ab"]), Pyrb::Bytes.call(["MFRGG==="]) => Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call(["MFRGGZA="]) => Pyrb::Bytes.call(["abcd"]), Pyrb::Bytes.call(["MFRGGZDF"]) => Pyrb::Bytes.call(["abcde"]), }])
      (tests.fcall('items')).each do |data, res|
        eq.call([base64.fcall('b32decode', [data]), res])
        eq.call([base64.fcall('b32decode', [data.fcall('decode', ["ascii"])]), res])
      end
      self_.fcall('check_other_types', [base64.attr('b32decode'), Pyrb::Bytes.call(["MFRGG==="]), Pyrb::Bytes.call(["abc"])])
      self_.fcall('check_decode_type_errors', [base64.attr('b32decode')])
    end

    klass.attrs['test_b32decode_casefold'] = Pyrb.defn(self: nil) do |self_|
      data_str, data, map01_str, map01 = nil

      eq = self_.attr('assertEqual')
      tests = Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["ME======"]) => Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["MFRA===="]) => Pyrb::Bytes.call(["ab"]), Pyrb::Bytes.call(["MFRGG==="]) => Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call(["MFRGGZA="]) => Pyrb::Bytes.call(["abcd"]), Pyrb::Bytes.call(["MFRGGZDF"]) => Pyrb::Bytes.call(["abcde"]), Pyrb::Bytes.call(["me======"]) => Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["mfra===="]) => Pyrb::Bytes.call(["ab"]), Pyrb::Bytes.call(["mfrgg==="]) => Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call(["mfrggza="]) => Pyrb::Bytes.call(["abcd"]), Pyrb::Bytes.call(["mfrggzdf"]) => Pyrb::Bytes.call(["abcde"]), }])
      (tests.fcall('items')).each do |data, res|
        eq.call([base64.fcall('b32decode', [data, true]), res])
        eq.call([base64.fcall('b32decode', [data.fcall('decode', ["ascii"]), true]), res])
      end
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b32decode'), Pyrb::Bytes.call(["me======"])])
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b32decode'), "me======"])
      eq.call([base64.fcall('b32decode', [Pyrb::Bytes.call(["MLO23456"])]), Pyrb::Bytes.call([String.new("b" + [0xdd, 0xad, 0xf3, 0xbe].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b32decode', ["MLO23456"]), Pyrb::Bytes.call([String.new("b" + [0xdd, 0xad, 0xf3, 0xbe].pack('C*'), encoding: 'ASCII-8BIT')])])
      map_tests = Pyrb::Dict.new([{[Pyrb::Bytes.call(["M1023456"]), Pyrb::Bytes.call(["L"])] => Pyrb::Bytes.call([String.new("b" + [0xdd, 0xad, 0xf3, 0xbe].pack('C*'), encoding: 'ASCII-8BIT')]), [Pyrb::Bytes.call(["M1023456"]), Pyrb::Bytes.call(["I"])] => Pyrb::Bytes.call([String.new("b" + [0x1d, 0xad, 0xf3, 0xbe].pack('C*'), encoding: 'ASCII-8BIT')]), }])
      (map_tests.fcall('items')).each do |(data,map01), res|
        data_str = data.fcall('decode', ["ascii"])
        map01_str = map01.fcall('decode', ["ascii"])
        eq.call([base64.fcall('b32decode', [data, ], { map01: map01 }), res])
        eq.call([base64.fcall('b32decode', [data_str, ], { map01: map01 }), res])
        eq.call([base64.fcall('b32decode', [data, ], { map01: map01_str }), res])
        eq.call([base64.fcall('b32decode', [data_str, ], { map01: map01_str }), res])
        self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b32decode'), data])
        self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b32decode'), data_str])
      end
    end

    klass.attrs['test_b32decode_error'] = Pyrb.defn(self: nil) do |self_|
      tests = [Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call(["ABCDEF=="]), Pyrb::Bytes.call(["==ABCDEF"])]
      prefixes = [Pyrb::Bytes.call(["M"]), Pyrb::Bytes.call(["ME"]), Pyrb::Bytes.call(["MFRA"]), Pyrb::Bytes.call(["MFRGG"]), Pyrb::Bytes.call(["MFRGGZA"]), Pyrb::Bytes.call(["MFRGGZDF"])]
      (Pyrb.range.call([0, 17])).each do |i|
        if !!(i)
          tests.fcall('append', [(Pyrb::Bytes.call(["="]) * i)])
        end

        (prefixes).each do |prefix|
          if !!(Pyrb.len.call([prefix]) + i != 8)
            tests.fcall('append', [prefix + (Pyrb::Bytes.call(["="]) * i)])
          end
        end
      end
      (tests).each do |data|
        Pyrb.with.call([self_.fcall('subTest', [], { data: data })]) do
          Pyrb.with.call([self_.fcall('assertRaises', [binascii.attr('Error')])]) do
            base64.fcall('b32decode', [data])
          end
          Pyrb.with.call([self_.fcall('assertRaises', [binascii.attr('Error')])]) do
            base64.fcall('b32decode', [data.fcall('decode', ["ascii"])])
          end
        end
      end
    end

    klass.attrs['test_b16encode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('b16encode', [Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])]), Pyrb::Bytes.call(["0102ABCDEF"])])
      eq.call([base64.fcall('b16encode', [Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')])]), Pyrb::Bytes.call(["00"])])
      self_.fcall('check_other_types', [base64.attr('b16encode'), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["0102ABCDEF"])])
      self_.fcall('check_encode_type_errors', [base64.attr('b16encode')])
    end

    klass.attrs['test_b16decode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('b16decode', [Pyrb::Bytes.call(["0102ABCDEF"])]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b16decode', ["0102ABCDEF"]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b16decode', [Pyrb::Bytes.call(["00"])]), Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b16decode', ["00"]), Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b16decode'), Pyrb::Bytes.call(["0102abcdef"])])
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b16decode'), "0102abcdef"])
      eq.call([base64.fcall('b16decode', [Pyrb::Bytes.call(["0102abcdef"]), true]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b16decode', ["0102abcdef", true]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      self_.fcall('check_other_types', [base64.attr('b16decode'), Pyrb::Bytes.call(["0102ABCDEF"]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      self_.fcall('check_decode_type_errors', [base64.attr('b16decode')])
      eq.call([base64.fcall('b16decode', [Pyrb::Bytearray.call([Pyrb::Bytes.call(["0102abcdef"])]), true]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b16decode', [Pyrb::Memoryview.call([Pyrb::Bytes.call(["0102abcdef"])]), true]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b16decode', [array.call(["B", Pyrb::Bytes.call(["0102abcdef"])]), true]), Pyrb::Bytes.call([String.new([0x01, 0x02, 0xab, 0xcd, 0xef].pack('C*'), encoding: 'ASCII-8BIT')])])
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b16decode'), "0102AG"])
      self_.fcall('assertRaises', [binascii.attr('Error'), base64.attr('b16decode'), "010"])
    end

    klass.attrs['test_a85encode'] = Pyrb.defn(self: nil) do |self_|
      adobe = nil

      eq = self_.attr('assertEqual')
      tests = Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["www.python.org"]) => Pyrb::Bytes.call(["GB\\6`E-ZP=Df.1GEb>"]), Pyrb::Bytes.call([Pyrb.range.call([255])]) => (Pyrb::Bytes.call(["!!*-'\"9eu7\#RLhG$k3[W&.oNg'GVB\"(`=52*$$"]) + Pyrb::Bytes.call(["(B+<_pR,UFcb-n-Vr/1iJ-0JP==1c70M3&s\#]4?Ykm5X@_(6q'R884cE"]) + Pyrb::Bytes.call(["H9MJ8X:f1+h<)lt\#=BSg3>[:ZC?t!MSA7]@cBPD3sCi+'.E,fo>FEMbN"]) + Pyrb::Bytes.call(["G^4U^I!pHnJ:W<)KS>/9Ll%\"IN/`jYOHG]iPa.Q$R$jD4S=Q7DTV8*TU"]) + Pyrb::Bytes.call(["nsrdW2ZetXKAY/Yd(L?['d?O\\@K2_]Y2%o^qmn*`5Ta:aN;TJbg\"GZd"]) + Pyrb::Bytes.call(["*^:jeCE.%f\\,!5gtgiEi8N\\UjQ5OekiqBum-X60nF?)@o_%qPq\"ad`"]) + Pyrb::Bytes.call(["r;HT"])), (Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"])) => (Pyrb::Bytes.call(["@:E_WAS,RgBkhF\"D/O92EH6,BF`qtRH$VbC6UX@47n?3D92&&T"]) + Pyrb::Bytes.call([":Jand;cHat='/U/0JP==1c70M3&r-I,;<FN.OZ`-3]oSW/g+A(H[P"])), Pyrb::Bytes.call(["no padding.."]) => Pyrb::Bytes.call(["DJpY:@:Wn_DJ(RS"]), Pyrb::Bytes.call(["zero compression\0\0\0\0"]) => Pyrb::Bytes.call(["H=_,8+Cf>,E,oN2F(oQ1z"]), Pyrb::Bytes.call(["zero compression\0\0\0"]) => Pyrb::Bytes.call(["H=_,8+Cf>,E,oN2F(oQ1!!!!"]), Pyrb::Bytes.call(["Boundary:\0\0\0\0"]) => Pyrb::Bytes.call(["6>q!aA79M(3WK-[!!"]), Pyrb::Bytes.call(["Space compr:    "]) => Pyrb::Bytes.call([";fH/TAKYK$D/aMV+<VdL"]), Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) => Pyrb::Bytes.call(["rr"]), (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 2) => Pyrb::Bytes.call(["s8N"]), (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 3) => Pyrb::Bytes.call(["s8W*"]), (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 4) => Pyrb::Bytes.call(["s8W-!"]), }])
      (tests.fcall('items')).each do |data, res|
        eq.call([base64.fcall('a85encode', [data]), res, data])
        eq.call([base64.fcall('a85encode', [data, ], { adobe: false }), res, data])
        eq.call([base64.fcall('a85encode', [data, ], { adobe: true }), Pyrb::Bytes.call(["<~"]) + res + Pyrb::Bytes.call(["~>"]), data])
      end
      self_.fcall('check_other_types', [base64.attr('a85encode'), Pyrb::Bytes.call(["www.python.org"]), Pyrb::Bytes.call(["GB\\6`E-ZP=Df.1GEb>"])])
      self_.fcall('assertRaises', [Pyrb::TypeError, base64.attr('a85encode'), ""])
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["www.python.org"]), ], { wrapcol: 7, adobe: false }), Pyrb::Bytes.call(["GB\\6`E-\nZP=Df.1\nGEb>"])])
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["\0\0\0\0www.python.org"]), ], { wrapcol: 7, adobe: false }), Pyrb::Bytes.call(["zGB\\6`E\n-ZP=Df.\n1GEb>"])])
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["www.python.org"]), ], { wrapcol: 7, adobe: true }), Pyrb::Bytes.call(["<~GB\\6`\nE-ZP=Df\n.1GEb>\n~>"])])
      eq.call([base64.fcall('a85encode', [(Pyrb::Bytes.call([" "]) * 8), ], { foldspaces: true, adobe: false }), Pyrb::Bytes.call(["yy"])])
      eq.call([base64.fcall('a85encode', [(Pyrb::Bytes.call([" "]) * 7), ], { foldspaces: true, adobe: false }), Pyrb::Bytes.call(["y+<Vd"])])
      eq.call([base64.fcall('a85encode', [(Pyrb::Bytes.call([" "]) * 6), ], { foldspaces: true, adobe: false }), Pyrb::Bytes.call(["y+<U"])])
      eq.call([base64.fcall('a85encode', [(Pyrb::Bytes.call([" "]) * 5), ], { foldspaces: true, adobe: false }), Pyrb::Bytes.call(["y+9"])])
    end

    klass.attrs['test_b85encode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      tests = Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["www.python.org"]) => Pyrb::Bytes.call(["cXxL\#aCvlSZ*DGca%T"]), Pyrb::Bytes.call([Pyrb.range.call([255])]) => (Pyrb::Bytes.call(["009C61O)~M2nh-c3=Iws5D^j+6crX17\#SKH9337X"]) + Pyrb::Bytes.call(["AR!_nBqb&%C@Cr{EG;fCFflSSG&MFiI5|2yJUu=?KtV!7L`6nNNJ&ad"]) + Pyrb::Bytes.call(["OifNtP*GA-R8>}2SXo+ITwPvYU}0ioWMyV&XlZI|Y;A6DaB*^Tbai%j"]) + Pyrb::Bytes.call(["czJqze0_d@fPsR8goTEOh>41ejE\#<ukdcy;l$Dm3n3<ZJoSmMZprN9p"]) + Pyrb::Bytes.call(["q@|{(sHv)}tgWuEu(7hUw6(UkxVgH!yuH4^z`?@9\#Kp$P$jQpf%+1cv"]) + Pyrb::Bytes.call(["(9zP<)YaD4*xB0K+}+;a;Njxq<mKk)=;`X~?CtLF@bU8V^!4`l`1$(#"]) + Pyrb::Bytes.call(["{Qdp"])), (Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"])) => (Pyrb::Bytes.call(["VPa!sWoBn+X=-b1ZEkOHadLBXb\#`}nd3r%YLqtVJM@UIZOH55pPf$@("]) + Pyrb::Bytes.call(["Q&d$}S6EqEFflSSG&MFiI5{CeBQRbjDkv\#CIy^osE+AW7dwl"])), Pyrb::Bytes.call(["no padding.."]) => Pyrb::Bytes.call(["Zf_uPVPs@!Zf7no"]), Pyrb::Bytes.call([String.new("zero compression" + [0x00, 0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]) => Pyrb::Bytes.call(["dS!BNAY*TBaB^jHb7^mG00000"]), Pyrb::Bytes.call([String.new("zero compression" + [0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]) => Pyrb::Bytes.call(["dS!BNAY*TBaB^jHb7^mG0000"]), Pyrb::Bytes.call([String.new("Boundary:" + [0x00, 0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]) => Pyrb::Bytes.call(["LT`0$WMOi7IsgCw00"]), Pyrb::Bytes.call(["Space compr:    "]) => Pyrb::Bytes.call(["Q*dEpWgug3ZE$irARr(h"]), Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) => Pyrb::Bytes.call(["{{"]), (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 2) => Pyrb::Bytes.call(["|Nj"]), (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 3) => Pyrb::Bytes.call(["|Ns9"]), (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 4) => Pyrb::Bytes.call(["|NsC0"]), }])
      (tests.fcall('items')).each do |data, res|
        eq.call([base64.fcall('b85encode', [data]), res])
      end
      self_.fcall('check_other_types', [base64.attr('b85encode'), Pyrb::Bytes.call(["www.python.org"]), Pyrb::Bytes.call(["cXxL\#aCvlSZ*DGca%T"])])
    end

    klass.attrs['test_a85decode'] = Pyrb.defn(self: nil) do |self_|
      adobe = nil

      eq = self_.attr('assertEqual')
      tests = Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["GB\\6`E-ZP=Df.1GEb>"]) => Pyrb::Bytes.call(["www.python.org"]), (Pyrb::Bytes.call(["! ! * -'\"\n\t\t9eu\r\n7\#  RL\vhG$k3[W&.oNg'GVB\"(`=52*$$"]) + Pyrb::Bytes.call(["(B+<_pR,UFcb-n-Vr/1iJ-0JP==1c70M3&s\#]4?Ykm5X@_(6q'R884cE"]) + Pyrb::Bytes.call(["H9MJ8X:f1+h<)lt\#=BSg3>[:ZC?t!MSA7]@cBPD3sCi+'.E,fo>FEMbN"]) + Pyrb::Bytes.call(["G^4U^I!pHnJ:W<)KS>/9Ll%\"IN/`jYOHG]iPa.Q$R$jD4S=Q7DTV8*TU"]) + Pyrb::Bytes.call(["nsrdW2ZetXKAY/Yd(L?['d?O\\@K2_]Y2%o^qmn*`5Ta:aN;TJbg\"GZd"]) + Pyrb::Bytes.call(["*^:jeCE.%f\\,!5gtgiEi8N\\UjQ5OekiqBum-X60nF?)@o_%qPq\"ad`"]) + Pyrb::Bytes.call(["r;HT"])) => Pyrb::Bytes.call([Pyrb.range.call([255])]), (Pyrb::Bytes.call(["@:E_WAS,RgBkhF\"D/O92EH6,BF`qtRH$VbC6UX@47n?3D92&&T:Jand;c"]) + Pyrb::Bytes.call(["Hat='/U/0JP==1c70M3&r-I,;<FN.OZ`-3]oSW/g+A(H[P"])) => (Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234"]) + Pyrb::Bytes.call(["56789!@\#0^&*();:<>,. []{}"])), Pyrb::Bytes.call(["DJpY:@:Wn_DJ(RS"]) => Pyrb::Bytes.call(["no padding.."]), Pyrb::Bytes.call(["H=_,8+Cf>,E,oN2F(oQ1z"]) => Pyrb::Bytes.call([String.new("zero compression" + [0x00, 0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["H=_,8+Cf>,E,oN2F(oQ1!!!!"]) => Pyrb::Bytes.call([String.new("zero compression" + [0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["6>q!aA79M(3WK-[!!"]) => Pyrb::Bytes.call([String.new("Boundary:" + [0x00, 0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([";fH/TAKYK$D/aMV+<VdL"]) => Pyrb::Bytes.call(["Space compr:    "]), Pyrb::Bytes.call(["rr"]) => Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["s8N"]) => (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 2), Pyrb::Bytes.call(["s8W*"]) => (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 3), Pyrb::Bytes.call(["s8W-!"]) => (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 4), }])
      (tests.fcall('items')).each do |data, res|
        eq.call([base64.fcall('a85decode', [data]), res, data])
        eq.call([base64.fcall('a85decode', [data, ], { adobe: false }), res, data])
        eq.call([base64.fcall('a85decode', [data.fcall('decode', ["ascii"]), ], { adobe: false }), res, data])
        eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["<~"]) + data + Pyrb::Bytes.call(["~>"]), ], { adobe: true }), res, data])
        eq.call([base64.fcall('a85decode', [data + Pyrb::Bytes.call(["~>"]), ], { adobe: true }), res, data])
        eq.call([base64.fcall('a85decode', [("<~%s~>" % data.fcall('decode', ["ascii"])), ], { adobe: true }), res, data])
      end
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["yy"]), ], { foldspaces: true, adobe: false }), (Pyrb::Bytes.call([" "]) * 8)])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["y+<Vd"]), ], { foldspaces: true, adobe: false }), (Pyrb::Bytes.call([" "]) * 7)])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["y+<U"]), ], { foldspaces: true, adobe: false }), (Pyrb::Bytes.call([" "]) * 6)])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["y+9"]), ], { foldspaces: true, adobe: false }), (Pyrb::Bytes.call([" "]) * 5)])
      self_.fcall('check_other_types', [base64.attr('a85decode'), Pyrb::Bytes.call(["GB\\6`E-ZP=Df.1GEb>"]), Pyrb::Bytes.call(["www.python.org"])])
    end

    klass.attrs['test_b85decode'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      tests = Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["cXxL\#aCvlSZ*DGca%T"]) => Pyrb::Bytes.call(["www.python.org"]), (Pyrb::Bytes.call(["009C61O)~M2nh-c3=Iws5D^j+6crX17\#SKH9337X"]) + Pyrb::Bytes.call(["AR!_nBqb&%C@Cr{EG;fCFflSSG&MFiI5|2yJUu=?KtV!7L`6nNNJ&ad"]) + Pyrb::Bytes.call(["OifNtP*GA-R8>}2SXo+ITwPvYU}0ioWMyV&XlZI|Y;A6DaB*^Tbai%j"]) + Pyrb::Bytes.call(["czJqze0_d@fPsR8goTEOh>41ejE\#<ukdcy;l$Dm3n3<ZJoSmMZprN9p"]) + Pyrb::Bytes.call(["q@|{(sHv)}tgWuEu(7hUw6(UkxVgH!yuH4^z`?@9\#Kp$P$jQpf%+1cv"]) + Pyrb::Bytes.call(["(9zP<)YaD4*xB0K+}+;a;Njxq<mKk)=;`X~?CtLF@bU8V^!4`l`1$(#"]) + Pyrb::Bytes.call(["{Qdp"])) => Pyrb::Bytes.call([Pyrb.range.call([255])]), (Pyrb::Bytes.call(["VPa!sWoBn+X=-b1ZEkOHadLBXb\#`}nd3r%YLqtVJM@UIZOH55pPf$@("]) + Pyrb::Bytes.call(["Q&d$}S6EqEFflSSG&MFiI5{CeBQRbjDkv\#CIy^osE+AW7dwl"])) => (Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["0123456789!@\#0^&*();:<>,. []{}"])), Pyrb::Bytes.call(["Zf_uPVPs@!Zf7no"]) => Pyrb::Bytes.call(["no padding.."]), Pyrb::Bytes.call(["dS!BNAY*TBaB^jHb7^mG00000"]) => Pyrb::Bytes.call([String.new("zero compression" + [0x00, 0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["dS!BNAY*TBaB^jHb7^mG0000"]) => Pyrb::Bytes.call([String.new("zero compression" + [0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["LT`0$WMOi7IsgCw00"]) => Pyrb::Bytes.call([String.new("Boundary:" + [0x00, 0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["Q*dEpWgug3ZE$irARr(h"]) => Pyrb::Bytes.call(["Space compr:    "]), Pyrb::Bytes.call(["{{"]) => Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["|Nj"]) => (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 2), Pyrb::Bytes.call(["|Ns9"]) => (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 3), Pyrb::Bytes.call(["|NsC0"]) => (Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) * 4), }])
      (tests.fcall('items')).each do |data, res|
        eq.call([base64.fcall('b85decode', [data]), res])
        eq.call([base64.fcall('b85decode', [data.fcall('decode', ["ascii"])]), res])
      end
      self_.fcall('check_other_types', [base64.attr('b85decode'), Pyrb::Bytes.call(["cXxL\#aCvlSZ*DGca%T"]), Pyrb::Bytes.call(["www.python.org"])])
    end

    klass.attrs['test_a85_padding'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["x"]), ], { pad: true }), Pyrb::Bytes.call(["GQ7^D"])])
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["xx"]), ], { pad: true }), Pyrb::Bytes.call(["G^'2g"])])
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["xxx"]), ], { pad: true }), Pyrb::Bytes.call(["G^+H5"])])
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["xxxx"]), ], { pad: true }), Pyrb::Bytes.call(["G^+IX"])])
      eq.call([base64.fcall('a85encode', [Pyrb::Bytes.call(["xxxxx"]), ], { pad: true }), Pyrb::Bytes.call(["G^+IXGQ7^D"])])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["GQ7^D"])]), Pyrb::Bytes.call([String.new("x" + [0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["G^'2g"])]), Pyrb::Bytes.call([String.new("xx" + [0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["G^+H5"])]), Pyrb::Bytes.call([String.new("xxx" + [0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["G^+IX"])]), Pyrb::Bytes.call(["xxxx"])])
      eq.call([base64.fcall('a85decode', [Pyrb::Bytes.call(["G^+IXGQ7^D"])]), Pyrb::Bytes.call([String.new("xxxxx" + [0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
    end

    klass.attrs['test_b85_padding'] = Pyrb.defn(self: nil) do |self_|
      eq = self_.attr('assertEqual')
      eq.call([base64.fcall('b85encode', [Pyrb::Bytes.call(["x"]), ], { pad: true }), Pyrb::Bytes.call(["cmMzZ"])])
      eq.call([base64.fcall('b85encode', [Pyrb::Bytes.call(["xx"]), ], { pad: true }), Pyrb::Bytes.call(["cz6H+"])])
      eq.call([base64.fcall('b85encode', [Pyrb::Bytes.call(["xxx"]), ], { pad: true }), Pyrb::Bytes.call(["czAdK"])])
      eq.call([base64.fcall('b85encode', [Pyrb::Bytes.call(["xxxx"]), ], { pad: true }), Pyrb::Bytes.call(["czAet"])])
      eq.call([base64.fcall('b85encode', [Pyrb::Bytes.call(["xxxxx"]), ], { pad: true }), Pyrb::Bytes.call(["czAetcmMzZ"])])
      eq.call([base64.fcall('b85decode', [Pyrb::Bytes.call(["cmMzZ"])]), Pyrb::Bytes.call([String.new("x" + [0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b85decode', [Pyrb::Bytes.call(["cz6H+"])]), Pyrb::Bytes.call([String.new("xx" + [0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b85decode', [Pyrb::Bytes.call(["czAdK"])]), Pyrb::Bytes.call([String.new("xxx" + [0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
      eq.call([base64.fcall('b85decode', [Pyrb::Bytes.call(["czAet"])]), Pyrb::Bytes.call(["xxxx"])])
      eq.call([base64.fcall('b85decode', [Pyrb::Bytes.call(["czAetcmMzZ"])]), Pyrb::Bytes.call([String.new("xxxxx" + [0x00, 0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')])])
    end

    klass.attrs['test_a85decode_errors'] = Pyrb.defn(self: nil) do |self_|
      adobe, msg = nil

      illegal = (Pyrb::Set.call([Pyrb.range.call([32])]) | Pyrb::Set.call([Pyrb.range.call([118, 256])])) - Pyrb::Set.call([Pyrb::Bytes.call([" \t\n\r\v"])])
      (illegal).each do |c|
        Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError, ], { msg: Pyrb::Bytes.call([[c]]) })]) do
          base64.fcall('a85decode', [Pyrb::Bytes.call(["!!!!"]) + Pyrb::Bytes.call([[c]])])
        end
        Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError, ], { msg: Pyrb::Bytes.call([[c]]) })]) do
          base64.fcall('a85decode', [Pyrb::Bytes.call(["!!!!"]) + Pyrb::Bytes.call([[c]]), ], { adobe: false })
        end
        Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError, ], { msg: Pyrb::Bytes.call([[c]]) })]) do
          base64.fcall('a85decode', [Pyrb::Bytes.call(["<~!!!!"]) + Pyrb::Bytes.call([[c]]) + Pyrb::Bytes.call(["~>"]), ], { adobe: true })
        end
      end
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["malformed"]), ], { adobe: true })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["<~still malformed"]), ], { adobe: true })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["<~~>"])])
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["<~~>"]), ], { adobe: false })
      base64.fcall('a85decode', [Pyrb::Bytes.call(["<~~>"]), ], { adobe: true })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["abcx"]), ], { adobe: false })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["abcdey"]), ], { adobe: false })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["a b\nc"]), ], { adobe: false, ignorechars: Pyrb::Bytes.call([""]) })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["s"]), ], { adobe: false })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["s8"]), ], { adobe: false })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["s8W"]), ], { adobe: false })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["s8W-"]), ], { adobe: false })
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('a85decode'), Pyrb::Bytes.call(["s8W-\""]), ], { adobe: false })
    end

    klass.attrs['test_b85decode_errors'] = Pyrb.defn(self: nil) do |self_|
      msg = nil

      illegal = Pyrb::List.call([Pyrb.range.call([33])]) + Pyrb::List.call([Pyrb::Bytes.call(["\"\',./:[\\]"])]) + Pyrb::List.call([Pyrb.range.call([128, 256])])
      (illegal).each do |c|
        Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError, ], { msg: Pyrb::Bytes.call([[c]]) })]) do
          base64.fcall('b85decode', [Pyrb::Bytes.call(["0000"]) + Pyrb::Bytes.call([[c]])])
        end
      end
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('b85decode'), Pyrb::Bytes.call(["|"])])
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('b85decode'), Pyrb::Bytes.call(["|N"])])
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('b85decode'), Pyrb::Bytes.call(["|Ns"])])
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('b85decode'), Pyrb::Bytes.call(["|NsC"])])
      self_.fcall('assertRaises', [Pyrb::ValueError, base64.attr('b85decode'), Pyrb::Bytes.call(["|NsC1"])])
    end

    klass.attrs['test_decode_nonascii_str'] = Pyrb.defn(self: nil) do |self_|
      decode_funcs = [base64.attr('b64decode'), base64.attr('standard_b64decode'), base64.attr('urlsafe_b64decode'), base64.attr('b32decode'), base64.attr('b16decode'), base64.attr('b85decode'), base64.attr('a85decode')]
      (decode_funcs).each do |f|
        self_.fcall('assertRaises', [Pyrb::ValueError, f, ("with non-ascii " + [0xcb].pack('U*'))])
      end
    end

    klass.attrs['test_ErrorHeritage'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [Pyrb.issubclass.call([binascii.attr('Error'), Pyrb::ValueError])])
    end
  end

  BaseXYTestCase = exports['BaseXYTestCase']

  exports['TestMain'] = Pyrb::PythonClass.new('TestMain', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['tearDown'] = Pyrb.defn(self: nil) do |self_|
      if !!(os.attr('path').fcall('exists', [support.attr('TESTFN')]))
        os.fcall('unlink', [support.attr('TESTFN')])
      end
    end

    klass.attrs['get_output'] = Pyrb.defn(self: nil, args: { splat: true }) do |self_, args|
      throw(:return, script_helper.fcall('assert_python_ok', ["-m", "base64", *Pyrb.splat(args)]).attr('out'))
    end

    klass.attrs['test_encode_decode'] = Pyrb.defn(self: nil) do |self_|
      output = self_.fcall('get_output', ["-t"])
      self_.fcall('assertSequenceEqual', [output.fcall('splitlines'), [Pyrb::Bytes.call(["b'Aladdin:open sesame'"]), Pyrb::Bytes.call([%q(b'QWxhZGRpbjpvcGVuIHNlc2FtZQ==\n')]), Pyrb::Bytes.call(["b'Aladdin:open sesame'"]), ]])
    end

    klass.attrs['test_encode_file'] = Pyrb.defn(self: nil) do |self_|
      fp = Pyrb.with.call([Pyrb.open.call([support.attr('TESTFN'), "wb"])]) do |fp|
        fp.fcall('write', [Pyrb::Bytes.call([String.new("a" + [0xff].pack('C*') + "b\n", encoding: 'ASCII-8BIT')])])
      end
      output = self_.fcall('get_output', ["-e", support.attr('TESTFN')])
      self_.fcall('assertEqual', [output.fcall('rstrip'), Pyrb::Bytes.call(["Yf9iCg=="])])
    end

    klass.attrs['test_encode_from_stdin'] = Pyrb.defn(self: nil) do |self_|
      out, err = nil

      proc_ = Pyrb.with.call([script_helper.fcall('spawn_python', ["-m", "base64", "-e"])]) do |proc_|
        out, err = Pyrb.deconstruct(2, proc_.fcall('communicate', [Pyrb::Bytes.call([String.new("a" + [0xff].pack('C*') + "b\n", encoding: 'ASCII-8BIT')])]))
      end
      self_.fcall('assertEqual', [out.fcall('rstrip'), Pyrb::Bytes.call(["Yf9iCg=="])])
      self_.fcall('assertIsNone', [err])
    end

    klass.attrs['test_decode'] = Pyrb.defn(self: nil) do |self_|
      fp = Pyrb.with.call([Pyrb.open.call([support.attr('TESTFN'), "wb"])]) do |fp|
        fp.fcall('write', [Pyrb::Bytes.call(["Yf9iCg=="])])
      end
      output = self_.fcall('get_output', ["-d", support.attr('TESTFN')])
      self_.fcall('assertEqual', [output.fcall('rstrip'), Pyrb::Bytes.call([String.new("a" + [0xff].pack('C*') + "b", encoding: 'ASCII-8BIT')])])
    end
  end

  TestMain = exports['TestMain']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
