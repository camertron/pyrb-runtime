# encoding: utf-8

require 'pyrb'

module Spec_test_urlparse
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  urllib, _ = import('urllib')
  DeprecationWarning, _ = import('DeprecationWarning', from: 'warnings')

  exports['RFC1808_BASE'] = "http://a/b/c/d;p?q\#f"
  exports['RFC2396_BASE'] = "http://a/b/c/d;p?q"
  exports['RFC3986_BASE'] = "http://a/b/c/d;p?q"
  exports['SIMPLE_BASE'] = "http://a/b/c/d"
  exports['parse_qsl_test_cases'] = [["", []], ["&", []], ["&&", []], ["=", [["", ""]]], ["=a", [["", "a"]]], ["a", [["a", ""]]], ["a=", [["a", ""]]], ["&a=b", [["a", "b"]]], ["a=a+b&b=b+c", [["a", "a b"], ["b", "b c"]]], ["a=1&a=2", [["a", "1"], ["a", "2"]]], [Pyrb::Bytes.call([""]), []], [Pyrb::Bytes.call(["&"]), []], [Pyrb::Bytes.call(["&&"]), []], [Pyrb::Bytes.call(["="]), [[Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]]], [Pyrb::Bytes.call(["=a"]), [[Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["a"])]]], [Pyrb::Bytes.call(["a"]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call([""])]]], [Pyrb::Bytes.call(["a="]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call([""])]]], [Pyrb::Bytes.call(["&a=b"]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["b"])]]], [Pyrb::Bytes.call(["a=a+b&b=b+c"]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["a b"])], [Pyrb::Bytes.call(["b"]), Pyrb::Bytes.call(["b c"])]]], [Pyrb::Bytes.call(["a=1&a=2"]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["1"])], [Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["2"])]]], [";", []], [";;", []], [";a=b", [["a", "b"]]], ["a=a+b;b=b+c", [["a", "a b"], ["b", "b c"]]], ["a=1;a=2", [["a", "1"], ["a", "2"]]], [Pyrb::Bytes.call([";"]), []], [Pyrb::Bytes.call([";;"]), []], [Pyrb::Bytes.call([";a=b"]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["b"])]]], [Pyrb::Bytes.call(["a=a+b;b=b+c"]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["a b"])], [Pyrb::Bytes.call(["b"]), Pyrb::Bytes.call(["b c"])]]], [Pyrb::Bytes.call(["a=1;a=2"]), [[Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["1"])], [Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["2"])]]], ]
  exports['parse_qs_test_cases'] = [["", Pyrb::Dict.new], ["&", Pyrb::Dict.new], ["&&", Pyrb::Dict.new], ["=", Pyrb::Dict.new([{"" => [""]}])], ["=a", Pyrb::Dict.new([{"" => ["a"]}])], ["a", Pyrb::Dict.new([{"a" => [""]}])], ["a=", Pyrb::Dict.new([{"a" => [""]}])], ["&a=b", Pyrb::Dict.new([{"a" => ["b"]}])], ["a=a+b&b=b+c", Pyrb::Dict.new([{"a" => ["a b"], "b" => ["b c"]}])], ["a=1&a=2", Pyrb::Dict.new([{"a" => ["1", "2"]}])], [Pyrb::Bytes.call([""]), Pyrb::Dict.new], [Pyrb::Bytes.call(["&"]), Pyrb::Dict.new], [Pyrb::Bytes.call(["&&"]), Pyrb::Dict.new], [Pyrb::Bytes.call(["="]), Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => [Pyrb::Bytes.call([""])]}])], [Pyrb::Bytes.call(["=a"]), Pyrb::Dict.new([{Pyrb::Bytes.call([""]) => [Pyrb::Bytes.call(["a"])]}])], [Pyrb::Bytes.call(["a"]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call([""])]}])], [Pyrb::Bytes.call(["a="]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call([""])]}])], [Pyrb::Bytes.call(["&a=b"]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call(["b"])]}])], [Pyrb::Bytes.call(["a=a+b&b=b+c"]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call(["a b"])], Pyrb::Bytes.call(["b"]) => [Pyrb::Bytes.call(["b c"])]}])], [Pyrb::Bytes.call(["a=1&a=2"]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call(["1"]), Pyrb::Bytes.call(["2"])]}])], [";", Pyrb::Dict.new], [";;", Pyrb::Dict.new], [";a=b", Pyrb::Dict.new([{"a" => ["b"]}])], ["a=a+b;b=b+c", Pyrb::Dict.new([{"a" => ["a b"], "b" => ["b c"]}])], ["a=1;a=2", Pyrb::Dict.new([{"a" => ["1", "2"]}])], [Pyrb::Bytes.call([";"]), Pyrb::Dict.new], [Pyrb::Bytes.call([";;"]), Pyrb::Dict.new], [Pyrb::Bytes.call([";a=b"]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call(["b"])]}])], [Pyrb::Bytes.call(["a=a+b;b=b+c"]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call(["a b"])], Pyrb::Bytes.call(["b"]) => [Pyrb::Bytes.call(["b c"])]}])], [Pyrb::Bytes.call(["a=1;a=2"]), Pyrb::Dict.new([{Pyrb::Bytes.call(["a"]) => [Pyrb::Bytes.call(["1"]), Pyrb::Bytes.call(["2"])]}])], ]
  exports['UrlParseTestCase'] = Pyrb::PythonClass.new('UrlParseTestCase', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['checkRoundtrips'] = Pyrb.defn(self: nil, url: nil, parsed: nil, split: nil) do |self_, url, parsed, split|
      result = urllib.attr('parse').fcall('urlparse', [url])
      self_.fcall('assertEqual', [result, parsed])
      t = [result.attr('scheme'), result.attr('netloc'), result.attr('path'), result.attr('params'), result.attr('query'), result.attr('fragment')]
      self_.fcall('assertEqual', [t, parsed])
      result2 = urllib.attr('parse').fcall('urlunparse', [result])
      self_.fcall('assertEqual', [result2, url])
      self_.fcall('assertEqual', [result2, result.fcall('geturl')])
      result3 = urllib.attr('parse').fcall('urlparse', [result.fcall('geturl')])
      self_.fcall('assertEqual', [result3.fcall('geturl'), result.fcall('geturl')])
      self_.fcall('assertEqual', [result3, result])
      self_.fcall('assertEqual', [result3.attr('scheme'), result.attr('scheme')])
      self_.fcall('assertEqual', [result3.attr('netloc'), result.attr('netloc')])
      self_.fcall('assertEqual', [result3.attr('path'), result.attr('path')])
      self_.fcall('assertEqual', [result3.attr('params'), result.attr('params')])
      self_.fcall('assertEqual', [result3.attr('query'), result.attr('query')])
      self_.fcall('assertEqual', [result3.attr('fragment'), result.attr('fragment')])
      self_.fcall('assertEqual', [result3.attr('username'), result.attr('username')])
      self_.fcall('assertEqual', [result3.attr('password'), result.attr('password')])
      self_.fcall('assertEqual', [result3.attr('hostname'), result.attr('hostname')])
      self_.fcall('assertEqual', [result3.attr('port'), result.attr('port')])
      result = urllib.attr('parse').fcall('urlsplit', [url])
      self_.fcall('assertEqual', [result, split])
      t = [result.attr('scheme'), result.attr('netloc'), result.attr('path'), result.attr('query'), result.attr('fragment')]
      self_.fcall('assertEqual', [t, split])
      result2 = urllib.attr('parse').fcall('urlunsplit', [result])
      self_.fcall('assertEqual', [result2, url])
      self_.fcall('assertEqual', [result2, result.fcall('geturl')])
      result3 = urllib.attr('parse').fcall('urlsplit', [result.fcall('geturl')])
      self_.fcall('assertEqual', [result3.fcall('geturl'), result.fcall('geturl')])
      self_.fcall('assertEqual', [result3, result])
      self_.fcall('assertEqual', [result3.attr('scheme'), result.attr('scheme')])
      self_.fcall('assertEqual', [result3.attr('netloc'), result.attr('netloc')])
      self_.fcall('assertEqual', [result3.attr('path'), result.attr('path')])
      self_.fcall('assertEqual', [result3.attr('query'), result.attr('query')])
      self_.fcall('assertEqual', [result3.attr('fragment'), result.attr('fragment')])
      self_.fcall('assertEqual', [result3.attr('username'), result.attr('username')])
      self_.fcall('assertEqual', [result3.attr('password'), result.attr('password')])
      self_.fcall('assertEqual', [result3.attr('hostname'), result.attr('hostname')])
      self_.fcall('assertEqual', [result3.attr('port'), result.attr('port')])
    end

    klass.attrs['test_qsl'] = Pyrb.defn(self: nil) do |self_|
      result, keep_blank_values, expect_without_blanks = nil

      (exports['parse_qsl_test_cases']).each do |orig, expect|
        result = urllib.attr('parse').fcall('parse_qsl', [orig, ], { keep_blank_values: true })
        self_.fcall('assertEqual', [result, expect, ("Error parsing %r" % orig)])
        expect_without_blanks = expect.fcall('__iter__').each_with_object([]) do |v, _ret_|
          if !!(Pyrb.len.call([v[1]]))
            _ret_ << v
          end
        end

        result = urllib.attr('parse').fcall('parse_qsl', [orig, ], { keep_blank_values: false })
        self_.fcall('assertEqual', [result, expect_without_blanks, ("Error parsing %r" % orig)])
      end
    end

    klass.attrs['test_qs'] = Pyrb.defn(self: nil) do |self_|
      result, keep_blank_values, expect_without_blanks = nil

      (exports['parse_qs_test_cases']).each do |orig, expect|
        result = urllib.attr('parse').fcall('parse_qs', [orig, ], { keep_blank_values: true })
        self_.fcall('assertEqual', [result, expect, ("Error parsing %r" % orig)])
        expect_without_blanks = expect.fcall('__iter__').each_with_object(Pyrb::Dict.new) do |(v), _ret_|
          if !!(Pyrb.len.call([expect[v][0]]))
            _ret_[v] = expect[v]
          end
        end

        result = urllib.attr('parse').fcall('parse_qs', [orig, ], { keep_blank_values: false })
        self_.fcall('assertEqual', [result, expect_without_blanks, ("Error parsing %r" % orig)])
      end
    end

    klass.attrs['test_roundtrips'] = Pyrb.defn(self: nil) do |self_|
      str_cases = [["file:///tmp/junk.txt", ["file", "", "/tmp/junk.txt", "", "", ""], ["file", "", "/tmp/junk.txt", "", ""]], ["imap://mail.python.org/mbox1", ["imap", "mail.python.org", "/mbox1", "", "", ""], ["imap", "mail.python.org", "/mbox1", "", ""]], ["mms://wms.sys.hinet.net/cts/Drama/09006251100.asf", ["mms", "wms.sys.hinet.net", "/cts/Drama/09006251100.asf", "", "", ""], ["mms", "wms.sys.hinet.net", "/cts/Drama/09006251100.asf", "", ""]], ["nfs://server/path/to/file.txt", ["nfs", "server", "/path/to/file.txt", "", "", ""], ["nfs", "server", "/path/to/file.txt", "", ""]], ["svn+ssh://svn.zope.org/repos/main/ZConfig/trunk/", ["svn+ssh", "svn.zope.org", "/repos/main/ZConfig/trunk/", "", "", ""], ["svn+ssh", "svn.zope.org", "/repos/main/ZConfig/trunk/", "", ""]], ["git+ssh://git@github.com/user/project.git", ["git+ssh", "git@github.com", "/user/project.git", "", "", ""], ["git+ssh", "git@github.com", "/user/project.git", "", ""]], ]
      _encode = Pyrb.defn(t: nil) do |t|
        throw(:return, [t[0].fcall('encode', ["ascii"]), Pyrb::List.call([t[1].fcall('__iter__').map do |x|
          x.fcall('encode', ["ascii"])
        end
        ]), Pyrb::List.call([t[2].fcall('__iter__').map do |x|
          x.fcall('encode', ["ascii"])
        end
        ])])
      end

      bytes_cases = str_cases.fcall('__iter__').map do |x|
        _encode.call([x])
      end

      (bytes_cases).each do |url, parsed, split|
        self_.fcall('checkRoundtrips', [url, parsed, split])
      end
    end

    klass.attrs['test_http_roundtrips'] = Pyrb.defn(self: nil) do |self_|
      str_cases = [["://www.python.org", ["www.python.org", "", "", "", ""], ["www.python.org", "", "", ""]], ["://www.python.org\#abc", ["www.python.org", "", "", "", "abc"], ["www.python.org", "", "", "abc"]], ["://www.python.org?q=abc", ["www.python.org", "", "", "q=abc", ""], ["www.python.org", "", "q=abc", ""]], ["://www.python.org/\#abc", ["www.python.org", "/", "", "", "abc"], ["www.python.org", "/", "", "abc"]], ["://a/b/c/d;p?q\#f", ["a", "/b/c/d", "p", "q", "f"], ["a", "/b/c/d;p", "q", "f"]], ]
      _encode = Pyrb.defn(t: nil) do |t|
        throw(:return, [t[0].fcall('encode', ["ascii"]), Pyrb::List.call([t[1].fcall('__iter__').map do |x|
          x.fcall('encode', ["ascii"])
        end
        ]), Pyrb::List.call([t[2].fcall('__iter__').map do |x|
          x.fcall('encode', ["ascii"])
        end
        ])])
      end

      bytes_cases = str_cases.fcall('__iter__').map do |x|
        _encode.call([x])
      end

      str_schemes = ["http", "https"]
      bytes_schemes = [Pyrb::Bytes.call(["http"]), Pyrb::Bytes.call(["https"])]
      str_tests = str_schemes, str_cases
      bytes_tests = bytes_schemes, bytes_cases
      ([str_tests, bytes_tests]).each do |schemes, test_cases|
        (schemes).each do |scheme|
          (test_cases).each do |url, parsed, split|
            url = scheme + url
            parsed = [scheme, ] + parsed
            split = [scheme, ] + split
            self_.fcall('checkRoundtrips', [url, parsed, split])
          end
        end
      end
    end

    klass.attrs['checkJoin'] = Pyrb.defn(self: nil, base: nil, relurl: nil, expected: nil) do |self_, base, relurl, expected|
      str_components = [base, relurl, expected]
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urljoin', [base, relurl]), expected])
      bytes_components = (baseb, relurlb, expectedb = Pyrb.deconstruct(3, str_components.fcall('__iter__').map do |x|
        x.fcall('encode', ["ascii"])
      end
      ))
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urljoin', [baseb, relurlb]), expectedb])
    end

    klass.attrs['test_unparse_parse'] = Pyrb.defn(self: nil) do |self_|
      str_cases = ["Python", "./Python", "x-newscheme://foo.com/stuff", "x://y", "x:/y", "x:/", "/", ]
      bytes_cases = str_cases.fcall('__iter__').map do |x|
        x.fcall('encode', ["ascii"])
      end

      (str_cases + bytes_cases).each do |u|
        self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlunsplit', [urllib.attr('parse').fcall('urlsplit', [u])]), u])
        self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlunparse', [urllib.attr('parse').fcall('urlparse', [u])]), u])
      end
    end

    klass.attrs['test_RFC1808'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('checkJoin', [exports['RFC1808_BASE'], "g/", "http://a/b/c/g/"])
    end

    klass.attrs['test_RFC2368'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["mailto:1337@example.org"]), ["mailto", "", "1337@example.org", "", "", ""]])
    end

    klass.attrs['test_RFC2396'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g:h", "g:h"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "./g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g/", "http://a/b/c/g/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "/g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "//g", "http://g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g?y", "http://a/b/c/g?y"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "\#s", "http://a/b/c/d;p?q\#s"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g\#s", "http://a/b/c/g\#s"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g?y\#s", "http://a/b/c/g?y\#s"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g;x", "http://a/b/c/g;x"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g;x?y\#s", "http://a/b/c/g;x?y\#s"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], ".", "http://a/b/c/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "./", "http://a/b/c/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "..", "http://a/b/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "../", "http://a/b/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "../g", "http://a/b/g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "../..", "http://a/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "../../", "http://a/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "../../g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "", exports['RFC2396_BASE']])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g.", "http://a/b/c/g."])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], ".g", "http://a/b/c/.g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g..", "http://a/b/c/g.."])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "..g", "http://a/b/c/..g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "./../g", "http://a/b/g"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "./g/.", "http://a/b/c/g/"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g/./h", "http://a/b/c/g/h"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g/../h", "http://a/b/c/h"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g;x=1/./y", "http://a/b/c/g;x=1/y"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g;x=1/../y", "http://a/b/c/y"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g?y/./x", "http://a/b/c/g?y/./x"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g?y/../x", "http://a/b/c/g?y/../x"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g\#s/./x", "http://a/b/c/g\#s/./x"])
      self_.fcall('checkJoin', [exports['RFC2396_BASE'], "g\#s/../x", "http://a/b/c/g\#s/../x"])
    end

    klass.attrs['test_RFC3986'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "?y", "http://a/b/c/d;p?y"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], ";x", "http://a/b/c/;x"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g:h", "g:h"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "./g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g/", "http://a/b/c/g/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "/g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "//g", "http://g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "?y", "http://a/b/c/d;p?y"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g?y", "http://a/b/c/g?y"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "\#s", "http://a/b/c/d;p?q\#s"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g\#s", "http://a/b/c/g\#s"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g?y\#s", "http://a/b/c/g?y\#s"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], ";x", "http://a/b/c/;x"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g;x", "http://a/b/c/g;x"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g;x?y\#s", "http://a/b/c/g;x?y\#s"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "", "http://a/b/c/d;p?q"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], ".", "http://a/b/c/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "./", "http://a/b/c/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "..", "http://a/b/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../", "http://a/b/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../g", "http://a/b/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../..", "http://a/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../../", "http://a/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../../g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../../../g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../../../g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "../../../../g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "/./g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "/../g", "http://a/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g.", "http://a/b/c/g."])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], ".g", "http://a/b/c/.g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g..", "http://a/b/c/g.."])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "..g", "http://a/b/c/..g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "./../g", "http://a/b/g"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "./g/.", "http://a/b/c/g/"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g/./h", "http://a/b/c/g/h"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g/../h", "http://a/b/c/h"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g;x=1/./y", "http://a/b/c/g;x=1/y"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g;x=1/../y", "http://a/b/c/y"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g?y/./x", "http://a/b/c/g?y/./x"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g?y/../x", "http://a/b/c/g?y/../x"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g\#s/./x", "http://a/b/c/g\#s/./x"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "g\#s/../x", "http://a/b/c/g\#s/../x"])
      self_.fcall('checkJoin', [exports['RFC3986_BASE'], "http:g", "http://a/b/c/g"])
      self_.fcall('checkJoin', ["http://a/b/c/de", ";x", "http://a/b/c/;x"])
    end

    klass.attrs['test_urljoins'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "g:h", "g:h"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "http:g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "http:", "http://a/b/c/d"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "./g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "g/", "http://a/b/c/g/"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "/g", "http://a/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "//g", "http://g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "?y", "http://a/b/c/d?y"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "g?y", "http://a/b/c/g?y"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "g?y/./x", "http://a/b/c/g?y/./x"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], ".", "http://a/b/c/"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "./", "http://a/b/c/"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "..", "http://a/b/"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "../", "http://a/b/"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "../g", "http://a/b/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "../..", "http://a/"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "../../g", "http://a/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "./../g", "http://a/b/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "./g/.", "http://a/b/c/g/"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "g/./h", "http://a/b/c/g/h"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "g/../h", "http://a/b/c/h"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "http:g", "http://a/b/c/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "http:", "http://a/b/c/d"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "http:?y", "http://a/b/c/d?y"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "http:g?y", "http://a/b/c/g?y"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'], "http:g?y/./x", "http://a/b/c/g?y/./x"])
      self_.fcall('checkJoin', ["http:///", "..", "http:///"])
      self_.fcall('checkJoin', ["", "http://a/b/c/g?y/./x", "http://a/b/c/g?y/./x"])
      self_.fcall('checkJoin', ["", "http://a/./g", "http://a/./g"])
      self_.fcall('checkJoin', ["svn://pathtorepo/dir1", "dir2", "svn://pathtorepo/dir2"])
      self_.fcall('checkJoin', ["svn+ssh://pathtorepo/dir1", "dir2", "svn+ssh://pathtorepo/dir2"])
      self_.fcall('checkJoin', ["ws://a/b", "g", "ws://a/g"])
      self_.fcall('checkJoin', ["wss://a/b", "g", "wss://a/g"])
      self_.fcall('checkJoin', [exports['SIMPLE_BASE'] + "/", "foo", exports['SIMPLE_BASE'] + "/foo"])
      self_.fcall('checkJoin', ["http://a/b/c/d/e/", "../../f/g/", "http://a/b/c/f/g/"])
      self_.fcall('checkJoin', ["http://a/b/c/d/e", "../../f/g/", "http://a/b/f/g/"])
      self_.fcall('checkJoin', ["http://a/b/c/d/e/", "/../../f/g/", "http://a/f/g/"])
      self_.fcall('checkJoin', ["http://a/b/c/d/e", "/../../f/g/", "http://a/f/g/"])
      self_.fcall('checkJoin', ["http://a/b/c/d/e/", "../../f/g", "http://a/b/c/f/g"])
      self_.fcall('checkJoin', ["http://a/b/", "../../f/g/", "http://a/f/g/"])
      self_.fcall('checkJoin', ["a", "b", "b"])
    end

    klass.attrs['test_RFC2732'] = Pyrb.defn(self: nil) do |self_|
      urlparsed = nil

      str_cases = [["http://Test.python.org:5432/foo/", "test.python.org", 5432], ["http://12.34.56.78:5432/foo/", "12.34.56.78", 5432], ["http://[::1]:5432/foo/", "::1", 5432], ["http://[dead:beef::1]:5432/foo/", "dead:beef::1", 5432], ["http://[dead:beef::]:5432/foo/", "dead:beef::", 5432], ["http://[dead:beef:cafe:5417:affe:8FA3:deaf:feed]:5432/foo/", "dead:beef:cafe:5417:affe:8fa3:deaf:feed", 5432], ["http://[::12.34.56.78]:5432/foo/", "::12.34.56.78", 5432], ["http://[::ffff:12.34.56.78]:5432/foo/", "::ffff:12.34.56.78", 5432], ["http://Test.python.org/foo/", "test.python.org", nil], ["http://12.34.56.78/foo/", "12.34.56.78", nil], ["http://[::1]/foo/", "::1", nil], ["http://[dead:beef::1]/foo/", "dead:beef::1", nil], ["http://[dead:beef::]/foo/", "dead:beef::", nil], ["http://[dead:beef:cafe:5417:affe:8FA3:deaf:feed]/foo/", "dead:beef:cafe:5417:affe:8fa3:deaf:feed", nil], ["http://[::12.34.56.78]/foo/", "::12.34.56.78", nil], ["http://[::ffff:12.34.56.78]/foo/", "::ffff:12.34.56.78", nil], ["http://Test.python.org:/foo/", "test.python.org", nil], ["http://12.34.56.78:/foo/", "12.34.56.78", nil], ["http://[::1]:/foo/", "::1", nil], ["http://[dead:beef::1]:/foo/", "dead:beef::1", nil], ["http://[dead:beef::]:/foo/", "dead:beef::", nil], ["http://[dead:beef:cafe:5417:affe:8FA3:deaf:feed]:/foo/", "dead:beef:cafe:5417:affe:8fa3:deaf:feed", nil], ["http://[::12.34.56.78]:/foo/", "::12.34.56.78", nil], ["http://[::ffff:12.34.56.78]:/foo/", "::ffff:12.34.56.78", nil], ]
      _encode = Pyrb.defn(t: nil) do |t|
        throw(:return, [t[0].fcall('encode', ["ascii"]), t[1].fcall('encode', ["ascii"]), t[2]])
      end

      bytes_cases = str_cases.fcall('__iter__').map do |x|
        _encode.call([x])
      end

      (str_cases + bytes_cases).each do |url, hostname, port|
        urlparsed = urllib.attr('parse').fcall('urlparse', [url])
        self_.fcall('assertEqual', [[urlparsed.attr('hostname'), urlparsed.attr('port')], [hostname, port]])
      end
      str_cases = ["http://::12.34.56.78]/", "http://[::1/foo/", "ftp://[::1/foo/bad]/bad", "http://[::1/foo/bad]/bad", "http://[::ffff:12.34.56.78"]
      bytes_cases = str_cases.fcall('__iter__').map do |x|
        x.fcall('encode', ["ascii"])
      end

      (str_cases + bytes_cases).each do |invalid_url|
        self_.fcall('assertRaises', [Pyrb::ValueError, urllib.attr('parse').attr('urlparse'), invalid_url])
      end
    end

    klass.attrs['test_urldefrag'] = Pyrb.defn(self: nil) do |self_|
      result = nil

      str_cases = [["http://python.org\#frag", "http://python.org", "frag"], ["http://python.org", "http://python.org", ""], ["http://python.org/\#frag", "http://python.org/", "frag"], ["http://python.org/", "http://python.org/", ""], ["http://python.org/?q\#frag", "http://python.org/?q", "frag"], ["http://python.org/?q", "http://python.org/?q", ""], ["http://python.org/p\#frag", "http://python.org/p", "frag"], ["http://python.org/p?q", "http://python.org/p?q", ""], [exports['RFC1808_BASE'], "http://a/b/c/d;p?q", "f"], [exports['RFC2396_BASE'], "http://a/b/c/d;p?q", ""], ]
      _encode = Pyrb.defn(t: nil) do |t|
        throw(:return, Pyrb.type.call([t]).call([t.fcall('__iter__').map do |x|
          x.fcall('encode', ["ascii"])
        end
        ]))
      end

      bytes_cases = str_cases.fcall('__iter__').map do |x|
        _encode.call([x])
      end

      (str_cases + bytes_cases).each do |url, defrag, frag|
        result = urllib.attr('parse').fcall('urldefrag', [url])
        self_.fcall('assertEqual', [result.fcall('geturl'), url])
        self_.fcall('assertEqual', [result, [defrag, frag]])
        self_.fcall('assertEqual', [result.attr('url'), defrag])
        self_.fcall('assertEqual', [result.attr('fragment'), frag])
      end
    end

    klass.attrs['test_urlsplit_scoped_IPv6'] = Pyrb.defn(self: nil) do |self_|
      p = urllib.attr('parse').fcall('urlsplit', ["http://[FE80::822a:a8ff:fe49:470c%tESt]:1234"])
      self_.fcall('assertEqual', [p.attr('hostname'), "fe80::822a:a8ff:fe49:470c%tESt"])
      self_.fcall('assertEqual', [p.attr('netloc'), "[FE80::822a:a8ff:fe49:470c%tESt]:1234"])
      p = urllib.attr('parse').fcall('urlsplit', [Pyrb::Bytes.call(["http://[FE80::822a:a8ff:fe49:470c%tESt]:1234"])])
      self_.fcall('assertEqual', [p.attr('hostname'), Pyrb::Bytes.call(["fe80::822a:a8ff:fe49:470c%tESt"])])
      self_.fcall('assertEqual', [p.attr('netloc'), Pyrb::Bytes.call(["[FE80::822a:a8ff:fe49:470c%tESt]:1234"])])
    end

    klass.attrs['test_urlsplit_attributes'] = Pyrb.defn(self: nil) do |self_|
      url = "HTTP://WWW.PYTHON.ORG/doc/\#frag"
      p = urllib.attr('parse').fcall('urlsplit', [url])
      self_.fcall('assertEqual', [p.attr('scheme'), "http"])
      self_.fcall('assertEqual', [p.attr('netloc'), "WWW.PYTHON.ORG"])
      self_.fcall('assertEqual', [p.attr('path'), "/doc/"])
      self_.fcall('assertEqual', [p.attr('query'), ""])
      self_.fcall('assertEqual', [p.attr('fragment'), "frag"])
      self_.fcall('assertEqual', [p.attr('username'), nil])
      self_.fcall('assertEqual', [p.attr('password'), nil])
      self_.fcall('assertEqual', [p.attr('hostname'), "www.python.org"])
      self_.fcall('assertEqual', [p.attr('port'), nil])
      self_.fcall('assertEqual', [p.fcall('geturl')[4..-1], url[4..-1]])
      url = "http://User:Pass@www.python.org:080/doc/?query=yes\#frag"
      p = urllib.attr('parse').fcall('urlsplit', [url])
      self_.fcall('assertEqual', [p.attr('scheme'), "http"])
      self_.fcall('assertEqual', [p.attr('netloc'), "User:Pass@www.python.org:080"])
      self_.fcall('assertEqual', [p.attr('path'), "/doc/"])
      self_.fcall('assertEqual', [p.attr('query'), "query=yes"])
      self_.fcall('assertEqual', [p.attr('fragment'), "frag"])
      self_.fcall('assertEqual', [p.attr('username'), "User"])
      self_.fcall('assertEqual', [p.attr('password'), "Pass"])
      self_.fcall('assertEqual', [p.attr('hostname'), "www.python.org"])
      self_.fcall('assertEqual', [p.attr('port'), 80])
      self_.fcall('assertEqual', [p.fcall('geturl'), url])
      url = "http://User@example.com:Pass@www.python.org:080/doc/?query=yes\#frag"
      p = urllib.attr('parse').fcall('urlsplit', [url])
      self_.fcall('assertEqual', [p.attr('scheme'), "http"])
      self_.fcall('assertEqual', [p.attr('netloc'), "User@example.com:Pass@www.python.org:080"])
      self_.fcall('assertEqual', [p.attr('path'), "/doc/"])
      self_.fcall('assertEqual', [p.attr('query'), "query=yes"])
      self_.fcall('assertEqual', [p.attr('fragment'), "frag"])
      self_.fcall('assertEqual', [p.attr('username'), "User@example.com"])
      self_.fcall('assertEqual', [p.attr('password'), "Pass"])
      self_.fcall('assertEqual', [p.attr('hostname'), "www.python.org"])
      self_.fcall('assertEqual', [p.attr('port'), 80])
      self_.fcall('assertEqual', [p.fcall('geturl'), url])
      url = Pyrb::Bytes.call(["HTTP://WWW.PYTHON.ORG/doc/\#frag"])
      p = urllib.attr('parse').fcall('urlsplit', [url])
      self_.fcall('assertEqual', [p.attr('scheme'), Pyrb::Bytes.call(["http"])])
      self_.fcall('assertEqual', [p.attr('netloc'), Pyrb::Bytes.call(["WWW.PYTHON.ORG"])])
      self_.fcall('assertEqual', [p.attr('path'), Pyrb::Bytes.call(["/doc/"])])
      self_.fcall('assertEqual', [p.attr('query'), Pyrb::Bytes.call([""])])
      self_.fcall('assertEqual', [p.attr('fragment'), Pyrb::Bytes.call(["frag"])])
      self_.fcall('assertEqual', [p.attr('username'), nil])
      self_.fcall('assertEqual', [p.attr('password'), nil])
      self_.fcall('assertEqual', [p.attr('hostname'), Pyrb::Bytes.call(["www.python.org"])])
      self_.fcall('assertEqual', [p.attr('port'), nil])
      self_.fcall('assertEqual', [p.fcall('geturl')[4..-1], url[4..-1]])
      url = Pyrb::Bytes.call(["http://User:Pass@www.python.org:080/doc/?query=yes\#frag"])
      p = urllib.attr('parse').fcall('urlsplit', [url])
      self_.fcall('assertEqual', [p.attr('scheme'), Pyrb::Bytes.call(["http"])])
      self_.fcall('assertEqual', [p.attr('netloc'), Pyrb::Bytes.call(["User:Pass@www.python.org:080"])])
      self_.fcall('assertEqual', [p.attr('path'), Pyrb::Bytes.call(["/doc/"])])
      self_.fcall('assertEqual', [p.attr('query'), Pyrb::Bytes.call(["query=yes"])])
      self_.fcall('assertEqual', [p.attr('fragment'), Pyrb::Bytes.call(["frag"])])
      self_.fcall('assertEqual', [p.attr('username'), Pyrb::Bytes.call(["User"])])
      self_.fcall('assertEqual', [p.attr('password'), Pyrb::Bytes.call(["Pass"])])
      self_.fcall('assertEqual', [p.attr('hostname'), Pyrb::Bytes.call(["www.python.org"])])
      self_.fcall('assertEqual', [p.attr('port'), 80])
      self_.fcall('assertEqual', [p.fcall('geturl'), url])
      url = Pyrb::Bytes.call(["http://User@example.com:Pass@www.python.org:080/doc/?query=yes\#frag"])
      p = urllib.attr('parse').fcall('urlsplit', [url])
      self_.fcall('assertEqual', [p.attr('scheme'), Pyrb::Bytes.call(["http"])])
      self_.fcall('assertEqual', [p.attr('netloc'), Pyrb::Bytes.call(["User@example.com:Pass@www.python.org:080"])])
      self_.fcall('assertEqual', [p.attr('path'), Pyrb::Bytes.call(["/doc/"])])
      self_.fcall('assertEqual', [p.attr('query'), Pyrb::Bytes.call(["query=yes"])])
      self_.fcall('assertEqual', [p.attr('fragment'), Pyrb::Bytes.call(["frag"])])
      self_.fcall('assertEqual', [p.attr('username'), Pyrb::Bytes.call(["User@example.com"])])
      self_.fcall('assertEqual', [p.attr('password'), Pyrb::Bytes.call(["Pass"])])
      self_.fcall('assertEqual', [p.attr('hostname'), Pyrb::Bytes.call(["www.python.org"])])
      self_.fcall('assertEqual', [p.attr('port'), 80])
      self_.fcall('assertEqual', [p.fcall('geturl'), url])
      url = Pyrb::Bytes.call(["HTTP://WWW.PYTHON.ORG:65536/doc/\#frag"])
      p = urllib.attr('parse').fcall('urlsplit', [url])
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::ValueError, "out of range"])]) do
        p.attr('port')
      end
    end

    klass.attrs['test_attributes_bad_port'] = Pyrb.defn(self: nil) do |self_|
      netloc, url, p = nil

      "Check handling of invalid ports."
      ([false, true]).each do |bytes|
        ([urllib.attr('parse').attr('urlsplit'), urllib.attr('parse').attr('urlparse')]).each do |parse|
          (["foo", "1.5", "-1", "0x10"]).each do |port|
            Pyrb.with.call([self_.fcall('subTest', [], { bytes: bytes, parse: parse, port: port })]) do
              netloc = "www.example.net:" + port
              url = "http://" + netloc
              if !!(bytes)
                netloc = netloc.fcall('encode', ["ascii"])
                url = url.fcall('encode', ["ascii"])
              end

              p = parse.call([url])
              self_.fcall('assertEqual', [p.attr('netloc'), netloc])
              Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
                p.attr('port')
              end
            end
          end
        end
      end
    end

    klass.attrs['test_attributes_without_netloc'] = Pyrb.defn(self: nil) do |self_|
      uri = "sip:alice@atlanta.com;maddr=239.255.255.1;ttl=15"
      p = urllib.attr('parse').fcall('urlsplit', [uri])
      self_.fcall('assertEqual', [p.attr('netloc'), ""])
      self_.fcall('assertEqual', [p.attr('username'), nil])
      self_.fcall('assertEqual', [p.attr('password'), nil])
      self_.fcall('assertEqual', [p.attr('hostname'), nil])
      self_.fcall('assertEqual', [p.attr('port'), nil])
      self_.fcall('assertEqual', [p.fcall('geturl'), uri])
      p = urllib.attr('parse').fcall('urlparse', [uri])
      self_.fcall('assertEqual', [p.attr('netloc'), ""])
      self_.fcall('assertEqual', [p.attr('username'), nil])
      self_.fcall('assertEqual', [p.attr('password'), nil])
      self_.fcall('assertEqual', [p.attr('hostname'), nil])
      self_.fcall('assertEqual', [p.attr('port'), nil])
      self_.fcall('assertEqual', [p.fcall('geturl'), uri])
      uri = Pyrb::Bytes.call(["sip:alice@atlanta.com;maddr=239.255.255.1;ttl=15"])
      p = urllib.attr('parse').fcall('urlsplit', [uri])
      self_.fcall('assertEqual', [p.attr('netloc'), Pyrb::Bytes.call([""])])
      self_.fcall('assertEqual', [p.attr('username'), nil])
      self_.fcall('assertEqual', [p.attr('password'), nil])
      self_.fcall('assertEqual', [p.attr('hostname'), nil])
      self_.fcall('assertEqual', [p.attr('port'), nil])
      self_.fcall('assertEqual', [p.fcall('geturl'), uri])
      p = urllib.attr('parse').fcall('urlparse', [uri])
      self_.fcall('assertEqual', [p.attr('netloc'), Pyrb::Bytes.call([""])])
      self_.fcall('assertEqual', [p.attr('username'), nil])
      self_.fcall('assertEqual', [p.attr('password'), nil])
      self_.fcall('assertEqual', [p.attr('hostname'), nil])
      self_.fcall('assertEqual', [p.attr('port'), nil])
      self_.fcall('assertEqual', [p.fcall('geturl'), uri])
    end

    klass.attrs['test_noslash'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["http://example.com?blahblah=/foo"]), ["http", "example.com", "", "", "blahblah=/foo", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["http://example.com?blahblah=/foo"])]), [Pyrb::Bytes.call(["http"]), Pyrb::Bytes.call(["example.com"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["blahblah=/foo"]), Pyrb::Bytes.call([""])]])
    end

    klass.attrs['test_withoutscheme'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["path"]), ["", "", "path", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["//www.python.org:80"]), ["", "www.python.org:80", "", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["http://www.python.org:80"]), ["http", "www.python.org:80", "", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["path"])]), [Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["path"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["//www.python.org:80"])]), [Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["www.python.org:80"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["http://www.python.org:80"])]), [Pyrb::Bytes.call(["http"]), Pyrb::Bytes.call(["www.python.org:80"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
    end

    klass.attrs['test_portseparator'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["path:80"]), ["", "", "path:80", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["http:"]), ["http", "", "", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["https:"]), ["https", "", "", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["http://www.python.org:80"]), ["http", "www.python.org:80", "", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["path:80"])]), [Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["path:80"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["http:"])]), [Pyrb::Bytes.call(["http"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["https:"])]), [Pyrb::Bytes.call(["https"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["http://www.python.org:80"])]), [Pyrb::Bytes.call(["http"]), Pyrb::Bytes.call(["www.python.org:80"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
    end

    klass.attrs['test_usingsys'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::TypeError, urllib.attr('parse').attr('urlencode'), "foo"])
    end

    klass.attrs['test_anyscheme'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["s3://foo.com/stuff"]), ["s3", "foo.com", "/stuff", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["x-newscheme://foo.com/stuff"]), ["x-newscheme", "foo.com", "/stuff", "", "", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["x-newscheme://foo.com/stuff?query\#fragment"]), ["x-newscheme", "foo.com", "/stuff", "", "query", "fragment"]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', ["x-newscheme://foo.com/stuff?query"]), ["x-newscheme", "foo.com", "/stuff", "", "query", ""]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["s3://foo.com/stuff"])]), [Pyrb::Bytes.call(["s3"]), Pyrb::Bytes.call(["foo.com"]), Pyrb::Bytes.call(["/stuff"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["x-newscheme://foo.com/stuff"])]), [Pyrb::Bytes.call(["x-newscheme"]), Pyrb::Bytes.call(["foo.com"]), Pyrb::Bytes.call(["/stuff"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["x-newscheme://foo.com/stuff?query\#fragment"])]), [Pyrb::Bytes.call(["x-newscheme"]), Pyrb::Bytes.call(["foo.com"]), Pyrb::Bytes.call(["/stuff"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["query"]), Pyrb::Bytes.call(["fragment"])]])
      self_.fcall('assertEqual', [urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["x-newscheme://foo.com/stuff?query"])]), [Pyrb::Bytes.call(["x-newscheme"]), Pyrb::Bytes.call(["foo.com"]), Pyrb::Bytes.call(["/stuff"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call(["query"]), Pyrb::Bytes.call([""])]])
    end

    klass.attrs['test_default_scheme'] = Pyrb.defn(self: nil) do |self_|
      result, scheme, function = nil

      ([urllib.attr('parse').attr('urlparse'), urllib.attr('parse').attr('urlsplit')]).each do |func|
        Pyrb.with.call([self_.fcall('subTest', [], { function: func })]) do
          result = func.call(["http://example.net/", "ftp"])
          self_.fcall('assertEqual', [result.attr('scheme'), "http"])
          result = func.call([Pyrb::Bytes.call(["http://example.net/"]), Pyrb::Bytes.call(["ftp"])])
          self_.fcall('assertEqual', [result.attr('scheme'), Pyrb::Bytes.call(["http"])])
          self_.fcall('assertEqual', [func.call(["path", "ftp"]).attr('scheme'), "ftp"])
          self_.fcall('assertEqual', [func.call(["path", ], { scheme: "ftp" }).attr('scheme'), "ftp"])
          self_.fcall('assertEqual', [func.call([Pyrb::Bytes.call(["path"]), ], { scheme: Pyrb::Bytes.call(["ftp"]) }).attr('scheme'), Pyrb::Bytes.call(["ftp"])])
          self_.fcall('assertEqual', [func.call(["path"]).attr('scheme'), ""])
          self_.fcall('assertEqual', [func.call([Pyrb::Bytes.call(["path"])]).attr('scheme'), Pyrb::Bytes.call([""])])
          self_.fcall('assertEqual', [func.call([Pyrb::Bytes.call(["path"]), ""]).attr('scheme'), Pyrb::Bytes.call([""])])
        end
      end
    end

    klass.attrs['test_parse_fragments'] = Pyrb.defn(self: nil) do |self_|
      result, allow_fragments, attr, function = nil

      tests = [["http:\#frag", "path", "frag"], ["//example.net\#frag", "path", "frag"], ["index.html\#frag", "path", "frag"], [";a=b\#frag", "params", "frag"], ["?a=b\#frag", "query", "frag"], ["\#frag", "path", "frag"], ["abc\#@frag", "path", "@frag"], ["//abc\#@frag", "path", "@frag"], ["//abc:80\#@frag", "path", "@frag"], ["//abc\#@frag:80", "path", "@frag:80"], ]
      (tests).each do |url, attr, expected_frag|
        ([urllib.attr('parse').attr('urlparse'), urllib.attr('parse').attr('urlsplit')]).each do |func|
          if !!((attr == "params").and { func.object_id == urllib.attr('parse').attr('urlsplit').object_id })
            attr = "path"
          end

          Pyrb.with.call([self_.fcall('subTest', [], { url: url, function: func })]) do
            result = func.call([url, ], { allow_fragments: false })
            self_.fcall('assertEqual', [result.attr('fragment'), ""])
            self_.fcall('assertTrue', [Pyrb.getattr.call([result, attr]).fcall('endswith', ["#" + expected_frag])])
            self_.fcall('assertEqual', [func.call([url, "", false]).attr('fragment'), ""])
            result = func.call([url, ], { allow_fragments: true })
            self_.fcall('assertEqual', [result.attr('fragment'), expected_frag])
            self_.fcall('assertFalse', [Pyrb.getattr.call([result, attr]).fcall('endswith', [expected_frag])])
            self_.fcall('assertEqual', [func.call([url, "", true]).attr('fragment'), expected_frag])
            self_.fcall('assertEqual', [func.call([url]).attr('fragment'), expected_frag])
          end
        end
      end
    end

    klass.attrs['test_mixed_types_rejected'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlparse', ["www.python.org", Pyrb::Bytes.call(["http"])])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlparse', [Pyrb::Bytes.call(["www.python.org"]), "http"])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlsplit', ["www.python.org", Pyrb::Bytes.call(["http"])])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlsplit', [Pyrb::Bytes.call(["www.python.org"]), "http"])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlunparse', [[Pyrb::Bytes.call(["http"]), "www.python.org", "", "", "", ""]])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlunparse', [["http", Pyrb::Bytes.call(["www.python.org"]), "", "", "", ""]])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlunsplit', [[Pyrb::Bytes.call(["http"]), "www.python.org", "", "", ""]])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urlunsplit', [["http", Pyrb::Bytes.call(["www.python.org"]), "", "", ""]])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urljoin', ["http://python.org", Pyrb::Bytes.call(["http://python.org"])])
      end
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::TypeError, "Cannot mix str"])]) do
        urllib.attr('parse').fcall('urljoin', [Pyrb::Bytes.call(["http://python.org"]), "http://python.org"])
      end
    end

    klass.attrs['_check_result_type'] = Pyrb.defn(self: nil, str_type: nil) do |self_, str_type|
      num_args = Pyrb.len.call([str_type.attr('_fields')])
      bytes_type = str_type.attr('_encoded_counterpart')
      self_.fcall('assertIs', [bytes_type.attr('_decoded_counterpart'), str_type])
      str_args = (["", ] * num_args)
      bytes_args = ([Pyrb::Bytes.call([""]), ] * num_args)
      str_result = str_type.call([*Pyrb.splat(str_args)])
      bytes_result = bytes_type.call([*Pyrb.splat(bytes_args)])
      encoding = "ascii"
      errors = "strict"
      self_.fcall('assertEqual', [str_result, str_args])
      self_.fcall('assertEqual', [bytes_result.fcall('decode'), str_args])
      self_.fcall('assertEqual', [bytes_result.fcall('decode'), str_result])
      self_.fcall('assertEqual', [bytes_result.fcall('decode', [encoding]), str_args])
      self_.fcall('assertEqual', [bytes_result.fcall('decode', [encoding]), str_result])
      self_.fcall('assertEqual', [bytes_result.fcall('decode', [encoding, errors]), str_args])
      self_.fcall('assertEqual', [bytes_result.fcall('decode', [encoding, errors]), str_result])
      self_.fcall('assertEqual', [bytes_result, bytes_args])
      self_.fcall('assertEqual', [str_result.fcall('encode'), bytes_args])
      self_.fcall('assertEqual', [str_result.fcall('encode'), bytes_result])
      self_.fcall('assertEqual', [str_result.fcall('encode', [encoding]), bytes_args])
      self_.fcall('assertEqual', [str_result.fcall('encode', [encoding]), bytes_result])
      self_.fcall('assertEqual', [str_result.fcall('encode', [encoding, errors]), bytes_args])
      self_.fcall('assertEqual', [str_result.fcall('encode', [encoding, errors]), bytes_result])
    end

    klass.attrs['test_result_pairs'] = Pyrb.defn(self: nil) do |self_|
      result_types = [urllib.attr('parse').attr('DefragResult'), urllib.attr('parse').attr('SplitResult'), urllib.attr('parse').attr('ParseResult'), ]
      (result_types).each do |result_type|
        self_.fcall('_check_result_type', [result_type])
      end
    end

    klass.attrs['test_parse_qs_encoding'] = Pyrb.defn(self: nil) do |self_|
      result = urllib.attr('parse').fcall('parse_qs', ["key=\u0141%E9", ], { encoding: "latin-1" })
      self_.fcall('assertEqual', [result, Pyrb::Dict.new([{"key" => [("\u0141" + [0xE9].pack('U*'))]}])])
      result = urllib.attr('parse').fcall('parse_qs', ["key=\u0141%C3%A9", ], { encoding: "utf-8" })
      self_.fcall('assertEqual', [result, Pyrb::Dict.new([{"key" => [("\u0141" + [0xE9].pack('U*'))]}])])
      result = urllib.attr('parse').fcall('parse_qs', ["key=\u0141%C3%A9", ], { encoding: "ascii" })
      self_.fcall('assertEqual', [result, Pyrb::Dict.new([{"key" => ["\u0141\ufffd\ufffd"]}])])
      result = urllib.attr('parse').fcall('parse_qs', ["key=\u0141%E9-", ], { encoding: "ascii" })
      self_.fcall('assertEqual', [result, Pyrb::Dict.new([{"key" => ["\u0141\ufffd-"]}])])
      result = urllib.attr('parse').fcall('parse_qs', ["key=\u0141%E9-", ], { encoding: "ascii", errors: "ignore" })
      self_.fcall('assertEqual', [result, Pyrb::Dict.new([{"key" => ["\u0141-"]}])])
    end

    klass.attrs['test_parse_qsl_encoding'] = Pyrb.defn(self: nil) do |self_|
      result = urllib.attr('parse').fcall('parse_qsl', ["key=\u0141%E9", ], { encoding: "latin-1" })
      self_.fcall('assertEqual', [result, [["key", ("\u0141" + [0xE9].pack('U*'))]]])
      result = urllib.attr('parse').fcall('parse_qsl', ["key=\u0141%C3%A9", ], { encoding: "utf-8" })
      self_.fcall('assertEqual', [result, [["key", ("\u0141" + [0xE9].pack('U*'))]]])
      result = urllib.attr('parse').fcall('parse_qsl', ["key=\u0141%C3%A9", ], { encoding: "ascii" })
      self_.fcall('assertEqual', [result, [["key", "\u0141\ufffd\ufffd"]]])
      result = urllib.attr('parse').fcall('parse_qsl', ["key=\u0141%E9-", ], { encoding: "ascii" })
      self_.fcall('assertEqual', [result, [["key", "\u0141\ufffd-"]]])
      result = urllib.attr('parse').fcall('parse_qsl', ["key=\u0141%E9-", ], { encoding: "ascii", errors: "ignore" })
      self_.fcall('assertEqual', [result, [["key", "\u0141-"]]])
    end

    klass.attrs['test_parse_qsl_max_num_fields'] = Pyrb.defn(self: nil) do |self_|
      max_num_fields = nil

      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        urllib.attr('parse').fcall('parse_qs', ["&".fcall('join', [(["a=a"] * 11)]), ], { max_num_fields: 10 })
      end
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        urllib.attr('parse').fcall('parse_qs', [";".fcall('join', [(["a=a"] * 11)]), ], { max_num_fields: 10 })
      end
      urllib.attr('parse').fcall('parse_qs', ["&".fcall('join', [(["a=a"] * 10)]), ], { max_num_fields: 10 })
    end

    klass.attrs['test_urlencode_sequences'] = Pyrb.defn(self: nil) do |self_|
      result = urllib.attr('parse').fcall('urlencode', [Pyrb::Dict.new([{"a" => [1, 2], "b" => [3, 4, 5]}]), true])
      Pyrb.assert(Pyrb::Set.call([result.fcall('split', ["&"])]) == Pyrb::Set.new([["a=1", "a=2", "b=3", "b=4", "b=5"]]))
      Trivial ||= Pyrb::PythonClass.new('Trivial', []).tap do |klass|
        klass.attrs['__str__'] = Pyrb.defn(self: nil) do |self2|
          throw(:return, "trivial")
        end
      end

      result = urllib.attr('parse').fcall('urlencode', [Pyrb::Dict.new([{"a" => Trivial.call()}]), true])
      self_.fcall('assertEqual', [result, "a=trivial"])
    end

    klass.attrs['test_urlencode_quote_via'] = Pyrb.defn(self: nil) do |self_|
      result = urllib.attr('parse').fcall('urlencode', [Pyrb::Dict.new([{"a" => "some value"}])])
      self_.fcall('assertEqual', [result, "a=some+value"])
      result = urllib.attr('parse').fcall('urlencode', [Pyrb::Dict.new([{"a" => "some value/another"}]), ], { quote_via: urllib.attr('parse').attr('quote') })
      self_.fcall('assertEqual', [result, "a=some%20value%2Fanother"])
      result = urllib.attr('parse').fcall('urlencode', [Pyrb::Dict.new([{"a" => "some value/another"}]), ], { safe: "/", quote_via: urllib.attr('parse').attr('quote') })
      self_.fcall('assertEqual', [result, "a=some%20value/another"])
    end

    klass.attrs['test_quote_from_bytes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::TypeError, urllib.attr('parse').attr('quote_from_bytes'), "foo"])
      result = urllib.attr('parse').fcall('quote_from_bytes', [Pyrb::Bytes.call(["archaeological arcana"])])
      self_.fcall('assertEqual', [result, "archaeological%20arcana"])
      result = urllib.attr('parse').fcall('quote_from_bytes', [Pyrb::Bytes.call([""])])
      self_.fcall('assertEqual', [result, ""])
    end

    klass.attrs['test_unquote_to_bytes'] = Pyrb.defn(self: nil) do |self_|
      result = urllib.attr('parse').fcall('unquote_to_bytes', ["abc%20def"])
      self_.fcall('assertEqual', [result, Pyrb::Bytes.call(["abc def"])])
      result = urllib.attr('parse').fcall('unquote_to_bytes', [""])
      self_.fcall('assertEqual', [result, Pyrb::Bytes.call([""])])
    end

    klass.attrs['test_quote_errors'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::TypeError, urllib.attr('parse').attr('quote'), Pyrb::Bytes.call(["foo"]), ], { encoding: "utf-8" })
      self_.fcall('assertRaises', [Pyrb::TypeError, urllib.attr('parse').attr('quote'), Pyrb::Bytes.call(["foo"]), ], { errors: "strict" })
    end

    klass.attrs['test_issue14072'] = Pyrb.defn(self: nil) do |self_|
      p1 = urllib.attr('parse').fcall('urlsplit', ["tel:+31-641044153"])
      self_.fcall('assertEqual', [p1.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p1.attr('path'), "+31-641044153"])
      p2 = urllib.attr('parse').fcall('urlsplit', ["tel:+31641044153"])
      self_.fcall('assertEqual', [p2.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p2.attr('path'), "+31641044153"])
      p1 = urllib.attr('parse').fcall('urlparse', ["tel:+31-641044153"])
      self_.fcall('assertEqual', [p1.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p1.attr('path'), "+31-641044153"])
      p2 = urllib.attr('parse').fcall('urlparse', ["tel:+31641044153"])
      self_.fcall('assertEqual', [p2.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p2.attr('path'), "+31641044153"])
    end

    klass.attrs['test_port_casting_failure_message'] = Pyrb.defn(self: nil) do |self_|
      message = "Port could not be cast to integer value as 'oracle'"
      p1 = urllib.attr('parse').fcall('urlparse', ["http://Server=sde; Service=sde:oracle"])
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::ValueError, message])]) do
        p1.attr('port')
      end
      p2 = urllib.attr('parse').fcall('urlsplit', ["http://Server=sde; Service=sde:oracle"])
      Pyrb.with.call([self_.fcall('assertRaisesRegex', [Pyrb::ValueError, message])]) do
        p2.attr('port')
      end
    end

    klass.attrs['test_telurl_params'] = Pyrb.defn(self: nil) do |self_|
      p1 = urllib.attr('parse').fcall('urlparse', ["tel:123-4;phone-context=+1-650-516"])
      self_.fcall('assertEqual', [p1.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p1.attr('path'), "123-4"])
      self_.fcall('assertEqual', [p1.attr('params'), "phone-context=+1-650-516"])
      p1 = urllib.attr('parse').fcall('urlparse', ["tel:+1-201-555-0123"])
      self_.fcall('assertEqual', [p1.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p1.attr('path'), "+1-201-555-0123"])
      self_.fcall('assertEqual', [p1.attr('params'), ""])
      p1 = urllib.attr('parse').fcall('urlparse', ["tel:7042;phone-context=example.com"])
      self_.fcall('assertEqual', [p1.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p1.attr('path'), "7042"])
      self_.fcall('assertEqual', [p1.attr('params'), "phone-context=example.com"])
      p1 = urllib.attr('parse').fcall('urlparse', ["tel:863-1234;phone-context=+1-914-555"])
      self_.fcall('assertEqual', [p1.attr('scheme'), "tel"])
      self_.fcall('assertEqual', [p1.attr('path'), "863-1234"])
      self_.fcall('assertEqual', [p1.attr('params'), "phone-context=+1-914-555"])
    end

    klass.attrs['test_Quoter_repr'] = Pyrb.defn(self: nil) do |self_|
      quoter = urllib.attr('parse').fcall('Quoter', [urllib.attr('parse').attr('_ALWAYS_SAFE')])
      self_.fcall('assertIn', ["Quoter", Pyrb.repr.call([quoter])])
    end

    klass.attrs['test_all'] = Pyrb.defn(self: nil) do |self_|
      object = nil

      expected = []
      undocumented = Pyrb::Set.new([["splitattr", "splithost", "splitnport", "splitpasswd", "splitport", "splitquery", "splittag", "splittype", "splituser", "splitvalue", "Quoter", "ResultBase", "clear_cache", "to_bytes", "unwrap", ]])
      (Pyrb.dir.call([urllib.attr('parse')])).each do |name|
        if !!(name.fcall('startswith', ["_"]).or { (name).in?(undocumented) })
          next
        end

        object = Pyrb.getattr.call([urllib.attr('parse'), name])
        if !!(Pyrb.getattr.call([object, "__module__", nil]) == "urllib.parse")
          expected.fcall('append', [name])
        end
      end
      self_.fcall('assertCountEqual', [urllib.attr('parse').attr('__all__'), expected])
    end
  end

  UrlParseTestCase = exports['UrlParseTestCase']

  exports['Utility_Tests'] = Pyrb::PythonClass.new('Utility_Tests', [unittest.attr('TestCase')]).tap do |klass|
    "Testcase to test the various utility functions in the urllib."
    klass.attrs['test_splittype'] = Pyrb.defn(self: nil) do |self_|
      splittype = urllib.attr('parse').attr('_splittype')
      self_.fcall('assertEqual', [splittype.call(["type:opaquestring"]), ["type", "opaquestring"]])
      self_.fcall('assertEqual', [splittype.call(["opaquestring"]), [nil, "opaquestring"]])
      self_.fcall('assertEqual', [splittype.call([":opaquestring"]), [nil, ":opaquestring"]])
      self_.fcall('assertEqual', [splittype.call(["type:"]), ["type", ""]])
      self_.fcall('assertEqual', [splittype.call(["type:opaque:string"]), ["type", "opaque:string"]])
    end

    klass.attrs['test_splithost'] = Pyrb.defn(self: nil) do |self_|
      splithost = urllib.attr('parse').attr('_splithost')
      self_.fcall('assertEqual', [splithost.call(["//www.example.org:80/foo/bar/baz.html"]), ["www.example.org:80", "/foo/bar/baz.html"]])
      self_.fcall('assertEqual', [splithost.call(["//www.example.org:80"]), ["www.example.org:80", ""]])
      self_.fcall('assertEqual', [splithost.call(["/foo/bar/baz.html"]), [nil, "/foo/bar/baz.html"]])
      self_.fcall('assertEqual', [splithost.call(["//127.0.0.1\#@host.com"]), ["127.0.0.1", "/\#@host.com"]])
      self_.fcall('assertEqual', [splithost.call(["//127.0.0.1\#@host.com:80"]), ["127.0.0.1", "/\#@host.com:80"]])
      self_.fcall('assertEqual', [splithost.call(["//127.0.0.1:80\#@host.com"]), ["127.0.0.1:80", "/\#@host.com"]])
      self_.fcall('assertEqual', [splithost.call(["///file"]), ["", "/file"]])
      self_.fcall('assertEqual', [splithost.call(["//example.net/file;"]), ["example.net", "/file;"]])
      self_.fcall('assertEqual', [splithost.call(["//example.net/file?"]), ["example.net", "/file?"]])
      self_.fcall('assertEqual', [splithost.call(["//example.net/file#"]), ["example.net", "/file#"]])
    end

    klass.attrs['test_splituser'] = Pyrb.defn(self: nil) do |self_|
      splituser = urllib.attr('parse').attr('_splituser')
      self_.fcall('assertEqual', [splituser.call(["User:Pass@www.python.org:080"]), ["User:Pass", "www.python.org:080"]])
      self_.fcall('assertEqual', [splituser.call(["@www.python.org:080"]), ["", "www.python.org:080"]])
      self_.fcall('assertEqual', [splituser.call(["www.python.org:080"]), [nil, "www.python.org:080"]])
      self_.fcall('assertEqual', [splituser.call(["User:Pass@"]), ["User:Pass", ""]])
      self_.fcall('assertEqual', [splituser.call(["User@example.com:Pass@www.python.org:080"]), ["User@example.com:Pass", "www.python.org:080"]])
    end

    klass.attrs['test_splitpasswd'] = Pyrb.defn(self: nil) do |self_|
      splitpasswd = urllib.attr('parse').attr('_splitpasswd')
      self_.fcall('assertEqual', [splitpasswd.call(["user:ab"]), ["user", "ab"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:a\nb"]), ["user", "a\nb"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:a\tb"]), ["user", "a\tb"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:a\rb"]), ["user", "a\rb"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:a\fb"]), ["user", "a\fb"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:a\vb"]), ["user", "a\vb"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:a:b"]), ["user", "a:b"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:a b"]), ["user", "a b"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user 2:ab"]), ["user 2", "ab"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user+1:a+b"]), ["user+1", "a+b"]])
      self_.fcall('assertEqual', [splitpasswd.call(["user:"]), ["user", ""]])
      self_.fcall('assertEqual', [splitpasswd.call(["user"]), ["user", nil]])
      self_.fcall('assertEqual', [splitpasswd.call([":ab"]), ["", "ab"]])
    end

    klass.attrs['test_splitport'] = Pyrb.defn(self: nil) do |self_|
      splitport = urllib.attr('parse').attr('_splitport')
      self_.fcall('assertEqual', [splitport.call(["parrot:88"]), ["parrot", "88"]])
      self_.fcall('assertEqual', [splitport.call(["parrot"]), ["parrot", nil]])
      self_.fcall('assertEqual', [splitport.call(["parrot:"]), ["parrot", nil]])
      self_.fcall('assertEqual', [splitport.call(["127.0.0.1"]), ["127.0.0.1", nil]])
      self_.fcall('assertEqual', [splitport.call(["parrot:cheese"]), ["parrot:cheese", nil]])
      self_.fcall('assertEqual', [splitport.call(["[::1]:88"]), ["[::1]", "88"]])
      self_.fcall('assertEqual', [splitport.call(["[::1]"]), ["[::1]", nil]])
      self_.fcall('assertEqual', [splitport.call([":88"]), ["", "88"]])
    end

    klass.attrs['test_splitnport'] = Pyrb.defn(self: nil) do |self_|
      splitnport = urllib.attr('parse').attr('_splitnport')
      self_.fcall('assertEqual', [splitnport.call(["parrot:88"]), ["parrot", 88]])
      self_.fcall('assertEqual', [splitnport.call(["parrot"]), ["parrot", -1]])
      self_.fcall('assertEqual', [splitnport.call(["parrot", 55]), ["parrot", 55]])
      self_.fcall('assertEqual', [splitnport.call(["parrot:"]), ["parrot", -1]])
      self_.fcall('assertEqual', [splitnport.call(["parrot:", 55]), ["parrot", 55]])
      self_.fcall('assertEqual', [splitnport.call(["127.0.0.1"]), ["127.0.0.1", -1]])
      self_.fcall('assertEqual', [splitnport.call(["127.0.0.1", 55]), ["127.0.0.1", 55]])
      self_.fcall('assertEqual', [splitnport.call(["parrot:cheese"]), ["parrot", nil]])
      self_.fcall('assertEqual', [splitnport.call(["parrot:cheese", 55]), ["parrot", nil]])
    end

    klass.attrs['test_splitquery'] = Pyrb.defn(self: nil) do |self_|
      splitquery = urllib.attr('parse').attr('_splitquery')
      self_.fcall('assertEqual', [splitquery.call(["http://python.org/fake?foo=bar"]), ["http://python.org/fake", "foo=bar"]])
      self_.fcall('assertEqual', [splitquery.call(["http://python.org/fake?foo=bar?"]), ["http://python.org/fake?foo=bar", ""]])
      self_.fcall('assertEqual', [splitquery.call(["http://python.org/fake"]), ["http://python.org/fake", nil]])
      self_.fcall('assertEqual', [splitquery.call(["?foo=bar"]), ["", "foo=bar"]])
    end

    klass.attrs['test_splittag'] = Pyrb.defn(self: nil) do |self_|
      splittag = urllib.attr('parse').attr('_splittag')
      self_.fcall('assertEqual', [splittag.call(["http://example.com?foo=bar\#baz"]), ["http://example.com?foo=bar", "baz"]])
      self_.fcall('assertEqual', [splittag.call(["http://example.com?foo=bar#"]), ["http://example.com?foo=bar", ""]])
      self_.fcall('assertEqual', [splittag.call(["\#baz"]), ["", "baz"]])
      self_.fcall('assertEqual', [splittag.call(["http://example.com?foo=bar"]), ["http://example.com?foo=bar", nil]])
      self_.fcall('assertEqual', [splittag.call(["http://example.com?foo=bar\#baz\#boo"]), ["http://example.com?foo=bar\#baz", "boo"]])
    end

    klass.attrs['test_splitattr'] = Pyrb.defn(self: nil) do |self_|
      splitattr = urllib.attr('parse').attr('_splitattr')
      self_.fcall('assertEqual', [splitattr.call(["/path;attr1=value1;attr2=value2"]), ["/path", ["attr1=value1", "attr2=value2"]]])
      self_.fcall('assertEqual', [splitattr.call(["/path;"]), ["/path", [""]]])
      self_.fcall('assertEqual', [splitattr.call([";attr1=value1;attr2=value2"]), ["", ["attr1=value1", "attr2=value2"]]])
      self_.fcall('assertEqual', [splitattr.call(["/path"]), ["/path", []]])
    end

    klass.attrs['test_splitvalue'] = Pyrb.defn(self: nil) do |self_|
      splitvalue = urllib.attr('parse').attr('_splitvalue')
      self_.fcall('assertEqual', [splitvalue.call(["foo=bar"]), ["foo", "bar"]])
      self_.fcall('assertEqual', [splitvalue.call(["foo="]), ["foo", ""]])
      self_.fcall('assertEqual', [splitvalue.call(["=bar"]), ["", "bar"]])
      self_.fcall('assertEqual', [splitvalue.call(["foobar"]), ["foobar", nil]])
      self_.fcall('assertEqual', [splitvalue.call(["foo=bar=baz"]), ["foo", "bar=baz"]])
    end

    klass.attrs['test_to_bytes'] = Pyrb.defn(self: nil) do |self_|
      result = urllib.attr('parse').fcall('_to_bytes', ["http://www.python.org"])
      self_.fcall('assertEqual', [result, "http://www.python.org"])
      self_.fcall('assertRaises', [Pyrb::UnicodeError, urllib.attr('parse').attr('_to_bytes'), "http://www.python.org/medi\u00e6val"])
    end

    klass.attrs['test_unwrap'] = Pyrb.defn(self: nil) do |self_|
      url = urllib.attr('parse').fcall('_unwrap', ["<URL:type://host/path>"])
      self_.fcall('assertEqual', [url, "type://host/path"])
    end
  end

  Utility_Tests = exports['Utility_Tests']

  exports['DeprecationTest'] = Pyrb::PythonClass.new('DeprecationTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_splittype_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splittype', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splittype() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splithost_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splithost', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splithost() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splituser_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splituser', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splituser() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splitpasswd_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splitpasswd', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splitpasswd() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splitport_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splitport', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splitport() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splitnport_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splitnport', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splitnport() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splitquery_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splitquery', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splitquery() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splittag_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splittag', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splittag() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splitattr_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splitattr', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splitattr() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead")])
    end

    klass.attrs['test_splitvalue_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('splitvalue', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), ("urllib.parse.splitvalue() is deprecated as of 3.8, " + "use urllib.parse.parse_qsl() instead")])
    end

    klass.attrs['test_to_bytes_deprecation'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('to_bytes', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), "urllib.parse.to_bytes() is deprecated as of 3.8"])
    end

    klass.attrs['test_unwrap'] = Pyrb.defn(self: nil) do |self_|
      cm = Pyrb.with.call([self_.fcall('assertWarns', [DeprecationWarning])]) do |cm|
        urllib.attr('parse').fcall('unwrap', [""])
      end
      self_.fcall('assertEqual', [Pyrb::Str.call([cm.attr('warning')]), "urllib.parse.unwrap() is deprecated as of 3.8"])
    end
  end

  DeprecationTest = exports['DeprecationTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
