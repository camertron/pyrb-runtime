# encoding: utf-8

require 'pyrb'

module Spec_test_struct
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  abc, _ = import('abc', from: 'collections')
  array, _ = import('array')
  math, _ = import('math')
  operator, _ = import('operator')
  unittest, _ = import('unittest')
  struct, _ = import('struct')
  sys, _ = import('sys')
  support, _ = import('support', from: 'test')

  exports['ISBIGENDIAN'] = sys.attr('byteorder') == "big"
  exports['integer_codes'] = "b", "B", "h", "H", "i", "I", "l", "L", "q", "Q", "n", "N"
  exports['byteorders'] = "", "@", "=", "<", ">", "!"
  exports['iter_integer_formats'] = Pyrb.defn(byteorders: { default: exports['byteorders'] }) do |byteorders|
    Pyrb::Generator.new([
      Enumerator.new do |__pyrb_gen__|
        (exports['integer_codes']).each do |code|
          (byteorders).each do |byteorder|
            if !!(((!(byteorder).in?(["", "@"])).and { (code).in?(["n", "N"]) }))
              next
            end

            __pyrb_gen__ << [code, byteorder]
          end
        end
      end
    ])
  end

  exports['string_reverse'] = Pyrb.defn(s: nil) do |s|
    throw(:return, s[Pyrb::Range.new([nil, nil, -1])])
  end

  exports['bigendian_to_native'] = Pyrb.defn(value: nil) do |value|
    if !!(exports['ISBIGENDIAN'])
      throw(:return, value)
    else
      throw(:return, exports['string_reverse'].call([value]))
    end
  end

  exports['StructTest'] = Pyrb::PythonClass.new('StructTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_isbigendian'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [(struct.fcall('pack', ["=i", 1])[0] == 0), exports['ISBIGENDIAN']])
    end

    klass.attrs['test_consistence'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('calcsize'), "Z"])
      sz = struct.fcall('calcsize', ["i"])
      self_.fcall('assertEqual', [(sz * 3), struct.fcall('calcsize', ["iii"])])
      fmt = "cbxxxxxxhhhhiillffd?"
      fmt3 = "3c3b18x12h6i6l6f3d3?"
      sz = struct.fcall('calcsize', [fmt])
      sz3 = struct.fcall('calcsize', [fmt3])
      self_.fcall('assertEqual', [(sz * 3), sz3])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), "iii", 3])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), "i", 3, 3, 3])
      self_.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), "i", "foo"])
      self_.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), "P", "foo"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack'), "d", Pyrb::Bytes.call(["flap"])])
      s = struct.fcall('pack', ["ii", 1, 2])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack'), "iii", s])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack'), "i", s])
    end

    klass.attrs['test_transitiveness'] = Pyrb.defn(self: nil) do |self_|
      s, cp, bp, hp, ip, lp, fp, dp, tp = nil

      c = Pyrb::Bytes.call(["a"])
      b = 1
      h = 255
      i = 65535
      l = 65536
      f = 3.1415
      d = 3.1415
      t = true
      (["", "@", "<", ">", "=", "!"]).each do |prefix|
        (["xcbhilfd?", "xcBHILfd?"]).each do |format_|
          format_ = prefix + format_
          s = struct.fcall('pack', [format_, c, b, h, i, l, f, d, t])
          cp, bp, hp, ip, lp, fp, dp, tp = Pyrb.deconstruct(8, struct.fcall('unpack', [format_, s]))
          self_.fcall('assertEqual', [cp, c])
          self_.fcall('assertEqual', [bp, b])
          self_.fcall('assertEqual', [hp, h])
          self_.fcall('assertEqual', [ip, i])
          self_.fcall('assertEqual', [lp, l])
          self_.fcall('assertEqual', [Pyrb::Int.call([(100 * fp)]), Pyrb::Int.call([(100 * f)])])
          self_.fcall('assertEqual', [Pyrb::Int.call([(100 * dp)]), Pyrb::Int.call([(100 * d)])])
          self_.fcall('assertEqual', [tp, t])
        end
      end
    end

    klass.attrs['test_new_features'] = Pyrb.defn(self: nil) do |self_|
      res, xfmt, exp, rev = nil

      tests = [["c", Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["a"]), 0], ["xc", Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["\0a"]), Pyrb::Bytes.call(["\0a"]), 0], ["cx", Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["a\0"]), Pyrb::Bytes.call(["a\0"]), 0], ["s", Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["a"]), Pyrb::Bytes.call(["a"]), 0], ["0s", Pyrb::Bytes.call(["helloworld"]), Pyrb::Bytes.call([""]), Pyrb::Bytes.call([""]), 1], ["1s", Pyrb::Bytes.call(["helloworld"]), Pyrb::Bytes.call(["h"]), Pyrb::Bytes.call(["h"]), 1], ["9s", Pyrb::Bytes.call(["helloworld"]), Pyrb::Bytes.call(["helloworl"]), Pyrb::Bytes.call(["helloworl"]), 1], ["10s", Pyrb::Bytes.call(["helloworld"]), Pyrb::Bytes.call(["helloworld"]), Pyrb::Bytes.call(["helloworld"]), 0], ["11s", Pyrb::Bytes.call(["helloworld"]), Pyrb::Bytes.call(["helloworld\0"]), Pyrb::Bytes.call(["helloworld\0"]), 1], ["20s", Pyrb::Bytes.call(["helloworld"]), Pyrb::Bytes.call(["helloworld"]) + (10 * Pyrb::Bytes.call(["\0"])), Pyrb::Bytes.call(["helloworld"]) + (10 * Pyrb::Bytes.call(["\0"])), 1], ["b", 7, Pyrb::Bytes.call(["\7"]), Pyrb::Bytes.call(["\7"]), 0], ["b", -7, Pyrb::Bytes.call(["\371"]), Pyrb::Bytes.call(["\371"]), 0], ["B", 7, Pyrb::Bytes.call(["\7"]), Pyrb::Bytes.call(["\7"]), 0], ["B", 249, Pyrb::Bytes.call(["\371"]), Pyrb::Bytes.call(["\371"]), 0], ["h", 700, Pyrb::Bytes.call(["\002\274"]), Pyrb::Bytes.call(["\274\002"]), 0], ["h", -700, Pyrb::Bytes.call(["\375D"]), Pyrb::Bytes.call(["D\375"]), 0], ["H", 700, Pyrb::Bytes.call(["\002\274"]), Pyrb::Bytes.call(["\274\002"]), 0], ["H", 0x10000 - 700, Pyrb::Bytes.call(["\375D"]), Pyrb::Bytes.call(["D\375"]), 0], ["i", 70000000, Pyrb::Bytes.call(["\004,\035\200"]), Pyrb::Bytes.call(["\200\035,\004"]), 0], ["i", -70000000, Pyrb::Bytes.call(["\373\323\342\200"]), Pyrb::Bytes.call(["\200\342\323\373"]), 0], ["I", 70000000, Pyrb::Bytes.call(["\004,\035\200"]), Pyrb::Bytes.call(["\200\035,\004"]), 0], ["I", 0x100000000 - 70000000, Pyrb::Bytes.call(["\373\323\342\200"]), Pyrb::Bytes.call(["\200\342\323\373"]), 0], ["l", 70000000, Pyrb::Bytes.call(["\004,\035\200"]), Pyrb::Bytes.call(["\200\035,\004"]), 0], ["l", -70000000, Pyrb::Bytes.call(["\373\323\342\200"]), Pyrb::Bytes.call(["\200\342\323\373"]), 0], ["L", 70000000, Pyrb::Bytes.call(["\004,\035\200"]), Pyrb::Bytes.call(["\200\035,\004"]), 0], ["L", 0x100000000 - 70000000, Pyrb::Bytes.call(["\373\323\342\200"]), Pyrb::Bytes.call(["\200\342\323\373"]), 0], ["f", 2.0, Pyrb::Bytes.call(["@\000\000\000"]), Pyrb::Bytes.call(["\000\000\000@"]), 0], ["d", 2.0, Pyrb::Bytes.call(["@\000\000\000\000\000\000\000"]), Pyrb::Bytes.call(["\000\000\000\000\000\000\000@"]), 0], ["f", -2.0, Pyrb::Bytes.call(["\300\000\000\000"]), Pyrb::Bytes.call(["\000\000\000\300"]), 0], ["d", -2.0, Pyrb::Bytes.call(["\300\000\000\000\000\000\000\000"]), Pyrb::Bytes.call(["\000\000\000\000\000\000\000\300"]), 0], ["?", 0, Pyrb::Bytes.call(["\0"]), Pyrb::Bytes.call(["\0"]), 0], ["?", 3, Pyrb::Bytes.call(["\1"]), Pyrb::Bytes.call(["\1"]), 1], ["?", true, Pyrb::Bytes.call(["\1"]), Pyrb::Bytes.call(["\1"]), 0], ["?", [], Pyrb::Bytes.call(["\0"]), Pyrb::Bytes.call(["\0"]), 1], ["?", [1, ], Pyrb::Bytes.call(["\1"]), Pyrb::Bytes.call(["\1"]), 1], ]
      (tests).each do |fmt, arg, big, lil, asy|
        ([[">" + fmt, big], ["!" + fmt, big], ["<" + fmt, lil], ["=" + fmt, exports['ISBIGENDIAN'].and { big }.or { lil }]]).each do |(xfmt,exp)|
          res = struct.fcall('pack', [xfmt, arg])
          self_.fcall('assertEqual', [res, exp])
          self_.fcall('assertEqual', [struct.fcall('calcsize', [xfmt]), Pyrb.len.call([res])])
          rev = struct.fcall('unpack', [xfmt, res])[0]
          if !!(rev != arg)
            self_.fcall('assertTrue', [asy])
          end
        end
      end
    end

    klass.attrs['test_calcsize'] = Pyrb.defn(self: nil) do |self_|
      format_, size, signed_size, unsigned_size = nil

      expected_size = Pyrb::Dict.new([{"b" => 1, "B" => 1, "h" => 2, "H" => 2, "i" => 4, "I" => 4, "l" => 4, "L" => 4, "q" => 8, "Q" => 8, }])
      (exports['iter_integer_formats'].call([["=", "<", ">", "!"]])).each do |code, byteorder|
        format_ = byteorder + code
        size = struct.fcall('calcsize', [format_])
        self_.fcall('assertEqual', [size, expected_size[code]])
      end
      native_pairs = "bB", "hH", "iI", "lL", "nN", "qQ"
      (native_pairs).each do |format_pair|
        ["", "@"].each do |byteorder|
          signed_size = struct.fcall('calcsize', [byteorder + format_pair[0]])
          unsigned_size = struct.fcall('calcsize', [byteorder + format_pair[1]])
          self_.fcall('assertEqual', [signed_size, unsigned_size])
        end
      end
      self_.fcall('assertEqual', [struct.fcall('calcsize', ["b"]), 1])
      self_.fcall('assertLessEqual', [2, struct.fcall('calcsize', ["h"])])
      self_.fcall('assertLessEqual', [4, struct.fcall('calcsize', ["l"])])
      self_.fcall('assertLessEqual', [struct.fcall('calcsize', ["h"]), struct.fcall('calcsize', ["i"])])
      self_.fcall('assertLessEqual', [struct.fcall('calcsize', ["i"]), struct.fcall('calcsize', ["l"])])
      self_.fcall('assertLessEqual', [8, struct.fcall('calcsize', ["q"])])
      self_.fcall('assertLessEqual', [struct.fcall('calcsize', ["l"]), struct.fcall('calcsize', ["q"])])
      self_.fcall('assertGreaterEqual', [struct.fcall('calcsize', ["n"]), struct.fcall('calcsize', ["i"])])
      self_.fcall('assertGreaterEqual', [struct.fcall('calcsize', ["n"]), struct.fcall('calcsize', ["P"])])
    end

    klass.attrs['test_integers'] = Pyrb.defn(self: nil) do |self_|
      format_, t = nil

      binascii, _ = import('binascii')
      IntTester ||= Pyrb::PythonClass.new('IntTester', [unittest.attr('TestCase')]).tap do |klass|
        klass.attrs['__init__'] = Pyrb.defn(self: nil, format: nil) do |self2, format_|
          Pyrb.zuper.call([IntTester, self2]).fcall('__init__', [], { methodName: "test_one" })
          self2.attrs['format'] = format_
          self2.attrs['code'] = format_[-1]
          self2.attrs['byteorder'] = format_[0...-1]
          if !!(((self2.attr('byteorder')).in?(exports['byteorders'])).not)
            Pyrb.raize(Pyrb::ValueError.call([("unrecognized packing byteorder: %s" % self2.attr('byteorder'))]))
          end

          self2.attrs['bytesize'] = struct.fcall('calcsize', [format_])
          self2.attrs['bitsize'] = (self2.attr('bytesize') * 8)
          if !!((self2.attr('code')).in?(Pyrb::List.call(["bhilqn"])))
            self2.attrs['signed'] = true
            self2.attrs['min_value'] = -(2 ** (self2.attr('bitsize') - 1))
            self2.attrs['max_value'] = 2 ** (self2.attr('bitsize') - 1) - 1
          elsif !!((self2.attr('code')).in?(Pyrb::List.call(["BHILQN"])))
            self2.attrs['signed'] = false
            self2.attrs['min_value'] = 0
            self2.attrs['max_value'] = 2 ** self2.attr('bitsize') - 1
          else
            Pyrb.raize(Pyrb::ValueError.call([("unrecognized format code: %s" % self2.attr('code'))]))
          end
        end

        klass.attrs['test_one'] = Pyrb.defn(self: nil, x: nil, pack: { default: struct.attr('pack') }, unpack: { default: struct.attr('unpack') }, unhexlify: { default: binascii.attr('unhexlify') }) do |self2, x, pack, unpack, unhexlify|
          format_ = self2.attr('format')
          if !!(self2.attr('min_value') <= x && x <= self2.attr('max_value'))
            expected = x
            if !!(self2.attr('signed').and { x < 0 })
              expected += 1 << self2.attr('bitsize')
            end

            self2.fcall('assertGreaterEqual', [expected, 0])
            expected = ("%x" % expected)
            if !!(Pyrb.len.call([expected]) & 1)
              expected = "0" + expected
            end

            expected = expected.fcall('encode', ["ascii"])
            expected = unhexlify.call([expected])
            expected = ((Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')]) * (self2.attr('bytesize') - Pyrb.len.call([expected]))) + expected)
            if !!(((self2.attr('byteorder') == "<").or { ((self2.attr('byteorder')).in?(["", "@", "="])).and { (exports['ISBIGENDIAN']).not } }))
              expected = exports['string_reverse'].call([expected])
            end

            self2.fcall('assertEqual', [Pyrb.len.call([expected]), self2.attr('bytesize')])
            got = pack.call([format_, x])
            self2.fcall('assertEqual', [got, expected])
            retrieved = unpack.call([format_, got])[0]
            self2.fcall('assertEqual', [x, retrieved])
            self2.fcall('assertRaises', [[struct.attr('error'), Pyrb::TypeError], unpack, format_, Pyrb::Bytes.call([String.new([0x01].pack('C*'), encoding: 'ASCII-8BIT')]) + got])
          else
            self2.fcall('assertRaises', [[Pyrb::OverflowError, Pyrb::ValueError, struct.attr('error')], pack, format_, x])
          end
        end

        klass.attrs['run'] = Pyrb.defn(self: nil) do |self2|
          val, x, format_ = nil

          randrange, _ = import('randrange', from: 'random')
          values = []
          (Pyrb.range.call([self2.attr('bitsize') + 3])).each do |exp|
            values.fcall('append', [1 << exp])
          end
          (Pyrb.range.call([self2.attr('bitsize')])).each do |i|
            val = 0
            (Pyrb.range.call([self2.attr('bytesize')])).each do |j|
              val = (val << 8) | randrange.call([256])
            end
            values.fcall('append', [val])
          end
          values.fcall('extend', [[300, 700000, (sys.attr('maxsize') * 4)]])
          (values).each do |base|
            [-base, base].each do |val2|
              [-1, 0, 1].each do |incr|
                x = val2 + incr
                self2.fcall('test_one', [x])
              end
            end
          end
          NotAnInt ||= Pyrb::PythonClass.new('NotAnInt', []).tap do |klass|
            klass.attrs['__int__'] = Pyrb.defn(self: nil) do |self3|
              throw(:return, 42)
            end
          end

          Indexable ||= Pyrb::PythonClass.new('Indexable', [Pyrb::Object]).tap do |klass|
            klass.attrs['__init__'] = Pyrb.defn(self: nil, value: nil) do |self3, value|
              self3.attrs['_value'] = value
            end

            klass.attrs['__index__'] = Pyrb.defn(self: nil) do |self3|
              throw(:return, self3.attr('_value'))
            end
          end

          BadIndex ||= Pyrb::PythonClass.new('BadIndex', [Pyrb::Object]).tap do |klass|
            klass.attrs['__index__'] = Pyrb.defn(self: nil) do |self3|
              Pyrb.raize(Pyrb::TypeError)
            end

            klass.attrs['__int__'] = Pyrb.defn(self: nil) do |self3|
              throw(:return, 42)
            end
          end

          self2.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), self2.attr('format'), "a string"])
          self2.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), self2.attr('format'), randrange])
          self2.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), self2.attr('format'), 3 + Complex(42)])
          self2.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), self2.attr('format'), NotAnInt.call()])
          self2.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), self2.attr('format'), BadIndex.call()])
          ([Indexable.call([0]), Indexable.call([10]), Indexable.call([17]), Indexable.call([42]), Indexable.call([100]), Indexable.call([127])]).each do |obj|
            begin
              struct.fcall('pack', [format_, obj])
            rescue
              self2.fcall('fail', [("integer code pack failed on object " + "with '__index__' method")])
            end
          end
          ([Indexable.call([Pyrb::Bytes.call(["a"])]), Indexable.call(["b"]), Indexable.call([nil]), Indexable.call([Pyrb::Dict.new([{"a" => 1}])]), Indexable.call([[1, 2, 3]])]).each do |obj|
            self2.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack'), self2.attr('format'), obj])
          end
        end
      end

      (exports['iter_integer_formats'].call()).each do |code, byteorder|
        format_ = byteorder + code
        t = IntTester.call([format_])
        t.fcall('run')
      end
    end

    klass.attrs['test_nN_code'] = Pyrb.defn(self: nil) do |self_|
      format_ = nil

      assertStructError = Pyrb.defn(func: nil, args: { splat: true }, kwargs: { kwsplat: true }) do |func, args, kwargs|
        cm = Pyrb.with.call([self_.fcall('assertRaises', [struct.attr('error')])]) do |cm|
          func.call([*Pyrb.splat(args)], { **(kwargs) })
        end
        self_.fcall('assertIn', ["bad char in struct format", Pyrb::Str.call([cm.attr('exception')])])
      end

      ("nN").each_char do |code|
        (["=", "<", ">", "!"]).each do |byteorder|
          format_ = byteorder + code
          assertStructError.call([struct.attr('calcsize'), format_])
          assertStructError.call([struct.attr('pack'), format_, 0])
          assertStructError.call([struct.attr('unpack'), format_, Pyrb::Bytes.call([""])])
        end
      end
    end

    klass.attrs['test_p_code'] = Pyrb.defn(self: nil) do |self_|
      got = nil

      ([["p", Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([""])], ["1p", Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call([String.new([0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([""])], ["2p", Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call([String.new([0x01].pack('C*') + "a", encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["a"])], ["3p", Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call([String.new([0x02].pack('C*') + "ab", encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["ab"])], ["4p", Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call([String.new([0x03].pack('C*') + "abc", encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["abc"])], ["5p", Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call([String.new([0x03].pack('C*') + "abc" + [0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["abc"])], ["6p", Pyrb::Bytes.call(["abc"]), Pyrb::Bytes.call([String.new([0x03].pack('C*') + "abc" + [0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call(["abc"])], ["1000p", (Pyrb::Bytes.call(["x"]) * 1000), Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]) + (Pyrb::Bytes.call(["x"]) * 999), (Pyrb::Bytes.call(["x"]) * 255)]]).each do |code, input, expected, expectedback|
        got = struct.fcall('pack', [code, input])
        self_.fcall('assertEqual', [got, expected])
        got,  = struct.fcall('unpack', [code, got])
        self_.fcall('assertEqual', [got, expectedback])
      end
    end

    klass.attrs['test_705836'] = Pyrb.defn(self: nil) do |self_|
      delta, smaller, packed, unpacked, bigpacked = nil

      (Pyrb.range.call([1, 33])).each do |base|
        delta = 0.5
        while !!(base - (delta / 2.0) != base)
          delta /= 2.0
        end

        smaller = base - delta
        packed = struct.fcall('pack', ["<f", smaller])
        unpacked = struct.fcall('unpack', ["<f", packed])[0]
        self_.fcall('assertEqual', [base, unpacked])
        bigpacked = struct.fcall('pack', [">f", smaller])
        self_.fcall('assertEqual', [bigpacked, exports['string_reverse'].call([packed])])
        unpacked = struct.fcall('unpack', [">f", bigpacked])[0]
        self_.fcall('assertEqual', [base, unpacked])
      end
      big = (1 << 24) - 1
      big = math.fcall('ldexp', [big, 127 - 23])
      packed = struct.fcall('pack', [">f", big])
      unpacked = struct.fcall('unpack', [">f", packed])[0]
      self_.fcall('assertEqual', [big, unpacked])
      big = (1 << 25) - 1
      big = math.fcall('ldexp', [big, 127 - 24])
      self_.fcall('assertRaises', [Pyrb::OverflowError, struct.attr('pack'), ">f", big])
    end

    klass.attrs['test_1530559'] = Pyrb.defn(self: nil) do |self_|
      format_ = nil

      (exports['iter_integer_formats'].call()).each do |code, byteorder|
        format_ = byteorder + code
        self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), format_, 1.0])
        self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), format_, 1.5])
      end
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), "P", 1.0])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), "P", 1.5])
    end

    klass.attrs['test_unpack_from'] = Pyrb.defn(self: nil) do |self_|
      data = nil

      test_string = Pyrb::Bytes.call(["abcd01234"])
      fmt = "4s"
      s = struct.fcall('Struct', [fmt])
      ([Pyrb::Bytes, Pyrb::Bytearray]).each do |cls|
        data = cls.call([test_string])
        self_.fcall('assertEqual', [s.fcall('unpack_from', [data]), [Pyrb::Bytes.call(["abcd"]), ]])
        self_.fcall('assertEqual', [s.fcall('unpack_from', [data, 2]), [Pyrb::Bytes.call(["cd01"]), ]])
        self_.fcall('assertEqual', [s.fcall('unpack_from', [data, 4]), [Pyrb::Bytes.call(["0123"]), ]])
        (Pyrb.range.call([6])).each do |i|
          self_.fcall('assertEqual', [s.fcall('unpack_from', [data, i]), [data[i...i + 4], ]])
        end
        (Pyrb.range.call([6, Pyrb.len.call([test_string]) + 1])).each do |i|
          self_.fcall('assertRaises', [struct.attr('error'), s.attr('unpack_from'), data, i])
        end
      end
      ([Pyrb::Bytes, Pyrb::Bytearray]).each do |cls|
        data = cls.call([test_string])
        self_.fcall('assertEqual', [struct.fcall('unpack_from', [fmt, data]), [Pyrb::Bytes.call(["abcd"]), ]])
        self_.fcall('assertEqual', [struct.fcall('unpack_from', [fmt, data, 2]), [Pyrb::Bytes.call(["cd01"]), ]])
        self_.fcall('assertEqual', [struct.fcall('unpack_from', [fmt, data, 4]), [Pyrb::Bytes.call(["0123"]), ]])
        (Pyrb.range.call([6])).each do |i|
          self_.fcall('assertEqual', [struct.fcall('unpack_from', [fmt, data, i]), [data[i...i + 4], ]])
        end
        (Pyrb.range.call([6, Pyrb.len.call([test_string]) + 1])).each do |i|
          self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack_from'), fmt, data, i])
        end
      end
      self_.fcall('assertEqual', [s.fcall('unpack_from', [], { buffer: test_string, offset: 2 }), [Pyrb::Bytes.call(["cd01"]), ]])
    end

    klass.attrs['test_pack_into'] = Pyrb.defn(self: nil) do |self_|
      test_string = Pyrb::Bytes.call(["Reykjavik rocks, eow!"])
      writable_buf = array.fcall('array', ["b", (Pyrb::Bytes.call([" "]) * 100)])
      fmt = "21s"
      s = struct.fcall('Struct', [fmt])
      s.fcall('pack_into', [writable_buf, 0, test_string])
      from_buf = writable_buf.fcall('tobytes')[0...Pyrb.len.call([test_string])]
      self_.fcall('assertEqual', [from_buf, test_string])
      s.fcall('pack_into', [writable_buf, 10, test_string])
      from_buf = writable_buf.fcall('tobytes')[0...Pyrb.len.call([test_string]) + 10]
      self_.fcall('assertEqual', [from_buf, test_string[0...10] + test_string])
      small_buf = array.fcall('array', ["b", (Pyrb::Bytes.call([" "]) * 10)])
      self_.fcall('assertRaises', [[Pyrb::ValueError, struct.attr('error')], s.attr('pack_into'), small_buf, 0, test_string])
      self_.fcall('assertRaises', [[Pyrb::ValueError, struct.attr('error')], s.attr('pack_into'), small_buf, 2, test_string])
      sb = small_buf
      self_.fcall('assertRaises', [[Pyrb::TypeError, struct.attr('error')], struct.attr('pack_into'), Pyrb::Bytes.call([""]), sb, nil])
    end

    klass.attrs['test_pack_into_fn'] = Pyrb.defn(self: nil) do |self_|
      test_string = Pyrb::Bytes.call(["Reykjavik rocks, eow!"])
      writable_buf = array.fcall('array', ["b", (Pyrb::Bytes.call([" "]) * 100)])
      fmt = "21s"
      pack_into = Pyrb.defn(args: { splat: true }) { |args| struct.fcall('pack_into', [fmt, *Pyrb.splat(args)]) }
      pack_into.call([writable_buf, 0, test_string])
      from_buf = writable_buf.fcall('tobytes')[0...Pyrb.len.call([test_string])]
      self_.fcall('assertEqual', [from_buf, test_string])
      pack_into.call([writable_buf, 10, test_string])
      from_buf = writable_buf.fcall('tobytes')[0...Pyrb.len.call([test_string]) + 10]
      self_.fcall('assertEqual', [from_buf, test_string[0...10] + test_string])
      small_buf = array.fcall('array', ["b", (Pyrb::Bytes.call([" "]) * 10)])
      self_.fcall('assertRaises', [[Pyrb::ValueError, struct.attr('error')], pack_into, small_buf, 0, test_string])
      self_.fcall('assertRaises', [[Pyrb::ValueError, struct.attr('error')], pack_into, small_buf, 2, test_string])
    end

    klass.attrs['test_unpack_with_buffer'] = Pyrb.defn(self: nil) do |self_|
      value = nil

      data1 = array.fcall('array', ["B", Pyrb::Bytes.call([String.new([0x12, 0x34, 0x56, 0x78].pack('C*'), encoding: 'ASCII-8BIT')])])
      data2 = Pyrb::Memoryview.call([Pyrb::Bytes.call([String.new([0x12, 0x34, 0x56, 0x78].pack('C*'), encoding: 'ASCII-8BIT')])])
      ([data1, data2]).each do |data|
        value,  = struct.fcall('unpack', [">I", data])
        self_.fcall('assertEqual', [value, 0x12345678])
      end
    end

    klass.attrs['test_bool'] = Pyrb.defn(self: nil) do |self_|
      false_, true_, falseFormat, packedFalse, unpackedFalse, trueFormat, packedTrue, unpackedTrue, packed, msg = nil

      ExplodingBool ||= Pyrb::PythonClass.new('ExplodingBool', [Pyrb::Object]).tap do |klass|
        klass.attrs['__bool__'] = Pyrb.defn(self: nil) do |self2|
          Pyrb.raize(Pyrb::OSError)
        end
      end

      (Pyrb::List.call(["<>!="]) + ["", ]).each do |prefix|
        false_ = (), [], [], "", 0
        true_ = [1], "test", 5, -1, 0xffffffff + 1, (0xffffffff / 2)
        falseFormat = prefix + ("?" * Pyrb.len.call([false_]))
        packedFalse = struct.fcall('pack', [falseFormat, *Pyrb.splat(false_)])
        unpackedFalse = struct.fcall('unpack', [falseFormat, packedFalse])
        trueFormat = prefix + ("?" * Pyrb.len.call([true_]))
        packedTrue = struct.fcall('pack', [trueFormat, *Pyrb.splat(true_)])
        unpackedTrue = struct.fcall('unpack', [trueFormat, packedTrue])
        self_.fcall('assertEqual', [Pyrb.len.call([true_]), Pyrb.len.call([unpackedTrue])])
        self_.fcall('assertEqual', [Pyrb.len.call([false_]), Pyrb.len.call([unpackedFalse])])
        (unpackedFalse).each do |t|
          self_.fcall('assertFalse', [t])
        end
        (unpackedTrue).each do |t|
          self_.fcall('assertTrue', [t])
        end
        packed = struct.fcall('pack', [prefix + "?", 1])
        self_.fcall('assertEqual', [Pyrb.len.call([packed]), struct.fcall('calcsize', [prefix + "?"])])
        if !!(Pyrb.len.call([packed]) != 1)
          self_.fcall('assertFalse', [prefix, ], { msg: ("encoded bool is not one byte: %r" % packed) })
        end

        begin
          struct.fcall('pack', [prefix + "?", ExplodingBool.call()])
        rescue *Pyrb.rezcue(Pyrb::OSError)
        else
          self_.fcall('fail', [(("Expected OSError: struct.pack(%r, " + "ExplodingBool())") % (prefix + "?"))])
        end
      end
      ([Pyrb::Bytes.call([String.new([0x01].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([String.new([0x7f].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([String.new([0xff].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([String.new([0x0f].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Bytes.call([String.new([0xf0].pack('C*'), encoding: 'ASCII-8BIT')])]).each do |c|
        self_.fcall('assertTrue', [struct.fcall('unpack', [">?", c])[0]])
      end
    end

    klass.attrs['test_count_overflow'] = Pyrb.defn(self: nil) do |self_|
      hugecount = "{}b".fcall('format', [sys.attr('maxsize') + 1])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('calcsize'), hugecount])
      hugecount2 = "{}b{}H".fcall('format', [Pyrb.int_divide(sys.attr('maxsize'), 2), Pyrb.int_divide(sys.attr('maxsize'), 2)])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('calcsize'), hugecount2])
    end

    klass.attrs['test_trailing_counter'] = Pyrb.defn(self: nil) do |self_|
      store = array.fcall('array', ["b", (Pyrb::Bytes.call([" "]) * 100)])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), "12345"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack'), "12345", ""])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack_into'), "12345", store, 0])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack_from'), "12345", store, 0])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), "c12345", "x"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack'), "c12345", "x"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack_into'), "c12345", store, 0, "x"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack_from'), "c12345", store, 0])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack'), "14s42", "spam and eggs"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack'), "14s42", "spam and eggs"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('pack_into'), "14s42", store, 0, "spam and eggs"])
      self_.fcall('assertRaises', [struct.attr('error'), struct.attr('unpack_from'), "14s42", store, 0])
    end

    klass.attrs['test_Struct_reinitialization'] = Pyrb.defn(self: nil) do |self_|
      s = struct.fcall('Struct', ["i"])
      s.fcall('__init__', ["ii"])
    end

    klass.attrs['check_sizeof'] = Pyrb.defn(self: nil, format_str: nil, number_of_codes: nil) do |self_, format_str, number_of_codes|
      totalsize = support.fcall('calcobjsize', ["2n3P"])
      totalsize += (struct.fcall('calcsize', ["P3n0P"]) * (number_of_codes + 1))
      support.fcall('check_sizeof', [self_, struct.fcall('Struct', [format_str]), totalsize])
    end
  end

  StructTest = exports['StructTest']

  exports['UnpackIteratorTest'] = Pyrb::PythonClass.new('UnpackIteratorTest', [unittest.attr('TestCase')]).tap do |klass|
    <<~__DOC__
    Tests for iterative unpacking (struct.Struct.iter_unpack).
    __DOC__

    klass.attrs['test_construct'] = Pyrb.defn(self: nil) do |self_|
      _check_iterator = Pyrb.defn(it: nil) do |it|
        self_.fcall('assertIsInstance', [it, abc.attr('Iterator')])
        self_.fcall('assertIsInstance', [it, abc.attr('Iterable')])
      end

      s = struct.fcall('Struct', [">ibcp"])
      it = s.fcall('iter_unpack', [Pyrb::Bytes.call([""])])
      _check_iterator.call([it])
      it = s.fcall('iter_unpack', [Pyrb::Bytes.call(["1234567"])])
      _check_iterator.call([it])
      Pyrb.with.call([self_.fcall('assertRaises', [struct.attr('error')])]) do
        s.fcall('iter_unpack', [Pyrb::Bytes.call(["123456"])])
      end
      Pyrb.with.call([self_.fcall('assertRaises', [struct.attr('error')])]) do
        s.fcall('iter_unpack', [Pyrb::Bytes.call(["12345678"])])
      end
      s = struct.fcall('Struct', [">"])
      Pyrb.with.call([self_.fcall('assertRaises', [struct.attr('error')])]) do
        s.fcall('iter_unpack', [Pyrb::Bytes.call([""])])
      end
      Pyrb.with.call([self_.fcall('assertRaises', [struct.attr('error')])]) do
        s.fcall('iter_unpack', [Pyrb::Bytes.call(["12"])])
      end
    end

    klass.attrs['test_iterate'] = Pyrb.defn(self: nil) do |self_|
      s = struct.fcall('Struct', [">IB"])
      b = Pyrb::Bytes.call([Pyrb.range.call([1, 16])])
      it = s.fcall('iter_unpack', [b])
      self_.fcall('assertEqual', [Pyrb.next_.call([it]), [0x01020304, 5]])
      self_.fcall('assertEqual', [Pyrb.next_.call([it]), [0x06070809, 10]])
      self_.fcall('assertEqual', [Pyrb.next_.call([it]), [0x0b0c0d0e, 15]])
      self_.fcall('assertRaises', [::StopIteration, Pyrb.next_, it])
      self_.fcall('assertRaises', [::StopIteration, Pyrb.next_, it])
    end

    klass.attrs['test_arbitrary_buffer'] = Pyrb.defn(self: nil) do |self_|
      s = struct.fcall('Struct', [">IB"])
      b = Pyrb::Bytes.call([Pyrb.range.call([1, 11])])
      it = s.fcall('iter_unpack', [Pyrb::Memoryview.call([b])])
      self_.fcall('assertEqual', [Pyrb.next_.call([it]), [0x01020304, 5]])
      self_.fcall('assertEqual', [Pyrb.next_.call([it]), [0x06070809, 10]])
      self_.fcall('assertRaises', [::StopIteration, Pyrb.next_, it])
      self_.fcall('assertRaises', [::StopIteration, Pyrb.next_, it])
    end

    klass.attrs['test_length_hint'] = Pyrb.defn(self: nil) do |self_|
      lh = operator.attr('length_hint')
      s = struct.fcall('Struct', [">IB"])
      b = Pyrb::Bytes.call([Pyrb.range.call([1, 16])])
      it = s.fcall('iter_unpack', [b])
      self_.fcall('assertEqual', [lh.call([it]), 3])
      Pyrb.next_.call([it])
      self_.fcall('assertEqual', [lh.call([it]), 2])
      Pyrb.next_.call([it])
      self_.fcall('assertEqual', [lh.call([it]), 1])
      Pyrb.next_.call([it])
      self_.fcall('assertEqual', [lh.call([it]), 0])
      self_.fcall('assertRaises', [::StopIteration, Pyrb.next_, it])
      self_.fcall('assertEqual', [lh.call([it]), 0])
    end

    klass.attrs['test_module_func'] = Pyrb.defn(self: nil) do |self_|
      it = struct.fcall('iter_unpack', [">IB", Pyrb::Bytes.call([Pyrb.range.call([1, 11])])])
      self_.fcall('assertEqual', [Pyrb.next_.call([it]), [0x01020304, 5]])
      self_.fcall('assertEqual', [Pyrb.next_.call([it]), [0x06070809, 10]])
      self_.fcall('assertRaises', [::StopIteration, Pyrb.next_, it])
      self_.fcall('assertRaises', [::StopIteration, Pyrb.next_, it])
    end

    klass.attrs['test_half_float'] = Pyrb.defn(self: nil) do |self_|
      be_bits = nil

      format_bits_float__cleanRoundtrip_list = [[Pyrb::Bytes.call([String.new([0x00, 0x3c].pack('C*'), encoding: 'ASCII-8BIT')]), 1.0], [Pyrb::Bytes.call([String.new([0x00, 0xc0].pack('C*'), encoding: 'ASCII-8BIT')]), -2.0], [Pyrb::Bytes.call([String.new([0xff, 0x7b].pack('C*'), encoding: 'ASCII-8BIT')]), 65504.0], [Pyrb::Bytes.call([String.new([0x00, 0x04].pack('C*'), encoding: 'ASCII-8BIT')]), 2 ** -14], [Pyrb::Bytes.call([String.new([0x01, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 2 ** -24], [Pyrb::Bytes.call([String.new([0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 0.0], [Pyrb::Bytes.call([String.new([0x00, 0x80].pack('C*'), encoding: 'ASCII-8BIT')]), -0.0], [Pyrb::Bytes.call([String.new([0x00, 0x7c].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Float.call(["+inf"])], [Pyrb::Bytes.call([String.new([0x00, 0xfc].pack('C*'), encoding: 'ASCII-8BIT')]), Pyrb::Float.call(["-inf"])], [Pyrb::Bytes.call([String.new([0x55, 0x35].pack('C*'), encoding: 'ASCII-8BIT')]), 0.333251953125], ]
      (format_bits_float__cleanRoundtrip_list).each do |le_bits, f|
        be_bits = le_bits[Pyrb::Range.new([nil, nil, -1])]
        self_.fcall('assertEqual', [f, struct.fcall('unpack', ["<e", le_bits])[0]])
        self_.fcall('assertEqual', [le_bits, struct.fcall('pack', ["<e", f])])
        self_.fcall('assertEqual', [f, struct.fcall('unpack', [">e", be_bits])[0]])
        self_.fcall('assertEqual', [be_bits, struct.fcall('pack', [">e", f])])
        if !!(sys.attr('byteorder') == "little")
          self_.fcall('assertEqual', [f, struct.fcall('unpack', ["e", le_bits])[0]])
          self_.fcall('assertEqual', [le_bits, struct.fcall('pack', ["e", f])])
        else
          self_.fcall('assertEqual', [f, struct.fcall('unpack', ["e", be_bits])[0]])
          self_.fcall('assertEqual', [be_bits, struct.fcall('pack', ["e", f])])
        end
      end
      format_bits__nan_list = [["<e", Pyrb::Bytes.call([String.new([0x01, 0xfc].pack('C*'), encoding: 'ASCII-8BIT')])], ["<e", Pyrb::Bytes.call([String.new([0x00, 0xfe].pack('C*'), encoding: 'ASCII-8BIT')])], ["<e", Pyrb::Bytes.call([String.new([0xff, 0xff].pack('C*'), encoding: 'ASCII-8BIT')])], ["<e", Pyrb::Bytes.call([String.new([0x01, 0x7c].pack('C*'), encoding: 'ASCII-8BIT')])], ["<e", Pyrb::Bytes.call([String.new([0x00, 0x7e].pack('C*'), encoding: 'ASCII-8BIT')])], ["<e", Pyrb::Bytes.call([String.new([0xff, 0x7f].pack('C*'), encoding: 'ASCII-8BIT')])], ]
      (format_bits__nan_list).each do |formatcode, bits|
        self_.fcall('assertTrue', [math.fcall('isnan', [struct.fcall('unpack', ["<e", bits])[0]])])
        self_.fcall('assertTrue', [math.fcall('isnan', [struct.fcall('unpack', [">e", bits[Pyrb::Range.new([nil, nil, -1])]])[0]])])
      end
      packed = struct.fcall('pack', ["<e", math.attr('nan')])
      self_.fcall('assertEqual', [packed[1] & 0x7e, 0x7e])
      packed = struct.fcall('pack', ["<e", -math.attr('nan')])
      self_.fcall('assertEqual', [packed[1] & 0x7e, 0x7e])
      format_bits_float__rounding_list = [[">e", Pyrb::Bytes.call([String.new([0x00, 0x01].pack('C*'), encoding: 'ASCII-8BIT')]), 2.0 ** -25 + 2.0 ** -35], [">e", Pyrb::Bytes.call([String.new([0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 2.0 ** -25], [">e", Pyrb::Bytes.call([String.new([0x00, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 2.0 ** -26], [">e", Pyrb::Bytes.call([String.new([0x03, 0xff].pack('C*'), encoding: 'ASCII-8BIT')]), 2.0 ** -14 - 2.0 ** -24], [">e", Pyrb::Bytes.call([String.new([0x03, 0xff].pack('C*'), encoding: 'ASCII-8BIT')]), 2.0 ** -14 - 2.0 ** -25 - 2.0 ** -65], [">e", Pyrb::Bytes.call([String.new([0x04, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 2.0 ** -14 - 2.0 ** -25], [">e", Pyrb::Bytes.call([String.new([0x04, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 2.0 ** -14], [">e", Pyrb::Bytes.call([String.new([0x3c, 0x01].pack('C*'), encoding: 'ASCII-8BIT')]), 1.0 + 2.0 ** -11 + 2.0 ** -16], [">e", Pyrb::Bytes.call([String.new([0x3c, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 1.0 + 2.0 ** -11], [">e", Pyrb::Bytes.call([String.new([0x3c, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), 1.0 + 2.0 ** -12], [">e", Pyrb::Bytes.call([String.new([0x7b, 0xff].pack('C*'), encoding: 'ASCII-8BIT')]), 65504], [">e", Pyrb::Bytes.call([String.new([0x7b, 0xff].pack('C*'), encoding: 'ASCII-8BIT')]), 65519], [">e", Pyrb::Bytes.call([String.new([0x80, 0x01].pack('C*'), encoding: 'ASCII-8BIT')]), -2.0 ** -25 - 2.0 ** -35], [">e", Pyrb::Bytes.call([String.new([0x80, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), -2.0 ** -25], [">e", Pyrb::Bytes.call([String.new([0x80, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), -2.0 ** -26], [">e", Pyrb::Bytes.call([String.new([0xbc, 0x01].pack('C*'), encoding: 'ASCII-8BIT')]), -1.0 - 2.0 ** -11 - 2.0 ** -16], [">e", Pyrb::Bytes.call([String.new([0xbc, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), -1.0 - 2.0 ** -11], [">e", Pyrb::Bytes.call([String.new([0xbc, 0x00].pack('C*'), encoding: 'ASCII-8BIT')]), -1.0 - 2.0 ** -12], [">e", Pyrb::Bytes.call([String.new([0xfb, 0xff].pack('C*'), encoding: 'ASCII-8BIT')]), -65519], ]
      (format_bits_float__rounding_list).each do |formatcode, bits, f|
        self_.fcall('assertEqual', [bits, struct.fcall('pack', [formatcode, f])])
      end
      format_bits_float__roundingError_list = [[">e", 65520.0], [">e", 65536.0], [">e", 1e300], [">e", -65520.0], [">e", -65536.0], [">e", -1e300], ["<e", 65520.0], ["<e", 65536.0], ["<e", 1e300], ["<e", -65520.0], ["<e", -65536.0], ["<e", -1e300], ]
      (format_bits_float__roundingError_list).each do |formatcode, f|
        self_.fcall('assertRaises', [Pyrb::OverflowError, struct.attr('pack'), formatcode, f])
      end
      format_bits_float__doubleRoundingError_list = [[">e", Pyrb::Bytes.call([String.new([0x67, 0xff].pack('C*'), encoding: 'ASCII-8BIT')]), (0x1ffdffffff * 2 ** -26)], ]
      (format_bits_float__doubleRoundingError_list).each do |formatcode, bits, f|
        self_.fcall('assertEqual', [bits, struct.fcall('pack', [formatcode, f])])
      end
    end
  end

  UnpackIteratorTest = exports['UnpackIteratorTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
