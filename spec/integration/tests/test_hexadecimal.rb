# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_hexadecimal
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  hexa, _ = import('hexa', from: 'integration.source.hexadecimal')

  exports['HexadecimalTest'] = Pyrb::PythonClass.new('HexadecimalTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_valid_hexa1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, hexa.call(["1"])])
    end

    klass.attrs['test_valid_hexa2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [12, hexa.call(["c"])])
    end

    klass.attrs['test_valid_hexa3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [16, hexa.call(["10"])])
    end

    klass.attrs['test_valid_hexa4'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [175, hexa.call(["af"])])
    end

    klass.attrs['test_valid_hexa5'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [256, hexa.call(["100"])])
    end

    klass.attrs['test_valid_hexa6'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [105166, hexa.call(["19ACE"])])
    end

    klass.attrs['test_valid_hexa7'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [0, hexa.call(["000000"])])
    end

    klass.attrs['test_valid_hexa8'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [16776960, hexa.call(["ffff00"])])
    end

    klass.attrs['test_valid_hexa9'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [65520, hexa.call(["00fff0"])])
    end

    klass.attrs['test_invalid_hexa'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        hexa.call(["carrot"])
      end
    end
  end

  HexadecimalTest = exports['HexadecimalTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
