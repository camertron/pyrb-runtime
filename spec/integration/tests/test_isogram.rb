# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_isogram
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  is_isogram, _ = import('is_isogram', from: 'integration.source.isogram')

  exports['TestIsogram'] = Pyrb::PythonClass.new('TestIsogram', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_empty_string'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call([""]), true])
    end

    klass.attrs['test_isogram_with_only_lower_case_characters'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["isogram"]), true])
    end

    klass.attrs['test_word_with_one_duplicated_character'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["eleven"]), false])
    end

    klass.attrs['test_longest_reported_english_isogram'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["subdermatoglyphic"]), true])
    end

    klass.attrs['test_word_with_duplicated_character_in_mixed_case'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["Alphabet"]), false])
    end

    klass.attrs['test_hypothetical_isogrammic_word_with_hyphen'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["thumbscrew-japingly"]), true])
    end

    klass.attrs['test_isogram_with_duplicated_hyphen'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["six-year-old"]), true])
    end

    klass.attrs['test_made_up_name_that_is_an_isogram'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["Emily Jung Schwartzkopf"]), true])
    end

    klass.attrs['test_duplicated_character_in_the_middle'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["accentor"]), false])
    end

    klass.attrs['test_isogram_with_duplicated_letter_and_nonletter_character'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_isogram.call(["Aleph Bot Chap"]), false])
    end
  end

  TestIsogram = exports['TestIsogram']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
