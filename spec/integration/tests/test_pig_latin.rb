# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_pig_latin
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  translate, _ = import('translate', from: 'integration.source.pig_latin')

  exports['PigLatinTests'] = Pyrb::PythonClass.new('PigLatinTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_word_beginning_with_a'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["appleay", translate.call(["apple"])])
    end

    klass.attrs['test_word_beginning_with_e'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["earay", translate.call(["ear"])])
    end

    klass.attrs['test_word_beginning_with_p'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["igpay", translate.call(["pig"])])
    end

    klass.attrs['test_word_beginning_with_k'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["oalakay", translate.call(["koala"])])
    end

    klass.attrs['test_word_beginning_with_ch'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["airchay", translate.call(["chair"])])
    end

    klass.attrs['test_word_beginning_with_qu'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["eenquay", translate.call(["queen"])])
    end

    klass.attrs['test_word_beginning_with_squ'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["aresquay", translate.call(["square"])])
    end

    klass.attrs['test_word_beginning_with_th'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["erapythay", translate.call(["therapy"])])
    end

    klass.attrs['test_word_beginning_with_thr'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["ushthray", translate.call(["thrush"])])
    end

    klass.attrs['test_word_beginning_with_sch'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["oolschay", translate.call(["school"])])
    end

    klass.attrs['test_translates_phrase'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["ickquay astfay unray", translate.call(["quick fast run"])])
    end

    klass.attrs['test_word_beginning_with_ye'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["ellowyay", translate.call(["yellow"])])
    end

    klass.attrs['test_word_beginning_with_yt'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["yttriaay", translate.call(["yttria"])])
    end

    klass.attrs['test_word_beginning_with_xe'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["enonxay", translate.call(["xenon"])])
    end

    klass.attrs['test_word_beginning_with_xr'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["xrayay", translate.call(["xray"])])
    end
  end

  PigLatinTests = exports['PigLatinTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
