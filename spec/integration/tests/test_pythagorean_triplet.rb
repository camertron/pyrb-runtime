# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_pythagorean_triplet
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  primitive_triplets, triplets_in_range, is_triplet = import('primitive_triplets', 'triplets_in_range', 'is_triplet', from: 'integration.source.pythagorean_triplet')

  exports['PythagoreanTripletTest'] = Pyrb::PythonClass.new('PythagoreanTripletTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_triplet1'] = Pyrb.defn(self: nil) do |self_|
      ans = Pyrb::Set.call([[[3, 4, 5]]])
      self_.fcall('assertEqual', [ans, primitive_triplets.call([4])])
    end

    klass.attrs['test_triplet2'] = Pyrb.defn(self: nil) do |self_|
      ans = Pyrb::Set.call([[[13, 84, 85], [84, 187, 205], [84, 437, 445], [84, 1763, 1765]]])
      self_.fcall('assertEqual', [ans, primitive_triplets.call([84])])
    end

    klass.attrs['test_triplet3'] = Pyrb.defn(self: nil) do |self_|
      ans = Pyrb::Set.call([[[29, 420, 421], [341, 420, 541], [420, 851, 949], [420, 1189, 1261], [420, 1739, 1789], [420, 4891, 4909], [420, 11021, 11029], [420, 44099, 44101]]])
      self_.fcall('assertEqual', [ans, primitive_triplets.call([420])])
    end

    klass.attrs['test_triplet4'] = Pyrb.defn(self: nil) do |self_|
      ans = Pyrb::Set.call([[[175, 288, 337], [288, 20735, 20737]]])
      self_.fcall('assertEqual', [ans, primitive_triplets.call([288])])
    end

    klass.attrs['test_range1'] = Pyrb.defn(self: nil) do |self_|
      ans = Pyrb::Set.call([[[3, 4, 5], [6, 8, 10]]])
      self_.fcall('assertEqual', [ans, triplets_in_range.call([1, 10])])
    end

    klass.attrs['test_range2'] = Pyrb.defn(self: nil) do |self_|
      ans = Pyrb::Set.call([[[57, 76, 95], [60, 63, 87]]])
      self_.fcall('assertEqual', [ans, triplets_in_range.call([56, 95])])
    end

    klass.attrs['test_is_triplet1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, is_triplet.call([[29, 20, 21]])])
    end

    klass.attrs['test_is_triplet2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [false, is_triplet.call([[25, 25, 1225]])])
    end

    klass.attrs['test_is_triplet3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, is_triplet.call([[924, 43, 925]])])
    end

    klass.attrs['test_odd_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, primitive_triplets, 5])
    end
  end

  PythagoreanTripletTest = exports['PythagoreanTripletTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
