# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_grains
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  on_square, total_after = import('on_square', 'total_after', from: 'integration.source.grains')

  exports['GrainsTest'] = Pyrb::PythonClass.new('GrainsTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_square_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, on_square.call([1])])
      self_.fcall('assertEqual', [1, total_after.call([1])])
    end

    klass.attrs['test_square_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [2, on_square.call([2])])
      self_.fcall('assertEqual', [3, total_after.call([2])])
    end

    klass.attrs['test_square_3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [4, on_square.call([3])])
      self_.fcall('assertEqual', [7, total_after.call([3])])
    end

    klass.attrs['test_square_4'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [8, on_square.call([4])])
      self_.fcall('assertEqual', [15, total_after.call([4])])
    end

    klass.attrs['test_square_16'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [32768, on_square.call([16])])
      self_.fcall('assertEqual', [65535, total_after.call([16])])
    end

    klass.attrs['test_square_32'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [2147483648, on_square.call([32])])
      self_.fcall('assertEqual', [4294967295, total_after.call([32])])
    end

    klass.attrs['test_square_64'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [9223372036854775808, on_square.call([64])])
      self_.fcall('assertEqual', [18446744073709551615, total_after.call([64])])
    end
  end

  GrainsTest = exports['GrainsTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
