# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_palindrome_products
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  smallest_palindrome, largest_palindrome = import('smallest_palindrome', 'largest_palindrome', from: 'integration.source.palindrome')

  <<~__DOC__
  Notes regarding the implementation of smallest_palindrome and
  largest_palindrome:

  Both functions must take two keyword arguments:
      max_factor -- int
      min_factor -- int, default 0

  Their return value must be a tuple (value, factors) where value is the
  palindrome itself, and factors is an iterable containing both factors of the
  palindrome in arbitrary order.
  __DOC__

  exports['PalindromesTests'] = Pyrb::PythonClass.new('PalindromesTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_largest_palindrome_from_single_digit_factors'] = Pyrb.defn(self: nil) do |self_|
      value, factors = Pyrb.deconstruct(2, largest_palindrome.call([], { max_factor: 9 }))
      self_.fcall('assertEqual', [9, value])
      self_.fcall('assertIn', [Pyrb::Set.call([factors]), [Pyrb::Set.new([[1, 9]]), Pyrb::Set.new([[3, 3]])]])
    end

    klass.attrs['test_smallest_palindrome_from_double_digit_factors'] = Pyrb.defn(self: nil) do |self_|
      value, factors = Pyrb.deconstruct(2, smallest_palindrome.call([], { max_factor: 99, min_factor: 10 }))
      self_.fcall('assertEqual', [121, value])
      self_.fcall('assertEqual', [Pyrb::Set.new([[11]]), Pyrb::Set.call([factors])])
    end
  end

  PalindromesTests = exports['PalindromesTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
