# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_space_age
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  SpaceAge, _ = import('SpaceAge', from: 'integration.source.space_age')

  exports['SpaceAgeTest'] = Pyrb::PythonClass.new('SpaceAgeTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_age_in_seconds'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([1e6])
      self_.fcall('assertEqual', [1e6, age.attr('seconds')])
    end

    klass.attrs['test_age_in_earth_years'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([1e9])
      self_.fcall('assertEqual', [31.69, age.fcall('on_earth')])
    end

    klass.attrs['test_age_in_mercury_years'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([2134835688])
      self_.fcall('assertEqual', [67.65, age.fcall('on_earth')])
      self_.fcall('assertEqual', [280.88, age.fcall('on_mercury')])
    end

    klass.attrs['test_age_in_venus_years'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([189839836])
      self_.fcall('assertEqual', [6.02, age.fcall('on_earth')])
      self_.fcall('assertEqual', [9.78, age.fcall('on_venus')])
    end

    klass.attrs['test_age_on_mars'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([2329871239])
      self_.fcall('assertEqual', [73.83, age.fcall('on_earth')])
      self_.fcall('assertEqual', [39.25, age.fcall('on_mars')])
    end

    klass.attrs['test_age_on_jupiter'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([901876382])
      self_.fcall('assertEqual', [28.58, age.fcall('on_earth')])
      self_.fcall('assertEqual', [2.41, age.fcall('on_jupiter')])
    end

    klass.attrs['test_age_on_saturn'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([3e9])
      self_.fcall('assertEqual', [95.06, age.fcall('on_earth')])
      self_.fcall('assertEqual', [3.23, age.fcall('on_saturn')])
    end

    klass.attrs['test_age_on_uranus'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([3210123456])
      self_.fcall('assertEqual', [101.72, age.fcall('on_earth')])
      self_.fcall('assertEqual', [1.21, age.fcall('on_uranus')])
    end

    klass.attrs['test_age_on_neptune'] = Pyrb.defn(self: nil) do |self_|
      age = SpaceAge.call([8210123456])
      self_.fcall('assertEqual', [260.16, age.fcall('on_earth')])
      self_.fcall('assertEqual', [1.58, age.fcall('on_neptune')])
    end
  end

  SpaceAgeTest = exports['SpaceAgeTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
