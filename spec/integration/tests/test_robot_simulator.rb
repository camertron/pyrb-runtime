# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_robot_simulator
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Robot, NORTH, EAST, SOUTH, WEST = import('Robot', 'NORTH', 'EAST', 'SOUTH', 'WEST', from: 'integration.source.robot_simulator')

  exports['RobotTests'] = Pyrb::PythonClass.new('RobotTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_init'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call()
      self_.fcall('assertEqual', [[0, 0], robot.attr('coordinates')])
      self_.fcall('assertEqual', [NORTH, robot.attr('bearing')])
    end

    klass.attrs['test_setup'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([SOUTH, -1, 1])
      self_.fcall('assertEqual', [[-1, 1], robot.attr('coordinates')])
      self_.fcall('assertEqual', [SOUTH, robot.attr('bearing')])
    end

    klass.attrs['test_turn_right'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call()
      ([EAST, SOUTH, WEST, NORTH]).each do |direction|
        robot.fcall('turn_right')
        self_.fcall('assertEqual', [robot.attr('bearing'), direction])
      end
    end

    klass.attrs['test_turn_left'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call()
      ([WEST, SOUTH, EAST, NORTH]).each do |direction|
        robot.fcall('turn_left')
        self_.fcall('assertEqual', [robot.attr('bearing'), direction])
      end
    end

    klass.attrs['test_advance_positive_north'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([NORTH, 0, 0])
      robot.fcall('advance')
      self_.fcall('assertEqual', [[0, 1], robot.attr('coordinates')])
      self_.fcall('assertEqual', [NORTH, robot.attr('bearing')])
    end

    klass.attrs['test_advance_positive_east'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([EAST, 0, 0])
      robot.fcall('advance')
      self_.fcall('assertEqual', [[1, 0], robot.attr('coordinates')])
      self_.fcall('assertEqual', [EAST, robot.attr('bearing')])
    end

    klass.attrs['test_advance_negative_south'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([SOUTH, 0, 0])
      robot.fcall('advance')
      self_.fcall('assertEqual', [[0, -1], robot.attr('coordinates')])
      self_.fcall('assertEqual', [SOUTH, robot.attr('bearing')])
    end

    klass.attrs['test_advance_positive_west'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([WEST, 0, 0])
      robot.fcall('advance')
      self_.fcall('assertEqual', [[-1, 0], robot.attr('coordinates')])
      self_.fcall('assertEqual', [WEST, robot.attr('bearing')])
    end

    klass.attrs['test_simulate_prog1'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([NORTH, 0, 0])
      robot.fcall('simulate', ["LAAARALA"])
      self_.fcall('assertEqual', [[-4, 1], robot.attr('coordinates')])
      self_.fcall('assertEqual', [WEST, robot.attr('bearing')])
    end

    klass.attrs['test_simulate_prog2'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([EAST, 2, -7])
      robot.fcall('simulate', ["RRAAAAALA"])
      self_.fcall('assertEqual', [[-3, -8], robot.attr('coordinates')])
      self_.fcall('assertEqual', [SOUTH, robot.attr('bearing')])
    end

    klass.attrs['test_simulate_prog3'] = Pyrb.defn(self: nil) do |self_|
      robot = Robot.call([SOUTH, 8, 4])
      robot.fcall('simulate', ["LAAARRRALLLL"])
      self_.fcall('assertEqual', [[11, 5], robot.attr('coordinates')])
      self_.fcall('assertEqual', [NORTH, robot.attr('bearing')])
    end
  end

  RobotTests = exports['RobotTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
