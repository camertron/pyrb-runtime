# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_allergies
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Allergies, _ = import('Allergies', from: 'integration.source.allergies')

  exports['AllergiesTests'] = Pyrb::PythonClass.new('AllergiesTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_no_allergies_means_not_allergic'] = Pyrb.defn(self: nil) do |self_|
      allergies = Allergies.call([0])
      self_.fcall('assertFalse', [allergies.fcall('is_allergic_to', ["peanuts"])])
      self_.fcall('assertFalse', [allergies.fcall('is_allergic_to', ["cats"])])
      self_.fcall('assertFalse', [allergies.fcall('is_allergic_to', ["strawberries"])])
    end

    klass.attrs['test_is_allergic_to_eggs'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [Allergies.call([1]).fcall('is_allergic_to', ["eggs"])])
    end

    klass.attrs['test_has_the_right_allergies'] = Pyrb.defn(self: nil) do |self_|
      allergies = Allergies.call([5])
      self_.fcall('assertTrue', [allergies.fcall('is_allergic_to', ["eggs"])])
      self_.fcall('assertTrue', [allergies.fcall('is_allergic_to', ["shellfish"])])
      self_.fcall('assertFalse', [allergies.fcall('is_allergic_to', ["strawberries"])])
    end

    klass.attrs['test_no_allergies_at_all'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[], Allergies.call([0]).attr('lst')])
    end

    klass.attrs['test_allergic_to_just_peanuts'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [["peanuts"], Allergies.call([2]).attr('lst')])
    end

    klass.attrs['test_allergic_to_everything'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb.sorted.call([(("eggs peanuts shellfish strawberries tomatoes " + "chocolate pollen cats")).fcall('split')]), Pyrb.sorted.call([Allergies.call([255]).attr('lst')])])
    end

    klass.attrs['test_ignore_non_allergen_score_parts'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [["eggs"], Allergies.call([257]).attr('lst')])
    end
  end

  AllergiesTests = exports['AllergiesTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
