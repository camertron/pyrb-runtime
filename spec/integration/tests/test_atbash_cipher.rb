# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_atbash_cipher
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  decode, encode = import('decode', 'encode', from: 'integration.source.atbash_cipher')

  exports['AtbashCipherTest'] = Pyrb::PythonClass.new('AtbashCipherTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_encode_no'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["ml", encode.call(["no"])])
    end

    klass.attrs['test_encode_yes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["bvh", encode.call(["yes"])])
    end

    klass.attrs['test_encode_OMG'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["lnt", encode.call(["OMG"])])
    end

    klass.attrs['test_encode_O_M_G'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["lnt", encode.call(["O M G"])])
    end

    klass.attrs['test_encode_long_word'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["nrmwy oldrm tob", encode.call(["mindblowingly"])])
    end

    klass.attrs['test_encode_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["gvhgr mt123 gvhgr mt", encode.call(["Testing, 1 2 3, testing."])])
    end

    klass.attrs['test_encode_sentence'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["gifgs rhurx grlm", encode.call(["Truth is fiction."])])
    end

    klass.attrs['test_encode_all_things'] = Pyrb.defn(self: nil) do |self_|
      plaintext = "The quick brown fox jumps over the lazy dog."
      ciphertext = "gsvjf rxpyi ldmul cqfnk hlevi gsvoz abwlt"
      self_.fcall('assertMultiLineEqual', [ciphertext, encode.call([plaintext])])
    end

    klass.attrs['test_decode_word'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["exercism", decode.call(["vcvix rhn"])])
    end

    klass.attrs['test_decode_sentence'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["anobstacleisoftenasteppingstone", decode.call(["zmlyh gzxov rhlug vmzhg vkkrm thglm v"])])
    end
  end

  AtbashCipherTest = exports['AtbashCipherTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
