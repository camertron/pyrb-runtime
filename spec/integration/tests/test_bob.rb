# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_bob
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  bob, _ = import('integration.source.bob')

  exports['BobTests'] = Pyrb::PythonClass.new('BobTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_stating_something'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whatever.", bob.fcall('hey', ["Tom-ay-to, tom-aaaah-to."])])
    end

    klass.attrs['test_shouting'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whoa, chill out!", bob.fcall('hey', ["WATCH OUT!"])])
    end

    klass.attrs['test_asking_a_question'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Sure.", bob.fcall('hey', ["Does this cryogenic chamber make me look fat?"])])
    end

    klass.attrs['test_asking_a_numeric_question'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Sure.", bob.fcall('hey', ["You are, what, like 15?"])])
    end

    klass.attrs['test_talking_forcefully'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whatever.", bob.fcall('hey', ["Let's go make out behind the gym!"])])
    end

    klass.attrs['test_using_acronyms_in_regular_speech'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whatever.", bob.fcall('hey', ["It's OK if you don't want to go to the DMV."])])
    end

    klass.attrs['test_forceful_questions'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whoa, chill out!", bob.fcall('hey', ["WHAT THE HELL WERE YOU THINKING?"])])
    end

    klass.attrs['test_shouting_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whoa, chill out!", bob.fcall('hey', ["1, 2, 3 GO!"])])
    end

    klass.attrs['test_only_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whatever.", bob.fcall('hey', ["1, 2, 3"])])
    end

    klass.attrs['test_question_with_only_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Sure.", bob.fcall('hey', ["4?"])])
    end

    klass.attrs['test_shouting_with_special_characters'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whoa, chill out!", bob.fcall('hey', ["ZOMG THE %^*@\#$(*^ ZOMBIES ARE COMING!!11!!1!"])])
    end

    klass.attrs['test_shouting_with_umlauts'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whoa, chill out!", bob.fcall('hey', ["ÜMLÄÜTS!"])])
    end

    klass.attrs['test_calmly_speaking_with_umlauts'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whatever.", bob.fcall('hey', ["ÜMLäÜTS!"])])
    end

    klass.attrs['test_shouting_with_no_exclamation_mark'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whoa, chill out!", bob.fcall('hey', ["I HATE YOU"])])
    end

    klass.attrs['test_statement_containing_question_mark'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whatever.", bob.fcall('hey', ["Ending with ? means a question."])])
    end

    klass.attrs['test_prattling_on'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Sure.", bob.fcall('hey', ["Wait! Hang on. Are you going to be OK?"])])
    end

    klass.attrs['test_silence'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Fine. Be that way!", bob.fcall('hey', [""])])
    end

    klass.attrs['test_prolonged_silence'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Fine. Be that way!", bob.fcall('hey', ["    \t"])])
    end

    klass.attrs['test_starts_with_whitespace'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Whatever.", bob.fcall('hey', ["         hmmmmmmm..."])])
    end

    klass.attrs['test_ends_with_whitespace'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Sure.", bob.fcall('hey', ["What if we end with whitespace?   "])])
    end
  end

  BobTests = exports['BobTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
