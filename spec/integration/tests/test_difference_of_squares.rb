# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_difference_of_squares
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  difference, square_of_sum, sum_of_squares = import('difference', 'square_of_sum', 'sum_of_squares', from: 'integration.source.difference_of_squares')

  exports['DifferenceOfSquaresTest'] = Pyrb::PythonClass.new('DifferenceOfSquaresTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_square_of_sum_5'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [225, square_of_sum.call([5])])
    end

    klass.attrs['test_sum_of_squares_5'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [55, sum_of_squares.call([5])])
    end

    klass.attrs['test_difference_5'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [170, difference.call([5])])
    end

    klass.attrs['test_square_of_sum_100'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [25502500, square_of_sum.call([100])])
    end

    klass.attrs['test_sum_of_squares_100'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [338350, sum_of_squares.call([100])])
    end

    klass.attrs['test_difference_100'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [25164150, difference.call([100])])
    end
  end

  DifferenceOfSquaresTest = exports['DifferenceOfSquaresTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
