# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_flatten_array
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  flatten, _ = import('flatten', from: 'integration.source.flatten_array')

  exports['FlattenArrayTests'] = Pyrb::PythonClass.new('FlattenArrayTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_no_nesting'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [flatten.call([[0, 1, 2]]), [0, 1, 2]])
    end

    klass.attrs['test_one_level_nesting'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [flatten.call([[0, [1], 2]]), [0, 1, 2]])
    end

    klass.attrs['test_two_level_nesting'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [flatten.call([[0, [1, [2, 3]], [4]]]), [0, 1, 2, 3, 4]])
    end

    klass.attrs['test_empty_nested_lists'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [flatten.call([[[()]]]), []])
    end

    klass.attrs['test_with_none_values'] = Pyrb.defn(self: nil) do |self_|
      inputs = [0, 2, [[2, 3], 8, [[100]], nil, [[nil]]], -2]
      expected = [0, 2, 2, 3, 8, 100, -2]
      self_.fcall('assertEqual', [flatten.call([inputs]), expected])
    end

    klass.attrs['test_six_level_nesting'] = Pyrb.defn(self: nil) do |self_|
      inputs = [1, [2, [[3]], [4, [[5]]], 6, 7], 8]
      expected = [1, 2, 3, 4, 5, 6, 7, 8]
      self_.fcall('assertEqual', [flatten.call([inputs]), expected])
    end

    klass.attrs['test_all_values_are_none'] = Pyrb.defn(self: nil) do |self_|
      inputs = [nil, [[[nil]]], nil, nil, [[nil, nil], nil], nil]
      expected = []
      self_.fcall('assertEqual', [flatten.call([inputs]), expected])
    end

    klass.attrs['test_strings'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [flatten.call([["0", ["1", "2"]]]), ["0", "1", "2"]])
    end
  end

  FlattenArrayTests = exports['FlattenArrayTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
