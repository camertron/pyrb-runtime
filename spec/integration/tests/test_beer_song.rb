# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_beer_song
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  song, verse = import('song', 'verse', from: 'integration.source.beer')

  exports['BeerTest'] = Pyrb::PythonClass.new('BeerTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_a_verse'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [verse.call([8]), ("8 bottles of beer on the wall, 8 bottles of beer.\n" + "Take one down and pass it around, " + "7 bottles of beer on the wall.\n")])
    end

    klass.attrs['test_verse_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [verse.call([1]), ("1 bottle of beer on the wall, 1 bottle of beer.\n" + "Take it down and pass it around, " + "no more bottles of beer on the wall.\n")])
    end

    klass.attrs['test_verse_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [verse.call([2]), ("2 bottles of beer on the wall, 2 bottles of beer.\n" + "Take one down and pass it around, 1 bottle of beer on the wall.\n")])
    end

    klass.attrs['test_verse_0'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [verse.call([0]), ("No more bottles of beer on the wall, no more bottles of beer.\n" + "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n")])
    end

    klass.attrs['test_songing_several_verses'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [song.call([8, 6]), ("8 bottles of beer on the wall, 8 bottles of beer.\n" + "Take one down and pass it around, " + "7 bottles of beer on the wall.\n\n" + "7 bottles of beer on the wall, 7 bottles of beer.\n" + "Take one down and pass it around, " + "6 bottles of beer on the wall.\n\n" + "6 bottles of beer on the wall, 6 bottles of beer.\n" + "Take one down and pass it around, " + "5 bottles of beer on the wall.\n\n")])
    end

    klass.attrs['test_song_all_the_rest_of_the_verses'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [song.call([3]), ("3 bottles of beer on the wall, 3 bottles of beer.\n" + "Take one down and pass it around, " + "2 bottles of beer on the wall.\n\n" + "2 bottles of beer on the wall, 2 bottles of beer.\n" + "Take one down and pass it around, " + "1 bottle of beer on the wall.\n\n" + "1 bottle of beer on the wall, 1 bottle of beer.\n" + "Take it down and pass it around, " + "no more bottles of beer on the wall.\n\n" + "No more bottles of beer on the wall, no more bottles of beer.\n" + "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n\n")])
    end
  end

  BeerTest = exports['BeerTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
