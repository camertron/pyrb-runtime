# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_nth_prime
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  nth_prime, _ = import('nth_prime', from: 'integration.source.prime')

  exports['NthPrimeTests'] = Pyrb::PythonClass.new('NthPrimeTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_first_prime'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [2, nth_prime.call([1])])
    end

    klass.attrs['test_sixth_prime'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [13, nth_prime.call([6])])
    end

    klass.attrs['test_first_twenty_primes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71], Pyrb.range.call([1, 21]).fcall('__iter__').map do |n|
        nth_prime.call([n])
      end
      ])
    end

    klass.attrs['test_prime_no_10000'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [104729, nth_prime.call([10000])])
    end
  end

  NthPrimeTests = exports['NthPrimeTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
