# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_protein_translation
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  proteins, _ = import('proteins', from: 'integration.source.protein_translation')

  exports['ProteinTranslationTest'] = Pyrb::PythonClass.new('ProteinTranslationTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_AUG_translates_to_methionine'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [proteins.call(["AUG"]), ["Methionine"]])
    end

    klass.attrs['test_identifies_Phenylalanine_codons'] = Pyrb.defn(self: nil) do |self_|
      (["UUU", "UUC"]).each do |codon|
        self_.fcall('assertEqual', [proteins.call([codon]), ["Phenylalanine"]])
      end
    end

    klass.attrs['test_identifies_Leucine_codons'] = Pyrb.defn(self: nil) do |self_|
      (["UUA", "UUG"]).each do |codon|
        self_.fcall('assertEqual', [proteins.call([codon]), ["Leucine"]])
      end
    end

    klass.attrs['test_identifies_Serine_codons'] = Pyrb.defn(self: nil) do |self_|
      (["UCU", "UCC", "UCA", "UCG"]).each do |codon|
        self_.fcall('assertEqual', [proteins.call([codon]), ["Serine"]])
      end
    end

    klass.attrs['test_identifies_Tyrosine_codons'] = Pyrb.defn(self: nil) do |self_|
      (["UAU", "UAC"]).each do |codon|
        self_.fcall('assertEqual', [proteins.call([codon]), ["Tyrosine"]])
      end
    end

    klass.attrs['test_identifies_Cysteine_codons'] = Pyrb.defn(self: nil) do |self_|
      (["UGU", "UGC"]).each do |codon|
        self_.fcall('assertEqual', [proteins.call([codon]), ["Cysteine"]])
      end
    end

    klass.attrs['test_identifies_Tryptophan_codons'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [proteins.call(["UGG"]), ["Tryptophan"]])
    end

    klass.attrs['test_identifies_stop_codons'] = Pyrb.defn(self: nil) do |self_|
      (["UAA", "UAG", "UGA"]).each do |codon|
        self_.fcall('assertEqual', [proteins.call([codon]), []])
      end
    end

    klass.attrs['test_translates_rna_strand_into_correct_protein_list'] = Pyrb.defn(self: nil) do |self_|
      strand = "AUGUUUUGG"
      expected = ["Methionine", "Phenylalanine", "Tryptophan"]
      self_.fcall('assertEqual', [proteins.call([strand]), expected])
    end

    klass.attrs['test_stops_translation_if_stop_codon_at_beginning_of_sequence'] = Pyrb.defn(self: nil) do |self_|
      strand = "UAGUGG"
      expected = []
      self_.fcall('assertEqual', [proteins.call([strand]), expected])
    end

    klass.attrs['test_stops_translation_if_stop_codon_at_end_of_two_codon_sequence'] = Pyrb.defn(self: nil) do |self_|
      strand = "UGGUAG"
      expected = ["Tryptophan"]
      self_.fcall('assertEqual', [proteins.call([strand]), expected])
    end

    klass.attrs['test_stops_translation_if_stop_codon_at_end_of_three_codon_sequence'] = Pyrb.defn(self: nil) do |self_|
      strand = "AUGUUUUAA"
      expected = ["Methionine", "Phenylalanine"]
      self_.fcall('assertEqual', [proteins.call([strand]), expected])
    end

    klass.attrs['test_stops_translation_if_stop_codon_in_middle_of_three_codon_sequence'] = Pyrb.defn(self: nil) do |self_|
      strand = "UGGUAGUGG"
      expected = ["Tryptophan"]
      self_.fcall('assertEqual', [proteins.call([strand]), expected])
    end

    klass.attrs['test_stops_translation_if_stop_codon_in_middle_of_six_codon_sequence'] = Pyrb.defn(self: nil) do |self_|
      strand = "UGGUGUUAUUAAUGGUUU"
      expected = ["Tryptophan", "Cysteine", "Tyrosine"]
      self_.fcall('assertEqual', [proteins.call([strand]), expected])
    end
  end

  ProteinTranslationTest = exports['ProteinTranslationTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
