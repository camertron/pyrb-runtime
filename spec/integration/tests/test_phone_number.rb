# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_phone_number
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Phone, _ = import('Phone', from: 'integration.source.phone')

  exports['PhoneTest'] = Pyrb::PythonClass.new('PhoneTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_cleans_number'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["(123) 456-7890"]).attr('number')
      self_.fcall('assertEqual', ["1234567890", number])
    end

    klass.attrs['test_cleans_number_with_dots'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["123.456.7890"]).attr('number')
      self_.fcall('assertEqual', ["1234567890", number])
    end

    klass.attrs['test_valid_when_11_digits_and_first_is_1'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["11234567890"]).attr('number')
      self_.fcall('assertEqual', ["1234567890", number])
    end

    klass.attrs['test_invalid_when_11_digits'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["21234567890"]).attr('number')
      self_.fcall('assertEqual', ["0000000000", number])
    end

    klass.attrs['test_invalid_when_9_digits'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["123456789"]).attr('number')
      self_.fcall('assertEqual', ["0000000000", number])
    end

    klass.attrs['test_area_code'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["1234567890"])
      self_.fcall('assertEqual', ["123", number.fcall('area_code')])
    end

    klass.attrs['test_pretty_print'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["1234567890"])
      self_.fcall('assertEqual', ["(123) 456-7890", number.fcall('pretty')])
    end

    klass.attrs['test_pretty_print_with_full_us_phone_number'] = Pyrb.defn(self: nil) do |self_|
      number = Phone.call(["11234567890"])
      self_.fcall('assertEqual', ["(123) 456-7890", number.fcall('pretty')])
    end
  end

  PhoneTest = exports['PhoneTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
