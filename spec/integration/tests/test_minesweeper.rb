# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_minesweeper
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  board, _ = import('board', from: 'integration.source.minesweeper')

  <<~__DOC__
  Tests for the minesweeper exercise

  Implementation note:
  The board function must validate its input and raise a
  ValueError with a meaningfull error message if the
  input turns out to be malformed.
  __DOC__

  exports['MinesweeperTest'] = Pyrb::PythonClass.new('MinesweeperTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_board1'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+------+", "| *  * |", "|  *   |", "|    * |", "|   * *|", "| *  * |", "|      |", "+------+"]
      out = ["+------+", "|1*22*1|", "|12*322|", "| 123*2|", "|112*4*|", "|1*22*2|", "|111111|", "+------+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board2'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-----+", "| * * |", "|     |", "|   * |", "|  * *|", "| * * |", "+-----+"]
      out = ["+-----+", "|1*2*1|", "|11322|", "| 12*2|", "|12*4*|", "|1*3*2|", "+-----+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board3'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-----+", "| * * |", "+-----+"]
      out = ["+-----+", "|1*2*1|", "+-----+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board4'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-+", "|*|", "| |", "|*|", "| |", "| |", "+-+"]
      out = ["+-+", "|*|", "|2|", "|*|", "|1|", "| |", "+-+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board5'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-+", "|*|", "+-+"]
      out = ["+-+", "|*|", "+-+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board6'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+--+", "|**|", "|**|", "+--+"]
      out = ["+--+", "|**|", "|**|", "+--+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board7'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+--+", "|**|", "|**|", "+--+"]
      out = ["+--+", "|**|", "|**|", "+--+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board8'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+---+", "|***|", "|* *|", "|***|", "+---+"]
      out = ["+---+", "|***|", "|*8*|", "|***|", "+---+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_board9'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-----+", "|     |", "|   * |", "|     |", "|     |", "| *   |", "+-----+"]
      out = ["+-----+", "|  111|", "|  1*1|", "|  111|", "|111  |", "|1*1  |", "+-----+"]
      self_.fcall('assertEqual', [out, board.call([inp])])
    end

    klass.attrs['test_different_len'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-+", "| |", "|*  |", "|  |", "+-+"]
      self_.fcall('assertRaises', [Pyrb::ValueError, board, inp])
    end

    klass.attrs['test_faulty_border'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-----+", "*   * |", "+-- --+"]
      self_.fcall('assertRaises', [Pyrb::ValueError, board, inp])
    end

    klass.attrs['test_invalid_char'] = Pyrb.defn(self: nil) do |self_|
      inp = ["+-----+", "|X  * |", "+-----+"]
      self_.fcall('assertRaises', [Pyrb::ValueError, board, inp])
    end
  end

  MinesweeperTest = exports['MinesweeperTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
