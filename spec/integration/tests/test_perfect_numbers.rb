# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_perfect_numbers
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  is_perfect, _ = import('is_perfect', from: 'integration.source.perfect_numbers')

  exports['PerfectNumbersTest'] = Pyrb::PythonClass.new('PerfectNumbersTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_first_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_perfect.call([6])])
    end

    klass.attrs['test_no_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertFalse', [is_perfect.call([8])])
    end

    klass.attrs['test_second_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_perfect.call([28])])
    end

    klass.attrs['test_abundant'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertFalse', [is_perfect.call([20])])
    end

    klass.attrs['test_answer_to_the_ultimate_question_of_life'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertFalse', [is_perfect.call([42])])
    end

    klass.attrs['test_third_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_perfect.call([496])])
    end

    klass.attrs['test_odd_abundant'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertFalse', [is_perfect.call([945])])
    end

    klass.attrs['test_fourth_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_perfect.call([8128])])
    end

    klass.attrs['test_fifth_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_perfect.call([33550336])])
    end

    klass.attrs['test_sixth_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_perfect.call([8589869056])])
    end

    klass.attrs['test_seventh_perfect_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_perfect.call([137438691328])])
    end
  end

  PerfectNumbersTest = exports['PerfectNumbersTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
