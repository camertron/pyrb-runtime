# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_ocr
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  grid, number = import('grid', 'number', from: 'integration.source.ocr')

  <<~__DOC__
  Tests for the ocr-numbers exercise

  Implementation note:
  Both ocr.grid and ocr.number should validate their input
  and raise ValueErrors with meaningful error messages
  if necessary.
  __DOC__

  exports['OcrTest'] = Pyrb::PythonClass.new('OcrTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_0'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["0", number.call([[" _ ", "| |", "|_|", "   "]])])
    end

    klass.attrs['test_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["1", number.call([["   ", "  |", "  |", "   "]])])
    end

    klass.attrs['test_garbage'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["?", number.call([[" _ ", " _|", "  |", "   "]])])
    end

    klass.attrs['test_last_line_nonblank'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["?", number.call([["   ", "  |", "  |", "| |"]])])
    end

    klass.attrs['test_unknown_char'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["?", number.call([[" - ", " _|", " X|", "   "]])])
    end

    klass.attrs['test_too_short_row'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, number, ["   ", " _|", " |", "   "]])
    end

    klass.attrs['test_insufficient_rows'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, number, ["   ", " _|", " X|"]])
    end

    klass.attrs['test_grid0'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[" _ ", "| |", "|_|", "   "], grid.call(["0"])])
    end

    klass.attrs['test_grid1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [["   ", "  |", "  |", "   "], grid.call(["1"])])
    end

    klass.attrs['test_0010110'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["0010110", number.call([[" _  _     _        _ ", "| || |  || |  |  || |", "|_||_|  ||_|  |  ||_|", "                     "]])])
    end

    klass.attrs['test_3186547290'] = Pyrb.defn(self: nil) do |self_|
      digits = "3186547290"
      self_.fcall('assertEqual', [digits, number.call([[" _     _  _  _     _  _  _  _ ", " _|  ||_||_ |_ |_|  | _||_|| |", " _|  ||_||_| _|  |  ||_  _||_|", "                              "]])])
    end

    klass.attrs['test_Lost'] = Pyrb.defn(self: nil) do |self_|
      digits = "4815162342"
      self_.fcall('assertEqual', [digits, number.call([["    _     _     _  _  _     _ ", "|_||_|  ||_   ||_  _| _||_| _|", "  ||_|  | _|  ||_||_  _|  ||_ ", "                              "]])])
    end

    klass.attrs['test_garble_middle'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["12?45", number.call([["    _  _     _ ", "  | _|  ||_||_ ", "  ||_  _|  | _|", "               "]])])
    end

    klass.attrs['test_grid3186547290'] = Pyrb.defn(self: nil) do |self_|
      digits = "3186547290"
      self_.fcall('assertEqual', [[" _     _  _  _     _  _  _  _ ", " _|  ||_||_ |_ |_|  | _||_|| |", " _|  ||_||_| _|  |  ||_  _||_|", "                              "], grid.call([digits])])
    end

    klass.attrs['test_invalid_grid'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, grid, "123a"])
    end
  end

  OcrTest = exports['OcrTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
