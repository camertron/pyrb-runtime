# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_hello_world
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  hello_world, _ = import('integration.source.hello_world')

  exports['HelloWorldTest'] = Pyrb::PythonClass.new('HelloWorldTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_hello'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [hello_world.fcall('hello'), "Hello, World!"])
    end
  end

  HelloWorldTest = exports['HelloWorldTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
