# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_wordy
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  calculate, _ = import('calculate', from: 'integration.source.wordy')

  exports['WordyTest'] = Pyrb::PythonClass.new('WordyTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_simple_add_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [18, calculate.call(["What is 5 plus 13?"])])
    end

    klass.attrs['test_simple_add_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [-8, calculate.call(["What is 5 plus -13?"])])
    end

    klass.attrs['test_simple_sub_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [6, calculate.call(["What is 103 minus 97?"])])
    end

    klass.attrs['test_simple_sub_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [-6, calculate.call(["What is 97 minus 103?"])])
    end

    klass.attrs['test_simple_mult'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [21, calculate.call(["What is 7 multiplied by 3?"])])
    end

    klass.attrs['test_simple_div'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [9, calculate.call(["What is 45 divided by 5?"])])
    end

    klass.attrs['test_add_negative_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [-11, calculate.call(["What is -1 plus -10?"])])
    end

    klass.attrs['test_add_more_digits'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [45801, calculate.call(["What is 123 plus 45678?"])])
    end

    klass.attrs['test_add_twice'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [4, calculate.call(["What is 1 plus 2 plus 1?"])])
    end

    klass.attrs['test_add_then_subtract'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [14, calculate.call(["What is 1 plus 5 minus -8?"])])
    end

    klass.attrs['test_subtract_twice'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [-7, calculate.call(["What is 20 minus 14 minus 13?"])])
    end

    klass.attrs['test_multiply_twice'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [-12, calculate.call([("What is 2 multiplied by -2 " + "multiplied by 3?")])])
    end

    klass.attrs['test_add_then_multiply'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [-8, calculate.call(["What is -3 plus 7 multiplied by -2?"])])
    end

    klass.attrs['test_divide_twice'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [16, calculate.call(["What is -12000 divided by 25 divided by -30?"])])
    end

    klass.attrs['test_invalid_operation'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, calculate, "What is 4 xor 7?"])
    end

    klass.attrs['test_missing_operation'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, calculate, "What is 2 2 minus 3?"])
    end

    klass.attrs['test_missing_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, calculate, ("What is 7 plus " + "multiplied by -2?")])
    end

    klass.attrs['test_irrelevant_question'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, calculate, "Which is greater, 3 or 2?"])
    end
  end

  WordyTest = exports['WordyTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
