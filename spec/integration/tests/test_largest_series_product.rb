# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_largest_series_product
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  largest_product, slices = import('largest_product', 'slices', from: 'integration.source.largest_series_product')

  <<~__DOC__
  Tests for the largest-series-product exercise

  Implementation note:
  In case of invalid inputs to the 'slices' or 'largest_product' functions
  your program should raise a ValueError with a meaningful error message.

  Feel free to reuse your code for the series exercise!
  __DOC__

  exports['SeriesTest'] = Pyrb::PythonClass.new('SeriesTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_slices_of_two'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[[9, 7], [7, 8], [8, 6], [6, 7], [7, 5], [5, 6], [6, 4]], slices.call(["97867564", 2])])
    end

    klass.attrs['test_overly_long_slice'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        slices.call(["012", 4])
      end
    end

    klass.attrs['test_largest_product_of_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [72, largest_product.call(["0123456789", 2])])
    end

    klass.attrs['test_tiny_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [9, largest_product.call(["19", 2])])
    end

    klass.attrs['test_largest_product_of_3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [270, largest_product.call(["1027839564", 3])])
    end

    klass.attrs['test_big_number'] = Pyrb.defn(self: nil) do |self_|
      series = "52677741234314237566414902593461595376319419139427"
      self_.fcall('assertEqual', [28350, largest_product.call([series, 6])])
    end

    klass.attrs['test_identity'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, largest_product.call(["", 0])])
    end

    klass.attrs['test_slices_bigger_than_number'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        largest_product.call(["012", 4])
      end
    end
  end

  SeriesTest = exports['SeriesTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
