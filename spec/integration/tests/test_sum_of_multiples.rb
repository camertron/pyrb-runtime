# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_sum_of_multiples
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  sum_of_multiples, _ = import('sum_of_multiples', from: 'integration.source.sum_of_multiples')

  <<~__DOC__
  You can make the following assumptions about the inputs to the
  'sum_of_multiples' function:
      * All input numbers are non-negative 'int's, i.e. natural numbers including
        zero.
      * If a list of factors is given, its elements are uniqe and sorted in
        ascending order.
      * If the 'factors' argument is missing, use the list [3, 5] instead.
  __DOC__

  exports['SumOfMultiplesTest'] = Pyrb::PythonClass.new('SumOfMultiplesTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_sum_to_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [0, sum_of_multiples.call([1])])
    end

    klass.attrs['test_sum_to_3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [3, sum_of_multiples.call([4])])
    end

    klass.attrs['test_sum_to_10'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [23, sum_of_multiples.call([10])])
    end

    klass.attrs['test_sum_to_1000'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [233168, sum_of_multiples.call([1000])])
    end

    klass.attrs['test_configurable_7_13_17_to_20'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [51, sum_of_multiples.call([20, [7, 13, 17]])])
    end

    klass.attrs['test_configurable_4_6_to_15'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [30, sum_of_multiples.call([15, [4, 6]])])
    end

    klass.attrs['test_configurable_5_6_8_to_150'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [4419, sum_of_multiples.call([150, [5, 6, 8]])])
    end

    klass.attrs['test_configurable_43_47_to_10000'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [2203160, sum_of_multiples.call([10000, [43, 47]])])
    end

    klass.attrs['test_configurable_0_to_10'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [0, sum_of_multiples.call([10, [0]])])
    end

    klass.attrs['test_configurable_0_1_to_10'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [45, sum_of_multiples.call([10, [0, 1]])])
    end
  end

  SumOfMultiplesTest = exports['SumOfMultiplesTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
