# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_luhn
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  Counter, _ = import('Counter', from: 'collections')
  unittest, _ = import('unittest')
  Luhn, _ = import('Luhn', from: 'integration.source.luhn')

  exports['LuhnTests'] = Pyrb::PythonClass.new('LuhnTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_addends'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Counter.call([[1, 4, 1, 4, 1]]), Counter.call([Luhn.call([12121]).fcall('addends')])])
    end

    klass.attrs['test_addends_large'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Counter.call([[7, 6, 6, 1]]), Counter.call([Luhn.call([8631]).fcall('addends')])])
    end

    klass.attrs['test_checksum1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [2, Luhn.call([4913]).fcall('checksum')])
    end

    klass.attrs['test_ckecksum2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, Luhn.call([201773]).fcall('checksum')])
    end

    klass.attrs['test_invalid_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertFalse', [Luhn.call([738]).fcall('is_valid')])
    end

    klass.attrs['test_valid_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [Luhn.call([8739567]).fcall('is_valid')])
    end

    klass.attrs['test_create_valid_number1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1230, Luhn.fcall('create', [123])])
    end

    klass.attrs['test_create_valid_number2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [8739567, Luhn.fcall('create', [873956])])
    end

    klass.attrs['test_create_valid_number3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [8372637564, Luhn.fcall('create', [837263756])])
    end

    klass.attrs['test_is_valid_can_be_called_repeatedly'] = Pyrb.defn(self: nil) do |self_|
      number = Luhn.call([8739567])
      self_.fcall('assertTrue', [number.fcall('is_valid')])
      self_.fcall('assertTrue', [number.fcall('is_valid')])
    end
  end

  LuhnTests = exports['LuhnTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
