# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_anagram
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  detect_anagrams, _ = import('detect_anagrams', from: 'integration.source.anagram')

  exports['AnagramTests'] = Pyrb::PythonClass.new('AnagramTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_no_matches'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[], detect_anagrams.call(["diaper", "hello world zombies pants".fcall('split')])])
    end

    klass.attrs['test_detect_simple_anagram'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [["tan"], detect_anagrams.call(["ant", "tan stand at".fcall('split')])])
    end

    klass.attrs['test_detect_multiple_anagrams'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [["stream", "maters"], detect_anagrams.call(["master", "stream pigeon maters".fcall('split')])])
    end

    klass.attrs['test_does_not_confuse_different_duplicates'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[], detect_anagrams.call(["galea", ["eagle"]])])
    end

    klass.attrs['test_eliminate_anagram_subsets'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[], detect_anagrams.call(["good", "dog goody".fcall('split')])])
    end

    klass.attrs['test_detect_anagram'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [["inlets"], detect_anagrams.call(["listen", "enlists google inlets banana".fcall('split')])])
    end

    klass.attrs['test_multiple_anagrams'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["gallery regally largely".fcall('split'), detect_anagrams.call(["allergy", "gallery ballerina regally clergy largely leading".fcall('split')])])
    end

    klass.attrs['test_anagrams_are_case_insensitive'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [["Carthorse"], detect_anagrams.call(["Orchestra", "cashregister Carthorse radishes".fcall('split')])])
    end

    klass.attrs['test_same_word_isnt_anagram'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[], detect_anagrams.call(["banana", ["banana"]])])
      self_.fcall('assertEqual', [[], detect_anagrams.call(["go", "go Go GO".fcall('split')])])
    end
  end

  AnagramTests = exports['AnagramTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
