# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_kindergarten_garden
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Garden, _ = import('Garden', from: 'integration.source.garden')

  exports['KindergartenGardenTests'] = Pyrb::PythonClass.new('KindergartenGardenTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_alices_garden'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["Radishes Clover Grass Grass".fcall('split'), Garden.call(["RC\nGG"]).fcall('plants', ["Alice"])])
    end

    klass.attrs['test_bob_and_charlies_gardens'] = Pyrb.defn(self: nil) do |self_|
      garden = Garden.call(["VVCCGG\nVVCCGG"])
      self_.fcall('assertEqual', [(["Clover"] * 4), garden.fcall('plants', ["Bob"])])
      self_.fcall('assertEqual', [(["Grass"] * 4), garden.fcall('plants', ["Charlie"])])
    end

    klass.attrs['test_full_garden'] = Pyrb.defn(self: nil) do |self_|
      garden = Garden.call(["VRCGVVRVCGGCCGVRGCVCGCGV\nVRCCCGCRRGVCGCRVVCVGCGCV"])
      self_.fcall('assertEqual', ["Violets Radishes Violets Radishes".fcall('split'), garden.fcall('plants', ["Alice"])])
      self_.fcall('assertEqual', ["Clover Grass Clover Clover".fcall('split'), garden.fcall('plants', ["Bob"])])
      self_.fcall('assertEqual', ["Grass Clover Clover Grass".fcall('split'), garden.fcall('plants', ["Kincaid"])])
      self_.fcall('assertEqual', ["Grass Violets Clover Violets".fcall('split'), garden.fcall('plants', ["Larry"])])
    end

    klass.attrs['test_disordered_test'] = Pyrb.defn(self: nil) do |self_|
      garden = Garden.call(["VCRRGVRG\nRVGCCGCV", ], { students: "Samantha Patricia Xander Roger".fcall('split') })
      self_.fcall('assertEqual', ["Violets Clover Radishes Violets".fcall('split'), garden.fcall('plants', ["Patricia"])])
      self_.fcall('assertEqual', ["Radishes Grass Clover Violets".fcall('split'), garden.fcall('plants', ["Xander"])])
    end
  end

  KindergartenGardenTests = exports['KindergartenGardenTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
