# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_pangram
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  is_pangram, _ = import('is_pangram', from: 'integration.source.pangram')

  exports['PangramTests'] = Pyrb::PythonClass.new('PangramTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_empty_string'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertFalse', [is_pangram.call([""])])
    end

    klass.attrs['test_valid_pangram'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_pangram.call(["the quick brown fox jumps over the lazy dog"])])
    end

    klass.attrs['test_missing_x'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertFalse', [is_pangram.call([("a quick movement of the enemy will " + "jeopardize five gunboats")])])
    end

    klass.attrs['test_mixedcase_and_punctuation'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_pangram.call(["\"Five quacking Zephyrs jolt my wax bed.\""])])
    end

    klass.attrs['test_unchecked_german_umlaute'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertTrue', [is_pangram.call([("Victor jagt zwölf Boxkämpfer quer über den" + " großen Sylter Deich.")])])
    end
  end

  PangramTests = exports['PangramTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
