# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_prime_factors
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  prime_factors, _ = import('prime_factors', from: 'integration.source.prime_factors')

  exports['PrimeFactorsTest'] = Pyrb::PythonClass.new('PrimeFactorsTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[], prime_factors.call([1])])
    end

    klass.attrs['test_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[2], prime_factors.call([2])])
    end

    klass.attrs['test_3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[3], prime_factors.call([3])])
    end

    klass.attrs['test_4'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[2, 2], prime_factors.call([4])])
    end

    klass.attrs['test_6'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[2, 3], prime_factors.call([6])])
    end

    klass.attrs['test_8'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[2, 2, 2], prime_factors.call([8])])
    end

    klass.attrs['test_9'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[3, 3], prime_factors.call([9])])
    end

    klass.attrs['test_27'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[3, 3, 3], prime_factors.call([27])])
    end

    klass.attrs['test_625'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[5, 5, 5, 5], prime_factors.call([625])])
    end

    klass.attrs['test_901255'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[5, 17, 23, 461], prime_factors.call([901255])])
    end

    klass.attrs['test_93819012551'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[11, 9539, 894119], prime_factors.call([93819012551])])
    end
  end

  PrimeFactorsTest = exports['PrimeFactorsTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
