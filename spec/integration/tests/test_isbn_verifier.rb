# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_isbn_verifier
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  verify, _ = import('verify', from: 'integration.source.isbn_verifier')

  exports['IsbnVerifierTests'] = Pyrb::PythonClass.new('IsbnVerifierTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_valid_isbn_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-21508-8"]), true])
    end

    klass.attrs['test_invalid_check_digit'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-21508-9"]), false])
    end

    klass.attrs['test_valid_with_X_check_digit'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-21507-X"]), true])
    end

    klass.attrs['test_invalid_check_digit_other_than_X'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-21507-A"]), false])
    end

    klass.attrs['test_invalid_character_in_isbn'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-2K507-0"]), false])
    end

    klass.attrs['test_invalid_X_other_than_check_digit'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-2X507-9"]), false])
    end

    klass.attrs['test_valid_isbn_without_separating_dashes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3598215088"]), true])
    end

    klass.attrs['test_valid_isbn_without_separating_dashes_with_X_check_digit'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["359821507X"]), true])
    end

    klass.attrs['test_invalid_isbn_without_check_digit_and_dashes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["359821507"]), false])
    end

    klass.attrs['test_invalid_too_long_isbn_with_no_dashes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3598215078X"]), false])
    end

    klass.attrs['test_invalid_isbn_without_check_digit'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-21507"]), false])
    end

    klass.attrs['test_invalid_too_long_isbn'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-21507-XX"]), false])
    end

    klass.attrs['test_invalid_check_digit_X_used_for_0'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call(["3-598-21515-X"]), false])
    end

    klass.attrs['test_valid_empty_isbn'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [verify.call([""]), false])
    end
  end

  IsbnVerifierTests = exports['IsbnVerifierTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
