# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_triangle
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Triangle, TriangleError = import('Triangle', 'TriangleError', from: 'integration.source.triangle')

  exports['TriangleTests'] = Pyrb::PythonClass.new('TriangleTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_equilateral_triangles_have_equal_sides'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["equilateral", Triangle.call([2, 2, 2]).fcall('kind')])
    end

    klass.attrs['test_larger_equilateral_triangles_also_have_equal_sides'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["equilateral", Triangle.call([10, 10, 10]).fcall('kind')])
    end

    klass.attrs['test_isosceles_triangles_have_last_two_sides_equal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["isosceles", Triangle.call([3, 4, 4]).fcall('kind')])
    end

    klass.attrs['test_isosceles_triangles_have_first_and_last_sides_equal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["isosceles", Triangle.call([4, 3, 4]).fcall('kind')])
    end

    klass.attrs['test_isosceles_triangles_have_two_first_sides_equal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["isosceles", Triangle.call([4, 4, 3]).fcall('kind')])
    end

    klass.attrs['test_isosceles_triangles_have_in_fact_exactly_two_sides_equal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["isosceles", Triangle.call([10, 10, 2]).fcall('kind')])
    end

    klass.attrs['test_scalene_triangles_have_no_equal_sides'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["scalene", Triangle.call([3, 4, 5]).fcall('kind')])
    end

    klass.attrs['test_scalene_triangles_have_no_equal_sides_at_a_larger_scale_too'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["scalene", Triangle.call([10, 11, 12]).fcall('kind')])
      self_.fcall('assertEqual', ["scalene", Triangle.call([5, 4, 2]).fcall('kind')])
    end

    klass.attrs['test_very_small_triangles_are_legal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["scalene", Triangle.call([0.4, 0.6, 0.3]).fcall('kind')])
    end

    klass.attrs['test_triangles_with_no_size_are_illegal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [TriangleError, Triangle, 0, 0, 0])
    end

    klass.attrs['test_triangles_with_negative_sides_are_illegal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [TriangleError, Triangle, 3, 4, -5])
    end

    klass.attrs['test_triangles_violating_triangle_inequality_are_illegal'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [TriangleError, Triangle, 1, 1, 3])
    end

    klass.attrs['test_triangles_violating_triangle_inequality_are_illegal_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [TriangleError, Triangle, 2, 4, 2])
    end

    klass.attrs['test_triangles_violating_triangle_inequality_are_illegal_3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [TriangleError, Triangle, 7, 3, 2])
    end
  end

  TriangleTests = exports['TriangleTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
