# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_binary
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  parse_binary, _ = import('parse_binary', from: 'integration.source.binary')

  <<~__DOC__
  Tests for the binary exercise

  Implementation note:
  If the argument to parse_binary isn't a valid binary number the
  function should raise a ValueError with a meaningful error message.
  __DOC__

  exports['BinaryTests'] = Pyrb::PythonClass.new('BinaryTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_binary_1_is_decimal_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, parse_binary.call(["1"])])
    end

    klass.attrs['test_binary_10_is_decimal_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [2, parse_binary.call(["10"])])
    end

    klass.attrs['test_binary_11_is_decimal_3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [3, parse_binary.call(["11"])])
    end

    klass.attrs['test_binary_100_is_decimal_4'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [4, parse_binary.call(["100"])])
    end

    klass.attrs['test_binary_1001_is_decimal_9'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [9, parse_binary.call(["1001"])])
    end

    klass.attrs['test_binary_11010_is_decimal_26'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [26, parse_binary.call(["11010"])])
    end

    klass.attrs['test_binary_10001101000_is_decimal_1128'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1128, parse_binary.call(["10001101000"])])
    end

    klass.attrs['test_invalid_binary_text_only'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, parse_binary, "carrot"])
    end

    klass.attrs['test_invalid_binary_number_not_base2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, parse_binary, "102011"])
    end

    klass.attrs['test_invalid_binary_numbers_with_text'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, parse_binary, "10nope"])
    end

    klass.attrs['test_invalid_binary_text_with_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, parse_binary, "nope10"])
    end
  end

  BinaryTests = exports['BinaryTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
