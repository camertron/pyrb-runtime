# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_rna_transcription
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  to_rna, _ = import('to_rna', from: 'integration.source.dna')

  exports['DNATests'] = Pyrb::PythonClass.new('DNATests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_transcribes_guanine_to_cytosine'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["C", to_rna.call(["G"])])
    end

    klass.attrs['test_transcribes_cytosine_to_guanine'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["G", to_rna.call(["C"])])
    end

    klass.attrs['test_transcribes_thymine_to_adenine'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["A", to_rna.call(["T"])])
    end

    klass.attrs['test_transcribes_adenine_to_uracil'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["U", to_rna.call(["A"])])
    end

    klass.attrs['test_transcribes_all_occurences'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["UGCACCAGAAUU", to_rna.call(["ACGTGGTCTTAA"])])
    end
  end

  DNATests = exports['DNATests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
