# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_reverse_string
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  reverse, _ = import('reverse', from: 'integration.source.reverse_string')

  exports['ReverseStringTests'] = Pyrb::PythonClass.new('ReverseStringTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_empty_string'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [reverse.call([""]), ""])
    end

    klass.attrs['test_a_word'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [reverse.call(["robot"]), "tobor"])
    end

    klass.attrs['test_a_capitalized_word'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [reverse.call(["Ramen"]), "nemaR"])
    end

    klass.attrs['test_a_sentence_with_punctuation'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [reverse.call(["I\'m hungry!"]), "!yrgnuh m\'I"])
    end

    klass.attrs['test_a_palindrome'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [reverse.call(["racecar"]), "racecar"])
    end
  end

  ReverseStringTests = exports['ReverseStringTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
