# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_word_count
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  word_count, _ = import('word_count', from: 'integration.source.wordcount')

  exports['WordCountTests'] = Pyrb::PythonClass.new('WordCountTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_count_one_word'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"word" => 1}]), word_count.call(["word"])])
    end

    klass.attrs['test_count_one_of_each'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"one" => 1, "of" => 1, "each" => 1}]), word_count.call(["one of each"])])
    end

    klass.attrs['test_count_multiple_occurences'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"one" => 1, "fish" => 4, "two" => 1, "red" => 1, "blue" => 1}]), word_count.call(["one fish two fish red fish blue fish"])])
    end

    klass.attrs['test_preserves_punctuation'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"car" => 1, "carpet" => 1, "as" => 1, "java" => 1, ":" => 2, "javascript!!&@$%^&" => 1}]), word_count.call(["car : carpet as java : javascript!!&@$%^&"])])
    end

    klass.attrs['test_include_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"testing" => 2, "1" => 1, "2" => 1}]), word_count.call(["testing 1 2 testing"])])
    end

    klass.attrs['test_mixed_case'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"go" => 1, "Go" => 1, "GO" => 1}]), word_count.call(["go Go GO"])])
    end

    klass.attrs['test_multiple_spaces'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"wait" => 1, "for" => 1, "it" => 1}]), word_count.call(["wait for       it"])])
    end

    klass.attrs['test_newlines'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new([{"rah" => 2, "ah" => 3, "roma" => 2, "ma" => 1, "ga" => 2, "oh" => 1, "la" => 2, "want" => 1, "your" => 1, "bad" => 1, "romance" => 1}]), word_count.call([("rah rah ah ah ah\nroma roma ma\nga ga oh la la\n" + "want your bad romance")])])
    end
  end

  WordCountTests = exports['WordCountTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
