# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_binary_search
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  binary_search, _ = import('binary_search', from: 'integration.source.binary_search')

  exports['BinarySearchTest'] = Pyrb::PythonClass.new('BinarySearchTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_finds_value_in_array_with_one_element'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [binary_search.call([[6], 6]), 0])
    end

    klass.attrs['test_finds_value_in_middle_of_array'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [binary_search.call([[1, 3, 4, 6, 8, 9, 11], 6]), 3])
    end

    klass.attrs['test_finds_value_at_beginning_of_array'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [binary_search.call([[1, 3, 4, 6, 8, 9, 11], 1]), 0])
    end

    klass.attrs['test_finds_value_at_end_of_array'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [binary_search.call([[1, 3, 4, 6, 8, 9, 11], 11]), 6])
    end

    klass.attrs['test_finds_value_in_array_of_odd_length'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [binary_search.call([[1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 634], 144]), 9])
    end

    klass.attrs['test_finds_value_in_array_of_even_length'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [binary_search.call([[1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377], 21]), 5])
    end

    klass.attrs['test_identifies_value_missing'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        binary_search.call([[1, 3, 4, 6, 8, 9, 11], 7])
      end
    end

    klass.attrs['test_value_smaller_than_arrays_minimum'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        binary_search.call([[1, 3, 4, 6, 8, 9, 11], 0])
      end
    end

    klass.attrs['test_value_larger_than_arrays_maximum'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        binary_search.call([[1, 3, 4, 6, 8, 9, 11], 13])
      end
    end

    klass.attrs['test_empty_array'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        binary_search.call([[], 1])
      end
    end
  end

  BinarySearchTest = exports['BinarySearchTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
