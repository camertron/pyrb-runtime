# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_bracket_push
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  check_brackets, _ = import('check_brackets', from: 'integration.source.bracket_push')

  exports['BracketPushTests'] = Pyrb::PythonClass.new('BracketPushTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_input_empty'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call([""]), true])
    end

    klass.attrs['test_single'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["{}"]), true])
    end

    klass.attrs['test_unclosed'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["{{"]), false])
    end

    klass.attrs['test_wrong_order'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["}{"]), false])
    end

    klass.attrs['test_mixed_not_nested'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["{}[]"]), true])
    end

    klass.attrs['test_mixed_nested'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["{[]}"]), true])
    end

    klass.attrs['test_improperly_nested'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["{[}]"]), false])
    end

    klass.attrs['test_not_opened_nested'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["{[)][]}"]), false])
    end

    klass.attrs['test_nested_ensemble'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [check_brackets.call(["{[]([()])}"]), true])
    end
  end

  BracketPushTests = exports['BracketPushTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
