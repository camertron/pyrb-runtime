# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_twelve_days
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  sing, verse, verses = import('sing', 'verse', 'verses', from: 'integration.source.twelve_days')

  exports['TwelveDaysTests'] = Pyrb::PythonClass.new('TwelveDaysTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_verse1'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the first day of Christmas my true love gave to me, " + "a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([1])])
    end

    klass.attrs['test_verse2'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the second day of Christmas my true love gave to me, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([2])])
    end

    klass.attrs['test_verse3'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the third day of Christmas my true love gave to me, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([3])])
    end

    klass.attrs['test_verse4'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the fourth day of Christmas my true love gave to me, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([4])])
    end

    klass.attrs['test_verse5'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the fifth day of Christmas my true love gave to me, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([5])])
    end

    klass.attrs['test_verse6'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the sixth day of Christmas my true love gave to me, " + "six Geese-a-Laying, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([6])])
    end

    klass.attrs['test_verse7'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the seventh day of Christmas my true love gave to me, " + "seven Swans-a-Swimming, " + "six Geese-a-Laying, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([7])])
    end

    klass.attrs['test_verse8'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the eighth day of Christmas my true love gave to me, " + "eight Maids-a-Milking, " + "seven Swans-a-Swimming, " + "six Geese-a-Laying, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([8])])
    end

    klass.attrs['test_verse9'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the ninth day of Christmas my true love gave to me, " + "nine Ladies Dancing, " + "eight Maids-a-Milking, " + "seven Swans-a-Swimming, " + "six Geese-a-Laying, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([9])])
    end

    klass.attrs['test_verse10'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the tenth day of Christmas my true love gave to me, " + "ten Lords-a-Leaping, " + "nine Ladies Dancing, " + "eight Maids-a-Milking, " + "seven Swans-a-Swimming, " + "six Geese-a-Laying, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([10])])
    end

    klass.attrs['test_verse11'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the eleventh day of Christmas " + "my true love gave to me, " + "eleven Pipers Piping, " + "ten Lords-a-Leaping, " + "nine Ladies Dancing, " + "eight Maids-a-Milking, " + "seven Swans-a-Swimming, " + "six Geese-a-Laying, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([11])])
    end

    klass.attrs['test_verse12'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the twelfth day of Christmas my true love gave to me, " + "twelve Drummers Drumming, " + "eleven Pipers Piping, " + "ten Lords-a-Leaping, " + "nine Ladies Dancing, " + "eight Maids-a-Milking, " + "seven Swans-a-Swimming, " + "six Geese-a-Laying, " + "five Gold Rings, " + "four Calling Birds, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n"))
      self_.fcall('assertEqual', [expected, verse.call([12])])
    end

    klass.attrs['test_multiple_verses'] = Pyrb.defn(self: nil) do |self_|
      expected = (("On the first day of Christmas my true love gave to me, " + "a Partridge in a Pear Tree.\n\n" + "On the second day of Christmas my true love gave to me, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n\n" + "On the third day of Christmas my true love gave to me, " + "three French Hens, " + "two Turtle Doves, " + "and a Partridge in a Pear Tree.\n\n"))
      self_.fcall('assertEqual', [expected, verses.call([1, 3])])
    end

    klass.attrs['test_the_whole_song'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [verses.call([1, 12]), sing.call()])
    end
  end

  TwelveDaysTests = exports['TwelveDaysTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
