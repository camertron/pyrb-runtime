# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_meetup
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  date, _ = import('date', from: 'datetime')
  unittest, _ = import('unittest')
  meetup_day, _ = import('meetup_day', from: 'integration.source.meetup')

  exports['MeetupTest'] = Pyrb::PythonClass.new('MeetupTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_monteenth_of_may_2013'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2013, 5, 13]), meetup_day.call([2013, 5, "Monday", "teenth"])])
    end

    klass.attrs['test_saturteenth_of_february_2013'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2013, 2, 16]), meetup_day.call([2013, 2, "Saturday", "teenth"])])
    end

    klass.attrs['test_first_tuesday_of_may_2013'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2013, 5, 7]), meetup_day.call([2013, 5, "Tuesday", "1st"])])
    end

    klass.attrs['test_second_monday_of_april_2013'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2013, 4, 8]), meetup_day.call([2013, 4, "Monday", "2nd"])])
    end

    klass.attrs['test_third_thursday_of_september_2013'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2013, 9, 19]), meetup_day.call([2013, 9, "Thursday", "3rd"])])
    end

    klass.attrs['test_fourth_sunday_of_march_2013'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2013, 3, 24]), meetup_day.call([2013, 3, "Sunday", "4th"])])
    end

    klass.attrs['test_last_thursday_of_october_2013'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2013, 10, 31]), meetup_day.call([2013, 10, "Thursday", "last"])])
    end

    klass.attrs['test_last_wednesday_of_february_2012'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2012, 2, 29]), meetup_day.call([2012, 2, "Wednesday", "last"])])
    end
  end

  MeetupTest = exports['MeetupTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
