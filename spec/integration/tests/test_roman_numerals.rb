# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_roman_numerals
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  roman, _ = import('integration.source.roman')

  exports['RomanTest'] = Pyrb::PythonClass.new('RomanTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['numerals'] = Pyrb::Dict.new([{1 => "I", 2 => "II", 3 => "III", 4 => "IV", 5 => "V", 6 => "VI", 9 => "IX", 27 => "XXVII", 48 => "XLVIII", 59 => "LIX", 93 => "XCIII", 141 => "CXLI", 163 => "CLXIII", 402 => "CDII", 575 => "DLXXV", 911 => "CMXI", 1024 => "MXXIV", 3000 => "MMM", }])
    klass.attrs['test_numerals'] = Pyrb.defn(self: nil) do |self_|
      (self_.attr('numerals').fcall('items')).each do |arabic, numeral|
        self_.fcall('assertEqual', [numeral, roman.fcall('numeral', [arabic])])
      end
    end
  end

  RomanTest = exports['RomanTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
