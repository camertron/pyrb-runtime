# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_run_length
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  encode, decode = import('encode', 'decode', from: 'integration.source.run_length')

  exports['WordCountTests'] = Pyrb::PythonClass.new('WordCountTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_encode'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["2A3B4C", encode.call(["AABBBCCCC"])])
    end

    klass.attrs['test_decode'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["AABBBCCCC", decode.call(["2A3B4C"])])
    end

    klass.attrs['test_encode_with_single'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["12WB12W3B24WB", encode.call(["WWWWWWWWWWWWBWWWWWWWWWWWWBBBWWWWWWWWWWWWWWWWWWWWWWWWB"])])
    end

    klass.attrs['test_decode_with_single'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["WWWWWWWWWWWWBWWWWWWWWWWWWBBBWWWWWWWWWWWWWWWWWWWWWWWWB", decode.call(["12WB12W3B24WB"])])
    end

    klass.attrs['test_combination'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["zzz ZZ  zZ", decode.call([encode.call(["zzz ZZ  zZ"])])])
    end

    klass.attrs['test_encode_unicode_s'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["⏰3⚽2⭐⏰", encode.call(["⏰⚽⚽⚽⭐⭐⏰"])])
    end

    klass.attrs['test_decode_unicode'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["⏰⚽⚽⚽⭐⭐⏰", decode.call(["⏰3⚽2⭐⏰"])])
    end
  end

  WordCountTests = exports['WordCountTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
