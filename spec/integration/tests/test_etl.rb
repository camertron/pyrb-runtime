# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_etl
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  etl, _ = import('integration.source.etl')

  exports['TransformTest'] = Pyrb::PythonClass.new('TransformTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_transform_one_value'] = Pyrb.defn(self: nil) do |self_|
      old = Pyrb::Dict.new([{1 => ["WORLD"]}])
      expected = Pyrb::Dict.new([{"world" => 1}])
      self_.fcall('assertEqual', [expected, etl.fcall('transform', [old])])
    end

    klass.attrs['test_transform_more_values'] = Pyrb.defn(self: nil) do |self_|
      old = Pyrb::Dict.new([{1 => ["WORLD", "GSCHOOLERS"]}])
      expected = Pyrb::Dict.new([{"world" => 1, "gschoolers" => 1}])
      self_.fcall('assertEqual', [expected, etl.fcall('transform', [old])])
    end

    klass.attrs['test_more_keys'] = Pyrb.defn(self: nil) do |self_|
      old = Pyrb::Dict.new([{1 => ["APPLE", "ARTICHOKE"], 2 => ["BOAT", "BALLERINA"]}])
      expected = Pyrb::Dict.new([{"apple" => 1, "artichoke" => 1, "boat" => 2, "ballerina" => 2}])
      self_.fcall('assertEqual', [expected, etl.fcall('transform', [old])])
    end

    klass.attrs['test_full_dataset'] = Pyrb.defn(self: nil) do |self_|
      old = Pyrb::Dict.new([{1 => "AEIOULNRST", 2 => "DG", 3 => "BCMP", 4 => "FHVWY", 5 => "K", 8 => "JX", 10 => "QZ", }])
      expected = Pyrb::Dict.new([{"a" => 1, "b" => 3, "c" => 3, "d" => 2, "e" => 1, "f" => 4, "g" => 2, "h" => 4, "i" => 1, "j" => 8, "k" => 5, "l" => 1, "m" => 3, "n" => 1, "o" => 1, "p" => 3, "q" => 10, "r" => 1, "s" => 1, "t" => 1, "u" => 1, "v" => 4, "w" => 4, "x" => 8, "y" => 4, "z" => 10}])
      self_.fcall('assertEqual', [expected, etl.fcall('transform', [old])])
    end
  end

  TransformTest = exports['TransformTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
