# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_gigasecond
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  date, _ = import('date', from: 'datetime')
  unittest, _ = import('unittest')
  add_gigasecond, _ = import('add_gigasecond', from: 'integration.source.gigasecond')

  exports['GigasecondTest'] = Pyrb::PythonClass.new('GigasecondTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2043, 1, 1]), add_gigasecond.call([date.call([2011, 4, 25])])])
    end

    klass.attrs['test_2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([2009, 2, 19]), add_gigasecond.call([date.call([1977, 6, 13])])])
    end

    klass.attrs['test_3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [date.call([1991, 3, 27]), add_gigasecond.call([date.call([1959, 7, 19])])])
    end

    klass.attrs['test_yourself'] = Pyrb.defn(self: nil) do |self_|
      your_birthday = date.call([1994, 7, 18])
      your_gigasecond = date.call([2026, 3, 26])
      self_.fcall('assertEqual', [your_gigasecond, add_gigasecond.call([your_birthday])])
    end
  end

  GigasecondTest = exports['GigasecondTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
