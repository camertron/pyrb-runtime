# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_circular_buffer
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  CircularBuffer, BufferFullException, BufferEmptyException = import('CircularBuffer', 'BufferFullException', 'BufferEmptyException', from: 'integration.source.circular_buffer')

  exports['CircularBufferTest'] = Pyrb::PythonClass.new('CircularBufferTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_read_empty_buffer'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([1])
      Pyrb.with.call([self_.fcall('assertRaises', [BufferEmptyException])]) do
        buf.fcall('read')
      end
    end

    klass.attrs['test_write_and_read_back_one_item'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([1])
      buf.fcall('write', ["1"])
      self_.fcall('assertEqual', ["1", buf.fcall('read')])
      Pyrb.with.call([self_.fcall('assertRaises', [BufferEmptyException])]) do
        buf.fcall('read')
      end
    end

    klass.attrs['test_write_and_read_back_multiple_items'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([2])
      buf.fcall('write', ["1"])
      buf.fcall('write', ["2"])
      self_.fcall('assertEqual', ["1", buf.fcall('read')])
      self_.fcall('assertEqual', ["2", buf.fcall('read')])
      Pyrb.with.call([self_.fcall('assertRaises', [BufferEmptyException])]) do
        buf.fcall('read')
      end
    end

    klass.attrs['test_clearing_buffer'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([3])
      ("123").each_char do |c|
        buf.fcall('write', [c])
      end
      buf.fcall('clear')
      Pyrb.with.call([self_.fcall('assertRaises', [BufferEmptyException])]) do
        buf.fcall('read')
      end
      buf.fcall('write', ["1"])
      buf.fcall('write', ["2"])
      self_.fcall('assertEqual', ["1", buf.fcall('read')])
      buf.fcall('write', ["3"])
      self_.fcall('assertEqual', ["2", buf.fcall('read')])
    end

    klass.attrs['test_alternate_write_and_read'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([2])
      buf.fcall('write', ["1"])
      self_.fcall('assertEqual', ["1", buf.fcall('read')])
      buf.fcall('write', ["2"])
      self_.fcall('assertEqual', ["2", buf.fcall('read')])
    end

    klass.attrs['test_read_back_oldest_item'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([3])
      buf.fcall('write', ["1"])
      buf.fcall('write', ["2"])
      buf.fcall('read')
      buf.fcall('write', ["3"])
      buf.fcall('read')
      self_.fcall('assertEqual', ["3", buf.fcall('read')])
    end

    klass.attrs['test_write_full_buffer'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([2])
      buf.fcall('write', ["1"])
      buf.fcall('write', ["2"])
      Pyrb.with.call([self_.fcall('assertRaises', [BufferFullException])]) do
        buf.fcall('write', ["A"])
      end
    end

    klass.attrs['test_overwrite_full_buffer'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([2])
      buf.fcall('write', ["1"])
      buf.fcall('write', ["2"])
      buf.fcall('overwrite', ["A"])
      self_.fcall('assertEqual', ["2", buf.fcall('read')])
      self_.fcall('assertEqual', ["A", buf.fcall('read')])
      Pyrb.with.call([self_.fcall('assertRaises', [BufferEmptyException])]) do
        buf.fcall('read')
      end
    end

    klass.attrs['test_overwrite_non_full_buffer'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([2])
      buf.fcall('overwrite', ["1"])
      buf.fcall('overwrite', ["2"])
      self_.fcall('assertEqual', ["1", buf.fcall('read')])
      self_.fcall('assertEqual', ["2", buf.fcall('read')])
      Pyrb.with.call([self_.fcall('assertRaises', [BufferEmptyException])]) do
        buf.fcall('read')
      end
    end

    klass.attrs['test_alternate_read_and_overwrite'] = Pyrb.defn(self: nil) do |self_|
      buf = CircularBuffer.call([5])
      ("123").each_char do |c|
        buf.fcall('write', [c])
      end
      buf.fcall('read')
      buf.fcall('read')
      buf.fcall('write', ["4"])
      buf.fcall('read')
      ("5678").each_char do |c|
        buf.fcall('write', [c])
      end
      buf.fcall('overwrite', ["A"])
      buf.fcall('overwrite', ["B"])
      self_.fcall('assertEqual', ["6", buf.fcall('read')])
      self_.fcall('assertEqual', ["7", buf.fcall('read')])
      self_.fcall('assertEqual', ["8", buf.fcall('read')])
      self_.fcall('assertEqual', ["A", buf.fcall('read')])
      self_.fcall('assertEqual', ["B", buf.fcall('read')])
      Pyrb.with.call([self_.fcall('assertRaises', [BufferEmptyException])]) do
        buf.fcall('read')
      end
    end
  end

  CircularBufferTest = exports['CircularBufferTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
