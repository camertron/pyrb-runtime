# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_series
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  slices, _ = import('slices', from: 'integration.source.series')

  <<~__DOC__
  Tests for the series exercise

  Implementation note:
  The slices function should raise a ValueError with a meaningful error
  message if its length argument doesn't fit the series.
  __DOC__

  exports['SeriesTest'] = Pyrb::PythonClass.new('SeriesTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_slices_of_one'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[[0], [1], [2], [3], [4]], slices.call(["01234", 1])])
    end

    klass.attrs['test_slices_of_two'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[[9, 7], [7, 8], [8, 6], [6, 7], [7, 5], [5, 6], [6, 4]], slices.call(["97867564", 2])])
    end

    klass.attrs['test_slices_of_three'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[[9, 7, 8], [7, 8, 6], [8, 6, 7], [6, 7, 5], [7, 5, 6], [5, 6, 4]], slices.call(["97867564", 3])])
    end

    klass.attrs['test_slices_of_four'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[[0, 1, 2, 3], [1, 2, 3, 4]], slices.call(["01234", 4])])
    end

    klass.attrs['test_slices_of_five'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [[[0, 1, 2, 3, 4]], slices.call(["01234", 5])])
    end

    klass.attrs['test_overly_long_slice'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        slices.call(["012", 4])
      end
    end

    klass.attrs['test_overly_short_slice'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        slices.call(["01234", 0])
      end
    end
  end

  SeriesTest = exports['SeriesTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
