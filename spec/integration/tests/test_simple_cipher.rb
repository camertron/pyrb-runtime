# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_simple_cipher
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Caesar, Cipher = import('Caesar', 'Cipher', from: 'integration.source.cipher')

  exports['CipherTest'] = Pyrb::PythonClass.new('CipherTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_caesar_encode1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["lwlvdzhvrphsurjudpplqjlqsbwkrq", Caesar.call().fcall('encode', ["itisawesomeprogramminginpython"])])
    end

    klass.attrs['test_caesar_encode2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["yhqlylglylfl", Caesar.call().fcall('encode', ["venividivici"])])
    end

    klass.attrs['test_caesar_encode3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["wzdvwkhqljkwehiruhfkulvwpdv", Caesar.call().fcall('encode', ["\'Twas the night before Christmas"])])
    end

    klass.attrs['test_caesar_encode_with_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["jr", Caesar.call().fcall('encode', ["1, 2, 3, Go!"])])
    end

    klass.attrs['test_caesar_decode'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["venividivici", Caesar.call().fcall('decode', ["yhqlylglylfl"])])
    end

    klass.attrs['test_cipher_encode1'] = Pyrb.defn(self: nil) do |self_|
      c = Cipher.call(["a"])
      self_.fcall('assertEqual', ["itisawesomeprogramminginpython", c.fcall('encode', ["itisawesomeprogramminginpython"])])
    end

    klass.attrs['test_cipher_encode2'] = Pyrb.defn(self: nil) do |self_|
      c = Cipher.call(["aaaaaaaaaaaaaaaaaaaaaa"])
      self_.fcall('assertEqual', ["itisawesomeprogramminginpython", c.fcall('encode', ["itisawesomeprogramminginpython"])])
    end

    klass.attrs['test_cipher_encode3'] = Pyrb.defn(self: nil) do |self_|
      c = Cipher.call(["dddddddddddddddddddddd"])
      self_.fcall('assertEqual', ["yhqlylglylfl", c.fcall('encode', ["venividivici"])])
    end

    klass.attrs['test_cipher_encode4'] = Pyrb.defn(self: nil) do |self_|
      key = (("duxrceqyaimciuucnelkeoxjhdyduucpmrxmaivacmybmsdrzwqxvbxsy" + "gzsabdjmdjabeorttiwinfrpmpogvabiofqexnohrqu"))
      c = Cipher.call([key])
      self_.fcall('assertEqual', ["gccwkixcltycv", c.fcall('encode', ["diffiehellman"])])
    end

    klass.attrs['test_cipher_encode_short_key'] = Pyrb.defn(self: nil) do |self_|
      c = Cipher.call(["abcd"])
      self_.fcall('assertEqual', ["abcdabcd", c.fcall('encode', ["aaaaaaaa"])])
    end

    klass.attrs['test_cipher_compositiion1'] = Pyrb.defn(self: nil) do |self_|
      key = (("duxrceqyaimciuucnelkeoxjhdyduucpmrxmaivacmybmsdrzwqxvbxsy" + "gzsabdjmdjabeorttiwinfrpmpogvabiofqexnohrqu"))
      plaintext = "adaywithoutlaughterisadaywasted"
      c = Cipher.call([key])
      self_.fcall('assertEqual', [plaintext, c.fcall('decode', [c.fcall('encode', [plaintext])])])
    end

    klass.attrs['test_cipher_compositiion2'] = Pyrb.defn(self: nil) do |self_|
      plaintext = "adaywithoutlaughterisadaywasted"
      c = Cipher.call()
      self_.fcall('assertEqual', [plaintext, c.fcall('decode', [c.fcall('encode', [plaintext])])])
    end

    klass.attrs['test_cipher_random_key'] = Pyrb.defn(self: nil) do |self_|
      c = Cipher.call()
      self_.fcall('assertTrue', [Pyrb.len.call([c.attr('key')]) >= 100, "A random key must be generated when no key is given!"])
      self_.fcall('assertTrue', [c.attr('key').fcall('islower').and { c.attr('key').fcall('isalpha') }, "All items in the key must be chars and lowercase!"])
    end

    klass.attrs['test_cipher_wrong_key'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertRaises', [Pyrb::ValueError, Cipher, "a1cde"])
      self_.fcall('assertRaises', [Pyrb::ValueError, Cipher, "aBcde"])
    end
  end

  CipherTest = exports['CipherTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
