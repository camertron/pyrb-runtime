# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_say
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  say, _ = import('say', from: 'integration.source.say')

  exports['SayTest'] = Pyrb::PythonClass.new('SayTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_one'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one", say.call([1])])
    end

    klass.attrs['test_fourteen'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["fourteen", say.call([14])])
    end

    klass.attrs['test_twenty'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["twenty", say.call([20])])
    end

    klass.attrs['test_twenty_two'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["twenty-two", say.call([22])])
    end

    klass.attrs['test_one_hundred'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one hundred", say.call([100])])
    end

    klass.attrs['test_one_hundred_twenty'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one hundred and twenty", say.call([120])])
    end

    klass.attrs['test_one_hundred_twenty_three'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one hundred and twenty-three", say.call([123])])
    end

    klass.attrs['test_one_thousand'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one thousand", say.call([1000])])
    end

    klass.attrs['test_one_thousand_two_hundred_thirty_four'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one thousand two hundred and thirty-four", say.call([1234])])
    end

    klass.attrs['test_one_million'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one million", say.call([1e6])])
    end

    klass.attrs['test_one_million_two'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one million and two", say.call([1000002])])
    end

    klass.attrs['test_1002345'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one million two thousand three hundred and forty-five", say.call([1002345])])
    end

    klass.attrs['test_one_billion'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["one billion", say.call([1e9])])
    end

    klass.attrs['test_number_to_large'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::AttributeError])]) do
        say.call([1e12])
      end
    end

    klass.attrs['test_number_negative'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::AttributeError])]) do
        say.call([-42])
      end
    end

    klass.attrs['test_zero'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["zero", say.call([0])])
    end

    klass.attrs['test_987654321123'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["nine hundred and eighty-seven billion " + "six hundred and fifty-four million " + "three hundred and twenty-one thousand " + "one hundred and twenty-three", say.call([987654321123])])
    end
  end

  SayTest = exports['SayTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
