# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_saddle_points
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  saddle_points, _ = import('saddle_points', from: 'integration.source.saddle_points')

  <<~__DOC__
  Tests for the saddle-points exercise

  Implementation note:
  The saddle_points function must validate the input matrix and raise a
  ValueError with a meaningful error message if the matrix turns out to be
  irregular.
  __DOC__

  exports['SaddlePointTest'] = Pyrb::PythonClass.new('SaddlePointTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_one_saddle'] = Pyrb.defn(self: nil) do |self_|
      inp = [[9, 8, 7], [5, 3, 2], [6, 6, 7]]
      self_.fcall('assertEqual', [Pyrb::Set.call([[[1, 0]]]), saddle_points.call([inp])])
    end

    klass.attrs['test_no_saddle'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Set.call(), saddle_points.call([[[2, 1], [1, 2]]])])
    end

    klass.attrs['test_mult_saddle'] = Pyrb.defn(self: nil) do |self_|
      inp = [[5, 3, 5, 4], [6, 4, 7, 3], [5, 1, 5, 3]]
      ans = Pyrb::Set.call([[[0, 0], [0, 2], [2, 0], [2, 2]]])
      self_.fcall('assertEqual', [ans, saddle_points.call([inp])])
    end

    klass.attrs['test_empty_matrix'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Set.call(), saddle_points.call([[]])])
    end

    klass.attrs['test_irregular_matrix'] = Pyrb.defn(self: nil) do |self_|
      inp = [[3, 2, 1], [0, 1], [2, 1, 0]]
      self_.fcall('assertRaises', [Pyrb::ValueError, saddle_points, inp])
    end
  end

  SaddlePointTest = exports['SaddlePointTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
