# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_rail_fence_cipher
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  encode, decode = import('encode', 'decode', from: 'integration.source.rail_fence_cipher')

  exports['RailFenceTests'] = Pyrb::PythonClass.new('RailFenceTests', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_encode_with_two_rails'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["XXXXXXXXXOOOOOOOOO", encode.call(["XOXOXOXOXOXOXOXOXO", 2])])
    end

    klass.attrs['test_encode_with_three_rails'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["WECRLTEERDSOEEFEAOCAIVDEN", encode.call(["WEAREDISCOVEREDFLEEATONCE", 3])])
    end

    klass.attrs['test_encode_with_middle_stop'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["ESXIEECSR", encode.call(["EXERCISES", 4])])
    end

    klass.attrs['test_decode_with_three_rails'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["THEDEVILISINTHEDETAILS", decode.call(["TEITELHDVLSNHDTISEIIEA", 3])])
    end

    klass.attrs['test_decode_with_five_rails'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["EXERCISMISAWESOME", decode.call(["EIEXMSMESAORIWSCE", 5])])
    end

    klass.attrs['test_decode_with_six_rails'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertMultiLineEqual', ["112358132134558914423337761098715972584418167651094617711286", decode.call(["133714114238148966225439541018335470986172518171757571896261", 6])])
    end
  end

  RailFenceTests = exports['RailFenceTests']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
