# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_matrix
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Matrix, _ = import('Matrix', from: 'integration.source.matrix')

  exports['MatrixTest'] = Pyrb::PythonClass.new('MatrixTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_extract_a_row'] = Pyrb.defn(self: nil) do |self_|
      matrix = Matrix.call(["1 2\n10 20"])
      self_.fcall('assertEqual', [[1, 2], matrix.attr('rows')[0]])
    end

    klass.attrs['test_extract_same_row_again'] = Pyrb.defn(self: nil) do |self_|
      matrix = Matrix.call(["9 7\n8 6"])
      self_.fcall('assertEqual', [[9, 7], matrix.attr('rows')[0]])
    end

    klass.attrs['test_extract_other_row'] = Pyrb.defn(self: nil) do |self_|
      matrix = Matrix.call(["9 8 7\n19 18 17"])
      self_.fcall('assertEqual', [[19, 18, 17], matrix.attr('rows')[1]])
    end

    klass.attrs['test_extract_other_row_again'] = Pyrb.defn(self: nil) do |self_|
      matrix = Matrix.call(["1 4 9\n16 25 36"])
      self_.fcall('assertEqual', [[16, 25, 36], matrix.attr('rows')[1]])
    end

    klass.attrs['test_extract_a_column'] = Pyrb.defn(self: nil) do |self_|
      matrix = Matrix.call(["1 2 3\n4 5 6\n7 8 9\n8 7 6"])
      self_.fcall('assertEqual', [[1, 4, 7, 8], matrix.attr('columns')[0]])
    end

    klass.attrs['test_extract_another_column'] = Pyrb.defn(self: nil) do |self_|
      matrix = Matrix.call(["89 1903 3\n18 3 1\n9 4 800"])
      self_.fcall('assertEqual', [[1903, 3, 4], matrix.attr('columns')[1]])
    end
  end

  MatrixTest = exports['MatrixTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
