# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_grade_school
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  Sequence, _ = import('Sequence', from: 'collections')
  GeneratorType, _ = import('GeneratorType', from: 'types')
  unittest, _ = import('unittest')
  School, _ = import('School', from: 'integration.source.school')

  exports['SchoolTest'] = Pyrb::PythonClass.new('SchoolTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['setUp'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['school'] = School.call(["Haleakala Hippy School"])
    end

    klass.attrs['test_an_empty_school'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Dict.new, self_.attr('school').attr('db')])
    end

    klass.attrs['test_add_student'] = Pyrb.defn(self: nil) do |self_|
      self_.attr('school').fcall('add', ["Aimee", 2])
      self_.fcall('assertEqual', [Pyrb::Dict.new([{2 => Pyrb::Set.new([["Aimee"]])}]), self_.attr('school').attr('db')])
    end

    klass.attrs['test_add_more_students_in_same_class'] = Pyrb.defn(self: nil) do |self_|
      self_.attr('school').fcall('add', ["James", 2])
      self_.attr('school').fcall('add', ["Blair", 2])
      self_.attr('school').fcall('add', ["Paul", 2])
      self_.fcall('assertEqual', [Pyrb::Dict.new([{2 => Pyrb::Set.new([["James", "Blair", "Paul"]])}]), self_.attr('school').attr('db')])
    end

    klass.attrs['test_add_students_to_different_grades'] = Pyrb.defn(self: nil) do |self_|
      self_.attr('school').fcall('add', ["Chelsea", 3])
      self_.attr('school').fcall('add', ["Logan", 7])
      self_.fcall('assertEqual', [Pyrb::Dict.new([{3 => Pyrb::Set.new([["Chelsea"]]), 7 => Pyrb::Set.new([["Logan"]])}]), self_.attr('school').attr('db')])
    end

    klass.attrs['test_get_students_in_a_grade'] = Pyrb.defn(self: nil) do |self_|
      self_.attr('school').fcall('add', ["Franklin", 5])
      self_.attr('school').fcall('add', ["Bradley", 5])
      self_.attr('school').fcall('add', ["Jeff", 1])
      self_.fcall('assertEqual', [Pyrb::Set.new([["Franklin", "Bradley"]]), self_.attr('school').fcall('grade', [5])])
    end

    klass.attrs['test_get_students_in_a_non_existant_grade'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Pyrb::Set.call(), self_.attr('school').fcall('grade', [1])])
    end

    klass.attrs['test_sort_school'] = Pyrb.defn(self: nil) do |self_|
      students = [[3, ["Kyle", ]], [4, ["Christopher", "Jennifer", ]], [6, ["Kareem", ]]]
      (students).each do |grade, students_in_grade|
        (students_in_grade).each do |student|
          self_.attr('school').fcall('add', [student, grade])
        end
      end
      result = self_.attr('school').fcall('sort')
      self_.fcall('assertTrue', [Pyrb.isinstance.call([result, Sequence]).or { Pyrb.isinstance.call([result, GeneratorType]) }.or { Pyrb.callable.call([Pyrb.getattr.call([result, "__reversed__", false])]) }])
      result_list = Pyrb::List.call([!!(Pyrb.hasattr.call([result, "items"])) ? result.fcall('items') : result])
      self_.fcall('assertEqual', [result_list, students])
    end
  end

  SchoolTest = exports['SchoolTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
