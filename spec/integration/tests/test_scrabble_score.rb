# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_scrabble_score
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  score, _ = import('score', from: 'integration.source.scrabble')

  exports['WordTest'] = Pyrb::PythonClass.new('WordTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_invalid_word_scores_zero'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [0, score.call([""])])
      self_.fcall('assertEqual', [0, score.call([" \t\n"])])
      self_.fcall('assertEqual', [0, score.call(["hous3"])])
      self_.fcall('assertEqual', [0, score.call(["wo rd"])])
    end

    klass.attrs['test_scores_very_short_word'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, score.call(["a"])])
    end

    klass.attrs['test_scores_other_very_short_word'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [4, score.call(["f"])])
    end

    klass.attrs['test_simple_word_scores_the_number_of_letters'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [6, score.call(["street"])])
    end

    klass.attrs['test_complicated_word_scores_more'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [22, score.call(["quirky"])])
    end

    klass.attrs['test_scores_are_case_insensitive'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [41, score.call(["OxyphenButazone"])])
    end
  end

  WordTest = exports['WordTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
