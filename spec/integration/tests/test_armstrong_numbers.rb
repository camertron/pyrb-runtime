# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_armstrong_numbers
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  is_armstrong, _ = import('is_armstrong', from: 'integration.source.armstrong_numbers')

  exports['ArmstrongNumbersTest'] = Pyrb::PythonClass.new('ArmstrongNumbersTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_single_digit_numbers_are_armstrong_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([5]), true])
    end

    klass.attrs['test_there_are_no_two_digit_armstrong_numbers'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([10]), false])
    end

    klass.attrs['test_three_digit_number_that_is_an_armstrong_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([153]), true])
    end

    klass.attrs['test_three_digit_number_that_is_not_an_armstrong_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([100]), false])
    end

    klass.attrs['test_four_digit_number_that_is_an_armstrong_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([9474]), true])
    end

    klass.attrs['test_four_digit_number_that_is_not_an_armstrong_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([9475]), false])
    end

    klass.attrs['test_seven_digit_number_that_is_an_armstrong_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([9926315]), true])
    end

    klass.attrs['test_seven_digit_number_that_is_not_an_armstrong_number'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_armstrong.call([9926314]), false])
    end
  end

  ArmstrongNumbersTest = exports['ArmstrongNumbersTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
