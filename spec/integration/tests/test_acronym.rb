# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_acronym
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  abbreviate, _ = import('abbreviate', from: 'integration.source.acronym')

  exports['AcronymTest'] = Pyrb::PythonClass.new('AcronymTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_basic'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["PNG", abbreviate.call(["Portable Network Graphics"])])
    end

    klass.attrs['test_lowercase_words'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["ROR", abbreviate.call(["Ruby on Rails"])])
    end

    klass.attrs['test_camelcase'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["HTML", abbreviate.call(["HyperText Markup Language"])])
    end

    klass.attrs['test_punctuation'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["FIFO", abbreviate.call(["First In, First Out"])])
    end

    klass.attrs['test_all_caps_words'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["PHP", abbreviate.call(["PHP: Hypertext Preprocessor"])])
    end

    klass.attrs['test_hyphenated'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["CMOS", abbreviate.call(["Complementary metal-oxide semiconductor"])])
    end
  end

  AcronymTest = exports['AcronymTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
