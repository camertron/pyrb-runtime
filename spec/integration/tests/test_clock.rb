# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_clock
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  Clock, _ = import('Clock', from: 'integration.source.clock')

  exports['ClockTest'] = Pyrb::PythonClass.new('ClockTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_on_the_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["08:00", Pyrb::Str.call([Clock.call([8, 0])])])
    end

    klass.attrs['test_past_the_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["11:09", Pyrb::Str.call([Clock.call([11, 9])])])
    end

    klass.attrs['test_midnight_is_zero_hours'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["00:00", Pyrb::Str.call([Clock.call([24, 0])])])
    end

    klass.attrs['test_hour_rolls_over'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["01:00", Pyrb::Str.call([Clock.call([25, 0])])])
    end

    klass.attrs['test_hour_rolls_over_continuously'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["04:00", Pyrb::Str.call([Clock.call([100, 0])])])
    end

    klass.attrs['test_sixty_minutes_is_next_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["02:00", Pyrb::Str.call([Clock.call([1, 60])])])
    end

    klass.attrs['test_minutes_roll_over'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["02:40", Pyrb::Str.call([Clock.call([0, 160])])])
    end

    klass.attrs['test_minutes_roll_over_continuously'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["04:43", Pyrb::Str.call([Clock.call([0, 1723])])])
    end

    klass.attrs['test_hour_and_minutes_roll_over'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["03:40", Pyrb::Str.call([Clock.call([25, 160])])])
    end

    klass.attrs['test_hour_and_minutes_roll_over_continuously'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["11:01", Pyrb::Str.call([Clock.call([201, 3001])])])
    end

    klass.attrs['test_hour_and_minutes_roll_over_to_exactly_midnight'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["00:00", Pyrb::Str.call([Clock.call([72, 8640])])])
    end

    klass.attrs['test_negative_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["23:15", Pyrb::Str.call([Clock.call([-1, 15])])])
    end

    klass.attrs['test_negative_hour_rolls_over'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["23:00", Pyrb::Str.call([Clock.call([-25, 0])])])
    end

    klass.attrs['test_negative_hour_rolls_over_continuously'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["05:00", Pyrb::Str.call([Clock.call([-91, 0])])])
    end

    klass.attrs['test_negative_minutes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["00:20", Pyrb::Str.call([Clock.call([1, -40])])])
    end

    klass.attrs['test_negative_minutes_roll_over'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["22:20", Pyrb::Str.call([Clock.call([1, -160])])])
    end

    klass.attrs['test_negative_minutes_roll_over_continuously'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["16:40", Pyrb::Str.call([Clock.call([1, -4820])])])
    end

    klass.attrs['test_negative_hour_and_minutes_both_roll_over'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["20:20", Pyrb::Str.call([Clock.call([-25, -160])])])
    end

    klass.attrs['test_negative_hour_and_minutes_both_roll_over_continuously'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["22:10", Pyrb::Str.call([Clock.call([-121, -5810])])])
    end

    klass.attrs['test_add_minutes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["10:03", Pyrb::Str.call([Clock.call([10, 0]).fcall('add', [3])])])
    end

    klass.attrs['test_add_no_minutes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["06:41", Pyrb::Str.call([Clock.call([6, 41]).fcall('add', [0])])])
    end

    klass.attrs['test_add_to_next_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["01:25", Pyrb::Str.call([Clock.call([0, 45]).fcall('add', [40])])])
    end

    klass.attrs['test_add_more_than_one_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["11:01", Pyrb::Str.call([Clock.call([10, 0]).fcall('add', [61])])])
    end

    klass.attrs['test_add_more_than_two_hours_with_carry'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["03:25", Pyrb::Str.call([Clock.call([0, 45]).fcall('add', [160])])])
    end

    klass.attrs['test_add_across_midnight'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["00:01", Pyrb::Str.call([Clock.call([23, 59]).fcall('add', [2])])])
    end

    klass.attrs['test_add_more_than_one_day'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["06:32", Pyrb::Str.call([Clock.call([5, 32]).fcall('add', [1500])])])
    end

    klass.attrs['test_add_more_than_two_days'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["11:21", Pyrb::Str.call([Clock.call([1, 1]).fcall('add', [3500])])])
    end

    klass.attrs['test_subtract_minutes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["10:00", Pyrb::Str.call([Clock.call([10, 3]).fcall('add', [-3])])])
    end

    klass.attrs['test_subtract_to_previous_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["10:00", Pyrb::Str.call([Clock.call([10, 3]).fcall('add', [-3])])])
    end

    klass.attrs['test_subtract_more_than_an_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["09:33", Pyrb::Str.call([Clock.call([10, 3]).fcall('add', [-30])])])
    end

    klass.attrs['test_subtract_across_midnight'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["08:53", Pyrb::Str.call([Clock.call([10, 3]).fcall('add', [-70])])])
    end

    klass.attrs['test_subtract_more_than_two_hours'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["21:20", Pyrb::Str.call([Clock.call([0, 0]).fcall('add', [-160])])])
    end

    klass.attrs['test_subtract_more_than_two_hours_with_borrow'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["03:35", Pyrb::Str.call([Clock.call([6, 15]).fcall('add', [-160])])])
    end

    klass.attrs['test_subtract_more_than_one_day'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["04:32", Pyrb::Str.call([Clock.call([5, 32]).fcall('add', [-1500])])])
    end

    klass.attrs['test_subtract_more_than_two_days'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', ["00:20", Pyrb::Str.call([Clock.call([2, 20]).fcall('add', [-3000])])])
    end

    klass.attrs['test_clocks_with_same_time'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([15, 37]), Clock.call([15, 37])])
    end

    klass.attrs['test_clocks_a_minute_apart'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertNotEqual', [Clock.call([15, 36]), Clock.call([15, 37])])
    end

    klass.attrs['test_clocks_an_hour_apart'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertNotEqual', [Clock.call([14, 37]), Clock.call([15, 37])])
    end

    klass.attrs['test_clocks_with_hour_overflow'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([10, 37]), Clock.call([34, 37])])
    end

    klass.attrs['test_clocks_with_hour_overflow_by_several_days'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([3, 11]), Clock.call([99, 11])])
    end

    klass.attrs['test_clocks_with_negative_hour'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([22, 40]), Clock.call([-2, 40])])
    end

    klass.attrs['test_clocks_with_negative_hour_that_wraps'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([17, 3]), Clock.call([-31, 3])])
    end

    klass.attrs['test_clocks_with_negative_hour_that_wraps_multiple_times'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([13, 49]), Clock.call([-83, 49])])
    end

    klass.attrs['test_clocks_with_minute_overflow'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([0, 1]), Clock.call([0, 1441])])
    end

    klass.attrs['test_clocks_with_minute_overflow_by_several_days'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([2, 2]), Clock.call([2, 4322])])
    end

    klass.attrs['test_clocks_with_negative_minute'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([2, 40]), Clock.call([3, -20])])
    end

    klass.attrs['test_clocks_with_negative_minute_that_wraps'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([4, 10]), Clock.call([5, -1490])])
    end

    klass.attrs['test_clocks_with_negative_minute_that_wraps_multiple_times'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([6, 15]), Clock.call([6, -4305])])
    end

    klass.attrs['test_clocks_with_negative_hours_and_minutes'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([7, 32]), Clock.call([-12, -268])])
    end

    klass.attrs['test_clocks_with_negative_hours_and_minutes_that_wrap'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [Clock.call([18, 7]), Clock.call([-54, -11513])])
    end
  end

  ClockTest = exports['ClockTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
