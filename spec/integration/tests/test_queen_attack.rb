# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_queen_attack
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  board, can_attack = import('board', 'can_attack', from: 'integration.source.queen_attack')

  exports['QueenAttackTest'] = Pyrb::PythonClass.new('QueenAttackTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_board1'] = Pyrb.defn(self: nil) do |self_|
      ans = ["________", "________", "___W____", "________", "________", "______B_", "________", "________"]
      self_.fcall('assertEqual', [ans, board.call([[2, 3], [5, 6]])])
    end

    klass.attrs['test_board2'] = Pyrb.defn(self: nil) do |self_|
      ans = ["______W_", "_______B", "________", "________", "________", "________", "________", "________"]
      self_.fcall('assertEqual', [ans, board.call([[0, 6], [1, 7]])])
    end

    klass.attrs['test_attack_true1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, can_attack.call([[2, 3], [5, 6]])])
    end

    klass.attrs['test_attack_true2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, can_attack.call([[2, 6], [5, 3]])])
    end

    klass.attrs['test_attack_true3'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, can_attack.call([[2, 4], [2, 7]])])
    end

    klass.attrs['test_attack_true4'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, can_attack.call([[5, 4], [2, 4]])])
    end

    klass.attrs['test_attack_true5'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, can_attack.call([[1, 1], [6, 6]])])
    end

    klass.attrs['test_attack_true6'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [true, can_attack.call([[0, 6], [1, 7]])])
    end

    klass.attrs['test_attack_false1'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [false, can_attack.call([[4, 2], [0, 5]])])
    end

    klass.attrs['test_attack_false2'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [false, can_attack.call([[2, 3], [4, 7]])])
    end

    klass.attrs['test_invalid_position_board'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        board.call([[0, 0], [7, 8]])
      end
    end

    klass.attrs['test_invalid_position_can_attack'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        can_attack.call([[0, 0], [7, 8]])
      end
    end

    klass.attrs['test_queens_same_position_board'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        board.call([[2, 2], [2, 2]])
      end
    end

    klass.attrs['test_queens_same_position_can_attack'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::ValueError])]) do
        can_attack.call([[2, 2], [2, 2]])
      end
    end
  end

  QueenAttackTest = exports['QueenAttackTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
