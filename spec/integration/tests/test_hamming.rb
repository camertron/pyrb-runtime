# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_hamming
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  hamming, _ = import('integration.source.hamming')

  exports['HammingTest'] = Pyrb::PythonClass.new('HammingTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_no_difference_between_identical_strands'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [0, hamming.fcall('distance', ["A", "A"])])
    end

    klass.attrs['test_complete_hamming_distance_of_for_single_nucleotide_strand'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, hamming.fcall('distance', ["A", "G"])])
    end

    klass.attrs['test_complete_hamming_distance_of_for_small_strand'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [2, hamming.fcall('distance', ["AG", "CT"])])
    end

    klass.attrs['test_small_hamming_distance'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, hamming.fcall('distance', ["AT", "CT"])])
    end

    klass.attrs['test_small_hamming_distance_in_longer_strand'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [1, hamming.fcall('distance', ["GGACG", "GGTCG"])])
    end

    klass.attrs['test_large_hamming_distance'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [4, hamming.fcall('distance', ["GATACA", "GCATAA"])])
    end

    klass.attrs['test_hamming_distance_in_very_long_strand'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertEqual', [9, hamming.fcall('distance', ["GGACGGATTCTG", "AGGACGGATTCT"])])
    end
  end

  HammingTest = exports['HammingTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
