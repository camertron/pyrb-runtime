# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_leap
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  is_leap_year, _ = import('is_leap_year', from: 'integration.source.leap')

  exports['LeapTest'] = Pyrb::PythonClass.new('LeapTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_year_not_divisible_by_4'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_leap_year.call([2015]), false])
    end

    klass.attrs['test_year_divisible_by_4_not_divisible_by_100'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_leap_year.call([1996]), true])
    end

    klass.attrs['test_year_divisible_by_100_not_divisible_by_400'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_leap_year.call([2100]), false])
    end

    klass.attrs['test_year_divisible_by_400'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_leap_year.call([2000]), true])
    end

    klass.attrs['test_year_divisible_by_200_not_divisible_by_400'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('assertIs', [is_leap_year.call([1800]), false])
    end
  end

  LeapTest = exports['LeapTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
