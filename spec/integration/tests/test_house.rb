# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_house
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  rhyme, verse = import('rhyme', 'verse', from: 'integration.source.house')

  exports['VerseTest'] = Pyrb::PythonClass.new('VerseTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_verse_0'] = Pyrb.defn(self: nil) do |self_|
      expected = "This is the house that Jack built."
      self_.fcall('assertEqual', [expected, verse.call([0])])
    end

    klass.attrs['test_verse_1'] = Pyrb.defn(self: nil) do |self_|
      expected = ("This is the malt\n" + "that lay in the house that Jack built.")
      self_.fcall('assertEqual', [expected, verse.call([1])])
    end

    klass.attrs['test_verse_2'] = Pyrb.defn(self: nil) do |self_|
      expected = ("This is the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.")
      self_.fcall('assertEqual', [expected, verse.call([2])])
    end

    klass.attrs['test_verse_3'] = Pyrb.defn(self: nil) do |self_|
      expected = ("This is the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.")
      self_.fcall('assertEqual', [expected, verse.call([3])])
    end

    klass.attrs['test_verse_11'] = Pyrb.defn(self: nil) do |self_|
      expected = ("This is the horse and the hound and the horn\n" + "that belonged to the farmer sowing his corn\n" + "that kept the rooster that crowed in the morn\n" + "that woke the priest all shaven and shorn\n" + "that married the man all tattered and torn\n" + "that kissed the maiden all forlorn\n" + "that milked the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.")
      self_.fcall('assertEqual', [expected, verse.call([11])])
    end

    klass.attrs['test_rhyme'] = Pyrb.defn(self: nil) do |self_|
      expected = ("This is the house that Jack built.\n\n" + "This is the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the maiden all forlorn\n" + "that milked the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the man all tattered and torn\n" + "that kissed the maiden all forlorn\n" + "that milked the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the priest all shaven and shorn\n" + "that married the man all tattered and torn\n" + "that kissed the maiden all forlorn\n" + "that milked the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the rooster that crowed in the morn\n" + "that woke the priest all shaven and shorn\n" + "that married the man all tattered and torn\n" + "that kissed the maiden all forlorn\n" + "that milked the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the farmer sowing his corn\n" + "that kept the rooster that crowed in the morn\n" + "that woke the priest all shaven and shorn\n" + "that married the man all tattered and torn\n" + "that kissed the maiden all forlorn\n" + "that milked the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.\n\n" + "This is the horse and the hound and the horn\n" + "that belonged to the farmer sowing his corn\n" + "that kept the rooster that crowed in the morn\n" + "that woke the priest all shaven and shorn\n" + "that married the man all tattered and torn\n" + "that kissed the maiden all forlorn\n" + "that milked the cow with the crumpled horn\n" + "that tossed the dog\n" + "that worried the cat\n" + "that killed the rat\n" + "that ate the malt\n" + "that lay in the house that Jack built.")
      self_.fcall('assertEqual', [expected, rhyme.call()])
    end
  end

  VerseTest = exports['VerseTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
