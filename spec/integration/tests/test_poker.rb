# encoding: utf-8

require 'pyrb'

module Spec_integration_tests_test_poker
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  unittest, _ = import('unittest')
  poker, _ = import('poker', from: 'integration.source.poker')

  exports['PokerTest'] = Pyrb::PythonClass.new('PokerTest', [unittest.attr('TestCase')]).tap do |klass|
    klass.attrs['test_one_hand'] = Pyrb.defn(self: nil) do |self_|
      hand = "4S 5S 7H 8D JC".fcall('split')
      self_.fcall('assertEqual', [[hand], poker.call([[hand]])])
    end

    klass.attrs['test_nothing_vs_one_pair'] = Pyrb.defn(self: nil) do |self_|
      nothing = "4S 5H 6S 8D JH".fcall('split')
      pairOf4 = "2S 4H 6S 4D JH".fcall('split')
      self_.fcall('assertEqual', [[pairOf4], poker.call([[nothing, pairOf4]])])
    end

    klass.attrs['test_two_pair'] = Pyrb.defn(self: nil) do |self_|
      pairOf2 = "4S 2H 6S 2D JH".fcall('split')
      pairOf4 = "2S 4H 6S 4D JH".fcall('split')
      self_.fcall('assertEqual', [[pairOf4], poker.call([[pairOf2, pairOf4]])])
    end

    klass.attrs['test_one_pair_vs_double_pair'] = Pyrb.defn(self: nil) do |self_|
      pairOf8 = "2S 8H 6S 8D JH".fcall('split')
      doublePair = "4S 5H 4S 8D 5H".fcall('split')
      self_.fcall('assertEqual', [[doublePair], poker.call([[pairOf8, doublePair]])])
    end

    klass.attrs['test_two_double_pair'] = Pyrb.defn(self: nil) do |self_|
      doublePair2and8 = "2S 8H 2S 8D JH".fcall('split')
      doublePair4and5 = "4S 5H 4S 8D 5H".fcall('split')
      self_.fcall('assertEqual', [[doublePair2and8], poker.call([[doublePair2and8, doublePair4and5]])])
    end

    klass.attrs['test_double_pair_vs_three'] = Pyrb.defn(self: nil) do |self_|
      doublePair2and8 = "2S 8H 2S 8D JH".fcall('split')
      threeOf4 = "4S 5H 4S 8D 4H".fcall('split')
      self_.fcall('assertEqual', [[threeOf4], poker.call([[doublePair2and8, threeOf4]])])
    end

    klass.attrs['test_two_three'] = Pyrb.defn(self: nil) do |self_|
      threeOf2 = "2S 2H 2S 8D JH".fcall('split')
      threeOf1 = "4S AH AS 8D AH".fcall('split')
      self_.fcall('assertEqual', [[threeOf1], poker.call([[threeOf2, threeOf1]])])
    end

    klass.attrs['test_three_vs_straight'] = Pyrb.defn(self: nil) do |self_|
      threeOf4 = "4S 5H 4S 8D 4H".fcall('split')
      straight = "3S 4H 2S 6D 5H".fcall('split')
      self_.fcall('assertEqual', [[straight], poker.call([[threeOf4, straight]])])
    end

    klass.attrs['test_two_straights'] = Pyrb.defn(self: nil) do |self_|
      straightTo8 = "4S 6H 7S 8D 5H".fcall('split')
      straightTo9 = "5S 7H 8S 9D 6H".fcall('split')
      self_.fcall('assertEqual', [[straightTo9], poker.call([[straightTo8, straightTo9]])])
      straightTo1 = "AS QH KS TD JH".fcall('split')
      straightTo5 = "4S AH 3S 2D 5H".fcall('split')
      self_.fcall('assertEqual', [[straightTo1], poker.call([[straightTo1, straightTo5]])])
    end

    klass.attrs['test_straight_vs_flush'] = Pyrb.defn(self: nil) do |self_|
      straightTo8 = "4S 6H 7S 8D 5H".fcall('split')
      flushTo7 = "2S 4S 5S 6S 7S".fcall('split')
      self_.fcall('assertEqual', [[flushTo7], poker.call([[straightTo8, flushTo7]])])
    end

    klass.attrs['test_two_flushes'] = Pyrb.defn(self: nil) do |self_|
      flushTo8 = "3H 6H 7H 8H 5H".fcall('split')
      flushTo7 = "2S 4S 5S 6S 7S".fcall('split')
      self_.fcall('assertEqual', [[flushTo8], poker.call([[flushTo8, flushTo7]])])
    end

    klass.attrs['test_flush_vs_full'] = Pyrb.defn(self: nil) do |self_|
      flushTo8 = "3H 6H 7H 8H 5H".fcall('split')
      full = "4S 5H 4S 5D 4H".fcall('split')
      self_.fcall('assertEqual', [[full], poker.call([[full, flushTo8]])])
    end

    klass.attrs['test_two_fulls'] = Pyrb.defn(self: nil) do |self_|
      fullOf4by9 = "4H 4S 4D 9S 9D".fcall('split')
      fullOf5by8 = "5H 5S 5D 8S 8D".fcall('split')
      self_.fcall('assertEqual', [[fullOf5by8], poker.call([[fullOf4by9, fullOf5by8]])])
    end

    klass.attrs['test_full_vs_square'] = Pyrb.defn(self: nil) do |self_|
      full = "4S 5H 4S 5D 4H".fcall('split')
      squareOf3 = "3S 3H 2S 3D 3H".fcall('split')
      self_.fcall('assertEqual', [[squareOf3], poker.call([[full, squareOf3]])])
    end

    klass.attrs['test_two_square'] = Pyrb.defn(self: nil) do |self_|
      squareOf2 = "2S 2H 2S 8D 2H".fcall('split')
      squareOf5 = "4S 5H 5S 5D 5H".fcall('split')
      self_.fcall('assertEqual', [[squareOf5], poker.call([[squareOf2, squareOf5]])])
    end

    klass.attrs['test_square_vs_straight_flush'] = Pyrb.defn(self: nil) do |self_|
      squareOf5 = "4S 5H 5S 5D 5H".fcall('split')
      straightFlushTo9 = "5S 7S 8S 9S 6S".fcall('split')
      self_.fcall('assertEqual', [[straightFlushTo9], poker.call([[squareOf5, straightFlushTo9]])])
    end

    klass.attrs['test_two_straight_flushes'] = Pyrb.defn(self: nil) do |self_|
      straightFlushTo8 = "4H 6H 7H 8H 5H".fcall('split')
      straightFlushTo9 = "5S 7S 8S 9S 6S".fcall('split')
      self_.fcall('assertEqual', [[straightFlushTo9], poker.call([[straightFlushTo8, straightFlushTo9]])])
    end

    klass.attrs['test_three_hand_with_tie'] = Pyrb.defn(self: nil) do |self_|
      spadeStraightTo9 = "9S 8S 7S 6S 5S".fcall('split')
      diamondStraightTo9 = "9D 8D 7D 6D 5D".fcall('split')
      threeOf4 = "4D 4S 4H QS KS".fcall('split')
      self_.fcall('assertEqual', [[spadeStraightTo9, diamondStraightTo9], poker.call([[spadeStraightTo9, diamondStraightTo9, threeOf4]])])
    end
  end

  PokerTest = exports['PokerTest']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
