# encoding: utf-8

require 'pyrb'

module Spec_integration_source_difference_of_squares
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['square_of_sum'] = Pyrb.defn(x: nil) do |x|
    throw(:return, Pyrb.sum.call([Pyrb.range.call([x + 1])]) ** 2)
  end

  exports['sum_of_squares'] = Pyrb.defn(x: nil) do |x|
    throw(:return, Pyrb.sum.call([Pyrb.range.call([x + 1]).fcall('__iter__').map do |y|
      y ** 2
    end
    ]))
  end

  exports['difference'] = Pyrb.defn(x: nil) do |x|
    throw(:return, exports['square_of_sum'].call([x]) - exports['sum_of_squares'].call([x]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
