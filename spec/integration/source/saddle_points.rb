# encoding: utf-8

require 'pyrb'

module Spec_integration_source_saddle_points
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['SaddlePoints'] = Pyrb::PythonClass.new('SaddlePoints', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, matrix: nil) do |self_, matrix|
      self_.attrs['matrix'] = matrix
      self_.attrs['columns'] = Pyrb::List.call([Pyrb::Zip.call([*Pyrb.splat(self_.attr('matrix'))])])
    end

    klass.attrs['get_saddle_points'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.fcall('invalid_matrix'))
        Pyrb.raize(Pyrb::ValueError.call(["Matrix has rows of unequal length"]))
      end

      throw(:return, self_.fcall('saddle_points'))
    end

    klass.attrs['saddle_points'] = Pyrb.defn(self: nil) do |self_|
      saddle_points = Pyrb::Set.call()
      (Pyrb.range.call([Pyrb.len.call([self_.attr('matrix')])])).each do |row|
        (Pyrb.range.call([Pyrb.len.call([self_.attr('matrix')[row]])])).each do |col|
          if !!(self_.fcall('saddle_point', [row, col]))
            saddle_points.fcall('add', [[row, col]])
          end
        end
      end
      throw(:return, saddle_points)
    end

    klass.attrs['saddle_point'] = Pyrb.defn(self: nil, row: nil, col: nil) do |self_, row, col|
      throw(:return, ((self_.attr('matrix')[row][col] == Pyrb.max.call([self_.attr('matrix')[row]])).and { self_.attr('matrix')[row][col] == Pyrb.min.call([self_.attr('columns')[col]]) }))
    end

    klass.attrs['invalid_matrix'] = Pyrb.defn(self: nil) do |self_|
      (Pyrb.range.call([Pyrb.len.call([self_.attr('matrix')])])).each do |row|
        if !!(Pyrb.len.call([self_.attr('matrix')[row]]) != Pyrb.len.call([self_.attr('matrix')[0]]))
          throw(:return, true)
        end
      end
      throw(:return, false)
    end
  end

  SaddlePoints = exports['SaddlePoints']

  exports['saddle_points'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['SaddlePoints'].call([inp]).fcall('get_saddle_points'))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
