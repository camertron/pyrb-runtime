# encoding: utf-8

require 'pyrb'

module Spec_integration_source_school
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['School'] = Pyrb::PythonClass.new('School', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, name: nil) do |self_, name|
      self_.attrs['name'] = name
      self_.attrs['db'] = Pyrb::Dict.new
    end

    klass.attrs['add'] = Pyrb.defn(self: nil, student: nil, grade: nil) do |self_, student, grade|
      if !!(!(grade).in?(self_.attr('db')))
        self_.fcall('initialize_grade', [grade])
      end

      self_.attr('db')[grade].fcall('add', [student])
    end

    klass.attrs['grade'] = Pyrb.defn(self: nil, grade: nil) do |self_, grade|
      throw(:return, !!((grade).in?(self_.attr('db'))) ? self_.attr('db')[grade] : Pyrb::Set.call())
    end

    klass.attrs['sort'] = Pyrb.defn(self: nil) do |self_|
      students = []
      (self_.attr('db').fcall('keys')).each do |grade|
        students.fcall('append', [[grade, Pyrb::List.call([self_.attr('db')[grade]])]])
      end
      throw(:return, students)
    end

    klass.attrs['initialize_grade'] = Pyrb.defn(self: nil, grade: nil) do |self_, grade|
      self_.attrs['db'][grade] = Pyrb::Set.call()
    end
  end

  School = exports['School']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
