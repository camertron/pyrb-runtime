# encoding: utf-8

require 'pyrb'

module Spec_integration_source_anagram
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['detect_anagrams'] = Pyrb.defn(w: nil, poss: nil) do |w, poss|
    throw(:return, Pyrb::Filter.call([Pyrb.defn(p: nil) { |p| exports['anagram'].call([w, p]) }, poss]))
  end

  exports['anagram'] = Pyrb.defn(w: nil, p: nil) do |w, p|
    throw(:return, (w.fcall('lower') != p.fcall('lower')).and { exports['sort'].call([w.fcall('lower')]) == exports['sort'].call([p.fcall('lower')]) })
  end

  exports['sort'] = Pyrb.defn(word: nil) do |word|
    throw(:return, Pyrb.sorted.call([Pyrb::List.call([word.fcall('lower')])]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
