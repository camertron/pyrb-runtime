# encoding: utf-8

require 'pyrb'

module Spec_integration_source_series
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['slices'] = Pyrb.defn(string: nil, size: nil) do |string, size|
    curr_slice, curr_indx = nil

    if !!((size <= 0).or { size > Pyrb.len.call([string]) })
      Pyrb.raize(Pyrb::ValueError.call(["Invalid slice size"]))
    end

    slices = []
    (Pyrb.enumerate.call([string])).each do |indx, elem|
      if !!(Pyrb.len.call([string]) - indx >= size)
        curr_slice = []
        curr_indx = indx
        while !!(Pyrb.len.call([curr_slice]) < size)
          curr_slice.fcall('append', [Pyrb::Int.call([string[curr_indx]])])
          curr_indx += 1
        end

        slices.fcall('append', [curr_slice])
      end
    end
    throw(:return, slices)
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
