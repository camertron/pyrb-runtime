# encoding: utf-8

require 'pyrb'

module Spec_integration_source_binary
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Binary'] = Pyrb::PythonClass.new('Binary', []).tap do |klass|
    klass.attrs['VALID_CHARS'] = Pyrb::Set.call(["01"])
    klass.attrs['parse_binary'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        if !!((cls.fcall('valid', [inp])).not)
          Pyrb.raize(Pyrb::ValueError)
        end

        throw(:return, cls.fcall('convert_to_decimal', [inp]))
      end

    )

    klass.attrs['convert_to_decimal'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb.sum.call([Pyrb.enumerate.call([Pyrb.reversed.call([inp])]).fcall('__iter__').each_with_object([]) do |(idx, val), _ret_|
          if !!(val == "1")
            _ret_ << 2 ** idx
          end
        end
        ]))
      end

    )

    klass.attrs['valid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb::Set.call([inp]) <= cls.attr('VALID_CHARS'))
      end

    )
  end

  Binary = exports['Binary']

  exports['parse_binary'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['Binary'].fcall('parse_binary', [inp]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
