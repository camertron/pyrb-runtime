# encoding: utf-8

require 'pyrb'

module Spec_integration_source_palindrome
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  mul, _ = import('mul', from: 'operator')
  reduce, _ = import('reduce', from: 'functools')

  exports['Palindromes'] = Pyrb::PythonClass.new('Palindromes', []).tap do |klass|
    klass.attrs['smallest_palindrome'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, max_factor: nil, min_factor: { default: 0 }) do |cls, max_factor, min_factor|
        throw(:return, Pyrb.min.call([cls.fcall('palindromes', [max_factor, min_factor]), ], { key: Pyrb.defn(item: nil) { |item| item[0] } }))
      end

    )

    klass.attrs['largest_palindrome'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, max_factor: nil, min_factor: { default: 0 }) do |cls, max_factor, min_factor|
        throw(:return, Pyrb.max.call([cls.fcall('palindromes', [max_factor, min_factor]), ], { key: Pyrb.defn(item: nil) { |item| item[0] } }))
      end

    )

    klass.attrs['palindromes'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, max_factor: nil, min_factor: nil) do |cls, max_factor, min_factor|
        throw(:return, cls.fcall('candidates', [max_factor, min_factor]).fcall('__iter__').each_with_object([]) do |candidate, _ret_|
          if !!(cls.fcall('is_palindrome', [cls.fcall('product', [candidate])]))
            _ret_ << [cls.fcall('product', [candidate]), candidate]
          end
        end
        )
      end

    )

    klass.attrs['candidates'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(max_factor: nil, min_factor: nil) do |max_factor, min_factor|
        throw(:return, Pyrb.range.call([min_factor, max_factor + 1]).fcall('__iter__').flat_map do |i|
          Pyrb.range.call([i, max_factor + 1]).fcall('__iter__').map do |j|
            [i, j]
          end
        end
        )
      end

    )

    klass.attrs['product'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(s: nil) do |s|
        throw(:return, reduce.call([mul, s, 1]))
      end

    )

    klass.attrs['is_palindrome'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(num: nil) do |num|
        throw(:return, Pyrb::Str.call([num]) == "".fcall('join', [Pyrb.reversed.call([Pyrb::Str.call([num])])]))
      end

    )
  end

  Palindromes = exports['Palindromes']

  exports['smallest_palindrome'] = Pyrb.defn(max_factor: nil, min_factor: { default: 0 }) do |max_factor, min_factor|
    throw(:return, exports['Palindromes'].fcall('smallest_palindrome', [max_factor, min_factor]))
  end

  exports['largest_palindrome'] = Pyrb.defn(max_factor: nil, min_factor: { default: 0 }) do |max_factor, min_factor|
    throw(:return, exports['Palindromes'].fcall('largest_palindrome', [max_factor, min_factor]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
