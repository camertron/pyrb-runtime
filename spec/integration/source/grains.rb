# encoding: utf-8

require 'pyrb'

module Spec_integration_source_grains
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['on_square'] = Pyrb.defn(x: nil) do |x|
    throw(:return, 2 ** (x - 1))
  end

  exports['total_after'] = Pyrb.defn(x: nil) do |x|
    throw(:return, !!(x == 1) ? 1 : exports['on_square'].call([x]) + exports['total_after'].call([x - 1]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
