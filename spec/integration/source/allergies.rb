# encoding: utf-8

require 'pyrb'

module Spec_integration_source_allergies
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Allergies'] = Pyrb::PythonClass.new('Allergies', []).tap do |klass|
    klass.attrs['ALLERGY_SCORES'] = Pyrb::Dict.new([{"eggs" => 1, "peanuts" => 2, "shellfish" => 4, "strawberries" => 8, "tomatoes" => 16, "chocolate" => 32, "pollen" => 64, "cats" => 128}])
    klass.attrs['__init__'] = Pyrb.defn(self: nil, score: nil) do |self_, score|
      self_.attrs['score'] = score
      self_.attrs['lst'] = Pyrb::List.call([self_.attr('ALLERGY_SCORES').fcall('__iter__').each_with_object([]) do |allergy, _ret_|
        if !!(self_.fcall('is_allergic_to', [allergy]))
          _ret_ << allergy
        end
      end
      ])
    end

    klass.attrs['is_allergic_to'] = Pyrb.defn(self: nil, allergen: nil) do |self_, allergen|
      throw(:return, self_.attr('ALLERGY_SCORES')[allergen] & self_.attr('score'))
    end
  end

  Allergies = exports['Allergies']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
