# encoding: utf-8

require 'pyrb'

module Spec_integration_source_isbn_verifier
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['IsbnVerifier'] = Pyrb::PythonClass.new('IsbnVerifier', [Pyrb::Object]).tap do |klass|
    klass.attrs['VALID_SEPERATOR'] = "-"
    klass.attrs['VALID_CHECK_CHARACTER'] = "X"
    klass.attrs['VALID_DIGITS'] = Pyrb::List.call([Pyrb::Map.call([Pyrb::Str, Pyrb.range.call([0, 10])])])
    klass.attrs['VALID_CHARACTERS'] = (Pyrb::Set.call([klass.attrs['VALID_DIGITS']]) | Pyrb::Set.call([[klass.attrs['VALID_SEPERATOR'], klass.attrs['VALID_CHECK_CHARACTER']]]))
    klass.attrs['VALID_LENGTH'] = 10
    klass.attrs['is_valid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        if !!(cls.fcall('invalid', [string]))
          throw(:return, false)
        end

        throw(:return, cls.fcall('verify', [string]))
      end

    )

    klass.attrs['verify'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        sum_so_far = 0
        (Pyrb.enumerate.call([cls.fcall('remove_seperator', [string])])).each do |i, c|
          sum_so_far += (cls.fcall('convert_char_to_int', [c]) * (10 - i))
        end
        throw(:return, (sum_so_far % 11) == 0)
      end

    )

    klass.attrs['invalid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        throw(:return, (cls.fcall('invalid_character', [string]).or { cls.fcall('invalid_length', [string]) }.or { cls.fcall('invalid_X_other_than_check_digit', [string]) }))
      end

    )

    klass.attrs['invalid_character'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        throw(:return, Pyrb.any.call([string.fcall('__iter__').map do |char|
          !(char).in?(cls.attr('VALID_CHARACTERS'))
        end
        ]))
      end

    )

    klass.attrs['invalid_length'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        throw(:return, (Pyrb.len.call([cls.fcall('remove_invalid_characters_and_slashes', [string])]) != cls.attr('VALID_LENGTH')))
      end

    )

    klass.attrs['invalid_X_other_than_check_digit'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        throw(:return, (((cls.attr('VALID_CHECK_CHARACTER')).in?(string)).and { (string.fcall('endswith', [cls.attr('VALID_CHECK_CHARACTER')])).not }))
      end

    )

    klass.attrs['remove_invalid_characters_and_slashes'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        throw(:return, cls.fcall('remove_seperator', [cls.fcall('remove_invalid_characters', [string])]))
      end

    )

    klass.attrs['remove_invalid_characters'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        throw(:return, "".fcall('join', [Pyrb::Filter.call([Pyrb.defn(char: nil) { |char| (char).in?(cls.attr('VALID_CHARACTERS')) }, string])]))
      end

    )

    klass.attrs['convert_char_to_int'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, char: nil) do |cls, char|
        throw(:return, Pyrb::Int.call([cls.fcall('convert_check_character_to_ten', [char])]))
      end

    )

    klass.attrs['convert_check_character_to_ten'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, char: nil) do |cls, char|
        throw(:return, !!(char == cls.attr('VALID_CHECK_CHARACTER')) ? 10 : char)
      end

    )

    klass.attrs['remove_seperator'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, string: nil) do |cls, string|
        throw(:return, "".fcall('join', [Pyrb::Filter.call([Pyrb.defn(char: nil) { |char| char != cls.attr('VALID_SEPERATOR') }, string])]))
      end

    )
  end

  IsbnVerifier = exports['IsbnVerifier']

  exports['verify'] = Pyrb.defn(isbn: nil) do |isbn|
    throw(:return, exports['IsbnVerifier'].fcall('is_valid', [isbn]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
