# encoding: utf-8

require 'pyrb'

module Spec_integration_source_protein_translation
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['CODON_TO_PROTEIN'] = Pyrb::Dict.new([{"AUG" => "Methionine", "UUU" => "Phenylalanine", "UUC" => "Phenylalanine", "UUA" => "Leucine", "UUG" => "Leucine", "UCU" => "Serine", "UCC" => "Serine", "UCA" => "Serine", "UCG" => "Serine", "UAU" => "Tyrosine", "UAC" => "Tyrosine", "UGU" => "Cysteine", "UGC" => "Cysteine", "UGG" => "Tryptophan"}])
  exports['STOP_CODONS'] = Pyrb::Set.call([Pyrb::Set.new([["UAA", "UAG", "UGA"]])])
  exports['CODON_LENGTH'] = 3
  exports['proteins'] = Pyrb.defn(strand: nil) do |strand|
    polypeptide = []
    codons = exports['split_into_codons'].call([strand])
    (codons).each do |codon|
      if !!(((codon).in?(exports['STOP_CODONS'])))
        throw(:return, polypeptide)
      else
        polypeptide.fcall('append', [exports['CODON_TO_PROTEIN'][codon]])
      end
    end
    throw(:return, polypeptide)
  end

  exports['split_into_codons'] = Pyrb.defn(strand: nil) do |strand|
    throw(:return, Pyrb.range.call([0, Pyrb.len.call([strand]), exports['CODON_LENGTH']]).fcall('__iter__').map do |i|
      strand[i...i + exports['CODON_LENGTH']]
    end
    )
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
