# encoding: utf-8

require 'pyrb'

module Spec_integration_source_matrix
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Matrix'] = Pyrb::PythonClass.new('Matrix', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, inp: nil) do |self_, inp|
      self_.attrs['rows'] = self_.fcall('build_rows', [inp])
      self_.attrs['columns'] = Pyrb::List.call([Pyrb::Map.call([Pyrb::List, Pyrb::Zip.call([*Pyrb.splat(self_.attr('rows'))])])])
    end

    klass.attrs['build_rows'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(inp: nil) do |inp|
        throw(:return, inp.fcall('split', ["\n"]).fcall('__iter__').map do |row|
          row.fcall('split').fcall('__iter__').map do |val|
            Pyrb::Int.call([val])
          end
        end
        )
      end

    )
  end

  Matrix = exports['Matrix']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
