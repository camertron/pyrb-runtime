# encoding: utf-8

require 'pyrb'

module Spec_integration_source_clock
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Clock'] = Pyrb::PythonClass.new('Clock', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, hours: nil, mins: nil) do |self_, hours, mins|
      self_.attrs['hours'] = hours
      self_.attrs['mins'] = mins
      self_.fcall('fixup')
    end

    klass.attrs['__eq__'] = Pyrb.defn(self: nil, other: nil) do |self_, other|
      throw(:return, ((self_.attr('hours') == other.attr('hours')).and { self_.attr('mins') == other.attr('mins') }))
    end

    klass.attrs['__str__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (self_.fcall('format_hours') + ":" + self_.fcall('format_mins')))
    end

    klass.attrs['add'] = Pyrb.defn(self: nil, additional_mins: nil) do |self_, additional_mins|
      self_.attrs['mins'] += additional_mins
      self_.fcall('fixup')
      throw(:return, self_)
    end

    klass.attrs['fixup'] = Pyrb.defn(self: nil) do |self_|
      self_.fcall('fixup_mins')
      self_.fcall('fixup_hours')
    end

    klass.attrs['fixup_hours'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['hours'] = (self_.attr('hours') % 24)
    end

    klass.attrs['fixup_mins'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['hours'] += Pyrb.int_divide(self_.attr('mins'), 60)
      self_.attrs['mins'] = (self_.attr('mins') % 60)
    end

    klass.attrs['format_hours'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb::Str.call([self_.attr('hours')]).fcall('zfill', [2]))
    end

    klass.attrs['format_mins'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb::Str.call([self_.attr('mins')]).fcall('zfill', [2]))
    end
  end

  Clock = exports['Clock']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
