# encoding: utf-8

require 'pyrb'

module Spec_integration_source_sum_of_multiples
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  reduce, _ = import('reduce', from: 'functools')

  exports['sum_of_multiples'] = Pyrb.defn(limit: nil, factors: { default: [3, 5] }) do |limit, factors|
    mult = Pyrb::Set.call()
    (factors).each do |factor|
      if !!(factor <= 0)
        next
      end

      (Pyrb.range.call([1, (limit / factor) + 1])).each do |curr_mult|
        if !!((factor * curr_mult) < limit)
          mult.fcall('add', [(factor * curr_mult)])
        end
      end
    end
    throw(:return, !!(Pyrb.len.call([mult]) == 0) ? 0 : reduce.call([Pyrb.defn(x: nil, y: nil) { |x, y| x + y }, mult]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
