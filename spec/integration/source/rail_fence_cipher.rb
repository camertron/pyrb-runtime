# encoding: utf-8

require 'pyrb'

module Spec_integration_source_rail_fence_cipher
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Rails'] = Pyrb::PythonClass.new('Rails', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, num_rails: nil) do |self_, num_rails|
      self_.attrs['num_rails'] = num_rails
      self_.attrs['rails'] = Pyrb.range.call([num_rails]).fcall('__iter__').map do |_|
        []
      end
    end

    klass.attrs['populate_rails_linear'] = Pyrb.defn(self: nil, message: nil, rail_lengths: nil) do |self_, message, rail_lengths|
      message_list = Pyrb::List.call([message])
      (self_.fcall('linear_iterator', [rail_lengths])).each do |rail|
        rail.fcall('append', [message_list.fcall('pop', [0])])
      end
    end

    klass.attrs['populate_rails_zig_zag'] = Pyrb.defn(self: nil, message: nil) do |self_, message|
      message_list = Pyrb::List.call([message])
      (self_.fcall('zig_zag_iterator', [message])).each do |rail|
        rail.fcall('append', [message_list.fcall('pop', [0])])
      end
    end

    klass.attrs['to_string_linear'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, "".fcall('join', [self_.attr('rails').fcall('__iter__').flat_map do |rail|
        rail.fcall('__iter__').map do |data|
          data
        end
      end
      ]))
    end

    klass.attrs['to_string_zig_zag'] = Pyrb.defn(self: nil, message: nil) do |self_, message|
      throw(:return, "".fcall('join', [self_.fcall('zig_zag_iterator', [message]).fcall('__iter__').map do |rail|
        rail.fcall('pop', [0])
      end
      ]))
    end

    klass.attrs['linear_iterator'] = Pyrb.defn(self: nil, rail_lengths: nil) do |self_, rail_lengths|
      Pyrb::Generator.new([
        Enumerator.new do |__pyrb_gen__|
          (Pyrb.range.call([Pyrb.len.call([self_.attr('rails')])])).each do |index|
            (Pyrb.range.call([rail_lengths[index]])).each do |rail_length|
              __pyrb_gen__ << self_.attr('rails')[index]
            end
          end
        end
      ])
    end

    klass.attrs['zig_zag_iterator'] = Pyrb.defn(self: nil, message: nil) do |self_, message|
      increasing, index = nil

      Pyrb::Generator.new([
        Enumerator.new do |__pyrb_gen__|
          index = 0
          increasing = true
          (message).each do |_|
            __pyrb_gen__ << self_.attr('rails')[index]
            increasing = self_.fcall('direction', [index, increasing])
            index = self_.fcall('increment_index', [index, increasing])
          end
        end
      ])
    end

    klass.attrs['increment_index'] = Pyrb.defn(self: nil, index: nil, increasing: nil) do |self_, index, increasing|
      if !!(increasing)
        throw(:return, index + 1)
      else
        throw(:return, index - 1)
      end
    end

    klass.attrs['direction'] = Pyrb.defn(self: nil, index: nil, increasing: nil) do |self_, index, increasing|
      if !!(index == 0)
        throw(:return, true)
      elsif !!(index == self_.attr('num_rails') - 1)
        throw(:return, false)
      else
        throw(:return, increasing)
      end
    end
  end

  Rails = exports['Rails']

  exports['encode'] = Pyrb.defn(message: nil, num_rails: nil) do |message, num_rails|
    rails = exports['Rails'].call([num_rails])
    rails.fcall('populate_rails_zig_zag', [message])
    throw(:return, rails.fcall('to_string_linear'))
  end

  exports['decode'] = Pyrb.defn(message: nil, num_rails: nil) do |message, num_rails|
    faulty_rails = exports['Rails'].call([num_rails])
    faulty_rails.fcall('populate_rails_zig_zag', [message])
    rail_lengths = faulty_rails.attr('rails').fcall('__iter__').map do |rail|
      Pyrb.len.call([rail])
    end

    rails = exports['Rails'].call([num_rails])
    rails.fcall('populate_rails_linear', [message, rail_lengths])
    throw(:return, rails.fcall('to_string_zig_zag', [message]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
