# encoding: utf-8

require 'pyrb'

module Spec_integration_source_robot_simulator
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['NORTH'] = Pyrb::PythonClass.new('NORTH', []).tap do |klass|
    klass.attrs['advance'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, x: nil, y: nil) do |self_, x, y|
        throw(:return, [x, y + 1])
      end

    )

    klass.attrs['turn_right'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['EAST'])
      end

    )

    klass.attrs['turn_left'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['WEST'])
      end

    )
  end

  NORTH = exports['NORTH']

  exports['EAST'] = Pyrb::PythonClass.new('EAST', []).tap do |klass|
    klass.attrs['advance'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, x: nil, y: nil) do |self_, x, y|
        throw(:return, [x + 1, y])
      end

    )

    klass.attrs['turn_right'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['SOUTH'])
      end

    )

    klass.attrs['turn_left'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['NORTH'])
      end

    )
  end

  EAST = exports['EAST']

  exports['SOUTH'] = Pyrb::PythonClass.new('SOUTH', []).tap do |klass|
    klass.attrs['advance'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, x: nil, y: nil) do |self_, x, y|
        throw(:return, [x, y - 1])
      end

    )

    klass.attrs['turn_right'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['WEST'])
      end

    )

    klass.attrs['turn_left'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['EAST'])
      end

    )
  end

  SOUTH = exports['SOUTH']

  exports['WEST'] = Pyrb::PythonClass.new('WEST', []).tap do |klass|
    klass.attrs['advance'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, x: nil, y: nil) do |self_, x, y|
        throw(:return, [x - 1, y])
      end

    )

    klass.attrs['turn_right'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['NORTH'])
      end

    )

    klass.attrs['turn_left'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil) do |self_|
        throw(:return, exports['SOUTH'])
      end

    )
  end

  WEST = exports['WEST']

  exports['Robot'] = Pyrb::PythonClass.new('Robot', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, direction: { default: exports['NORTH'] }, x: { default: 0 }, y: { default: 0 }) do |self_, direction, x, y|
      self_.attrs['coordinates'] = [x, y]
      self_.attrs['bearing'] = direction
    end

    klass.attrs['advance'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['coordinates'] = self_.attr('bearing').fcall('advance', [self_.attr('bearing'), self_.fcall('x'), self_.fcall('y')])
    end

    klass.attrs['turn_right'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['bearing'] = self_.attr('bearing').fcall('turn_right', [self_.attr('bearing')])
    end

    klass.attrs['turn_left'] = Pyrb.defn(self: nil) do |self_|
      self_.attrs['bearing'] = self_.attr('bearing').fcall('turn_left', [self_.attr('bearing')])
    end

    klass.attrs['simulate'] = Pyrb.defn(self: nil, instructions: nil) do |self_, instructions|
      (instructions).each do |i|
        self_.fcall('execute_instruction', [i])
      end
    end

    klass.attrs['execute_instruction'] = Pyrb.defn(self: nil, i: nil) do |self_, i|
      if !!(i == "A")
        self_.fcall('advance')
      elsif !!(i == "L")
        self_.fcall('turn_left')
      elsif !!(i == "R")
        self_.fcall('turn_right')
      end
    end

    klass.attrs['x'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('coordinates')[0])
    end

    klass.attrs['y'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('coordinates')[1])
    end
  end

  Robot = exports['Robot']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
