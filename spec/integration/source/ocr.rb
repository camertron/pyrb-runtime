# encoding: utf-8

require 'pyrb'

module Spec_integration_source_ocr
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Ocr'] = Pyrb::PythonClass.new('Ocr', []).tap do |klass|
    klass.attrs['NUM_ROWS'] = 4
    klass.attrs['NUM_COLS'] = 3
    klass.attrs['UNRECOGNIZED_NUM'] = "?"
    klass.attrs['GRID_NUMS'] = Pyrb::Dict.new([{"0" => [" _ ", "| |", "|_|", "   "], "1" => ["   ", "  |", "  |", "   "], "2" => [" _ ", " _|", "|_ ", "   "], "3" => [" _ ", " _|", " _|", "   "], "4" => ["   ", "|_|", "  |", "   "], "5" => [" _ ", "|_ ", " _|", "   "], "6" => [" _ ", "|_ ", "|_|", "   "], "7" => [" _ ", "  |", "  |", "   "], "8" => [" _ ", "|_|", "|_|", "   "], "9" => [" _ ", "|_|", " _|", "   "]}])
    klass.attrs['NUMS'] = klass.attrs['GRID_NUMS'].fcall('items').fcall('__iter__').each_with_object(Pyrb::Dict.new) do |(key, value), _ret_|
      _ret_["".fcall('join', [value])] = key
    end

    klass.attrs['numbers'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, "".fcall('join', [Pyrb::Map.call([cls.attr('number'), Pyrb::Zip.call([*Pyrb.splat(Pyrb::Map.call([cls.attr('split_every_three'), inp]))])])]))
      end

    )

    klass.attrs['grids'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb::List.call([Pyrb::Map.call(["".attr('join'), Pyrb::Zip.call([*Pyrb.splat(Pyrb::Map.call([cls.attr('grid'), inp]))])])]))
      end

    )

    klass.attrs['number'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        if !!((cls.fcall('valid_num', [inp])).not)
          Pyrb.raize(Pyrb::ValueError)
        end

        throw(:return, cls.attr('NUMS').fcall('get', ["".fcall('join', [inp]), cls.attr('UNRECOGNIZED_NUM')]))
      end

    )

    klass.attrs['grid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        if !!((cls.fcall('valid_grid', [inp])).not)
          Pyrb.raize(Pyrb::ValueError)
        end

        throw(:return, cls.attr('GRID_NUMS').fcall('get', [inp]))
      end

    )

    klass.attrs['valid_num'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, (Pyrb.all.call([inp.fcall('__iter__').map do |row|
          Pyrb.len.call([row]) == cls.attr('NUM_COLS')
        end
        ]).and { Pyrb.len.call([inp]) == cls.attr('NUM_ROWS') }))
      end

    )

    klass.attrs['valid_grid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb.all.call([inp.fcall('__iter__').map do |char|
          (char).in?(cls.attr('GRID_NUMS').fcall('keys'))
        end
        ]))
      end

    )

    klass.attrs['split_every_three'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, cls.fcall('split', [inp, 3]))
      end

    )

    klass.attrs['split'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(inp: nil, size: nil) do |inp, size|
        throw(:return, Pyrb.range.call([0, Pyrb.len.call([inp]), size]).fcall('__iter__').map do |start|
          inp[start...start + size]
        end
        )
      end

    )
  end

  Ocr = exports['Ocr']

  exports['number'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['Ocr'].fcall('numbers', [inp]))
  end

  exports['grid'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['Ocr'].fcall('grids', [inp]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
