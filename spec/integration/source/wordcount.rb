# encoding: utf-8

require 'pyrb'

module Spec_integration_source_wordcount
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['word_count'] = Pyrb.defn(phrase: nil) do |phrase|
    words = phrase.fcall('strip').fcall('replace', ["\n", " "]).fcall('split', [" "])
    words = Pyrb::Filter.call([nil, words])
    count = Pyrb::Dict.new
    (words).each do |word|
      count[word] = !!(!(word).in?(count)) ? 1 : count[word] + 1
    end
    throw(:return, count)
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
