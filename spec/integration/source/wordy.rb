# encoding: utf-8

require 'pyrb'

module Spec_integration_source_wordy
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Calculator'] = Pyrb::PythonClass.new('Calculator', []).tap do |klass|
    klass.attrs['OPERATORS'] = Pyrb::Dict.new([{"plus" => "+", "minus" => "-", "multiplied by" => "*", "divided by" => "/", "What is " => "", "?" => ""}])
    klass.attrs['VALID_TOKENS'] = Pyrb::Set.call([klass.attrs['OPERATORS'].fcall('values')])
    klass.attrs['__init__'] = Pyrb.defn(self: nil, inp: nil) do |self_, inp|
      self_.attrs['inp'] = inp
      self_.attrs['tokenized'] = self_.fcall('tokenize', [inp])
      self_.attrs['tokens'] = self_.attr('tokenized').fcall('split', [" "])
    end

    klass.attrs['calculate'] = Pyrb.defn(self: nil) do |self_|
      if !!((self_.fcall('valid')).not)
        Pyrb.raize(Pyrb::ValueError)
      end

      operator_stack = self_.fcall('operator_stack')
      num_stack = self_.fcall('num_stack')
      while !!(Pyrb.len.call([operator_stack]) > 0)
        operator = operator_stack.fcall('pop', [0])
        num1 = num_stack.fcall('pop', [0])
        num2 = num_stack.fcall('pop', [0])
        num_stack.fcall('insert', [0, self_.fcall('evaluate', [operator, num1, num2])])
      end

      throw(:return, num_stack.fcall('pop', [0]))
    end

    klass.attrs['evaluate'] = Pyrb.defn(self: nil, operator: nil, num1: nil, num2: nil) do |self_, operator, num1, num2|
      throw(:return, Pyrb.eeval.call([Pyrb::Str.call([num1]) + operator + Pyrb::Str.call([num2])]))
    end

    klass.attrs['num_stack'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb::List.call([Pyrb::Map.call([Pyrb::Int, Pyrb::Filter.call([self_.attr('digit'), self_.attr('tokens')])])]))
    end

    klass.attrs['operator_stack'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb::List.call([Pyrb::Filter.call([self_.attr('operator'), self_.attr('tokens')])]))
    end

    klass.attrs['valid'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (self_.fcall('valid_elements').and { (self_.fcall('consecutive_tokens')).not }.and { (self_.fcall('consecutive_digits')).not }))
    end

    klass.attrs['consecutive_tokens'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.any.call([self_.fcall('slices_of_two').fcall('__iter__').map do |(i, j)|
        self_.fcall('operator', [i]).and { self_.fcall('operator', [j]) }
      end
      ]))
    end

    klass.attrs['consecutive_digits'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.any.call([self_.fcall('slices_of_two').fcall('__iter__').map do |(i, j)|
        self_.fcall('digit', [i]).and { self_.fcall('digit', [j]) }
      end
      ]))
    end

    klass.attrs['slices_of_two'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb::List.call([Pyrb::Zip.call([self_.attr('tokens'), self_.attr('tokens')[1..-1]])]))
    end

    klass.attrs['valid_elements'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.all.call([self_.attr('tokens').fcall('__iter__').map do |element|
        self_.fcall('valid_element', [element])
      end
      ]))
    end

    klass.attrs['valid_element'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, element: nil) do |cls, element|
        throw(:return, ((element).in?(cls.attr('VALID_TOKENS'))).or { cls.fcall('digit', [element]) })
      end

    )

    klass.attrs['tokenize'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        (cls.attr('OPERATORS').fcall('items')).each do |operator, token|
          inp = inp.fcall('replace', [operator, token])
        end
        throw(:return, inp)
      end

    )

    klass.attrs['digit'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(element: nil) do |element|
        throw(:return, element.fcall('lstrip', ["-"]).fcall('isdigit'))
      end

    )

    klass.attrs['operator'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, element: nil) do |cls, element|
        throw(:return, (element).in?(cls.attr('OPERATORS').fcall('values')))
      end

    )
  end

  Calculator = exports['Calculator']

  exports['calculate'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['Calculator'].call([inp]).fcall('calculate'))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
