# encoding: utf-8

require 'pyrb'

module Spec_integration_source_armstrong_numbers
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['get_digits'] = Pyrb.defn(number: nil) do |number|
    throw(:return, Pyrb::List.call([Pyrb::Map.call([Pyrb::Int, Pyrb::List.call([Pyrb::Str.call([number])])])]))
  end

  exports['is_armstrong'] = Pyrb.defn(number: nil) do |number|
    digits = exports['get_digits'].call([number])
    num_digits = Pyrb.len.call([digits])
    digits_raised_to_num_digits = digits.fcall('__iter__').map do |digit|
      digit ** num_digits
    end

    throw(:return, number == Pyrb.sum.call([digits_raised_to_num_digits]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
