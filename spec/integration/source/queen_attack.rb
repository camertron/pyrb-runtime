# encoding: utf-8

require 'pyrb'

module Spec_integration_source_queen_attack
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Board'] = Pyrb::PythonClass.new('Board', []).tap do |klass|
    klass.attrs['EMPTY_BOARD'] = Pyrb.range.call([8]).fcall('__iter__').map do |_|
      ("_" * 8)
    end

    klass.attrs['__init__'] = Pyrb.defn(self: nil, white_coords: nil, black_coords: nil) do |self_, white_coords, black_coords|
      self_.attrs['white_coords'] = white_coords
      self_.attrs['black_coords'] = black_coords
      if !!((self_.fcall('valid_coords')).not)
        Pyrb.raize(Pyrb::ValueError)
      end

      self_.attrs['board'] = self_.fcall('generate_board')
    end

    klass.attrs['generate_board'] = Pyrb.defn(self: nil) do |self_|
      board = Pyrb::List.call([Pyrb::Map.call([Pyrb.defn(row: nil) { |row| Pyrb::List.call([row]) }, self_.attr('EMPTY_BOARD')])])
      board = self_.fcall('place_piece', [board, "W", self_.attr('white_coords')])
      board = self_.fcall('place_piece', [board, "B", self_.attr('black_coords')])
      throw(:return, Pyrb::List.call([Pyrb::Map.call([Pyrb.defn(row: nil) { |row| "".fcall('join', [row]) }, board])]))
    end

    klass.attrs['can_attack'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('same_row').or { self_.fcall('same_col') }.or { self_.fcall('same_diag') })
    end

    klass.attrs['same_row'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('white_coords')[0] == self_.attr('black_coords')[0])
    end

    klass.attrs['same_col'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('white_coords')[1] == self_.attr('black_coords')[1])
    end

    klass.attrs['same_diag'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (Pyrb.abs.call([self_.attr('white_coords')[0] - self_.attr('black_coords')[0]]) == Pyrb.abs.call([self_.attr('white_coords')[1] - self_.attr('black_coords')[1]])))
    end

    klass.attrs['valid_coords'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (self_.fcall('different_coords').and { self_.fcall('valid_coord', [self_.attr('white_coords')]) }.and { self_.fcall('valid_coord', [self_.attr('black_coords')]) }))
    end

    klass.attrs['different_coords'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('white_coords') != self_.attr('black_coords'))
    end

    klass.attrs['valid_coord'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(coord: nil) do |coord|
        throw(:return, ((0 <= coord[0] && coord[0] <= 7).and { 0 <= coord[1] && coord[1] <= 7 }))
      end

    )

    klass.attrs['place_piece'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(board: nil, piece: nil, coords: nil) do |board, piece, coords|
        board[coords[0]][coords[1]] = piece
        throw(:return, board)
      end

    )
  end

  Board = exports['Board']

  exports['board'] = Pyrb.defn(white_coords: nil, black_coords: nil) do |white_coords, black_coords|
    throw(:return, exports['Board'].call([white_coords, black_coords]).attr('board'))
  end

  exports['can_attack'] = Pyrb.defn(white_coords: nil, black_coords: nil) do |white_coords, black_coords|
    throw(:return, exports['Board'].call([white_coords, black_coords]).fcall('can_attack'))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
