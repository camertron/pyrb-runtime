# encoding: utf-8

require 'pyrb'

module Spec_integration_source_sieve
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['sieve'] = Pyrb.defn(limit: nil) do |limit|
    primes = []
    unmarked = Pyrb::List.call([Pyrb.range.call([2, limit + 1])])
    while !!(Pyrb.len.call([unmarked]) > 0)
      prime = unmarked.fcall('pop', [0])
      (Pyrb.range.call([2, Pyrb.int_divide(limit, prime) + 1])).each do |mult|
        if !!(((mult * prime)).in?(unmarked))
          unmarked.fcall('remove', [(mult * prime)])
        end
      end
      primes.fcall('append', [prime])
    end

    throw(:return, primes)
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
