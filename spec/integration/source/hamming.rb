# encoding: utf-8

require 'pyrb'

module Spec_integration_source_hamming
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['distance'] = Pyrb.defn(strand1: nil, strand2: nil) do |strand1, strand2|
    distance = 0
    index = 0
    while !!(index < Pyrb.len.call([strand1]))
      if !!(strand1[index] != strand2[index])
        distance += 1
      end

      index += 1
    end

    throw(:return, distance)
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
