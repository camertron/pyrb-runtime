# encoding: utf-8

require 'pyrb'

module Spec_integration_source_house
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['House'] = Pyrb::PythonClass.new('House', []).tap do |klass|
    klass.attrs['LYRICS'] = [["built", "house that Jack"], ["ate", "malt"], ["killed", "rat"], ["worried", "cat"], ["tossed", "dog"], ["milked", "cow with the crumpled horn"], ["kissed", "maiden all forlorn"], ["married", "man all tattered and torn"], ["woke", "priest all shaven and shorn"], ["kept", "rooster that crowed in the morn"], ["belonged to", "farmer sowing his corn"], ["", "horse and the hound and the horn"]]
    klass.attrs['LAST_LINE'] = "that lay in the house that Jack built."
    klass.attrs['rhyme'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil) do |cls|
        throw(:return, "\n\n".fcall('join', [Pyrb.range.call([12]).fcall('__iter__').map do |i|
          cls.fcall('verse', [i])
        end
        ]))
      end

    )

    klass.attrs['verse'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        throw(:return, "\n".fcall('join', [Pyrb::Filter.call([nil, cls.fcall('parts', [verse_num])])]))
      end

    )

    klass.attrs['parts'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        throw(:return, [cls.fcall('first', [verse_num]), cls.fcall('middle', [verse_num]), cls.fcall('last', [verse_num])])
      end

    )

    klass.attrs['first'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        if !!(verse_num != 0)
          throw(:return, cls.fcall('first_partial', [verse_num]))
        end

        throw(:return, cls.fcall('first_partial', [verse_num]) + " " + cls.fcall('verb', [verse_num]) + ".")
      end

    )

    klass.attrs['first_partial'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        throw(:return, "This is the " + cls.fcall('noun', [verse_num]))
      end

    )

    klass.attrs['middle'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        if !!(verse_num >= 2)
          throw(:return, "\n".fcall('join', [Pyrb.range.call([verse_num - 1, 0, -1]).fcall('__iter__').map do |num|
            cls.fcall('middle_partial', [num])
          end
          ]))
        end
      end

    )

    klass.attrs['middle_partial'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, num: nil) do |cls, num|
        throw(:return, "that " + cls.fcall('verb', [num]) + " the " + cls.fcall('noun', [num]))
      end

    )

    klass.attrs['verb'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, num: nil) do |cls, num|
        throw(:return, cls.attr('LYRICS')[num][0])
      end

    )

    klass.attrs['noun'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, num: nil) do |cls, num|
        throw(:return, cls.attr('LYRICS')[num][1])
      end

    )

    klass.attrs['last'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        if !!(verse_num != 0)
          throw(:return, cls.attr('LAST_LINE'))
        end
      end

    )
  end

  House = exports['House']

  exports['verse'] = Pyrb.defn(verse_num: nil) do |verse_num|
    throw(:return, exports['House'].fcall('verse', [verse_num]))
  end

  exports['rhyme'] = Pyrb.defn() do ||
    throw(:return, exports['House'].fcall('rhyme'))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
