# encoding: utf-8

require 'pyrb'

module Spec_integration_source_poker
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['STRAIGHT_FLUSH'] = 800
  exports['FOUR_OF_A_KIND'] = 700
  exports['FULL_HOUSE'] = 600
  exports['FLUSH'] = 500
  exports['STRAIGHT'] = 400
  exports['THREE_OF_A_KIND'] = 300
  exports['TWO_PAIR'] = 200
  exports['ONE_PAIR'] = 100
  exports['HIGH_CARD'] = 0
  exports['Poker'] = Pyrb::PythonClass.new('Poker', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, hands: nil) do |self_, hands|
      self_.attrs['hands'] = hands.fcall('__iter__').map do |hand|
        exports['Hand'].call([hand])
      end
    end

    klass.attrs['best_hand'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('scores').fcall('items').fcall('__iter__').each_with_object([]) do |(hand, score), _ret_|
        if !!(score == self_.fcall('best_score'))
          _ret_ << hand.attr('hand')
        end
      end
      )
    end

    klass.attrs['best_score'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.max.call([self_.fcall('scores'), ], { key: self_.fcall('scores').attr('get') }).fcall('score'))
    end

    klass.attrs['scores'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('hands').fcall('__iter__').each_with_object(Pyrb::Dict.new) do |(hand), _ret_|
        _ret_[hand] = hand.fcall('score')
      end
      )
    end
  end

  Poker = exports['Poker']

  exports['Hand'] = Pyrb::PythonClass.new('Hand', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, hand: nil) do |self_, hand|
      self_.attrs['hand'] = hand
      self_.attrs['cards'] = hand.fcall('__iter__').map do |card|
        exports['Card'].call([card])
      end

      self_.attrs['ranks'] = self_.attr('cards').fcall('__iter__').map do |card|
        card.attr('rank')
      end

      self_.attrs['suits'] = self_.attr('cards').fcall('__iter__').map do |card|
        card.attr('suit')
      end
    end

    klass.attrs['score'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.fcall('straight_flush'))
        throw(:return, exports['STRAIGHT_FLUSH'] + self_.fcall('high_card'))
      elsif !!(self_.fcall('four_of_a_kind'))
        throw(:return, exports['FOUR_OF_A_KIND'] + self_.fcall('rank_of_card_with_highest_occurence'))
      elsif !!(self_.fcall('full_house'))
        throw(:return, exports['FULL_HOUSE'] + self_.fcall('rank_of_card_with_highest_occurence'))
      elsif !!(self_.fcall('flush'))
        throw(:return, exports['FLUSH'] + self_.fcall('high_card'))
      elsif !!(self_.fcall('straight'))
        throw(:return, exports['STRAIGHT'] + self_.fcall('high_card'))
      elsif !!(self_.fcall('three_of_a_kind'))
        throw(:return, exports['THREE_OF_A_KIND'] + self_.fcall('rank_of_card_with_highest_occurence'))
      elsif !!(self_.fcall('two_pair'))
        throw(:return, exports['TWO_PAIR'] + self_.fcall('high_card'))
      elsif !!(self_.fcall('one_pair'))
        throw(:return, exports['ONE_PAIR'] + self_.fcall('rank_of_card_with_highest_occurence'))
      else
        throw(:return, self_.fcall('high_card'))
      end
    end

    klass.attrs['straight_flush'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('straight').and { self_.fcall('flush') })
    end

    klass.attrs['straight'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb::List.call([Pyrb.range.call([Pyrb.min.call([self_.attr('ranks')]), Pyrb.max.call([self_.attr('ranks')]) + 1])]) == Pyrb.sorted.call([self_.attr('ranks')]))
    end

    klass.attrs['flush'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.len.call([Pyrb::Set.call([self_.attr('suits')])]) == 1)
    end

    klass.attrs['four_of_a_kind'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.max.call([self_.fcall('rank_occurences').fcall('values')]) == 4)
    end

    klass.attrs['full_house'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('three_of_a_kind').and { self_.fcall('one_pair') })
    end

    klass.attrs['three_of_a_kind'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (3).in?(self_.fcall('rank_occurences').fcall('values')))
    end

    klass.attrs['two_pair'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, 2 == Pyrb::List.call([self_.fcall('rank_occurences').fcall('values')]).fcall('count', [2]))
    end

    klass.attrs['one_pair'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (2).in?(self_.fcall('rank_occurences').fcall('values')))
    end

    klass.attrs['high_card'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.max.call([self_.attr('ranks')]))
    end

    klass.attrs['rank_occurences'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('ranks').fcall('__iter__').each_with_object(Pyrb::Dict.new) do |(rank), _ret_|
        _ret_[rank] = self_.attr('ranks').fcall('count', [rank])
      end
      )
    end

    klass.attrs['rank_of_card_with_highest_occurence'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.max.call([self_.fcall('rank_occurences'), ], { key: self_.fcall('rank_occurences').attr('get') }))
    end
  end

  Hand = exports['Hand']

  exports['Card'] = Pyrb::PythonClass.new('Card', []).tap do |klass|
    klass.attrs['FACE_CARDS'] = Pyrb::Dict.new([{"T" => 10, "J" => 11, "Q" => 12, "K" => 13, "A" => 14}])
    klass.attrs['__init__'] = Pyrb.defn(self: nil, inp: nil) do |self_, inp|
      self_.attrs['rank'] = self_.fcall('numberify_face_cards', [inp[0]])
      self_.attrs['suit'] = inp[1]
    end

    klass.attrs['__str__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb::Str.call([self_.attr('rank')]) + self_.attr('suit'))
    end

    klass.attrs['numberify_face_cards'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, rank: nil) do |cls, rank|
        if !!((rank).in?(cls.attr('FACE_CARDS').fcall('keys')))
          throw(:return, cls.attr('FACE_CARDS')[rank])
        end

        throw(:return, Pyrb::Int.call([rank]))
      end

    )
  end

  Card = exports['Card']

  exports['poker'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['Poker'].call([inp]).fcall('best_hand'))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
