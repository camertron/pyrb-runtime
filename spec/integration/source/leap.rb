# encoding: utf-8

require 'pyrb'

module Spec_integration_source_leap
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['is_leap_year'] = Pyrb.defn(year: nil) do |year|
    if !!((year % 400) == 0)
      throw(:return, true)
    elsif !!((year % 100) == 0)
      throw(:return, false)
    elsif !!((year % 4) == 0)
      throw(:return, true)
    else
      throw(:return, false)
    end
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
