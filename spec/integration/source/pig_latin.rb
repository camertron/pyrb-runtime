# encoding: utf-8

require 'pyrb'

module Spec_integration_source_pig_latin
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  string, _ = import('string')

  exports['PigLatinTranslator'] = Pyrb::PythonClass.new('PigLatinTranslator', []).tap do |klass|
    klass.attrs['alpha'] = Pyrb::Set.call([string.attr('ascii_lowercase')])
    klass.attrs['vowels'] = Pyrb::Set.call([["a", "e", "i", "o", "u"]])
    klass.attrs['consonants'] = klass.attrs['alpha'] - klass.attrs['vowels']
    klass.attrs['translate_phrase'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, phrase: nil) do |cls, phrase|
        throw(:return, " ".fcall('join', [phrase.fcall('split').fcall('__iter__').map do |word|
          cls.fcall('translate', [word])
        end
        ]))
      end

    )

    klass.attrs['translate'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, word: nil) do |cls, word|
        if !!((((word[0]).in?(cls.attr('vowels'))).or { word.fcall('startswith', ["yt"]) }.or { word.fcall('startswith', ["xr"]) }))
          throw(:return, word + "ay")
        elsif !!((word.fcall('startswith', ["squ"]).or { word.fcall('startswith', ["sch"]) }.or { word.fcall('startswith', ["thr"]) }))
          throw(:return, word[3..-1] + word[0...3] + "ay")
        elsif !!(((((word[0]).in?(cls.attr('consonants'))).and { (word[1]).in?(cls.attr('consonants')) }).or { word.fcall('startswith', ["qu"]) }))
          throw(:return, word[2..-1] + word[0...2] + "ay")
        elsif !!(((word[0]).in?(cls.attr('consonants'))))
          throw(:return, word[1..-1] + word[0] + "ay")
        end
      end

    )
  end

  PigLatinTranslator = exports['PigLatinTranslator']

  exports['translate'] = Pyrb.defn(phrase: nil) do |phrase|
    throw(:return, exports['PigLatinTranslator'].fcall('translate_phrase', [phrase]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
