# encoding: utf-8

require 'pyrb'

module Spec_integration_source_prime
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  math, _ = import('math')

  exports['prime'] = Pyrb::PythonClass.new('prime', []).tap do |klass|
    klass.attrs['nth_prime'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, n: nil) do |cls, n|
        primes = []
        possible = cls.fcall('possible_primes')
        while !!(Pyrb.len.call([primes]) < n)
          x = Pyrb.next_.call([possible])
          if !!(cls.fcall('is_prime', [x]))
            primes.fcall('append', [x])
          end

        end

        throw(:return, primes[n - 1])
      end

    )

    klass.attrs['is_prime'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(x: nil) do |x|
        (Pyrb.range.call([2, Pyrb::Int.call([math.fcall('sqrt', [x])]) + 1])).each do |i|
          if !!((x % i) == 0)
            throw(:return, false)
          end
        end
        throw(:return, true)
      end

    )

    klass.attrs['possible_primes'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn() do ||
        Pyrb::Generator.new([
          Enumerator.new do |__pyrb_gen__|
            __pyrb_gen__ << 2
            n = 3
            while !!(true)
              __pyrb_gen__ << n
              n += 2
            end

          end
        ])
      end

    )
  end

  Prime = exports['prime']

  exports['nth_prime'] = Pyrb.defn(n: nil) do |n|
    throw(:return, exports['prime'].fcall('nth_prime', [n]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
