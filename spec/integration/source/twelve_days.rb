# encoding: utf-8

require 'pyrb'

module Spec_integration_source_twelve_days
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['TwelveDays'] = Pyrb::PythonClass.new('TwelveDays', []).tap do |klass|
    klass.attrs['CARDINALS'] = Pyrb::Dict.new([{1 => "first", 2 => "second", 3 => "third", 4 => "fourth", 5 => "fifth", 6 => "sixth", 7 => "seventh", 8 => "eighth", 9 => "ninth", 10 => "tenth", 11 => "eleventh", 12 => "twelfth"}])
    klass.attrs['PHRASES'] = Pyrb::Dict.new([{2 => "two Turtle Doves", 3 => "three French Hens", 4 => "four Calling Birds", 5 => "five Gold Rings", 6 => "six Geese-a-Laying", 7 => "seven Swans-a-Swimming", 8 => "eight Maids-a-Milking", 9 => "nine Ladies Dancing", 10 => "ten Lords-a-Leaping", 11 => "eleven Pipers Piping", 12 => "twelve Drummers Drumming"}])
    klass.attrs['verses'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, start: nil, stop: nil) do |cls, start, stop|
        throw(:return, "\n".fcall('join', [Pyrb.range.call([start, stop + 1]).fcall('__iter__').map do |i|
          cls.fcall('verse', [i])
        end
        ]) + "\n")
      end

    )

    klass.attrs['verse'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        throw(:return, ", ".fcall('join', [Pyrb::Filter.call([nil, [cls.fcall('head', [verse_num]), cls.fcall('mid', [verse_num]), cls.fcall('tail', [verse_num])]])]))
      end

    )

    klass.attrs['head'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        throw(:return, ((("On the %(cardinality)s day of Christmas my true love gave to " + "me") % (Pyrb::Dict.new([{"cardinality" => cls.attr('CARDINALS')[verse_num]}])))))
      end

    )

    klass.attrs['tail'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(verse_num: nil) do |verse_num|
        if !!(verse_num == 1)
          throw(:return, "a Partridge in a Pear Tree.\n")
        end

        throw(:return, "and a Partridge in a Pear Tree.\n")
      end

    )

    klass.attrs['mid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        if !!(verse_num != 1)
          throw(:return, ", ".fcall('join', [Pyrb.range.call([verse_num, 1, -1]).fcall('__iter__').map do |i|
            cls.attr('PHRASES')[i]
          end
          ]))
        end
      end

    )
  end

  TwelveDays = exports['TwelveDays']

  exports['verse'] = Pyrb.defn(verse_num: nil) do |verse_num|
    throw(:return, exports['TwelveDays'].fcall('verse', [verse_num]))
  end

  exports['verses'] = Pyrb.defn(start: nil, stop: nil) do |start, stop|
    throw(:return, exports['TwelveDays'].fcall('verses', [start, stop]))
  end

  exports['sing'] = Pyrb.defn() do ||
    throw(:return, exports['TwelveDays'].fcall('verses', [1, 12]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
