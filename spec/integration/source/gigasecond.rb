# encoding: utf-8

require 'pyrb'

module Spec_integration_source_gigasecond
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  timedelta, _ = import('timedelta', from: 'datetime')

  exports['add_gigasecond'] = Pyrb.defn(date: nil) do |date|
    throw(:return, date + timedelta.call([], { seconds: 10 ** 9 }))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
