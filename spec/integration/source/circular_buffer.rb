# encoding: utf-8

require 'pyrb'

module Spec_integration_source_circular_buffer
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  deque, _ = import('deque', from: 'collections')

  exports['CircularBuffer'] = Pyrb::PythonClass.new('CircularBuffer', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, capacity: nil) do |self_, capacity|
      self_.attrs['capacity'] = capacity
      self_.attrs['buffer'] = deque.call([[], capacity])
    end

    klass.attrs['read'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.fcall('empty'))
        Pyrb.raize(exports['BufferEmptyException'])
      end

      throw(:return, self_.attr('buffer').fcall('popleft'))
    end

    klass.attrs['write'] = Pyrb.defn(self: nil, data: nil) do |self_, data|
      if !!(self_.fcall('full'))
        Pyrb.raize(exports['BufferFullException'])
      end

      self_.attr('buffer').fcall('append', [data])
    end

    klass.attrs['overwrite'] = Pyrb.defn(self: nil, data: nil) do |self_, data|
      if !!(self_.fcall('full'))
        self_.attr('buffer').fcall('popleft')
      end

      self_.attr('buffer').fcall('append', [data])
    end

    klass.attrs['clear'] = Pyrb.defn(self: nil) do |self_|
      self_.attr('buffer').fcall('clear')
    end

    klass.attrs['empty'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.len.call([self_.attr('buffer')]) == 0)
    end

    klass.attrs['full'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.len.call([self_.attr('buffer')]) == self_.attr('capacity'))
    end
  end

  CircularBuffer = exports['CircularBuffer']

  exports['BufferFullException'] = Pyrb::PythonClass.new('BufferFullException', [Pyrb::Exception])
  BufferFullException = exports['BufferFullException']

  exports['BufferEmptyException'] = Pyrb::PythonClass.new('BufferEmptyException', [Pyrb::Exception])
  BufferEmptyException = exports['BufferEmptyException']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
