# encoding: utf-8

require 'pyrb'

module Spec_integration_source_pangram
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['ALPHABET'] = "abcdefghijklmnopqrstuvwxyz "
  exports['is_pangram'] = Pyrb.defn(s: nil) do |s|
    throw(:return, Pyrb::Set.call([Pyrb::List.call([s.fcall('lower')])]) >= Pyrb::Set.call([exports['ALPHABET']]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
