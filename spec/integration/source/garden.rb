# encoding: utf-8

require 'pyrb'

module Spec_integration_source_garden
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Garden'] = Pyrb::PythonClass.new('Garden', []).tap do |klass|
    klass.attrs['DEFAULT_STUDENTS'] = (("Alice Bob Charlie David Eve Fred Ginny " + "Harriet Ileana Joseph Kincaid Larry")).fcall('split')
    klass.attrs['PLANTS'] = Pyrb::Dict.new([{"G" => "Grass", "C" => "Clover", "R" => "Radishes", "V" => "Violets"}])
    klass.attrs['__init__'] = Pyrb.defn(self: nil, diagram: nil, students: { default: klass.attrs['DEFAULT_STUDENTS'] }) do |self_, diagram, students|
      self_.attrs['diagram'] = diagram
      self_.attrs['rows'] = diagram.fcall('split').fcall('__iter__').map do |row|
        Pyrb::List.call([row])
      end

      self_.attrs['plant_rows'] = self_.attr('rows').fcall('__iter__').map do |row|
        row.fcall('__iter__').map do |c|
          self_.attr('PLANTS')[c]
        end
      end

      self_.attrs['students'] = Pyrb.sorted.call([students])
    end

    klass.attrs['plants'] = Pyrb.defn(self: nil, name: nil) do |self_, name|
      throw(:return, self_.fcall('plants_for_index', [self_.attr('students').fcall('index', [name])]))
    end

    klass.attrs['plants_for_index'] = Pyrb.defn(self: nil, i: nil) do |self_, i|
      throw(:return, [self_.attr('plant_rows')[0][(i * 2)], self_.attr('plant_rows')[0][(i * 2) + 1], self_.attr('plant_rows')[1][(i * 2)], self_.attr('plant_rows')[1][(i * 2) + 1]])
    end
  end

  Garden = exports['Garden']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
