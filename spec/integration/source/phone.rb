# encoding: utf-8

require 'pyrb'

module Spec_integration_source_phone
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Phone'] = Pyrb::PythonClass.new('Phone', []).tap do |klass|
    klass.attrs['INVALID_NUM'] = ("0" * 10)
    klass.attrs['AREA_CODE_END'] = 3
    klass.attrs['EXCHANGE_CODE_END'] = 6
    klass.attrs['__init__'] = Pyrb.defn(self: nil, inp: nil) do |self_, inp|
      self_.attrs['number'] = self_.fcall('number', [inp])
    end

    klass.attrs['number'] = Pyrb.defn(self: nil, inp: nil) do |self_, inp|
      cleaned = self_.fcall('strip', [inp])
      if !!(self_.fcall('valid_11_digits', [cleaned]))
        throw(:return, cleaned[1..-1])
      elsif !!(self_.fcall('valid_10_digits', [cleaned]))
        throw(:return, cleaned)
      end

      throw(:return, self_.attr('INVALID_NUM'))
    end

    klass.attrs['area_code'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('number')[0...self_.attr('AREA_CODE_END')])
    end

    klass.attrs['exchange_code'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('number')[self_.attr('AREA_CODE_END')...self_.attr('EXCHANGE_CODE_END')])
    end

    klass.attrs['subscriber_code'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('number')[self_.attr('EXCHANGE_CODE_END')..-1])
    end

    klass.attrs['pretty'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, "({}) {}-{}".fcall('format', [self_.fcall('area_code'), self_.fcall('exchange_code'), self_.fcall('subscriber_code')]))
    end

    klass.attrs['valid_11_digits'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(inp: nil) do |inp|
        throw(:return, (Pyrb.len.call([inp]) == 11).and { inp.fcall('startswith', ["1"]) })
      end

    )

    klass.attrs['valid_10_digits'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(inp: nil) do |inp|
        throw(:return, Pyrb.len.call([inp]) == 10)
      end

    )

    klass.attrs['strip'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(inp: nil) do |inp|
        throw(:return, "".fcall('join', [inp.fcall('__iter__').each_with_object([]) do |char, _ret_|
          if !!(char.fcall('isdigit'))
            _ret_ << char
          end
        end
        ]))
      end

    )
  end

  Phone = exports['Phone']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
