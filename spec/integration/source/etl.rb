# encoding: utf-8

require 'pyrb'

module Spec_integration_source_etl
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['transform'] = Pyrb.defn(old: nil) do |old|
    throw(:return, old.fcall('items').fcall('__iter__').each_with_object(Pyrb::Dict.new) do |(score, values), _ret_|
      values.fcall('__iter__').map do |value|
        _ret_[value.fcall('lower')] = score
      end
    end
    )
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
