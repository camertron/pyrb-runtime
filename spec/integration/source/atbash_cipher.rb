# encoding: utf-8

require 'pyrb'

module Spec_integration_source_atbash_cipher
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  string, _ = import('string')

  exports['Atbash'] = Pyrb::PythonClass.new('Atbash', []).tap do |klass|
    klass.attrs['PLAIN'] = "abcdefghijklmnopqrstuvwxyz"
    klass.attrs['PRIME'] = "zyxwvutsrqponmlkjihgfedcba"
    klass.attrs['CIPHER'] = Pyrb::Dict.call([Pyrb::Zip.call([Pyrb::List.call([klass.attrs['PLAIN']]), Pyrb::List.call([klass.attrs['PRIME']])])])
    klass.attrs['EXCLUDE'] = Pyrb::Set.call([string.attr('punctuation') + " "])
    klass.attrs['encode'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, msg: nil) do |self_, msg|
        throw(:return, self_.fcall('split_every_five', [self_, self_.fcall('encoded', [self_, msg])]))
      end

    )

    klass.attrs['split_every_five'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, encoded: nil) do |self_, encoded|
        throw(:return, " ".fcall('join', [Pyrb.range.call([0, Pyrb.len.call([encoded]), 5]).fcall('__iter__').map do |i|
          encoded[i...i + 5]
        end
        ]))
      end

    )

    klass.attrs['encoded'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, msg: nil) do |self_, msg|
        throw(:return, "".fcall('join', [(self_.fcall('clean', [self_, msg]).fcall('__iter__').map do |char|
          !!(char.fcall('isdigit')) ? char : self_.attr('CIPHER')[char]
        end
        )]))
      end

    )

    klass.attrs['clean'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, msg: nil) do |self_, msg|
        throw(:return, msg.fcall('lower').fcall('__iter__').each_with_object([]) do |char, _ret_|
          if !!(!(char).in?(self_.attr('EXCLUDE')))
            _ret_ << char
          end
        end
        )
      end

    )

    klass.attrs['decode'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(self: nil, msg: nil) do |self_, msg|
        throw(:return, self_.fcall('encoded', [self_, msg]))
      end

    )
  end

  Atbash = exports['Atbash']

  exports['encode'] = Pyrb.defn(msg: nil) do |msg|
    throw(:return, exports['Atbash'].fcall('encode', [exports['Atbash'], msg]))
  end

  exports['decode'] = Pyrb.defn(msg: nil) do |msg|
    throw(:return, exports['Atbash'].fcall('decode', [exports['Atbash'], msg]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
