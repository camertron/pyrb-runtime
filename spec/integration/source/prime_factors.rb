# encoding: utf-8

require 'pyrb'

module Spec_integration_source_prime_factors
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['prime_factors'] = Pyrb.defn(num: nil) do |num|
    factors = []
    i = 2
    while !!(i <= num)
      if !!((num % i) == 0)
        factors.fcall('append', [i])
        num /= i
      else
        i += 1
      end

    end

    throw(:return, factors)
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
