# encoding: utf-8

require 'pyrb'

module Spec_integration_source_run_length
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  groupby, _ = import('groupby', from: 'itertools')
  re, _ = import('re')

  exports['encode'] = Pyrb.defn(s: nil) do |s|
    throw(:return, "".fcall('join', [Pyrb::Map.call([Pyrb.defn(g: nil) { |g| exports['helper'].call([g]) }, groupby.call([s]).fcall('__iter__').map do |(k, g)|
      Pyrb::List.call([g])
    end
    ])]))
  end

  exports['helper'] = Pyrb.defn(g: nil) do |g|
    throw(:return, !!(Pyrb.len.call([g]) == 1) ? g[0] : Pyrb::Str.call([Pyrb.len.call([g])]) + g[0])
  end

  exports['decode'] = Pyrb.defn(s: nil) do |s|
    groups = re.fcall('findall', [%q[(\d*\D{1})], s])
    pairs = Pyrb::Map.call([Pyrb.defn(g: nil) { |g| [re.fcall('match', [%q(\d*), g]).fcall('group'), g[-1]] }, groups])
    throw(:return, "".fcall('join', [Pyrb::Map.call([Pyrb.defn(x: nil) { |x| !!(x[0].fcall('isdigit')) ? (Pyrb::Int.call([x[0]]) * x[1]) : x[1] }, pairs])]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
