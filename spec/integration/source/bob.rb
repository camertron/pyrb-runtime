# encoding: utf-8

require 'pyrb'

module Spec_integration_source_bob
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['hey'] = Pyrb.defn(phrase: nil) do |phrase|
    phrase = phrase.fcall('strip')
    if !!((phrase).not)
      throw(:return, "Fine. Be that way!")
    elsif !!(phrase.fcall('isupper'))
      throw(:return, "Whoa, chill out!")
    elsif !!(phrase.fcall('endswith', ["?"]))
      throw(:return, "Sure.")
    else
      throw(:return, "Whatever.")
    end
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
