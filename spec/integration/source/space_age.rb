# encoding: utf-8

require 'pyrb'

module Spec_integration_source_space_age
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['SpaceAge'] = Pyrb::PythonClass.new('SpaceAge', []).tap do |klass|
    klass.attrs['ORBITAL_PERIODS'] = Pyrb::Dict.new([{"mercury" => 7600530.24, "venus" => 19413907.2, "earth" => 31558149.76, "mars" => 59354294.4, "jupiter" => 374335776.0, "saturn" => 929596608.0, "uranus" => 2661041808.0, "neptune" => 5200418592.0}])
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seconds: nil) do |self_, seconds|
      self_.attrs['seconds'] = seconds
    end

    klass.attrs['on_planet'] = Pyrb.defn(self: nil, planet: nil) do |self_, planet|
      throw(:return, Pyrb.round.call([(self_.attr('seconds') / self_.attr('ORBITAL_PERIODS')[planet]), 2]))
    end
  end

  SpaceAge = exports['SpaceAge']

  exports['add_on_planet_fn'] = Pyrb.defn(planet: nil) do |planet|
    Pyrb.setattr.call([exports['SpaceAge'], "on_" + planet, Pyrb.defn(self: nil) { |self_| self_.fcall('on_planet', [planet]) }])
  end

  (exports['SpaceAge'].attr('ORBITAL_PERIODS').fcall('keys')).each do |planet|
    exports['add_on_planet_fn'].call([planet])
  end
  Pyrb::Sys.exports['modules'][__FILE__] = self
end
