# encoding: utf-8

require 'pyrb'

module Spec_integration_source_dna
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['to_rna'] = Pyrb.defn(strand: nil) do |strand|
    result = ""
    (strand).each do |i|
      result += exports['pairing_for'].call([i])
    end
    throw(:return, result)
  end

  exports['pairing_for'] = Pyrb.defn(nucleobase: nil) do |nucleobase|
    pairings = Pyrb::Dict.new([{"G" => "C", "C" => "G", "A" => "U", "T" => "A"}])
    throw(:return, pairings[nucleobase])
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
