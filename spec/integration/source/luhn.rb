# encoding: utf-8

require 'pyrb'

module Spec_integration_source_luhn
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Luhn'] = Pyrb::PythonClass.new('Luhn', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, number: nil) do |self_, number|
      self_.attrs['number'] = number
    end

    klass.attrs['checksum'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (Pyrb.sum.call([self_.fcall('addends')]) % 10))
    end

    klass.attrs['addends'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.enumerate.call([Pyrb.reversed.call([Pyrb::Str.call([self_.attr('number')])])]).fcall('__iter__').map do |(idx, val)|
        self_.fcall('addend', [idx, Pyrb::Int.call([val])])
      end
      )
    end

    klass.attrs['addend'] = Pyrb.defn(self: nil, idx: nil, val: nil) do |self_, idx, val|
      throw(:return, self_.fcall('subtract_nine', [idx, self_.fcall('double_every_other', [idx, val])]))
    end

    klass.attrs['double_every_other'] = Pyrb.defn(self: nil, idx: nil, val: nil) do |self_, idx, val|
      throw(:return, !!((idx % 2) == 1) ? (val * 2) : val)
    end

    klass.attrs['subtract_nine'] = Pyrb.defn(self: nil, idx: nil, val: nil) do |self_, idx, val|
      throw(:return, !!(val > 10) ? val - 9 : val)
    end

    klass.attrs['is_valid'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('checksum') == 0)
    end

    klass.attrs['create'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, num: nil) do |cls, num|
        (Pyrb.range.call([0, 10])).each do |i|
          if !!(cls.call([Pyrb::Int.call([Pyrb::Str.call([num]) + Pyrb::Str.call([i])])]).fcall('is_valid'))
            throw(:return, Pyrb::Int.call([Pyrb::Str.call([num]) + Pyrb::Str.call([i])]))
          end
        end
      end

    )
  end

  Luhn = exports['Luhn']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
