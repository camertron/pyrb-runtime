# encoding: utf-8

require 'pyrb'

module Spec_integration_source_flatten_array
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['flatten'] = Pyrb.defn(nested_list: nil) do |nested_list|
    throw(:return, exports['flatten_list'].call([nested_list]).fcall('__iter__').map do |element|
      element
    end
    )
  end

  exports['flatten_list'] = Pyrb.defn(nested_list: nil) do |nested_list|
    Pyrb::Generator.new([
      Enumerator.new do |__pyrb_gen__|
        (nested_list).each do |element|
          if !!((Pyrb.isinstance.call([element, Pyrb::List])).not)
            if !!((element.object_id != nil.object_id).and { (Pyrb.isinstance.call([element, Pyrb::List])).not })
              __pyrb_gen__ << element
            end

          else
            (exports['flatten_list'].call([element])).each do |subelement|
              __pyrb_gen__ << subelement
            end
          end
        end
      end
    ])
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
