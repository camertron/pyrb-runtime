# encoding: utf-8

require 'pyrb'

module Spec_integration_source_largest_series_product
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  reduce, _ = import('reduce', from: 'functools')

  exports['largest_product'] = Pyrb.defn(digits: nil, size: nil) do |digits, size|
    products = exports['slices'].call([digits, size]).fcall('__iter__').map do |s|
      reduce.call([Pyrb.defn(x: nil, y: nil) { |x, y| (x * y) }, s, 1])
    end

    throw(:return, Pyrb.max.call([products]))
  end

  exports['slices'] = Pyrb.defn(digits: nil, size: nil) do |digits, size|
    if !!(Pyrb.len.call([digits]) < size)
      Pyrb.raize(Pyrb::ValueError)
    end

    throw(:return, exports['each_cons'].call([Pyrb::List.call([Pyrb::Map.call([Pyrb::Int, Pyrb::List.call([digits])])]), size]))
  end

  exports['each_cons'] = Pyrb.defn(x: nil, size: nil) do |x, size|
    throw(:return, Pyrb.range.call([Pyrb.len.call([x]) - size + 1]).fcall('__iter__').map do |i|
      x[i...i + size]
    end
    )
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
