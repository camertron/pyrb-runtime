# encoding: utf-8

require 'pyrb'

module Spec_integration_source_bracket_push
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['CheckBrackets'] = Pyrb::PythonClass.new('CheckBrackets', []).tap do |klass|
    klass.attrs['OPENERS'] = Pyrb::Dict.new([{"{" => "}", "[" => "]", "(" => ")"}])
    klass.attrs['CLOSERS'] = Pyrb::Set.call([klass.attrs['OPENERS'].fcall('values')])
    klass.attrs['__init__'] = Pyrb.defn(self: nil, inp: nil) do |self_, inp|
      self_.attrs['check_brackets'] = self_.fcall('build_stack', [inp])
    end

    klass.attrs['build_stack'] = Pyrb.defn(self: nil, inp: nil) do |self_, inp|
      stack = []
      (Pyrb::List.call([inp])).each do |char|
        if !!((char).in?(self_.attr('OPENERS')))
          stack.fcall('append', [char])
        elsif !!((((char).in?(self_.attr('CLOSERS'))).and { stack }.and { self_.fcall('corresponding_brackets', [stack[-1], char]) }))
          stack.fcall('pop')
        else
          throw(:return, false)
        end
      end
      throw(:return, (Pyrb::Bool.call([stack])).not)
    end

    klass.attrs['corresponding_brackets'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, opener: nil, closer: nil) do |cls, opener, closer|
        throw(:return, cls.attr('OPENERS')[opener] == closer)
      end

    )
  end

  CheckBrackets = exports['CheckBrackets']

  exports['check_brackets'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['CheckBrackets'].call([inp]).attr('check_brackets'))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
