# encoding: utf-8

require 'pyrb'

module Spec_integration_source_hello_world
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['hello'] = Pyrb.defn() do ||
    throw(:return, "Hello, World!")
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
