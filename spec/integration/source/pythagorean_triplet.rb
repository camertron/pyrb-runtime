# encoding: utf-8

require 'pyrb'

module Spec_integration_source_pythagorean_triplet
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  gcd, _ = import('gcd', from: 'math')
  combinations, _ = import('combinations', from: 'itertools')

  exports['is_triplet'] = Pyrb.defn(sides: nil) do |sides|
    throw(:return, exports['Triplet'].call([sides]).fcall('valid'))
  end

  exports['primitive_triplets'] = Pyrb.defn(n: nil) do |n|
    throw(:return, exports['Triplet'].fcall('primitive_triplets', [n]))
  end

  exports['triplets_in_range'] = Pyrb.defn(low: nil, high: nil) do |low, high|
    throw(:return, exports['Triplet'].fcall('triplets_in_range', [low, high]))
  end

  exports['Triplet'] = Pyrb::PythonClass.new('Triplet', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, sides: nil) do |self_, sides|
      self_.attrs['sides'] = sides
      self_.attrs['sorted_sides'] = Pyrb.sorted.call([sides])
      self_.attrs['a'] = self_.attr('sorted_sides')[0]
      self_.attrs['b'] = self_.attr('sorted_sides')[1]
      self_.attrs['c'] = self_.attr('sorted_sides')[2]
    end

    klass.attrs['valid'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('a') ** 2 + self_.attr('b') ** 2 == self_.attr('c') ** 2)
    end

    klass.attrs['valid_primitive'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('valid').and { ((gcd.call([self_.attr('a'), self_.attr('b')]) == 1).and { gcd.call([self_.attr('b'), self_.attr('c')]) == 1 }.and { gcd.call([self_.attr('a'), self_.attr('c')]) == 1 }) })
    end

    klass.attrs['valid_in_range'] = Pyrb.defn(self: nil, low: nil, high: nil) do |self_, low, high|
      valid_range = Pyrb.range.call([low, high + 1])
      throw(:return, self_.fcall('valid').and { (((self_.attr('a')).in?(valid_range)).and { (self_.attr('b')).in?(valid_range) }.and { (self_.attr('c')).in?(valid_range) }) })
    end

    klass.attrs['triplets'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, n: nil) do |cls, n|
        throw(:return, Pyrb::Set.call([cls.fcall('valid_pairs', [(n / 2)]).fcall('__iter__').map do |pair|
          cls.fcall('generate_sides', [pair])
        end
        ]))
      end

    )

    klass.attrs['primitive_triplets'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, n: nil) do |cls, n|
        if !!(cls.fcall('odd', [n]))
          Pyrb.raize(Pyrb::ValueError)
        else
          throw(:return, Pyrb::Set.call([cls.fcall('triplets', [n]).fcall('__iter__').each_with_object([]) do |triplet, _ret_|
            if !!(cls.call([triplet]).fcall('valid_primitive'))
              _ret_ << triplet
            end
          end
          ]))
        end
      end

    )

    klass.attrs['triplets_in_range'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, low: nil, high: nil) do |cls, low, high|
        throw(:return, Pyrb::Set.call([cls.fcall('possible_triplets', [low, high + 1]).fcall('__iter__').each_with_object([]) do |triplet, _ret_|
          if !!(cls.call([triplet]).fcall('valid_in_range', [low, high]))
            _ret_ << triplet
          end
        end
        ]))
      end

    )

    klass.attrs['valid_pairs'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, n: nil) do |cls, n|
        throw(:return, cls.fcall('factor_pairs', [n]).fcall('__iter__').each_with_object([]) do |pair, _ret_|
          if !!(cls.fcall('valid_pair', [pair]))
            _ret_ << pair
          end
        end
        )
      end

    )

    klass.attrs['valid_pair'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, pair: nil) do |cls, pair|
        n, m = Pyrb.deconstruct(2, pair)
        throw(:return, cls.fcall('odd', [m - n]))
      end

    )

    klass.attrs['generate_sides'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(pair: nil) do |pair|
        n, m = Pyrb.deconstruct(2, pair)
        throw(:return, Pyrb::List.call([Pyrb.sorted.call([[m ** 2 - n ** 2, (2 * m * n), m ** 2 + n ** 2]])]))
      end

    )

    klass.attrs['factor_pairs'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(n: nil) do |n|
        throw(:return, Pyrb.range.call([1, Pyrb::Int.call([n ** 0.5]) + 1]).fcall('__iter__').each_with_object([]) do |i, _ret_|
          if !!((n % i) == 0)
            _ret_ << [Pyrb::Int.call([i]), Pyrb::Int.call([(n / i)])]
          end
        end
        )
      end

    )

    klass.attrs['possible_triplets'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(low: nil, high: nil) do |low, high|
        throw(:return, combinations.call([Pyrb.range.call([low, high]), 3]))
      end

    )

    klass.attrs['odd'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(n: nil) do |n|
        throw(:return, (n % 2) == 1)
      end

    )
  end

  Triplet = exports['Triplet']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
