# encoding: utf-8

require 'pyrb'

module Spec_integration_source_say
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Say'] = Pyrb::PythonClass.new('Say', []).tap do |klass|
    klass.attrs['VALS'] = Pyrb::Dict.new([{1000000000 => "billion", 1000000 => "million", 1000 => "thousand", 90 => "ninety", 80 => "eighty", 70 => "seventy", 60 => "sixty", 50 => "fifty", 40 => "forty", 30 => "thirty", 20 => "twenty", 19 => "nineteen", 17 => "seventeen", 16 => "sixteen", 15 => "fifteen", 14 => "fourteen", 13 => "thirteen", 12 => "twelve", 11 => "eleven", 10 => "ten", 9 => "nine", 8 => "eight", 7 => "seven", 6 => "six", 5 => "five", 4 => "four", 3 => "three", 2 => "two", 1 => "one", 0 => ""}])
    klass.attrs['__init__'] = Pyrb.defn(self: nil, num: nil) do |self_, num|
      self_.attrs['num'] = num
      self_.attrs['words'] = !!(num == 0) ? "zero" : self_.fcall('wordify', [num])
    end

    klass.attrs['wordify'] = Pyrb.defn(self: nil, num: nil) do |self_, num|
      self_.fcall('check_valid', [num])
      words = Pyrb::List.call([Pyrb::Map.call([Pyrb.defn(i_chunk: nil) { |i_chunk| self_.fcall('wordify_chunk', [i_chunk[1], i_chunk[0]]) }, Pyrb.enumerate.call([self_.fcall('chunkify', [num])])])])
      while !!(words[-1] == "and")
        words.del(-1)
      end

      throw(:return, " ".fcall('join', [words]).fcall('rstrip'))
    end

    klass.attrs['chunkify'] = Pyrb.defn(self: nil, num: nil) do |self_, num|
      rev = Pyrb::Str.call([num])[Pyrb::Range.new([nil, nil, -1])]
      rev_chunks = (Pyrb.range.call([0, Pyrb.len.call([rev]), 3]).fcall('__iter__').map do |i|
        rev[i...i + 3]
      end
      )[Pyrb::Range.new([nil, nil, -1])]
      throw(:return, Pyrb::List.call([Pyrb::Map.call([Pyrb.defn(x: nil) { |x| Pyrb::Int.call([x[Pyrb::Range.new([nil, nil, -1])]]) }, rev_chunks])]))
    end

    klass.attrs['wordify_chunk'] = Pyrb.defn(self: nil, chunk: nil, i: nil) do |self_, chunk, i|
      hundreds_digit, left_over = Pyrb.deconstruct(2, Pyrb.divmod.call([chunk, 100]))
      hundreds = self_.fcall('get_val', [hundreds_digit])
      tens_digit, ones_digit = Pyrb.deconstruct(2, Pyrb.divmod.call([left_over, 10]))
      if !!((left_over > 10).and { left_over < 20 })
        tens = self_.fcall('get_val', [left_over])
        ones = nil
      else
        tens = self_.fcall('get_val', [(tens_digit * 10)])
        ones = self_.fcall('get_val', [ones_digit])
      end

      word_chunk = self_.fcall('frmt_chunk', [hundreds, tens, ones])
      units = self_.fcall('get_units', [Pyrb.len.call([self_.fcall('chunkify', [self_.attr('num')])]) - 1 - i])
      throw(:return, !!(word_chunk) ? word_chunk + " " + units : "and")
    end

    klass.attrs['frmt_chunk'] = Pyrb.defn(self: nil, hundreds: nil, tens: nil, ones: nil) do |self_, hundreds, tens, ones|
      chunk = ""
      if !!(hundreds)
        chunk += hundreds + " hundred "
      end

      if !!(hundreds.and { tens })
        chunk += "and "
      end

      if !!(tens)
        chunk += tens
      end

      if !!(tens.and { ones })
        chunk += "-"
      end

      if !!(ones)
        chunk += ones
      end

      throw(:return, chunk)
    end

    klass.attrs['check_valid'] = Pyrb.defn(self: nil, num: nil) do |self_, num|
      if !!((num < 0).or { num > 999999999999 })
        Pyrb.raize(Pyrb::AttributeError)
      end
    end

    klass.attrs['get_units'] = Pyrb.defn(self: nil, d: nil) do |self_, d|
      throw(:return, !!(1000 ** d > 1) ? self_.fcall('get_val', [1000 ** d]) : "")
    end

    klass.attrs['get_val'] = Pyrb.defn(self: nil, d: nil) do |self_, d|
      throw(:return, self_.attr('VALS')[d])
    end
  end

  Say = exports['Say']

  exports['say'] = Pyrb.defn(num: nil) do |num|
    throw(:return, exports['Say'].call([Pyrb::Int.call([num])]).attr('words'))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
