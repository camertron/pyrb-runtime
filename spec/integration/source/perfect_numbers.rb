# encoding: utf-8

require 'pyrb'

module Spec_integration_source_perfect_numbers
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  math, _ = import('math')
  reduce, _ = import('reduce', from: 'functools')

  exports['is_perfect'] = Pyrb.defn(number: nil) do |number|
    throw(:return, Pyrb.sum.call([exports['factors'].call([number])]) == number)
  end

  exports['factors'] = Pyrb.defn(n: nil) do |n|
    throw(:return, Pyrb::Set.call([reduce.call([Pyrb::List.attr('__add__'), exports['pairs_of_factors'].call([n])])]) - Pyrb::Set.call([[n]]))
  end

  exports['pairs_of_factors'] = Pyrb.defn(n: nil) do |n|
    throw(:return, Pyrb.range.call([1, Pyrb::Int.call([math.fcall('sqrt', [n])]) + 1]).fcall('__iter__').each_with_object([]) do |i, _ret_|
      if !!((n % i) == 0)
        _ret_ << [i, (n / i)]
      end
    end
    )
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
