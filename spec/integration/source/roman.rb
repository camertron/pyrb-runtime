# encoding: utf-8

require 'pyrb'

module Spec_integration_source_roman
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Roman'] = Pyrb::PythonClass.new('Roman', []).tap do |klass|
    klass.attrs['NUMERALS'] = Pyrb::Dict.new([{1 => "I", 4 => "IV", 5 => "V", 9 => "IX", 10 => "X", 40 => "XL", 50 => "L", 90 => "XC", 100 => "C", 400 => "CD", 500 => "D", 900 => "CM", 1000 => "M"}])
    klass.attrs['numeral'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, arabic: nil) do |cls, arabic|
        throw(:return, "".fcall('join', [Pyrb::Map.call([Pyrb.defn(key: nil) { |key| cls.attr('NUMERALS')[key] }, cls.fcall('get_components', [arabic])])]))
      end

    )

    klass.attrs['get_components'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, arabic: nil) do |cls, arabic|
        components = []
        (Pyrb.reversed.call([Pyrb.sorted.call([cls.attr('NUMERALS').fcall('keys')])])).each do |key|
          while !!(arabic >= key)
            arabic -= key
            components.fcall('append', [key])
          end
        end
        throw(:return, components)
      end

    )
  end

  Roman = exports['Roman']

  exports['numeral'] = Pyrb.defn(arabic: nil) do |arabic|
    throw(:return, exports['Roman'].fcall('numeral', [arabic]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
