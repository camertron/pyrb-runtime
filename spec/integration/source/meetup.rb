# encoding: utf-8

require 'pyrb'

module Spec_integration_source_meetup
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  Calendar, _ = import('Calendar', from: 'calendar')

  exports['meetup_day'] = Pyrb.defn(year: nil, month: nil, weekday: nil, schedule: nil) do |year, month, weekday, schedule|
    candidates = Calendar.call().fcall('itermonthdates', [year, month]).fcall('__iter__').each_with_object([]) do |date, _ret_|
      if !!((date.attr('month') == month).and { date.fcall('strftime', ["%A"]) == weekday })
        _ret_ << date
      end
    end

    if !!(schedule == "teenth")
      throw(:return, exports['find_teenth'].call([candidates]))
    else
      throw(:return, exports['find'].call([candidates, schedule]))
    end
  end

  exports['find_teenth'] = Pyrb.defn(candidates: nil) do |candidates|
    (candidates).each do |date|
      if !!((date.attr('day') >= 13).and { date.attr('day') <= 19 })
        throw(:return, date)
      end
    end
  end

  exports['find'] = Pyrb.defn(candidates: nil, schedule: nil) do |candidates, schedule|
    index = !!(schedule == "last") ? -1 : Pyrb::Int.call([schedule[0]]) - 1
    throw(:return, candidates[index])
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
