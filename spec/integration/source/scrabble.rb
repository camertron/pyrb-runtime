# encoding: utf-8

require 'pyrb'

module Spec_integration_source_scrabble
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Scrabble'] = Pyrb::PythonClass.new('Scrabble', []).tap do |klass|
    klass.attrs['LETTER_VALUES'] = Pyrb::Dict.new([{"a" => 1, "b" => 3, "c" => 3, "d" => 2, "e" => 1, "f" => 4, "g" => 2, "h" => 4, "i" => 1, "j" => 8, "k" => 5, "l" => 1, "m" => 3, "n" => 1, "o" => 1, "p" => 3, "q" => 10, "r" => 1, "s" => 1, "t" => 1, "u" => 1, "v" => 4, "w" => 4, "x" => 8, "y" => 4, "z" => 10}])
    klass.attrs['score'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, word: nil) do |cls, word|
        if !!((cls.fcall('valid', [word])).not)
          throw(:return, 0)
        end

        throw(:return, Pyrb.sum.call([word.fcall('__iter__').map do |c|
          cls.attr('LETTER_VALUES')[c.fcall('lower')]
        end
        ]))
      end

    )

    klass.attrs['valid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, word: nil) do |cls, word|
        throw(:return, word.fcall('isalpha'))
      end

    )
  end

  Scrabble = exports['Scrabble']

  exports['score'] = Pyrb.defn(word: nil) do |word|
    throw(:return, exports['Scrabble'].fcall('score', [word]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
