# encoding: utf-8

require 'pyrb'

module Spec_integration_source_high_scores
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  heapq, _ = import('heapq')

  exports['HighScores'] = Pyrb::PythonClass.new('HighScores', [Pyrb::Object]).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, scores: nil) do |self_, scores|
      self_.attrs['scores'] = scores
    end

    klass.attrs['highest'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, Pyrb.max.call([self_.attr('scores')]))
    end

    klass.attrs['latest'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('scores')[-1])
    end

    klass.attrs['top'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, heapq.fcall('nlargest', [3, self_.attr('scores')]))
    end

    klass.attrs['report'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, "#{self_.fcall('latest_score_message')} #{self_.fcall('personal_best_message')}")
    end

    klass.attrs['latest_score_message'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, "Your latest score was #{self_.fcall('latest')}.")
    end

    klass.attrs['personal_best_message'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.fcall('amount_short') == 0)
        throw(:return, "That's your personal best!")
      else
        throw(:return, "That's #{self_.fcall('amount_short')} short of your personal best!")
      end
    end

    klass.attrs['amount_short'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('highest') - self_.fcall('latest'))
    end
  end

  HighScores = exports['HighScores']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
