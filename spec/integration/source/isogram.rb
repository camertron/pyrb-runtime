# encoding: utf-8

require 'pyrb'

module Spec_integration_source_isogram
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['is_isogram'] = Pyrb.defn(word: nil) do |word|
    throw(:return, Pyrb.len.call([Pyrb::List.call([exports['remove_punctuation'].call([word])])]) == Pyrb.len.call([Pyrb::Set.call([exports['remove_punctuation'].call([word])])]))
  end

  exports['remove_punctuation'] = Pyrb.defn(word: nil) do |word|
    throw(:return, Pyrb::Filter.call([Pyrb::Str.attr('isalpha'), word.fcall('lower')]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
