# encoding: utf-8

require 'pyrb'

module Spec_integration_source_cipher
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  random, _ = import('random')
  string, _ = import('string')

  exports['Cipher'] = Pyrb::PythonClass.new('Cipher', []).tap do |klass|
    klass.attrs['random_key_length'] = 100
    klass.attrs['__init__'] = Pyrb.defn(self: nil, key: { default: nil }) do |self_, key|
      self_.attrs['key'] = !!(key.object_id != nil.object_id) ? key : self_.fcall('generate_random_key')
      if !!((self_.fcall('valid_key')).not)
        Pyrb.raize(Pyrb::ValueError.call())
      end
    end

    klass.attrs['encode'] = Pyrb.defn(self: nil, phrase: nil) do |self_, phrase|
      throw(:return, "".fcall('join', [Pyrb.enumerate.call([self_.fcall('clean', [phrase])]).fcall('__iter__').map do |(i, c)|
        Pyrb.chr.call([self_.fcall('wrap', [Pyrb.ord.call([c]) + self_.fcall('offset', [i])])])
      end
      ]))
    end

    klass.attrs['decode'] = Pyrb.defn(self: nil, phrase: nil) do |self_, phrase|
      throw(:return, "".fcall('join', [Pyrb.enumerate.call([self_.fcall('clean', [phrase])]).fcall('__iter__').map do |(i, c)|
        Pyrb.chr.call([self_.fcall('wrap', [Pyrb.ord.call([c]) - self_.fcall('offset', [i])])])
      end
      ]))
    end

    klass.attrs['clean'] = Pyrb.defn(self: nil, phrase: nil) do |self_, phrase|
      throw(:return, Pyrb::Filter.call([Pyrb::Str.attr('isalpha'), phrase.fcall('lower')]))
    end

    klass.attrs['generate_random_key'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, "".fcall('join', [Pyrb.range.call([self_.attr('random_key_length')]).fcall('__iter__').map do |_|
        random.fcall('SystemRandom').fcall('choice', [string.attr('ascii_lowercase')])
      end
      ]))
    end

    klass.attrs['valid_key'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('key').fcall('isalpha').and { self_.attr('key').fcall('islower') })
    end

    klass.attrs['offset'] = Pyrb.defn(self: nil, index: nil) do |self_, index|
      throw(:return, self_.fcall('wrap', [Pyrb.ord.call([self_.attr('key')[(index % Pyrb.len.call([self_.attr('key')]))]]) - 97]))
    end

    klass.attrs['wrap'] = Pyrb.defn(self: nil, val: nil) do |self_, val|
      while !!(val > 122)
        val -= 26
      end

      while !!(val < 97)
        val += 26
      end

      throw(:return, val)
    end
  end

  Cipher = exports['Cipher']

  exports['Caesar'] = Pyrb::PythonClass.new('Caesar', []).tap do |klass|
    klass.attrs['cipher'] = exports['Cipher'].call(["d"])
    klass.attrs['encode'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, phrase: nil) do |cls, phrase|
        throw(:return, cls.attr('cipher').fcall('encode', [phrase]))
      end

    )

    klass.attrs['decode'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, phrase: nil) do |cls, phrase|
        throw(:return, cls.attr('cipher').fcall('decode', [phrase]))
      end

    )
  end

  Caesar = exports['Caesar']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
