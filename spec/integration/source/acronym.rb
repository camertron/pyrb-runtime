# encoding: utf-8

require 'pyrb'

module Spec_integration_source_acronym
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  re, _ = import('re')

  exports['abbreviate'] = Pyrb.defn(words: nil) do |words|
    throw(:return, "".fcall('join', [re.fcall('findall', ["[A-Z]+[a-z]*|[a-z]+", words]).fcall('__iter__').map do |word|
      word[0].fcall('upper')
    end
    ]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
