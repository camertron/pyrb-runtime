# encoding: utf-8

require 'pyrb'

module Spec_integration_source_beer
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Beer'] = Pyrb::PythonClass.new('Beer', []).tap do |klass|
    klass.attrs['LAST_LINE'] = (("Go to the store and buy some more, " + "99 bottles of beer on the wall."))
    klass.attrs['song'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, start: nil, stop: nil) do |cls, start, stop|
        throw(:return, "\n".fcall('join', [Pyrb.reversed.call([Pyrb.range.call([stop, start + 1])]).fcall('__iter__').map do |verse_num|
          cls.fcall('verse', [verse_num])
        end
        ]) + "\n")
      end

    )

    klass.attrs['verse'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        throw(:return, "\n".fcall('join', [[cls.fcall('prefix', [verse_num]), cls.fcall('suffix', [verse_num])]]) + "\n")
      end

    )

    klass.attrs['prefix'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        throw(:return, ((("%(quantity)s %(container)s of beer on the wall, " + "%(quantity)s %(container)s of beer.") % cls.fcall('vals_for', [verse_num]))).fcall('capitalize'))
      end

    )

    klass.attrs['suffix'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, verse_num: nil) do |cls, verse_num|
        if !!(verse_num == 0)
          throw(:return, cls.attr('LAST_LINE'))
        else
          throw(:return, ((("Take %(cardinality)s down and pass it around, " + "%(quantity)s %(container)s of beer on the wall.") % cls.fcall('vals_for', [verse_num - 1]))))
        end
      end

    )

    klass.attrs['vals_for'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, num: nil) do |cls, num|
        throw(:return, Pyrb::Dict.new([{"quantity" => cls.fcall('quantity', [num]), "container" => cls.fcall('container', [num]), "cardinality" => cls.fcall('cardinality', [num])}]))
      end

    )

    klass.attrs['quantity'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(num: nil) do |num|
        throw(:return, !!(num == 0) ? "no more" : Pyrb::Str.call([num]))
      end

    )

    klass.attrs['container'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(num: nil) do |num|
        throw(:return, !!(num == 1) ? "bottle" : "bottles")
      end

    )

    klass.attrs['cardinality'] = Pyrb.staticmethod.call(
      klass, Pyrb.defn(num: nil) do |num|
        throw(:return, !!(num == 0) ? "it" : "one")
      end

    )
  end

  Beer = exports['Beer']

  exports['verse'] = Pyrb.defn(verse_num: nil) do |verse_num|
    throw(:return, exports['Beer'].fcall('verse', [verse_num]))
  end

  exports['song'] = Pyrb.defn(start: nil, stop: { default: 0 }) do |start, stop|
    throw(:return, exports['Beer'].fcall('song', [start, stop]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
