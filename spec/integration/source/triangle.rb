# encoding: utf-8

require 'pyrb'

module Spec_integration_source_triangle
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Triangle'] = Pyrb::PythonClass.new('Triangle', []).tap do |klass|
    klass.attrs['EQUILATERAL'] = "equilateral"
    klass.attrs['ISOSCELES'] = "isosceles"
    klass.attrs['SCALENE'] = "scalene"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, a: nil, b: nil, c: nil) do |self_, a, b, c|
      self_.attrs['a'] = a
      self_.attrs['b'] = b
      self_.attrs['c'] = c
      if !!(self_.fcall('error'))
        Pyrb.raize(exports['TriangleError'])
      end
    end

    klass.attrs['kind'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.fcall('equilateral'))
        throw(:return, self_.attr('EQUILATERAL'))
      end

      if !!(self_.fcall('isosceles'))
        throw(:return, self_.attr('ISOSCELES'))
      end

      throw(:return, self_.attr('SCALENE'))
    end

    klass.attrs['equilateral'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('a') == self_.attr('b') && self_.attr('b') == self_.attr('c'))
    end

    klass.attrs['isosceles'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (self_.attr('a') == self_.attr('b')).or { self_.attr('b') == self_.attr('c') }.or { self_.attr('a') == self_.attr('c') })
    end

    klass.attrs['error'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.fcall('negative_sides').or { self_.fcall('triangle_inequality') })
    end

    klass.attrs['negative_sides'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, (self_.attr('a') <= 0).or { self_.attr('b') <= 0 }.or { self_.attr('c') <= 0 })
    end

    klass.attrs['triangle_inequality'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, ((self_.attr('a') + self_.attr('b') <= self_.attr('c')).or { self_.attr('b') + self_.attr('c') <= self_.attr('a') }.or { self_.attr('a') + self_.attr('c') <= self_.attr('b') }))
    end
  end

  Triangle = exports['Triangle']

  exports['TriangleError'] = Pyrb::PythonClass.new('TriangleError', [Pyrb::Exception])
  TriangleError = exports['TriangleError']

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
