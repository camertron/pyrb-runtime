# encoding: utf-8

require 'pyrb'

module Spec_integration_source_reverse_string
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['reverse'] = Pyrb.defn(input: { default: "" }) do |input|
    throw(:return, "".fcall('join', [Pyrb.reversed.call([input])]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
