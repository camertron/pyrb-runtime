# encoding: utf-8

require 'pyrb'

module Spec_integration_source_minesweeper
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Minesweeper'] = Pyrb::PythonClass.new('Minesweeper', []).tap do |klass|
    klass.attrs['MINE'] = "*"
    klass.attrs['SPACE'] = " "
    klass.attrs['CORNER'] = "+"
    klass.attrs['VERTICAL_EDGE'] = "|"
    klass.attrs['HORIZONTAL_EDGE'] = "-"
    klass.attrs['board'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        if !!((cls.fcall('valid', [inp])).not)
          Pyrb.raize(Pyrb::ValueError)
        end

        throw(:return, cls.fcall('solve', [inp]))
      end

    )

    klass.attrs['solve'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        inp = Pyrb::List.call([Pyrb::Map.call([Pyrb.defn(row: nil) { |row| Pyrb::List.call([row]) }, inp])])
        throw(:return, Pyrb::List.call([Pyrb::Map.call([Pyrb.defn(row: nil) { |row| "".fcall('join', [row]) }, cls.fcall('generate_board', [inp])])]))
      end

    )

    klass.attrs['generate_board'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb.enumerate.call([inp]).fcall('__iter__').map do |(y, row)|
          Pyrb.enumerate.call([row]).fcall('__iter__').map do |(x, square)|
            cls.fcall('convert_square', [inp, y, x])
          end
        end
        )
      end

    )

    klass.attrs['convert_square'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil, y: nil, x: nil) do |cls, inp, y, x|
        if !!((cls.fcall('is_space', [inp[y][x]])).not)
          throw(:return, inp[y][x])
        end

        throw(:return, Pyrb::Str.call([cls.fcall('output_of_neighbor_mines', [inp, y, x])]))
      end

    )

    klass.attrs['output_of_neighbor_mines'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil, y: nil, x: nil) do |cls, inp, y, x|
        num_mines = cls.fcall('num_of_neighbor_mines', [inp, y, x])
        throw(:return, !!(num_mines == 0) ? " " : num_mines)
      end

    )

    klass.attrs['num_of_neighbor_mines'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil, y: nil, x: nil) do |cls, inp, y, x|
        throw(:return, Pyrb.len.call([Pyrb::List.call([Pyrb::Filter.call([Pyrb.defn(neighbor: nil) { |neighbor| cls.fcall('is_neighbor_a_mine', [inp, neighbor]) }, cls.fcall('all_neighbor_coords', [inp, y, x])])])]))
      end

    )

    klass.attrs['is_neighbor_a_mine'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil, neighbor: nil) do |cls, inp, neighbor|
        y, x = Pyrb.deconstruct(2, neighbor[0], neighbor[1])
        throw(:return, ((0 < y && y < Pyrb.len.call([inp])).and { 0 < x && x < Pyrb.len.call([inp[0]]) }.and { cls.fcall('is_mine', [inp[y][x]]) }))
      end

    )

    klass.attrs['all_neighbor_coords'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil, y: nil, x: nil) do |cls, inp, y, x|
        throw(:return, Pyrb.range.call([-1, 2]).fcall('__iter__').flat_map do |dy|
          Pyrb.range.call([-1, 2]).fcall('__iter__').each_with_object([]) do |dx, _ret_|
            if !!((dy != 0).or { dx != 0 })
              _ret_ << [y + dy, x + dx]
            end
          end
        end
        )
      end

    )

    klass.attrs['valid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, (cls.fcall('valid_len', [inp]).and { cls.fcall('valid_border', [inp]) }.and { cls.fcall('valid_squares', [inp]) }))
      end

    )

    klass.attrs['valid_len'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb.all.call([inp.fcall('__iter__').map do |row|
          Pyrb.len.call([row]) == Pyrb.len.call([inp[0]])
        end
        ]))
      end

    )

    klass.attrs['valid_border'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, (cls.fcall('valid_middle_borders', [inp]).and { cls.fcall('valid_first_and_last_borders', [inp]) }))
      end

    )

    klass.attrs['valid_middle_borders'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb.all.call([inp[1...-1].fcall('__iter__').map do |row|
          cls.fcall('valid_middle_border', [row])
        end
        ]))
      end

    )

    klass.attrs['valid_middle_border'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, row: nil) do |cls, row|
        throw(:return, (cls.fcall('is_vertical_edge', [row[0]]).and { cls.fcall('is_vertical_edge', [row[-1]]) }))
      end

    )

    klass.attrs['valid_first_and_last_borders'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, (cls.fcall('valid_first_or_last_border', [inp[0]]).and { cls.fcall('valid_first_or_last_border', [inp[-1]]) }))
      end

    )

    klass.attrs['valid_first_or_last_border'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, row: nil) do |cls, row|
        throw(:return, (cls.fcall('is_corner', [row[0]]).and { cls.fcall('is_corner', [row[-1]]) }.and { Pyrb.all.call([row[1...-1].fcall('__iter__').map do |square|
          cls.fcall('is_horizontal_edge', [square])
        end
        ]) }))
      end

    )

    klass.attrs['valid_squares'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb.all.call([inp.fcall('__iter__').flat_map do |row|
          row.fcall('__iter__').map do |square|
            cls.fcall('valid_square', [square])
          end
        end
        ]))
      end

    )

    klass.attrs['valid_square'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, square: nil) do |cls, square|
        throw(:return, (cls.fcall('is_mine', [square]).or { cls.fcall('is_space', [square]) }.or { cls.fcall('is_corner', [square]) }.or { cls.fcall('is_vertical_edge', [square]) }.or { cls.fcall('is_horizontal_edge', [square]) }))
      end

    )

    klass.attrs['valid_non_border'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, square: nil) do |cls, square|
        throw(:return, cls.fcall('is_mine', [square]).or { cls.fcall('is_space', [square]) })
      end

    )

    klass.attrs['is_mine'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, square: nil) do |cls, square|
        throw(:return, square == cls.attr('MINE'))
      end

    )

    klass.attrs['is_space'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, square: nil) do |cls, square|
        throw(:return, square == cls.attr('SPACE'))
      end

    )

    klass.attrs['is_corner'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, square: nil) do |cls, square|
        throw(:return, square == cls.attr('CORNER'))
      end

    )

    klass.attrs['is_vertical_edge'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, square: nil) do |cls, square|
        throw(:return, square == cls.attr('VERTICAL_EDGE'))
      end

    )

    klass.attrs['is_horizontal_edge'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, square: nil) do |cls, square|
        throw(:return, square == cls.attr('HORIZONTAL_EDGE'))
      end

    )
  end

  Minesweeper = exports['Minesweeper']

  exports['board'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['Minesweeper'].fcall('board', [inp]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
