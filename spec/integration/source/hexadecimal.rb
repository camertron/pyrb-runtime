# encoding: utf-8

require 'pyrb'

module Spec_integration_source_hexadecimal
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['Hexa'] = Pyrb::PythonClass.new('Hexa', []).tap do |klass|
    klass.attrs['CHAR_VALUES'] = Pyrb::Dict.new([{"a" => 10, "b" => 11, "c" => 12, "d" => 13, "e" => 14, "f" => 15}])
    klass.attrs['VALID_CHARS'] = Pyrb::Set.call([Pyrb::Map.call([Pyrb::Str, Pyrb.range.call([0, 10])]) + klass.attrs['CHAR_VALUES'].fcall('keys')])
    klass.attrs['BASE'] = 16
    klass.attrs['convert'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        if !!((cls.fcall('valid', [inp])).not)
          Pyrb.raize(Pyrb::ValueError)
        end

        throw(:return, Pyrb.sum.call([Pyrb.enumerate.call([Pyrb.reversed.call([inp])]).fcall('__iter__').map do |(index, char)|
          (cls.fcall('convert_char', [char]) * cls.attr('BASE') ** index)
        end
        ]))
      end

    )

    klass.attrs['valid'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, inp: nil) do |cls, inp|
        throw(:return, Pyrb::Set.call([inp]) <= cls.attr('VALID_CHARS'))
      end

    )

    klass.attrs['convert_char'] = Pyrb.classmethod.call(
      klass, Pyrb.defn(cls: nil, char: nil) do |cls, char|
        throw(:return, !!(char.fcall('isdigit')) ? Pyrb::Int.call([char]) : cls.attr('CHAR_VALUES')[char])
      end

    )
  end

  Hexa = exports['Hexa']

  exports['hexa'] = Pyrb.defn(inp: nil) do |inp|
    throw(:return, exports['Hexa'].fcall('convert', [inp.fcall('lower')]))
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
