# encoding: utf-8

require 'pyrb'

module Spec_integration_source_binary_search
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['binary_search'] = Pyrb.defn(array: nil, value: nil) do |array, value|
    throw(:return, exports['binary_search_with_bounds'].call([array, value, 0, Pyrb.len.call([array]) - 1]))
  end

  exports['binary_search_with_bounds'] = Pyrb.defn(array: nil, value: nil, left: nil, right: nil) do |array, value, left, right|
    if !!((left > right))
      Pyrb.raize(Pyrb::ValueError.call(["Value not found"]))
    end

    middle = left + Pyrb.int_divide((right - left), 2)
    middle_value = array[middle]
    if !!((value == middle_value))
      throw(:return, middle)
    elsif !!((value < middle_value))
      throw(:return, exports['binary_search_with_bounds'].call([array, value, left, middle - 1]))
    elsif !!((value > middle_value))
      throw(:return, exports['binary_search_with_bounds'].call([array, value, middle + 1, right]))
    end
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
