require 'spec_helper'

describe 'string' do
  using Pyrb::Runtime

  describe '#split' do
    it 'splits correctly with a single-char delimiter' do
      expect('1-2-3'.fcall('split', ['-'])).to eq(['1', '2', '3'])
    end

    it 'respects the maxsplit limit' do
      expect('1-2-3'.fcall('split', ['-'], maxsplit: 1)).to eq(['1', '2-3'])
    end

    it 'splits correctly with a multi-char delimiter' do
      expect('1<>2<>3'.fcall('split', ['<>'])).to eq(['1', '2', '3'])
    end

    it 'includes blank entries when no content exists between separators' do
      expect('==='.fcall('split', ['='])).to eq(['', '', '', ''])
    end

    it 'includes blank entry for trailing separator' do
      expect('g/'.fcall('split', ['/'])).to eq(['g', ''])
    end

    it 'returns a single-element array when splitting an empty string' do
      expect(''.fcall('split', [','])).to eq([''])
    end

    it 'returns an empty array when splitting an empty string with no separator' do
      expect(''.fcall('split')).to eq([])
    end

    context 'special whitespace rules' do
      it 'removes runs of whitespace' do
        expect('  1    2       3   '.fcall('split')).to eq(['1', '2', '3'])
      end

      it 'includes remaining whitespace after reaching max' do
        expect('   1   2   3   '.fcall('split', maxsplit: 1)).to eq(['1', '2   3   '])
      end
    end
  end
end
