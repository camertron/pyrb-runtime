# encoding: utf-8

require 'pyrb'

module Spec_test_heapq
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  random, _ = import('random')
  unittest, _ = import('unittest')
  TestCase, _ = import('TestCase', from: 'unittest')
  itemgetter, _ = import('itemgetter', from: 'operator')
  heapq, _ = import('heapq')
  chain, _ = import('chain', from: 'itertools')

  "Unittests for heapq."
  exports['TestHeap'] = Pyrb::PythonClass.new('TestHeap', []).tap do |klass|
    klass.attrs['test_push_pop'] = Pyrb.defn(self: nil) do |self_|
      item = nil

      heap = []
      data = []
      self_.fcall('check_invariant', [heap])
      (Pyrb.range.call([5])).each do |i|
        item = random.fcall('random')
        data.fcall('append', [item])
        self_.attr('module').fcall('heappush', [heap, item])
        self_.fcall('check_invariant', [heap])
      end
      results = []
      while !!(heap)
        item = self_.attr('module').fcall('heappop', [heap])
        self_.fcall('check_invariant', [heap])
        results.fcall('append', [item])
      end

      data_sorted = data[0..-1]
      data_sorted.fcall('sort')
      self_.fcall('assertEqual', [data_sorted, results])
      self_.fcall('check_invariant', [results])
      self_.fcall('assertRaises', [Pyrb::TypeError, self_.attr('module').attr('heappush'), []])
      self_.fcall('assertRaises', [Pyrb::AttributeError, self_.attr('module').attr('heappush'), nil, nil])
      self_.fcall('assertRaises', [Pyrb::AttributeError, self_.attr('module').attr('heappop'), nil])
    end

    klass.attrs['check_invariant'] = Pyrb.defn(self: nil, heap: nil) do |self_, heap|
      parentpos = nil

      (Pyrb.enumerate.call([heap])).each do |pos, item|
        if !!(pos)
          parentpos = (pos - 1) >> 1
          self_.fcall('assertTrue', [heap[parentpos] <= item])
        end
      end
    end

    klass.attrs['test_heapify'] = Pyrb.defn(self: nil) do |self_|
      heap = nil

      (Pyrb::List.call([Pyrb.range.call([30])]) + [20000]).each do |size|
        heap = Pyrb.range.call([size]).fcall('__iter__').map do |dummy|
          random.fcall('random')
        end

        self_.attr('module').fcall('heapify', [heap])
        self_.fcall('check_invariant', [heap])
      end
      self_.fcall('assertRaises', [Pyrb::TypeError, self_.attr('module').attr('heapify'), nil])
    end

    klass.attrs['test_naive_nbest'] = Pyrb.defn(self: nil) do |self_|
      data = Pyrb.range.call([1000]).fcall('__iter__').map do |i|
        random.fcall('randrange', [2000])
      end

      heap = []
      (data).each do |item|
        self_.attr('module').fcall('heappush', [heap, item])
        if !!(Pyrb.len.call([heap]) > 10)
          self_.attr('module').fcall('heappop', [heap])
        end
      end
      heap.fcall('sort')
      self_.fcall('assertEqual', [heap, Pyrb.sorted.call([data])[-10..-1]])
    end

    klass.attrs['heapiter'] = Pyrb.defn(self: nil, heap: nil) do |self_, heap|
      Pyrb::Generator.new([
        Enumerator.new do |__pyrb_gen__|
          begin
            while !!(1)
              __pyrb_gen__ << self_.attr('module').fcall('heappop', [heap])
            end

          rescue *Pyrb.rezcue(Pyrb::IndexError)
          end

        end
      ])
    end

    klass.attrs['test_nbest'] = Pyrb.defn(self: nil) do |self_|
      data = Pyrb.range.call([1000]).fcall('__iter__').map do |i|
        random.fcall('randrange', [2000])
      end

      heap = data[0...10]
      self_.attr('module').fcall('heapify', [heap])
      (data[10..-1]).each do |item|
        if !!(item > heap[0])
          self_.attr('module').fcall('heapreplace', [heap, item])
        end
      end
      self_.fcall('assertEqual', [Pyrb::List.call([self_.fcall('heapiter', [heap])]), Pyrb.sorted.call([data])[-10..-1]])
      self_.fcall('assertRaises', [Pyrb::TypeError, self_.attr('module').attr('heapreplace'), nil])
      self_.fcall('assertRaises', [Pyrb::TypeError, self_.attr('module').attr('heapreplace'), nil, nil])
      self_.fcall('assertRaises', [Pyrb::IndexError, self_.attr('module').attr('heapreplace'), [], nil])
    end

    klass.attrs['test_nbest_with_pushpop'] = Pyrb.defn(self: nil) do |self_|
      data = Pyrb.range.call([1000]).fcall('__iter__').map do |i|
        random.fcall('randrange', [2000])
      end

      heap = data[0...10]
      self_.attr('module').fcall('heapify', [heap])
      (data[10..-1]).each do |item|
        self_.attr('module').fcall('heappushpop', [heap, item])
      end
      self_.fcall('assertEqual', [Pyrb::List.call([self_.fcall('heapiter', [heap])]), Pyrb.sorted.call([data])[-10..-1]])
      self_.fcall('assertEqual', [self_.attr('module').fcall('heappushpop', [[], "x"]), "x"])
    end

    klass.attrs['test_heappushpop'] = Pyrb.defn(self: nil) do |self_|
      h = []
      x = self_.attr('module').fcall('heappushpop', [h, 10])
      self_.fcall('assertEqual', [[h, x], [[], 10]])
      h = [10]
      x = self_.attr('module').fcall('heappushpop', [h, 10.0])
      self_.fcall('assertEqual', [[h, x], [[10], 10.0]])
      self_.fcall('assertEqual', [Pyrb.type.call([h[0]]), Pyrb::Int])
      self_.fcall('assertEqual', [Pyrb.type.call([x]), Pyrb::Float])
      h = [10]
      x = self_.attr('module').fcall('heappushpop', [h, 9])
      self_.fcall('assertEqual', [[h, x], [[10], 9]])
      h = [10]
      x = self_.attr('module').fcall('heappushpop', [h, 11])
      self_.fcall('assertEqual', [[h, x], [[11], 10]])
    end

    klass.attrs['test_heapsort'] = Pyrb.defn(self: nil) do |self_|
      size, data, heap, heap_sorted = nil

      (Pyrb.range.call([100])).each do |trial|
        size = random.fcall('randrange', [50])
        data = Pyrb.range.call([size]).fcall('__iter__').map do |i|
          random.fcall('randrange', [25])
        end

        if !!(trial & 1)
          heap = data[0..-1]
          self_.attr('module').fcall('heapify', [heap])
        else
          heap = []
          (data).each do |item|
            self_.attr('module').fcall('heappush', [heap, item])
          end
        end

        heap_sorted = Pyrb.range.call([size]).fcall('__iter__').map do |i|
          self_.attr('module').fcall('heappop', [heap])
        end

        self_.fcall('assertEqual', [heap_sorted, Pyrb.sorted.call([data])])
      end
    end

    klass.attrs['test_merge'] = Pyrb.defn(self: nil) do |self_|
      tup, row, seqs = nil

      inputs = []
      (Pyrb.range.call([random.fcall('randrange', [25])])).each do |i|
        row = []
        (Pyrb.range.call([random.fcall('randrange', [100])])).each do |j|
          tup = random.fcall('choice', ["ABC"]), random.fcall('randrange', [-500, 500])
          row.fcall('append', [tup])
        end
        inputs.fcall('append', [row])
      end
      ([nil, itemgetter.call([0]), itemgetter.call([1]), itemgetter.call([1, 0])]).each do |key|
        ([false, true]).each do |reverse|
          seqs = []
          (inputs).each do |seq|
            seqs.fcall('append', [Pyrb.sorted.call([seq, ], { key: key, reverse: reverse })])
          end
          self_.fcall('assertEqual', [Pyrb.sorted.call([chain.call([*Pyrb.splat(inputs)]), ], { key: key, reverse: reverse }), Pyrb::List.call([self_.attr('module').fcall('merge', [*Pyrb.splat(seqs), ], { key: key, reverse: reverse })])])
          self_.fcall('assertEqual', [Pyrb::List.call([self_.attr('module').fcall('merge')]), []])
        end
      end
    end

    klass.attrs['test_merge_does_not_suppress_index_error'] = Pyrb.defn(self: nil) do |self_|
      iterable = nil

      Pyrb::Generator.new([
        Enumerator.new do |__pyrb_gen__|
          Pyrb.with.call([self_.fcall('assertRaises', [Pyrb::IndexError])]) do
            Pyrb::List.call([self_.attr('module').fcall('merge', [iterable.call(), iterable.call()])])
          end
        end
      ])
    end

    klass.attrs['test_merge_stability'] = Pyrb.defn(self: nil) do |self_|
      stream, x, obj = nil

      Int ||= Pyrb::PythonClass.new('Int', [Pyrb::Int])
      inputs = [[], [], [], []]
      (Pyrb.range.call([20000])).each do |i|
        stream = random.fcall('randrange', [4])
        x = random.fcall('randrange', [500])
        obj = Int.call([x])
        obj.attrs['pair'] = [x, stream]
        inputs[stream].fcall('append', [obj])
      end
      (inputs).each do |stream2|
        stream2.fcall('sort')
      end
      result = self_.attr('module').fcall('merge', [*Pyrb.splat(inputs)]).fcall('__iter__').map do |i|
        i.attr('pair')
      end

      self_.fcall('assertEqual', [result, Pyrb.sorted.call([result])])
    end

    klass.attrs['test_nsmallest'] = Pyrb.defn(self: nil) do |self_|
      key = nil

      data = Pyrb.range.call([1000]).fcall('__iter__').map do |i|
        [random.fcall('randrange', [2000]), i]
      end

      ([nil, Pyrb.defn(x: nil) { |x| (x[0] * 547 % 2000) }]).each do |f|
        ([0, 1, 2, 10, 100, 400, 999, 1000, 1100]).each do |n|
          self_.fcall('assertEqual', [Pyrb::List.call([self_.attr('module').fcall('nsmallest', [n, data])]), Pyrb.sorted.call([data])[0...n]])
          self_.fcall('assertEqual', [Pyrb::List.call([self_.attr('module').fcall('nsmallest', [n, data, ], { key: f })]), Pyrb.sorted.call([data, ], { key: f })[0...n]])
        end
      end
    end

    klass.attrs['test_nlargest'] = Pyrb.defn(self: nil) do |self_|
      reverse, key = nil

      data = Pyrb.range.call([1000]).fcall('__iter__').map do |i|
        [random.fcall('randrange', [2000]), i]
      end

      ([nil, Pyrb.defn(x: nil) { |x| (x[0] * 547 % 2000) }]).each do |f|
        ([0, 1, 2, 10, 100, 400, 999, 1000, 1100]).each do |n|
          self_.fcall('assertEqual', [Pyrb::List.call([self_.attr('module').fcall('nlargest', [n, data])]), Pyrb.sorted.call([data, ], { reverse: true })[0...n]])
          self_.fcall('assertEqual', [Pyrb::List.call([self_.attr('module').fcall('nlargest', [n, data, ], { key: f })]), Pyrb.sorted.call([data, ], { key: f, reverse: true })[0...n]])
        end
      end
    end

    klass.attrs['test_comparison_operator'] = Pyrb.defn(self: nil) do |self_|
      hsort = Pyrb.defn(data: nil, comp: nil) do |data, comp|
        data = data.fcall('__iter__').map do |x|
          comp.call([x])
        end

        self_.attr('module').fcall('heapify', [data])
        throw(:return, Pyrb.range.call([Pyrb.len.call([data])]).fcall('__iter__').map do |i|
          self_.attr('module').fcall('heappop', [data]).attr('x')
        end
        )
      end

      LT ||= Pyrb::PythonClass.new('LT', []).tap do |klass|
        klass.attrs['__init__'] = Pyrb.defn(self: nil, x: nil) do |self2, x|
          self2.attrs['x'] = x
        end

        klass.attrs['__lt__'] = Pyrb.defn(self: nil, other: nil) do |self2, other|
          throw(:return, self2.attr('x') > other.attr('x'))
        end
      end

      LE ||= Pyrb::PythonClass.new('LE', []).tap do |klass|
        klass.attrs['__init__'] = Pyrb.defn(self: nil, x: nil) do |self2, x|
          self2.attrs['x'] = x
        end

        klass.attrs['__le__'] = Pyrb.defn(self: nil, other: nil) do |self2, other|
          throw(:return, self2.attr('x') >= other.attr('x'))
        end
      end

      data = Pyrb.range.call([100]).fcall('__iter__').map do |i|
        random.fcall('random')
      end

      target = Pyrb.sorted.call([data, ], { reverse: true })
      self_.fcall('assertEqual', [hsort.call([data, LT]), target])
      self_.fcall('assertRaises', [Pyrb::TypeError, data, LE])
    end
  end

  TestHeap = exports['TestHeap']

  exports['TestHeapPython'] = Pyrb::PythonClass.new('TestHeapPython', [exports['TestHeap'], TestCase]).tap do |klass|
    klass.attrs['module'] = heapq
  end

  TestHeapPython = exports['TestHeapPython']

  exports['LenOnly'] = Pyrb::PythonClass.new('LenOnly', []).tap do |klass|
    "Dummy sequence class defining __len__ but not __getitem__."
    klass.attrs['__len__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, 10)
    end
  end

  LenOnly = exports['LenOnly']

  exports['GetOnly'] = Pyrb::PythonClass.new('GetOnly', []).tap do |klass|
    "Dummy sequence class defining __getitem__ but not __len__."
    klass.attrs['__getitem__'] = Pyrb.defn(self: nil, ndx: nil) do |self_, ndx|
      throw(:return, 10)
    end
  end

  GetOnly = exports['GetOnly']

  exports['CmpErr'] = Pyrb::PythonClass.new('CmpErr', []).tap do |klass|
    "Dummy element that always raises an error during comparison"
    klass.attrs['__eq__'] = Pyrb.defn(self: nil, other: nil) do |self_, other|
      Pyrb.raize(Pyrb::ZeroDivisionError)
    end

    klass.attrs['__ne__'] = klass.attrs['__lt__'] = klass.attrs['__le__'] = klass.attrs['__gt__'] = klass.attrs['__ge__'] = klass.attrs['__eq__']
  end

  CmpErr = exports['CmpErr']

  exports['R'] = Pyrb.defn(seqn: nil) do |seqn|
    Pyrb::Generator.new([
      Enumerator.new do |__pyrb_gen__|
        "Regular generator"
        (seqn).each do |i|
          __pyrb_gen__ << i
        end
      end
    ])
  end

  exports['G'] = Pyrb::PythonClass.new('G', []).tap do |klass|
    "Sequence using __getitem__"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seqn: nil) do |self_, seqn|
      self_.attrs['seqn'] = seqn
    end

    klass.attrs['__getitem__'] = Pyrb.defn(self: nil, i: nil) do |self_, i|
      throw(:return, self_.attr('seqn')[i])
    end
  end

  G = exports['G']

  exports['I'] = Pyrb::PythonClass.new('I', []).tap do |klass|
    "Sequence using iterator protocol"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seqn: nil) do |self_, seqn|
      self_.attrs['seqn'] = seqn
      self_.attrs['i'] = 0
    end

    klass.attrs['__iter__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_)
    end

    klass.attrs['__next__'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.attr('i') >= Pyrb.len.call([self_.attr('seqn')]))
        Pyrb.raize(::StopIteration)
      end

      v = self_.attr('seqn')[self_.attr('i')]
      self_.attrs['i'] += 1
      throw(:return, v)
    end
  end

  I = exports['I']

  exports['Ig'] = Pyrb::PythonClass.new('Ig', []).tap do |klass|
    "Sequence using iterator protocol defined with a generator"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seqn: nil) do |self_, seqn|
      self_.attrs['seqn'] = seqn
      self_.attrs['i'] = 0
    end

    klass.attrs['__iter__'] = Pyrb.defn(self: nil) do |self_|
      Pyrb::Generator.new([
        Enumerator.new do |__pyrb_gen__|
          (self_.attr('seqn')).each do |val|
            __pyrb_gen__ << val
          end
        end
      ])
    end
  end

  Ig = exports['Ig']

  exports['X'] = Pyrb::PythonClass.new('X', []).tap do |klass|
    "Missing __getitem__ and __iter__"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seqn: nil) do |self_, seqn|
      self_.attrs['seqn'] = seqn
      self_.attrs['i'] = 0
    end

    klass.attrs['__next__'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.attr('i') >= Pyrb.len.call([self_.attr('seqn')]))
        Pyrb.raize(::StopIteration)
      end

      v = self_.attr('seqn')[self_.attr('i')]
      self_.attrs['i'] += 1
      throw(:return, v)
    end
  end

  X = exports['X']

  exports['N'] = Pyrb::PythonClass.new('N', []).tap do |klass|
    "Iterator missing __next__()"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seqn: nil) do |self_, seqn|
      self_.attrs['seqn'] = seqn
      self_.attrs['i'] = 0
    end

    klass.attrs['__iter__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_)
    end
  end

  N = exports['N']

  exports['E'] = Pyrb::PythonClass.new('E', []).tap do |klass|
    "Test propagation of exceptions"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seqn: nil) do |self_, seqn|
      self_.attrs['seqn'] = seqn
      self_.attrs['i'] = 0
    end

    klass.attrs['__iter__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_)
    end

    klass.attrs['__next__'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.int_divide(3, 0)
    end
  end

  E = exports['E']

  exports['S'] = Pyrb::PythonClass.new('S', []).tap do |klass|
    "Test immediate stop"
    klass.attrs['__init__'] = Pyrb.defn(self: nil, seqn: nil) do |self_, seqn|

    end

    klass.attrs['__iter__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_)
    end

    klass.attrs['__next__'] = Pyrb.defn(self: nil) do |self_|
      Pyrb.raize(::StopIteration)
    end
  end

  S = exports['S']

  exports['L'] = Pyrb.defn(seqn: nil) do |seqn|
    "Test multiple tiers of iterators"
    throw(:return, chain.call([Pyrb::Map.call([Pyrb.defn(x: nil) { |x| x }, exports['R'].call([exports['Ig'].call([exports['G'].call([seqn])])])])]))
  end

  exports['SideEffectLT'] = Pyrb::PythonClass.new('SideEffectLT', []).tap do |klass|
    klass.attrs['__init__'] = Pyrb.defn(self: nil, value: nil, heap: nil) do |self_, value, heap|
      self_.attrs['value'] = value
      self_.attrs['heap'] = heap
    end

    klass.attrs['__lt__'] = Pyrb.defn(self: nil, other: nil) do |self_, other|
      self_.attrs['heap'][0..-1] = []
      throw(:return, self_.attr('value') < other.attr('value'))
    end
  end

  SideEffectLT = exports['SideEffectLT']

  exports['TestErrorHandling'] = Pyrb::PythonClass.new('TestErrorHandling', []).tap do |klass|
    klass.attrs['test_non_sequence'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('heapify'), self_.attr('module').attr('heappop')]).each do |f|
        self_.fcall('assertRaises', [[Pyrb::TypeError, Pyrb::AttributeError], f, 10])
      end
      ([self_.attr('module').attr('heappush'), self_.attr('module').attr('heapreplace'), self_.attr('module').attr('nlargest'), self_.attr('module').attr('nsmallest')]).each do |f|
        self_.fcall('assertRaises', [[Pyrb::TypeError, Pyrb::AttributeError], f, 10, 10])
      end
    end

    klass.attrs['test_len_only'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('heapify'), self_.attr('module').attr('heappop')]).each do |f|
        self_.fcall('assertRaises', [[Pyrb::TypeError, Pyrb::AttributeError], f, exports['LenOnly'].call()])
      end
      ([self_.attr('module').attr('heappush'), self_.attr('module').attr('heapreplace')]).each do |f|
        self_.fcall('assertRaises', [[Pyrb::TypeError, Pyrb::AttributeError], f, exports['LenOnly'].call(), 10])
      end
      ([self_.attr('module').attr('nlargest'), self_.attr('module').attr('nsmallest')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, 2, exports['LenOnly'].call()])
      end
    end

    klass.attrs['test_get_only'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('heapify'), self_.attr('module').attr('heappop')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, exports['GetOnly'].call()])
      end
      ([self_.attr('module').attr('heappush'), self_.attr('module').attr('heapreplace')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, exports['GetOnly'].call(), 10])
      end
      ([self_.attr('module').attr('nlargest'), self_.attr('module').attr('nsmallest')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::TypeError, f, 2, exports['GetOnly'].call()])
      end
    end

    klass.attrs['test_get_only'] = Pyrb.defn(self: nil) do |self_|
      seq = [exports['CmpErr'].call(), exports['CmpErr'].call(), exports['CmpErr'].call()]
      ([self_.attr('module').attr('heapify'), self_.attr('module').attr('heappop')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::ZeroDivisionError, f, seq])
      end
      ([self_.attr('module').attr('heappush'), self_.attr('module').attr('heapreplace')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::ZeroDivisionError, f, seq, 10])
      end
      ([self_.attr('module').attr('nlargest'), self_.attr('module').attr('nsmallest')]).each do |f|
        self_.fcall('assertRaises', [Pyrb::ZeroDivisionError, f, 2, seq])
      end
    end

    klass.attrs['test_arg_parsing'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('heapify'), self_.attr('module').attr('heappop'), self_.attr('module').attr('heappush'), self_.attr('module').attr('heapreplace'), self_.attr('module').attr('nlargest'), self_.attr('module').attr('nsmallest')]).each do |f|
        self_.fcall('assertRaises', [[Pyrb::TypeError, Pyrb::AttributeError], f, 10])
      end
    end

    klass.attrs['test_iterable_args'] = Pyrb.defn(self: nil) do |self_|
      ([self_.attr('module').attr('nlargest'), self_.attr('module').attr('nsmallest')]).each do |f|
        (["123", "", Pyrb.range.call([1000]), [1, 1.2], Pyrb.range.call([2000, 2200, 5])]).each do |s|
          ([exports['G'], exports['I'], exports['Ig'], exports['L'], exports['R']]).each do |g|
            self_.fcall('assertEqual', [Pyrb::List.call([f.call([2, g.call([s])])]), Pyrb::List.call([f.call([2, s])])])
          end
          self_.fcall('assertEqual', [Pyrb::List.call([f.call([2, exports['S'].call([s])])]), []])
          self_.fcall('assertRaises', [Pyrb::TypeError, f, 2, exports['X'].call([s])])
          self_.fcall('assertRaises', [Pyrb::TypeError, f, 2, exports['N'].call([s])])
          self_.fcall('assertRaises', [Pyrb::ZeroDivisionError, f, 2, exports['E'].call([s])])
        end
      end
    end

    klass.attrs['test_heappush_mutating_heap'] = Pyrb.defn(self: nil) do |self_|
      heap = []
      heap.fcall('extend', [Pyrb.range.call([200]).fcall('__iter__').map do |i|
        exports['SideEffectLT'].call([i, heap])
      end
      ])
      Pyrb.with.call([self_.fcall('assertRaises', [[Pyrb::IndexError, RuntimeError]])]) do
        self_.attr('module').fcall('heappush', [heap, exports['SideEffectLT'].call([5, heap])])
      end
    end

    klass.attrs['test_heappop_mutating_heap'] = Pyrb.defn(self: nil) do |self_|
      heap = []
      heap.fcall('extend', [Pyrb.range.call([200]).fcall('__iter__').map do |i|
        exports['SideEffectLT'].call([i, heap])
      end
      ])
      Pyrb.with.call([self_.fcall('assertRaises', [[Pyrb::IndexError, RuntimeError]])]) do
        self_.attr('module').fcall('heappop', [heap])
      end
    end
  end

  TestErrorHandling = exports['TestErrorHandling']

  exports['TestErrorHandlingPython'] = Pyrb::PythonClass.new('TestErrorHandlingPython', [exports['TestErrorHandling'], TestCase]).tap do |klass|
    klass.attrs['module'] = heapq
  end

  TestErrorHandlingPython = exports['TestErrorHandlingPython']

  if !!(__name__ == "__main__")
    unittest.fcall('main')
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
