require 'pyrb'
require 'benchmark/ips'
require 'ruby-prof'

from = ''.b.tap { |s| (0..255).each { |b| s << b } }.freeze

to = from.dup
to['+'.ord] = '_'
to['/'.ord] = '-'

h = Hash[from.chars.zip(to.chars)]

str = 'abc+def/ghiabc+def/ghiabc+def/ghiabc+def/ghiabc+def/ghiabc+def/ghiabc+def/ghi'
# puts str.gsub(/./, h)

Benchmark.ips do |x|
  x.report('gsub') do
    str.gsub(/./) { |c| to[c.ord] }
  end

  x.report('gsub with hash') do
    str.gsub(/./, h)
  end

  x.report('tr') do
    str.tr(from, to)
  end

  x.compare!
end
