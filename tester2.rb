# encoding: utf-8

require 'pyrb'

module Tester
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime


  exports['Foo'] = Pyrb::PythonClass.new('Foo', [Pyrb::Object]).tap do |klass|
    fmt = nil

    klass.attrs['baz'] = Pyrb.defn(self: nil) do |self_|
      boo = Pyrb.defn() do ||
        Pyrb.print.call([fmt])
      end

      fmt = 1
      boo.call()
    end

  end

  Foo = exports['Foo']


  exports['Foo'].call().fcall('baz')
  Pyrb::Sys.exports['modules'][__FILE__] = self
end
