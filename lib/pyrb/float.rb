module Pyrb
  module Float_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['float'] = Pyrb::PythonClass.new('float', [Pyrb::Object], {
      '__name__' => 'float',

      '__str__' => -> (args = [], **kwargs) do
        args.get(0).value.to_s
      end,

      '__eq__' => -> (args = [], **kwargs) do
        args.get(0).eq(args.get(1))
      end
    })

    exports['float'].instance_eval do
      def call(args = [], **kwargs)
        str = args.get(0)
        return ::Float::INFINITY if str == '+inf' || str == 'inf'
        return -::Float::INFINITY if str == '-inf'
        str.to_f
      end
    end
  end

  Float = Float_rb.exports['float']
end
