module Pyrb
  module Re_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['IGNORECASE'] = ::Regexp::IGNORECASE
    exports['I']          = ::Regexp::IGNORECASE

    exports['DOTALL']     = ::Regexp::MULTILINE
    exports['S']          = ::Regexp::MULTILINE

    exports['MULTILINE']  = ::Regexp::MULTILINE
    exports['M']          = ::Regexp::MULTILINE

    exports['VERBOSE']    = ::Regexp::EXTENDED
    exports['X']          = ::Regexp::EXTENDED

    exports['ASCII']      = ::Regexp::FIXEDENCODING
    exports['A']          = ::Regexp::FIXEDENCODING

    exports['match'] = -> (args = [], **kwargs) do
      re_str = args.get(0).value
      str = args.get(1).value

      m = exports['compile'].call([re_str]).match(str)

      # match must start at the beginning of the string
      if m && m.begin(0) == 0
        exports['Match'].new([m])
      end
    end

    exports['findall'] = -> (args = [], **kwargs) do
      re_str = args.get(0)
      str = args.get(1)

      str.scan(Regexp.new(re_str)).map do |captures|
        captures.size > 1 ? captures : captures.first
      end
    end

    exports['search'] = Pyrb.defn(pattern: nil, string: nil, flags: { default: 0 }) do |pattern, string, flags|
      if m = Regexp.new(pattern.value, flags).match(string.value)
        exports['Match'].new([m])
      end
    end

    exports['compile'] = -> (args = [], **kwargs) do
      str = args.get(0)
      flags = args.get(1)

      # In non-multiline mode, ^ and $ in python match only the beginning
      # and end of a string. In Ruby they match the beginning and end of
      # individual lines in all modes. For this reason, we have to replace
      # them with \A and \z in non-multiline mode.
      if (flags & Regexp::MULTILINE) == 0
        str.value.gsub!(/([^\\]?)\^/, '\\1\\A')
        str.value.gsub!(/([^\\]?)\$/, '\\1\\z')
      end

      Regexp.compile(str.value, flags)
    end

    exports['Match'] = Pyrb::PythonClass.new('Match', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        match_data = args.get(1)
        self_.ivars['value'] = match_data
      end,

      'group' => -> (args = [], **kwargs) do
        self_ = args.shift
        args = [0] if args.empty?
        return self_.value[args.first] if args.size == 1
        args.map { |i| self_.value[i] }
      end,

      'groups' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        default = args.get(1)
        self_.value.captures.map { |g| g || default }
      end
    })

    exports['Match'].klass.class_eval do
      def value
        ivars['value']
      end
    end

    exports['Pattern'] = Pyrb::PythonClass.new('Pattern', [Pyrb::Object], {
      '__name__' => 'Pattern',

      'match' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        str = args.get(1)

        if m = self_.value.match(str)
          exports['Match'].new([m])
        end
      end,

      'split' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        string = kwargs.fetch(:string) { args.get(1) }.value
        max = kwargs.fetch(:maxsplit) { args.get(2) }.value

        result = []
        last_idx = 0
        idx = 0

        while last_idx <= string.length
          idx = string.index(self_, last_idx)

          if idx.nil?
            result << string[last_idx..-1]
            break
          end

          if !max.nil? && result.size >= max
            result << string[last_idx..-1]
            break
          end

          if idx - last_idx > 0 || (idx == 0 && last_idx == 0)
            result << string[last_idx...idx]
          end

          lm = Regexp.last_match

          if lm.captures.size > 0
            last_idx = idx

            lm.captures.each do |capture|
              result << capture
              last_idx += capture.length
            end
          else
            last_idx = idx + lm[0].length
          end

          if last_idx > string.length
            result << ''
          end
        end

        result
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Re = Re_rb.exports['re']
  Pattern = Re_rb.exports['Pattern']
end
