module Pyrb
  class Buffer
    attr_accessor :data, :exporter, :range
    attr_accessor :fmt, :shape, :ndim, :itemsize, :length, :readonly

    def self.struct
      @struct ||= Pyrb.import('struct').first
    end

    def initialize(data, exporter, range: nil, fmt: 'B', shape: nil, readonly: false)
      @data = data
      @exporter = exporter
      @range = range
      @fmt = fmt
      @shape = shape
      @readonly = readonly

      @itemsize = self.class.struct.fcall('calcsize', [@fmt])

      if @shape
        # note: can't have both a shape and a range, since exporters with
        # a shape can't be sliced
        @length = @shape.inject(1) { |prod, i| prod * i } # * @itemsize
        @ndim = @shape.size

        if @length * @itemsize != @data.length
          raise Pyrb::TypeError.klass, 'product(shape) * itemsize != buffer size'
        end
      else
        if @range
          @length = Pyrb.len.call([@range])
        else
          @length = Pyrb.len.call([exporter]) / @itemsize
        end

        @ndim = 1
        @shape = [@length]
      end
    end

    def slice(new_range)
      self.class.new(
        data, self,
        range: new_range,
        fmt: fmt.dup,
        readonly: readonly
      )
    end

    def [](idx)
      case idx
        when ::Range, Pyrb::Range.klass
          slice(idx)
        else
          self.class.struct.fcall('unpack_from', [fmt, data, offset_for(idx)]).first
      end
    end

    def contiguous?
      return true if length < 2
      offset_for(0) + itemsize == offset_for(1)
    end

    def to_contiguous
      if contiguous?
        return data unless range
        offset = offset_for(0)
        return data[offset..(offset + (length * itemsize))]
      end

      ''.b.tap do |result|
        length.times do |i|
          offset = offset_for(i)
          result << data[offset..(offset + itemsize)]
        end
      end
    end

    def offset_for(idx)
      offset = case range
        when ::Range
          range.first + idx
        when ::Pyrb::Range.klass
          range[idx]
        else
          idx * itemsize
      end

      # represents the "base" case, i.e. when we need to access
      # raw data (this would be a pointer in C)
      return offset if exporter.is_a?(::String)

      exporter.offset_for(offset)
    end

    def getbuffer
      self
    end
  end
end
