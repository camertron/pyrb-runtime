module Pyrb
  module Dict_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['dict'] = Pyrb::PythonClass.new('dict', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        h = args.get(1) || {}

        self_.ivars['key_hashes'] = {}
        self_.ivars['value'] = {}

        # eventually use #update here (can't for now because doing so throws a SystemStackError)
        # because we end up recursively creating new hashes
        h.each_pair do |key, value|
          kh = key.hash
          self_.key_hashes[kh] = key
          self_.value[kh] = value
        end
      end,

      '__len__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.value.size
      end,

      '__eq__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1).value
        self_.all? { |k, v| other[k] == v }
      end,

      'items' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        # this is supposed to return a "view object" or whatever the f*ck
        # for now it's gonna just be an array
        self_.value.map { |k, v| [self_.key_hashes[k], v] }
      end,

      'get' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        key = args.get(1)
        default = args.get(2)

        self_.value.fetch(key.hash, default)
      end,

      '__getitem__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        key = args.get(1)

        if self_.value.include?(key.hash)
          self_.value[key.hash]
        else
          raise Pyrb::KeyError.klass, "'#{key}'"
        end
      end,

      '__setitem__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        key = args.get(1)
        new_val = args.get(2)
        kh = key.hash
        self_.key_hashes[kh] = key
        self_.value[kh] = new_val
      end,

      '__iter__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        Enumerator.new do |y|
          self_.value.each_key { |kh| y << self_.key_hashes[kh] }
        end
      end,

      'keys' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.value.each_key.map { |kh| self_.key_hashes[kh] }
      end,

      'values' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.value.values
      end,

      'update' => -> (args = [], **kwargs) do
        self_ = args.shift

        case args.first
          when Pyrb::Dict.klass
            args.first.value.each_pair do |k, v|
              self_[args.first.key_hashes[k]] = v
            end

          when ::Hash
            args.first.each_pair do |k, v|
              self_[k] = v
            end

          when ::Enumerable
            args.first.each do |k, v|
              self_[k] = v
            end

          else
            if Pyrb.hasattr.call([args.first, '__iter__'])
              args.first.attrs['__iter__'].each do |(k, v)|
                self_[k] = v
              end
            else
              kwargs.each_pair do |k, v|
                self_[k] = v
              end
            end
        end
      end,

      'clear' => -> (args = [], **kwargs) do
        args.get(0).value.clear
      end
    })

    exports['dict'].instance_module.class_eval do
      def value
        ivars['value'] ||= {}
      end

      def key_hashes
        ivars['key_hashes'] ||= {}
      end

      def is_a?(type)
        type == ::Hash || type == self.class
      end

      def include?(key)
        value.include?(key.hash)
      end

      def del(key)
        value.delete(key.hash)
      end

      def to_h
        key_hashes.each_with_object({}) do |(kh, k), ret|
          ret[k] = ivars['value'][kh]
        end
      end

      def inspect
        entries = fcall('__iter__').map { |k| "#{k.fcall('__repr__')}=>#{self[k].fcall('__repr__')}" }.join(', ')
        "#<#{prototype.class_name}:0x#{(object_id << 1).to_s(16).rjust(16, '0')} {#{entries}}>"
      end
    end

    exports['dict'].instance_eval do
      def call(args = [], **kwargs)
        obj = args.get(0)
        iter = Utils.unwrap_iter(obj)
        result = {}
        idx = 0

        loop do
          # loop implicitly rescues StopIteration
          elem = Pyrb.next_.call([iter])

          if !elem.is_a?(::Array)
            raise Pyrb::TypeError.klass, "cannot convert dictionary update sequence element ##{idx} to a sequence"
          end

          if elem.size != 2
            raise Pyrb::ValueError.klass, "dictionary update sequence element ##{idx} has length #{elem.size}; 2 is required"
          end

          result[elem.first] = elem.last
          idx += 1
        end

        new([result])
      end
    end
  end

  Dict = Dict_rb.exports['dict']
end
