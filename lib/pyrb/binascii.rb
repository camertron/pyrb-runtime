# encoding: utf-8

require 'pyrb'
require 'binascii'

module Lib_pyrb_binascii
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  UU_MAX = 45
  BASE_64_CHARS = (('A'..'Z').to_a + ('a'..'z').to_a + ('0'..'9').to_a + ['+', '/', "\n", '=']).join.freeze
  BASE_64_FILTER = "^#{BASE_64_CHARS}".freeze

  exports['Error'] = Pyrb::PythonClass.new('Error', [Pyrb::ValueError], {
    '__ruby_super__' => ::StandardError,

    '__init__' => Pyrb.defn(self: nil, message: nil) do |self_, message|
      self_.ivars['message'] = message
    end,

    '__str__' => Pyrb.defn(self: nil) do |self_|
      self_.ivars['message']
    end
  })

  exports['a2b_uu'] = Pyrb.defn(string: nil) do |string|
    Pyrb.check_buf(string)
    Pyrb::Bytes.new([::Binascii.a2b_uu(string.getbuffer.data)])
  end

  exports['b2a_uu'] = Pyrb.defn(data: nil, backtick: { default: false }) do |data, backtick|
    if data.value.bytesize > UU_MAX
      raise exports['Error'].klass, "At most #{UU_MAX} bytes at once"
    end

    Pyrb.check_buf(data)

    Pyrb::Bytes.new([::Binascii.b2a_uu(data.getbuffer.data, backtick: backtick)])
  end

  exports['a2b_base64'] = Pyrb.defn(string: nil) do |string|
    Pyrb.check_buf(string)

    string = string
      .getbuffer
      .to_contiguous
      .delete(BASE_64_FILTER)

    len = string.value.count("^\n")

    if len > 2 && (len % 4) != 0
      raise exports['Error'].klass, 'Incorrect padding'
    end

    Pyrb::Bytes.new([::Binascii.a2b_base64(string)])
  end

  exports['b2a_base64'] = Pyrb.defn(data: nil, newline: { default: true }) do |data, newline|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.b2a_base64(data.getbuffer.data, newline: newline)])
  end

  exports['a2b_qp'] = Pyrb.defn(data: nil, header: { default: false }) do |data, header|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.a2b_qp(data.getbuffer.data, header: header)])
  end

  exports['b2a_qp'] = Pyrb.defn(data: nil, quotetabs: { default: false }, istext: { default: true }, header: { default: false }) do |data, quote_tabs, is_text, header|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.b2a_qp(data.getbuffer.data, quote_tabs: quote_tabs, is_text: is_text, header: header)])
  end

  exports['a2b_hqx'] = Pyrb.defn(string: nil) do |string|
    Pyrb.check_buf(string)
    Pyrb::Bytes.new([::Binascii.a2b_hqx(string.value)])
  end

  exports['b2a_hqx'] = Pyrb.defn(data: nil) do |data|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.b2a_hqx(data.getbuffer.data)])
  end

  exports['rledecode_hqx'] = Pyrb.defn(data: nil) do |data|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.rledecode_hqx(data.getbuffer.data)])
  end

  exports['rlecode_hqx'] = Pyrb.defn(data: nil) do |data|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.rlecode_hqx(data.getbuffer.data)])
  end

  exports['crc_hqx'] = Pyrb.defn(data: nil, value: nil) do |data, value|
    Pyrb.check_buf(data)
    ::Binascii.crc_hqx(data.getbuffer.data, value)
  end

  exports['crc32'] = Pyrb.defn(data: nil, value: { default: 0 }) do |data, value|
    Pyrb.check_buf(data)
    ::Binascii.crc32(data.getbuffer.data, value)
  end

  exports['a2b_hex'] = Pyrb.defn(hexstr: nil) do |hexstr|
    Pyrb.check_buf(hexstr)
    Pyrb::Bytes.new([::Binascii.a2b_hex(hexstr.getbuffer.data)])
  end

  exports['b2a_hex'] = Pyrb.defn(data: nil) do |data|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.b2a_hex(data.getbuffer.data)])
  end

  exports['unhexlify'] = Pyrb.defn(hexstr: nil) do |hexstr|
    Pyrb.check_buf(hexstr)
    data = hexstr.getbuffer.data

    if data.bytesize % 2 == 1
      raise exports['Error'].klass, 'Odd-length string'
    end

    Pyrb::Bytes.new([::Binascii.unhexlify(data)])
  end

  exports['hexlify'] = Pyrb.defn(data: nil) do |data|
    Pyrb.check_buf(data)
    Pyrb::Bytes.new([::Binascii.hexlify(data.getbuffer.data)])
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
