module Pyrb
  module Int_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['int'] = Pyrb::PythonClass.new('int', [Pyrb::Object], {
      '__name__' => 'int',

      # This constructor and the use of #value is to support wrapper types,
      # i.e. classes that inherit from int. The constructor should not be
      # called except during the custruction of instances of subclasses.
      '__init__' => -> (args = [], **kwargs) do
        args.get(0).ivars['value'] = Pyrb::Int.call([args.get(1)])
      end,

      '__str__' => -> (args = [], **kwargs) do
        args.get(0).value.to_s
      end,

      '__eq__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1).value
        return true if self_.eq(0) && other.eq(false)
        self_.eq(other)
      end,

      '__ne__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1).value
        return false if self_.eq(0) && other.eq(false)
        self_.neq(other)
      end,

      '__lt__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1).value

        if Pyrb.hasattr.call([other, '__gt__'])
          other.fcall('__gt__', [self_])
        else
          self_.lt(other)
        end
      end,

      '__gt__' => -> (args = [], **kwargs) do
        args.get(0).value.gt(args.get(1).value)
      end,

      '__le__' => -> (args = [], **kwargs) do
        args.get(0).value.le(args.get(1).value)
      end,

      '__ge__' => -> (args = [], **kwargs) do
        args.get(0).value.ge(args.get(1).value)
      end,

      '__add__' => -> (args = [], **kwargs) do
        args.get(0).value.add(args.get(1).value)
      end,

      '__sub__' => -> (args = [], **kwargs) do
        args.get(0).value.sub(args.get(1).value)
      end,

      '__mul__' => -> (args = [], **kwargs) do
        args.get(0).value.mul(args.get(1).value)
      end,

      '__div__' => -> (args = [], **kwargs) do
        args.get(0).value.div(args.get(1).value)
      end
    })

    # "int" is sort of a loose term that appears to mean simply "whole number"
    # in Python just as it does in Ruby. For that reason this map packs and
    # unpacks using the 'Q' format (long long) in order to handle a number of
    # any size.
    FROM_BYTES_MAP = {
      'big' => {
        true => 'Q>',
        false => 'q>'
      },

      'little' => {
        true => 'Q<',
        false => 'q<'
      }
    }.freeze

    exports['int'].instance_eval do
      RADIX_CHARS = '0123456789abcdefghijklmnopqrstuvwxyz'.freeze
      RADIX_CHAR_CACHE = {}

      def call(args = [], **kwargs)
        val = args.get(0)
        radix = args.get(1)

        if radix && (radix < 2 || radix > 36)
          raise Pyrb::ValueError.klass, 'int() base must be >= 2 and <= 36'
        end

        case val
          when ::Integer, ::Float
            if radix
              raise Pyrb::ValueError.klass, "int() can't convert non-string with explicit base"
            end

            val.to_i
          when ::String
            radix ||= 10
            re = (RADIX_CHAR_CACHE[radix] ||= /\A-?[#{RADIX_CHARS[0...radix]}]*\z/)

            unless val =~ re
              raise Pyrb::ValueError.klass, "invalid literal for int() with base #{radix}: '#{val}'"
            end

            val.to_i(radix)
          else
            raise Pyrb::ValueError.klass,
              "int() argument must be a string, a bytes-like object or a number, not '#{val.class.name}'"
        end
      end
    end
  end

  Int = Int_rb.exports['int']

  module Int_rb
    using Pyrb::Runtime

    Int.attrs['from_bytes'] = Pyrb.classmethod.call(
      Int, Pyrb.defn(cls: nil, bytes: nil, byteorder: nil, signed: { default: false }) do |cls, bytes, byteorder, signed|
        if bytes.respond_to?(:getbuffer)
          bytes = bytes.getbuffer.to_contiguous
        end

        if bytes.bytesize < Fiddle::SIZEOF_LONG_LONG
          bytes = if byteorder == 'big'
            bytes.rjust(Fiddle::SIZEOF_LONG_LONG, "\x00")
          else
            bytes.ljust(Fiddle::SIZEOF_LONG_LONG, "\x00")
          end
        end

        bytes.unpack1(Int_rb::FROM_BYTES_MAP[byteorder][signed])
      end
    )

    Int.attrs['to_bytes'] = Pyrb.defn(self: nil, length: nil, byteorder: nil, signed: { default: false }) do |orig, length, byteorder, signed|
      if orig < 0 && !signed
        raise Pyrb::OverflowError.klass, "can't convert negative int to unsigned"
      end

      if signed
        max = 2**((length * 8) - 1) - 1
        min = -2**((length * 8) - 1)
      else
        max = 2**(length * 8) - 1
        min = -2**(length * 8)
      end

      if orig < min || orig > max
        raise Pyrb::OverflowError.klass, "int too big to convert"
      end

      num = orig.abs

      if orig < 0
        # invert
        num = (num ^ ((1 << (bytesize * 8)) - 1)) + 1
      end

      bytes = []

      if num == 0
        bytes = [0]
      else
        mask = 2**8 - 1

        while num > 0
          bytes << (num & mask)
          num >>= 8
        end
      end

      result = bytes.pack('C*')

      if result.bytesize < length
        result = if orig < 0
          result.ljust(length, "\xFF")
        else
          result.ljust(length, "\x00")
        end
      end

      result.reverse! if byteorder.value == 'big'
      Pyrb::Bytes.new([result])
    end
  end
end
