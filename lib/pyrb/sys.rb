module Pyrb
  module Sys_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    def self.init
      io, _ = import('io')
      attrs['stdin'] = io.attr('TextIOWrapper').new([io.attr('BufferedReader').new([::STDIN])])
      attrs['stdout'] = io.attr('TextIOWrapper').new([io.attr('BufferedWriter').new([::STDOUT])])
      attrs['stderr'] = io.attr('TextIOWrapper').new([io.attr('BufferedWriter').new([::STDERR])])
    end

    exports['path'] = [
      '', __dir__, File.expand_path('../../spec', __dir__),
      *ENV.fetch('PYTHONPATH', [])
    ]

    exports['argv'] = ARGV

    exports['modules'] = {
      Pyrb::List_rb.location => Pyrb::List,
      Pyrb::Dict_rb.location => Pyrb::Dict,
      Pyrb::Exception_rb.location => Pyrb::Exception,
      Pyrb::Int_rb.location => Pyrb::Int,
      Pyrb::String_rb.location => Pyrb::String,
      location => self
    }

    exports['maxsize'] = case ['0'].pack('p').size
      when 4
        # 32-bit arch
        2 ** 31 - 1
      when 8
        # 64-bit arch
        2 ** 63 - 1
    end

    exports['exc_info'] = Pyrb.defn do
      if $!
        throw :return, [$!.class, $!, $!.backtrace]
      else
        throw :return, [nil, nil, nil]
      end
    end

    attrs['byteorder'] = if [1].pack('I') == [1].pack('N')
      'big'
    else
      'little'
    end

    exports['gettotalrefcount'] = Pyrb.defn do
      # is this right?
      ObjectSpace.count_objects[:TOTAL]
    end

    exports['executable'] = File.expand_path(File.join('..', '..', 'bin', 'pyrb'), __dir__)
  end

  Sys = Sys_rb
end
