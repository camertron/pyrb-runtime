# encoding: utf-8

require 'pyrb'

# @TODO: this should eventually be transpiled from gettext.py.
# For now it just needs to exist and export the gettext function.
module Lib_pyrb_gettext
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  exports['gettext'] = Pyrb.defn(message: nil) do |message|
    message
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
