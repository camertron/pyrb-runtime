module Pyrb
  module Random_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['sample'] = -> (args = [], **kwargs) do
      # TODO: pop can also be a range
      pop = args.get(0)
      k = args.get(1)
      pop.sample(k)
    end

    exports['random'] = -> (args = [], **kwargs) do
      rand
    end

    exports['randrange'] = -> (args = [], **kwargs) do
      if args.size == 1
        start = 0
        stop = args.get(0)
        step = nil
      else
        start = args.get(0)
        stop = args.get(1)
        step = args.get(2)
      end

      range = start...stop

      return rand(range) if step.nil?

      choice = rand(range)

      while (choice - start) % step != 0
        choice = rand(range)
      end

      choice
    end

    exports['choice'] = -> (args = [], **kwargs) do
      seq = args[0]
      seq[rand(0...Pyrb.len.call([seq]))]
    end

    exports['SystemRandom'] = Pyrb::PythonClass.new('SystemRandom', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
      end,

      'choice' => -> (args = [], **kwargs) do
        seq = args.get(1)
        seq.sample
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
