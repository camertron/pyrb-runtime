# encoding: utf-8

require 'pyrb'

module Lib_pyrb_urllib_parse
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  re, _ = import('re')
  sys, _ = import('sys')
  collections, _ = import('collections')
  warnings, _ = import('warnings')
  namedtuple, _ = import('namedtuple', from: 'collections')

  <<~__DOC__
  Parse (absolute and relative) URLs.

  urlparse module is based upon the following RFC specifications.

  RFC 3986 (STD66): "Uniform Resource Identifiers" by T. Berners-Lee, R. Fielding
  and L.  Masinter, January 2005.

  RFC 2732 : "Format for Literal IPv6 Addresses in URL's by R.Hinden, B.Carpenter
  and L.Masinter, December 1999.

  RFC 2396:  "Uniform Resource Identifiers (URI)": Generic Syntax by T.
  Berners-Lee, R. Fielding, and L. Masinter, August 1998.

  RFC 2368: "The mailto URL scheme", by P.Hoffman , L Masinter, J. Zawinski, July 1998.

  RFC 1808: "Relative Uniform Resource Locators", by R. Fielding, UC Irvine, June
  1995.

  RFC 1738: "Uniform Resource Locators (URL)" by T. Berners-Lee, L. Masinter, M.
  McCahill, December 1994

  RFC 3986 is considered the current standard and any future changes to
  urlparse module should conform with it.  The urlparse module is
  currently not entirely compliant with this RFC due to defacto
  scenarios for parsing, and for backward compatibility purposes, some
  parsing quirks from older RFCs are retained. The testcases in
  test_urlparse.py provides a good indicator of parsing behavior.
  __DOC__

  exports['DeprecationWarning'] = warnings.attr('DeprecationWarning')
  exports['__all__'] = ["urlparse", "urlunparse", "urljoin", "urldefrag", "urlsplit", "urlunsplit", "urlencode", "parse_qs", "parse_qsl", "quote", "quote_plus", "quote_from_bytes", "unquote", "unquote_plus", "unquote_to_bytes", "DefragResult", "ParseResult", "SplitResult", "DefragResultBytes", "ParseResultBytes", "SplitResultBytes"]
  exports['uses_relative'] = ["", "ftp", "http", "gopher", "nntp", "imap", "wais", "file", "https", "shttp", "mms", "prospero", "rtsp", "rtspu", "sftp", "svn", "svn+ssh", "ws", "wss"]
  exports['uses_netloc'] = ["", "ftp", "http", "gopher", "nntp", "telnet", "imap", "wais", "file", "mms", "https", "shttp", "snews", "prospero", "rtsp", "rtspu", "rsync", "svn", "svn+ssh", "sftp", "nfs", "git", "git+ssh", "ws", "wss"]
  exports['uses_params'] = ["", "ftp", "hdl", "prospero", "http", "imap", "https", "shttp", "rtsp", "rtspu", "sip", "sips", "mms", "sftp", "tel"]
  exports['non_hierarchical'] = ["gopher", "hdl", "mailto", "news", "telnet", "wais", "imap", "snews", "sip", "sips"]
  exports['uses_query'] = ["", "http", "wais", "imap", "https", "shttp", "mms", "gopher", "rtsp", "rtspu", "sip", "sips"]
  exports['uses_fragment'] = ["", "ftp", "hdl", "http", "gopher", "news", "nntp", "wais", "https", "shttp", "snews", "file", "prospero"]
  exports['scheme_chars'] = (("abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "+-."))
  exports['MAX_CACHE_SIZE'] = 20
  exports['_parse_cache'] = Pyrb::Dict.new
  exports['clear_cache'] = Pyrb.defn() do ||
    "Clear the parse cache and the quoters cache."
    exports['_parse_cache'].fcall('clear')
    exports['_safe_quoters'].fcall('clear')
  end

  exports['_implicit_encoding'] = "ascii"
  exports['_implicit_errors'] = "strict"
  exports['_noop'] = Pyrb.defn(obj: nil) do |obj|
    throw(:return, obj)
  end

  exports['_encode_result'] = Pyrb.defn(obj: nil, encoding: { default: exports['_implicit_encoding'] }, errors: { default: exports['_implicit_errors'] }) do |obj, encoding, errors|
    throw(:return, obj.fcall('encode', [encoding, errors]))
  end

  exports['_decode_args'] = Pyrb.defn(args: nil, encoding: { default: exports['_implicit_encoding'] }, errors: { default: exports['_implicit_errors'] }) do |args, encoding, errors|
    throw(:return, Pyrb::List.call([args.fcall('__iter__').map do |x|
      !!(x) ? x.fcall('decode', [encoding, errors]) : ""
    end
    ]))
  end

  exports['_coerce_args'] = Pyrb.defn(args: { splat: true }) do |args|
    str_input = Pyrb.isinstance.call([args[0], Pyrb::Str])
    (args[1..-1]).each do |arg|
      if !!(arg.and { Pyrb.isinstance.call([arg, Pyrb::Str]) != str_input })
        Pyrb.raize(Pyrb::TypeError.call(["Cannot mix str and non-str arguments"]))
      end
    end
    if !!(str_input)
      throw(:return, args + [exports['_noop'], ])
    end

    throw(:return, exports['_decode_args'].call([args]) + [exports['_encode_result'], ])
  end

  exports['_ResultMixinStr'] = Pyrb::PythonClass.new('_ResultMixinStr', [Pyrb::Object]).tap do |klass|
    "Standard approach to encoding parsed results from str to bytes"
    klass.attrs['__slots__'] = ()
    klass.attrs['encode'] = Pyrb.defn(self: nil, encoding: { default: "ascii" }, errors: { default: "strict" }) do |self_, encoding, errors|
      throw(:return, self_.fcall('_encoded_counterpart', [*Pyrb.splat(self_.fcall('__iter__').map do |x|
        x.fcall('encode', [encoding, errors])
      end
      )]))
    end
  end

  _ResultMixinStr = exports['_ResultMixinStr']

  exports['_ResultMixinBytes'] = Pyrb::PythonClass.new('_ResultMixinBytes', [Pyrb::Object]).tap do |klass|
    "Standard approach to decoding parsed results from bytes to str"
    klass.attrs['__slots__'] = ()
    klass.attrs['decode'] = Pyrb.defn(self: nil, encoding: { default: "ascii" }, errors: { default: "strict" }) do |self_, encoding, errors|
      throw(:return, self_.fcall('_decoded_counterpart', [*Pyrb.splat(self_.fcall('__iter__').map do |x|
        x.fcall('decode', [encoding, errors])
      end
      )]))
    end
  end

  _ResultMixinBytes = exports['_ResultMixinBytes']

  exports['_NetlocResultMixinBase'] = Pyrb::PythonClass.new('_NetlocResultMixinBase', [Pyrb::Object]).tap do |klass|
    "Shared methods for the parsed result objects containing a netloc element"
    klass.attrs['__slots__'] = ()
    klass.properties['username'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        throw(:return, self_.attr('_userinfo')[0])
      end

    ])

    klass.properties['password'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        throw(:return, self_.attr('_userinfo')[1])
      end

    ])

    klass.properties['hostname'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        hostname = self_.attr('_hostinfo')[0]
        if !!((hostname).not)
          throw(:return, nil)
        end

        separator = !!(Pyrb.isinstance.call([hostname, Pyrb::Str])) ? "%" : Pyrb::Bytes.call(["%"])
        hostname, percent, zone = Pyrb.deconstruct(3, hostname.fcall('partition', [separator]))
        throw(:return, hostname.fcall('lower') + percent + zone)
      end

    ])

    klass.properties['port'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        port = self_.attr('_hostinfo')[1]
        if !!(port.object_id != nil.object_id)
          begin
            port = Pyrb::Int.call([port, 10])
          rescue *Pyrb.rezcue(Pyrb::ValueError)
            message = "Port could not be cast to integer value as #{port.fcall('__repr__')}"
            Pyrb.raize(Pyrb::ValueError.call([message]), from: nil)
          end

          if !!(((0 <= port && port <= 65535)).not)
            Pyrb.raize(Pyrb::ValueError.call(["Port out of range 0-65535"]))
          end

        end

        throw(:return, port)
      end

    ])
  end

  _NetlocResultMixinBase = exports['_NetlocResultMixinBase']

  exports['_NetlocResultMixinStr'] = Pyrb::PythonClass.new('_NetlocResultMixinStr', [exports['_NetlocResultMixinBase'], exports['_ResultMixinStr']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.properties['_userinfo'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        netloc = self_.attr('netloc')
        userinfo, have_info, hostinfo = Pyrb.deconstruct(3, netloc.fcall('rpartition', ["@"]))
        if !!(have_info)
          username, have_password, password = Pyrb.deconstruct(3, userinfo.fcall('partition', [":"]))
          if !!((have_password).not)
            password = nil
          end

        else
          username = password = nil
        end

        throw(:return, [username, password])
      end

    ])

    klass.properties['_hostinfo'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        netloc = self_.attr('netloc')
        _, _, hostinfo = Pyrb.deconstruct(3, netloc.fcall('rpartition', ["@"]))
        _, have_open_br, bracketed = Pyrb.deconstruct(3, hostinfo.fcall('partition', ["["]))
        if !!(have_open_br)
          hostname, _, port = Pyrb.deconstruct(3, bracketed.fcall('partition', ["]"]))
          _, _, port = Pyrb.deconstruct(3, port.fcall('partition', [":"]))
        else
          hostname, _, port = Pyrb.deconstruct(3, hostinfo.fcall('partition', [":"]))
        end

        if !!((port).not)
          port = nil
        end

        throw(:return, [hostname, port])
      end

    ])
  end

  _NetlocResultMixinStr = exports['_NetlocResultMixinStr']

  exports['_NetlocResultMixinBytes'] = Pyrb::PythonClass.new('_NetlocResultMixinBytes', [exports['_NetlocResultMixinBase'], exports['_ResultMixinBytes']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.properties['_userinfo'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        netloc = self_.attr('netloc')
        userinfo, have_info, hostinfo = Pyrb.deconstruct(3, netloc.fcall('rpartition', [Pyrb::Bytes.call(["@"])]))
        if !!(have_info)
          username, have_password, password = Pyrb.deconstruct(3, userinfo.fcall('partition', [Pyrb::Bytes.call([":"])]))
          if !!((have_password).not)
            password = nil
          end

        else
          username = password = nil
        end

        throw(:return, [username, password])
      end

    ])

    klass.properties['_hostinfo'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
        netloc = self_.attr('netloc')
        _, _, hostinfo = Pyrb.deconstruct(3, netloc.fcall('rpartition', [Pyrb::Bytes.call(["@"])]))
        _, have_open_br, bracketed = Pyrb.deconstruct(3, hostinfo.fcall('partition', [Pyrb::Bytes.call(["["])]))
        if !!(have_open_br)
          hostname, _, port = Pyrb.deconstruct(3, bracketed.fcall('partition', [Pyrb::Bytes.call(["]"])]))
          _, _, port = Pyrb.deconstruct(3, port.fcall('partition', [Pyrb::Bytes.call([":"])]))
        else
          hostname, _, port = Pyrb.deconstruct(3, hostinfo.fcall('partition', [Pyrb::Bytes.call([":"])]))
        end

        if !!((port).not)
          port = nil
        end

        throw(:return, [hostname, port])
      end

    ])
  end

  _NetlocResultMixinBytes = exports['_NetlocResultMixinBytes']

  exports['_DefragResultBase'] = namedtuple.call(["DefragResult", "url fragment"])
  exports['_SplitResultBase'] = namedtuple.call(["SplitResult", "scheme netloc path query fragment"])
  exports['_ParseResultBase'] = namedtuple.call(["ParseResult", "scheme netloc path params query fragment"])
  exports['_DefragResultBase'].attrs['__doc__'] = <<~__DOC__
  DefragResult(url, fragment)

  A 2-tuple that contains the url without fragment identifier and the fragment
  identifier as a separate argument.
  __DOC__

  exports['_DefragResultBase'].attrs['url'].attrs['__doc__'] = "The URL with no fragment identifier."
  exports['_DefragResultBase'].attrs['fragment'].attrs['__doc__'] = <<~__DOC__
  Fragment identifier separated from URL, that allows indirect identification of a
  secondary resource by reference to a primary resource and additional identifying
  information.
  __DOC__

  exports['_SplitResultBase'].attrs['__doc__'] = <<~__DOC__
  SplitResult(scheme, netloc, path, query, fragment)

  A 5-tuple that contains the different components of a URL. Similar to
  ParseResult, but does not split params.
  __DOC__

  exports['_SplitResultBase'].attrs['scheme'].attrs['__doc__'] = "Specifies URL scheme for the request."
  exports['_SplitResultBase'].attrs['netloc'].attrs['__doc__'] = <<~__DOC__
  Network location where the request is made to.
  __DOC__

  exports['_SplitResultBase'].attrs['path'].attrs['__doc__'] = <<~__DOC__
  The hierarchical path, such as the path to a file to download.
  __DOC__

  exports['_SplitResultBase'].attrs['query'].attrs['__doc__'] = <<~__DOC__
  The query component, that contains non-hierarchical data, that along with data
  in path component, identifies a resource in the scope of URI's scheme and
  network location.
  __DOC__

  exports['_SplitResultBase'].attrs['fragment'].attrs['__doc__'] = <<~__DOC__
  Fragment identifier, that allows indirect identification of a secondary resource
  by reference to a primary resource and additional identifying information.
  __DOC__

  exports['_ParseResultBase'].attrs['__doc__'] = <<~__DOC__
  ParseResult(scheme, netloc, path, params, query, fragment)

  A 6-tuple that contains components of a parsed URL.
  __DOC__

  exports['_ParseResultBase'].attrs['scheme'].attrs['__doc__'] = exports['_SplitResultBase'].attr('scheme').attr('__doc__')
  exports['_ParseResultBase'].attrs['netloc'].attrs['__doc__'] = exports['_SplitResultBase'].attr('netloc').attr('__doc__')
  exports['_ParseResultBase'].attrs['path'].attrs['__doc__'] = exports['_SplitResultBase'].attr('path').attr('__doc__')
  exports['_ParseResultBase'].attrs['params'].attrs['__doc__'] = <<~__DOC__
  Parameters for last path element used to dereference the URI in order to provide
  access to perform some operation on the resource.
  __DOC__

  exports['_ParseResultBase'].attrs['query'].attrs['__doc__'] = exports['_SplitResultBase'].attr('query').attr('__doc__')
  exports['_ParseResultBase'].attrs['fragment'].attrs['__doc__'] = exports['_SplitResultBase'].attr('fragment').attr('__doc__')
  exports['ResultBase'] = exports['_NetlocResultMixinStr']
  exports['DefragResult'] = Pyrb::PythonClass.new('DefragResult', [exports['_DefragResultBase'], exports['_ResultMixinStr']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.attrs['geturl'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.attr('fragment'))
        throw(:return, self_.attr('url') + "#" + self_.attr('fragment'))
      else
        throw(:return, self_.attr('url'))
      end
    end
  end

  DefragResult = exports['DefragResult']

  exports['SplitResult'] = Pyrb::PythonClass.new('SplitResult', [exports['_SplitResultBase'], exports['_NetlocResultMixinStr']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.attrs['geturl'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, exports['urlunsplit'].call([self_]))
    end
  end

  SplitResult = exports['SplitResult']

  exports['ParseResult'] = Pyrb::PythonClass.new('ParseResult', [exports['_ParseResultBase'], exports['_NetlocResultMixinStr']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.attrs['geturl'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, exports['urlunparse'].call([self_]))
    end
  end

  ParseResult = exports['ParseResult']

  exports['DefragResultBytes'] = Pyrb::PythonClass.new('DefragResultBytes', [exports['_DefragResultBase'], exports['_ResultMixinBytes']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.attrs['geturl'] = Pyrb.defn(self: nil) do |self_|
      if !!(self_.attr('fragment'))
        throw(:return, self_.attr('url') + Pyrb::Bytes.call(["#"]) + self_.attr('fragment'))
      else
        throw(:return, self_.attr('url'))
      end
    end
  end

  DefragResultBytes = exports['DefragResultBytes']

  exports['SplitResultBytes'] = Pyrb::PythonClass.new('SplitResultBytes', [exports['_SplitResultBase'], exports['_NetlocResultMixinBytes']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.attrs['geturl'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, exports['urlunsplit'].call([self_]))
    end
  end

  SplitResultBytes = exports['SplitResultBytes']

  exports['ParseResultBytes'] = Pyrb::PythonClass.new('ParseResultBytes', [exports['_ParseResultBase'], exports['_NetlocResultMixinBytes']]).tap do |klass|
    klass.attrs['__slots__'] = ()
    klass.attrs['geturl'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, exports['urlunparse'].call([self_]))
    end
  end

  ParseResultBytes = exports['ParseResultBytes']

  exports['_fix_result_transcoding'] = Pyrb.defn() do ||
    _result_pairs = [[exports['DefragResult'], exports['DefragResultBytes']], [exports['SplitResult'], exports['SplitResultBytes']], [exports['ParseResult'], exports['ParseResultBytes']], ]
    (_result_pairs).each do |_decoded, _encoded|
      _decoded.attrs['_encoded_counterpart'] = _encoded
      _encoded.attrs['_decoded_counterpart'] = _decoded
    end
  end

  exports['_fix_result_transcoding'].call()
  exports.del('_fix_result_transcoding')
  exports['urlparse'] = Pyrb.defn(url: nil, scheme: { default: "" }, allow_fragments: { default: true }) do |url, scheme, allow_fragments|
    <<~__DOC__
    Parse a URL into 6 components:
    <scheme>://<netloc>/<path>;<params>?<query>#<fragment>
    Return a 6-tuple: (scheme, netloc, path, params, query, fragment).
    Note that we don't break the components up in smaller bits
    (e.g. netloc is a single string) and we don't expand % escapes.
    __DOC__

    url, scheme, _coerce_result = Pyrb.deconstruct(3, exports['_coerce_args'].call([url, scheme]))
    splitresult = exports['urlsplit'].call([url, scheme, allow_fragments])
    scheme, netloc, url, query, fragment = Pyrb.deconstruct(5, splitresult)
    if !!(((scheme).in?(exports['uses_params'])).and { (";").in?(url) })
      url, params = Pyrb.deconstruct(2, exports['_splitparams'].call([url]))
    else
      params = ""
    end

    result = exports['ParseResult'].call([scheme, netloc, url, params, query, fragment])
    throw(:return, _coerce_result.call([result]))
  end

  exports['_splitparams'] = Pyrb.defn(url: nil) do |url|
    if !!(("/").in?(url))
      i = url.fcall('find', [";", url.fcall('rfind', ["/"])])
      if !!(i < 0)
        throw(:return, [url, ""])
      end

    else
      i = url.fcall('find', [";"])
    end

    throw(:return, [url[0...i], url[i + 1..-1]])
  end

  exports['_splitnetloc'] = Pyrb.defn(url: nil, start: { default: 0 }) do |url, start|
    wdelim = nil

    delim = Pyrb.len.call([url])
    ("/?#").each_char do |c|
      wdelim = url.fcall('find', [c, start])
      if !!(wdelim >= 0)
        delim = Pyrb.min.call([delim, wdelim])
      end
    end
    throw(:return, [url[start...delim], url[delim..-1]])
  end

  exports['urlsplit'] = Pyrb.defn(url: nil, scheme: { default: "" }, allow_fragments: { default: true }) do |url, scheme, allow_fragments|
    <<~__DOC__
    Parse a URL into 5 components:
    <scheme>://<netloc>/<path>?<query>#<fragment>
    Return a 5-tuple: (scheme, netloc, path, query, fragment).
    Note that we don't break the components up in smaller bits
    (e.g. netloc is a single string) and we don't expand % escapes.
    __DOC__

    url, scheme, _coerce_result = Pyrb.deconstruct(3, exports['_coerce_args'].call([url, scheme]))
    allow_fragments = Pyrb::Bool.call([allow_fragments])
    key = url, scheme, allow_fragments, Pyrb.type.call([url]), Pyrb.type.call([scheme])
    cached = exports['_parse_cache'].fcall('get', [key, nil])
    if !!(cached)
      throw(:return, _coerce_result.call([cached]))
    end

    if !!(Pyrb.len.call([exports['_parse_cache']]) >= exports['MAX_CACHE_SIZE'])
      exports['clear_cache'].call()
    end

    netloc = query = fragment = ""
    i = url.fcall('find', [":"])
    if !!(i > 0)
      if !!(url[0...i] == "http")
        url = url[i + 1..-1]
        if !!(url[0...2] == "//")
          netloc, url = Pyrb.deconstruct(2, exports['_splitnetloc'].call([url, 2]))
          if !!((((("[").in?(netloc)).and { !("]").in?(netloc) }).or { ((("]").in?(netloc)).and { !("[").in?(netloc) }) }))
            Pyrb.raize(Pyrb::ValueError.call(["Invalid IPv6 URL"]))
          end

        end

        if !!(allow_fragments.and { ("#").in?(url) })
          url, fragment = Pyrb.deconstruct(2, url.fcall('split', ["#", 1]))
        end

        if !!(("?").in?(url))
          url, query = Pyrb.deconstruct(2, url.fcall('split', ["?", 1]))
        end

        v = exports['SplitResult'].call(["http", netloc, url, query, fragment])
        exports['_parse_cache'][key] = v
        throw(:return, _coerce_result.call([v]))
      end

      catch(:break) do
        (url[0...i]).each do |c|
          if !!(!(c).in?(exports['scheme_chars']))
            throw :break, true
          end
        end
        false
      end.tap do |_caught|
        next if _caught
        rest = url[i + 1..-1]
        if !!((rest).not.or { Pyrb.any.call([rest.fcall('__iter__').map do |c|
          !(c).in?("0123456789")
        end
        ]) })
          scheme, url = Pyrb.deconstruct(2, url[0...i].fcall('lower'), rest)
        end

      end

    end

    if !!(url[0...2] == "//")
      netloc, url = Pyrb.deconstruct(2, exports['_splitnetloc'].call([url, 2]))
      if !!((((("[").in?(netloc)).and { !("]").in?(netloc) }).or { ((("]").in?(netloc)).and { !("[").in?(netloc) }) }))
        Pyrb.raize(Pyrb::ValueError.call(["Invalid IPv6 URL"]))
      end

    end

    if !!(allow_fragments.and { ("#").in?(url) })
      url, fragment = Pyrb.deconstruct(2, url.fcall('split', ["#", 1]))
    end

    if !!(("?").in?(url))
      url, query = Pyrb.deconstruct(2, url.fcall('split', ["?", 1]))
    end

    v = exports['SplitResult'].call([scheme, netloc, url, query, fragment])
    exports['_parse_cache'][key] = v
    throw(:return, _coerce_result.call([v]))
  end

  exports['urlunparse'] = Pyrb.defn(components: nil) do |components|
    <<~__DOC__
    Put a parsed URL back together again.  This may result in a
    slightly different, but equivalent URL, if the URL that was parsed
    originally had redundant delimiters, e.g. a ? with an empty query
    (the draft states that these are equivalent).
    __DOC__

    scheme, netloc, url, params, query, fragment, _coerce_result = Pyrb.deconstruct(7, (exports['_coerce_args'].call([*Pyrb.splat(components)])))
    if !!(params)
      url = ("%s;%s" % [url, params])
    end

    throw(:return, _coerce_result.call([exports['urlunsplit'].call([[scheme, netloc, url, query, fragment]])]))
  end

  exports['urlunsplit'] = Pyrb.defn(components: nil) do |components|
    <<~__DOC__
    Combine the elements of a tuple as returned by urlsplit() into a
    complete URL as a string. The data argument can be any five-item iterable.
    This may result in a slightly different, but equivalent URL, if the URL that
    was parsed originally had unnecessary delimiters (for example, a ? with an
    empty query; the RFC states that these are equivalent).
    __DOC__

    scheme, netloc, url, query, fragment, _coerce_result = Pyrb.deconstruct(6, (exports['_coerce_args'].call([*Pyrb.splat(components)])))
    if !!(netloc.or { (scheme.and { (scheme).in?(exports['uses_netloc']) }.and { url[0...2] != "//" }) })
      if !!(url.and { url[0...1] != "/" })
        url = "/" + url
      end

      url = "//" + (netloc.or { "" }) + url
    end

    if !!(scheme)
      url = scheme + ":" + url
    end

    if !!(query)
      url = url + "?" + query
    end

    if !!(fragment)
      url = url + "#" + fragment
    end

    throw(:return, _coerce_result.call([url]))
  end

  exports['urljoin'] = Pyrb.defn(base: nil, url: nil, allow_fragments: { default: true }) do |base, url, allow_fragments|
    <<~__DOC__
    Join a base URL and a possibly relative URL to form an absolute
    interpretation of the latter.
    __DOC__

    if !!((base).not)
      throw(:return, url)
    end

    if !!((url).not)
      throw(:return, base)
    end

    base, url, _coerce_result = Pyrb.deconstruct(3, exports['_coerce_args'].call([base, url]))
    bscheme, bnetloc, bpath, bparams, bquery, bfragment = Pyrb.deconstruct(6, exports['urlparse'].call([base, "", allow_fragments]))
    scheme, netloc, path, params, query, fragment = Pyrb.deconstruct(6, exports['urlparse'].call([url, bscheme, allow_fragments]))
    if !!((scheme != bscheme).or { !(scheme).in?(exports['uses_relative']) })
      throw(:return, _coerce_result.call([url]))
    end

    if !!((scheme).in?(exports['uses_netloc']))
      if !!(netloc)
        throw(:return, _coerce_result.call([exports['urlunparse'].call([[scheme, netloc, path, params, query, fragment]])]))
      end

      netloc = bnetloc
    end

    if !!((path).not.and { (params).not })
      path = bpath
      params = bparams
      if !!((query).not)
        query = bquery
      end

      throw(:return, _coerce_result.call([exports['urlunparse'].call([[scheme, netloc, path, params, query, fragment]])]))
    end

    base_parts = bpath.fcall('split', ["/"])
    if !!(base_parts[-1] != "")
      base_parts.del(-1)
    end

    if !!(path[0...1] == "/")
      segments = path.fcall('split', ["/"])
    else
      segments = base_parts + path.fcall('split', ["/"])
      segments[1...-1] = Pyrb::Filter.call([nil, segments[1...-1]])
    end

    resolved_path = []
    (segments).each do |seg|
      if !!(seg == "..")
        begin
          resolved_path.fcall('pop')
        rescue *Pyrb.rezcue(Pyrb::IndexError)
        end

      elsif !!(seg == ".")
        next
      else
        resolved_path.fcall('append', [seg])
      end
    end
    if !!((segments[-1]).in?([".", ".."]))
      resolved_path.fcall('append', [""])
    end

    throw(:return, _coerce_result.call([exports['urlunparse'].call([[scheme, netloc, "/".fcall('join', [resolved_path]).or { "/" }, params, query, fragment]])]))
  end

  exports['urldefrag'] = Pyrb.defn(url: nil) do |url|
    <<~__DOC__
    Removes any existing fragment from URL.

    Returns a tuple of the defragmented URL and the fragment.  If
    the URL contained no fragments, the second element is the
    empty string.
    __DOC__

    url, _coerce_result = Pyrb.deconstruct(2, exports['_coerce_args'].call([url]))
    if !!(("#").in?(url))
      s, n, p, a, q, frag = Pyrb.deconstruct(6, exports['urlparse'].call([url]))
      defrag = exports['urlunparse'].call([[s, n, p, a, q, ""]])
    else
      frag = ""
      defrag = url
    end

    throw(:return, _coerce_result.call([exports['DefragResult'].call([defrag, frag])]))
  end

  exports['_hexdig'] = "0123456789ABCDEFabcdef"
  exports['_hextobyte'] = nil
  exports['unquote_to_bytes'] = Pyrb.defn(string: nil) do |string|
    "unquote_to_bytes('abc%20def') -> b'abc def'."
    if !!((string).not)
      string.attr('split')
      throw(:return, Pyrb::Bytes.call([""]))
    end

    if !!(Pyrb.isinstance.call([string, Pyrb::Str]))
      string = string.fcall('encode', ["utf-8"])
    end

    bits = string.fcall('split', [Pyrb::Bytes.call(["%"])])
    if !!(Pyrb.len.call([bits]) == 1)
      throw(:return, string)
    end

    res = [bits[0]]
    append = res.attr('append')
    if !!(exports['_hextobyte'].object_id == nil.object_id)
      exports['_hextobyte'] = exports['_hexdig'].fcall('__iter__').each_with_object(Pyrb::Dict.new) do |(a), _ret_|
        exports['_hexdig'].fcall('__iter__').map do |b|
          _ret_[(a + b).fcall('encode')] = Pyrb::Bytes.fcall('fromhex', [a + b])
        end
      end

    end

    (bits[1..-1]).each do |item|
      begin
        append.call([exports['_hextobyte'][item[0...2]]])
        append.call([item[2..-1]])
      rescue *Pyrb.rezcue(Pyrb::KeyError)
        append.call([Pyrb::Bytes.call(["%"])])
        append.call([item])
      end
    end
    throw(:return, Pyrb::Bytes.call([""]).fcall('join', [res]))
  end

  exports['_asciire'] = re.fcall('compile', [("([" + [0x00].pack('U*') + "-" + [0x7f].pack('U*') + "]+)")])
  exports['unquote'] = Pyrb.defn(string: nil, encoding: { default: "utf-8" }, errors: { default: "replace" }) do |string, encoding, errors|
    <<~__DOC__
    Replace %xx escapes by their single-character equivalent. The optional
    encoding and errors parameters specify how to decode percent-encoded
    sequences into Unicode characters, as accepted by the bytes.decode()
    method.
    By default, percent-encoded sequences are decoded with UTF-8, and invalid
    sequences are replaced by a placeholder character.

    unquote('abc%20def') -> 'abc def'.
    __DOC__

    if !!(!("%").in?(string))
      string.attr('split')
      throw(:return, string)
    end

    if !!(encoding.object_id == nil.object_id)
      encoding = "utf-8"
    end

    if !!(errors.object_id == nil.object_id)
      errors = "replace"
    end

    bits = exports['_asciire'].fcall('split', [string])
    res = [bits[0]]
    append = res.attr('append')
    (Pyrb.range.call([1, Pyrb.len.call([bits]), 2])).each do |i|
      append.call([exports['unquote_to_bytes'].call([bits[i]]).fcall('decode', [encoding, errors])])
      append.call([bits[i + 1]])
    end
    throw(:return, "".fcall('join', [res]))
  end

  exports['parse_qs'] = Pyrb.defn(qs: nil, keep_blank_values: { default: false }, strict_parsing: { default: false }, encoding: { default: "utf-8" }, errors: { default: "replace" }, max_num_fields: { default: nil }) do |qs, keep_blank_values, strict_parsing, encoding, errors, max_num_fields|
    <<~__DOC__
    Parse a query given as a string argument.

    Arguments:

    qs: percent-encoded query string to be parsed

    keep_blank_values: flag indicating whether blank values in
        percent-encoded queries should be treated as blank strings.
        A true value indicates that blanks should be retained as
        blank strings.  The default false value indicates that
        blank values are to be ignored and treated as if they were
        not included.

    strict_parsing: flag indicating what to do with parsing errors.
        If false (the default), errors are silently ignored.
        If true, errors raise a ValueError exception.

    encoding and errors: specify how to decode percent-encoded sequences
        into Unicode characters, as accepted by the bytes.decode() method.

    max_num_fields: int. If set, then throws a ValueError if there
        are more than n fields read by parse_qsl().

    Returns a dictionary.
    __DOC__

    parsed_result = Pyrb::Dict.new
    pairs = exports['parse_qsl'].call([qs, keep_blank_values, strict_parsing, ], { encoding: encoding, errors: errors, max_num_fields: max_num_fields })
    (pairs).each do |name, value|
      if !!((name).in?(parsed_result))
        parsed_result[name].fcall('append', [value])
      else
        parsed_result[name] = [value]
      end
    end
    throw(:return, parsed_result)
  end

  exports['parse_qsl'] = Pyrb.defn(qs: nil, keep_blank_values: { default: false }, strict_parsing: { default: false }, encoding: { default: "utf-8" }, errors: { default: "replace" }, max_num_fields: { default: nil }) do |qs, keep_blank_values, strict_parsing, encoding, errors, max_num_fields|
    nv, name, value = nil

    <<~__DOC__
    Parse a query given as a string argument.

    Arguments:

    qs: percent-encoded query string to be parsed

    keep_blank_values: flag indicating whether blank values in
        percent-encoded queries should be treated as blank strings.
        A true value indicates that blanks should be retained as blank
        strings.  The default false value indicates that blank values
        are to be ignored and treated as if they were  not included.

    strict_parsing: flag indicating what to do with parsing errors. If
        false (the default), errors are silently ignored. If true,
        errors raise a ValueError exception.

    encoding and errors: specify how to decode percent-encoded sequences
        into Unicode characters, as accepted by the bytes.decode() method.

    max_num_fields: int. If set, then throws a ValueError
        if there are more than n fields read by parse_qsl().

    Returns a list, as G-d intended.
    __DOC__

    qs, _coerce_result = Pyrb.deconstruct(2, exports['_coerce_args'].call([qs]))
    if !!(max_num_fields.object_id != nil.object_id)
      num_fields = 1 + qs.fcall('count', ["&"]) + qs.fcall('count', [";"])
      if !!(max_num_fields < num_fields)
        Pyrb.raize(Pyrb::ValueError.call(["Max number of fields exceeded"]))
      end

    end

    pairs = qs.fcall('split', ["&"]).fcall('__iter__').flat_map do |s1|
      s1.fcall('split', [";"]).fcall('__iter__').map do |s2|
        s2
      end
    end

    r = []
    (pairs).each do |name_value|
      if !!((name_value).not.and { (strict_parsing).not })
        next
      end

      nv = name_value.fcall('split', ["=", 1])
      if !!(Pyrb.len.call([nv]) != 2)
        if !!(strict_parsing)
          Pyrb.raize(Pyrb::ValueError.call([("bad query field: %r" % [name_value, ])]))
        end

        if !!(keep_blank_values)
          nv.fcall('append', [""])
        else
          next
        end

      end

      if !!(Pyrb.len.call([nv[1]]).or { keep_blank_values })
        name = nv[0].fcall('replace', ["+", " "])
        name = exports['unquote'].call([name, ], { encoding: encoding, errors: errors })
        name = _coerce_result.call([name])
        value = nv[1].fcall('replace', ["+", " "])
        value = exports['unquote'].call([value, ], { encoding: encoding, errors: errors })
        value = _coerce_result.call([value])
        r.fcall('append', [[name, value]])
      end
    end
    throw(:return, r)
  end

  exports['unquote_plus'] = Pyrb.defn(string: nil, encoding: { default: "utf-8" }, errors: { default: "replace" }) do |string, encoding, errors|
    <<~__DOC__
    Like unquote(), but also replace plus signs by spaces, as required for
    unquoting HTML form values.

    unquote_plus('%7e/abc+def') -> '~/abc def'
    __DOC__

    string = string.fcall('replace', ["+", " "])
    throw(:return, exports['unquote'].call([string, encoding, errors]))
  end

  exports['_ALWAYS_SAFE'] = Pyrb::Frozenset.call([(Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz"]) + Pyrb::Bytes.call(["0123456789"]) + Pyrb::Bytes.call(["_.-~"]))])
  exports['_ALWAYS_SAFE_BYTES'] = Pyrb::Bytes.call([exports['_ALWAYS_SAFE']])
  exports['_safe_quoters'] = Pyrb::Dict.new
  exports['Quoter'] = Pyrb::PythonClass.new('Quoter', [collections.attr('defaultdict')]).tap do |klass|
    <<~__DOC__
    A mapping from bytes (in range(0,256)) to strings.

    String values are percent-encoded byte values, unless the key < 128, and
    in the "safe" set (either the specified safe set, or default set).
    __DOC__

    klass.attrs['__init__'] = Pyrb.defn(self: nil, safe: nil) do |self_, safe|
      "safe: bytes object."
      self_.attrs['safe'] = exports['_ALWAYS_SAFE'].fcall('union', [safe])
    end

    klass.attrs['__repr__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, ("<%s %r>" % [self_.attr('__class__').attr('__name__'), Pyrb::Dict.call([self_])]))
    end

    klass.attrs['__missing__'] = Pyrb.defn(self: nil, b: nil) do |self_, b|
      res = !!((b).in?(self_.attr('safe'))) ? Pyrb.chr.call([b]) : "%{:02X}".fcall('format', [b])
      self_[b] = res
      throw(:return, res)
    end
  end

  Quoter = exports['Quoter']

  exports['quote'] = Pyrb.defn(string: nil, safe: { default: "/" }, encoding: { default: nil }, errors: { default: nil }) do |string, safe, encoding, errors|
    <<~__DOC__
    quote('abc def') -> 'abc%20def'

    Each part of a URL, e.g. the path info, the query, etc., has a
    different set of reserved characters that must be quoted.

    RFC 3986 Uniform Resource Identifiers (URI): Generic Syntax lists
    the following reserved characters.

    reserved    = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" |
                  "$" | "," | "~"

    Each of these characters is reserved in some component of a URL,
    but not necessarily in all of them.

    Python 3.7 updates from using RFC 2396 to RFC 3986 to quote URL strings.
    Now, "~" is included in the set of reserved characters.

    By default, the quote function is intended for quoting the path
    section of a URL.  Thus, it will not encode '/'.  This character
    is reserved, but in typical usage the quote function is being
    called on a path where the existing slash characters are used as
    reserved characters.

    string and safe may be either str or bytes objects. encoding and errors
    must not be specified if string is a bytes object.

    The optional encoding and errors parameters specify how to deal with
    non-ASCII characters, as accepted by the str.encode method.
    By default, encoding='utf-8' (characters are encoded with UTF-8), and
    errors='strict' (unsupported characters raise a UnicodeEncodeError).
    __DOC__

    if !!(Pyrb.isinstance.call([string, Pyrb::Str]))
      if !!((string).not)
        throw(:return, string)
      end

      if !!(encoding.object_id == nil.object_id)
        encoding = "utf-8"
      end

      if !!(errors.object_id == nil.object_id)
        errors = "strict"
      end

      string = string.fcall('encode', [encoding, errors])
    else
      if !!(encoding.object_id != nil.object_id)
        Pyrb.raize(Pyrb::TypeError.call(["quote() doesn't support 'encoding' for bytes"]))
      end

      if !!(errors.object_id != nil.object_id)
        Pyrb.raize(Pyrb::TypeError.call(["quote() doesn't support 'errors' for bytes"]))
      end

    end

    throw(:return, exports['quote_from_bytes'].call([string, safe]))
  end

  exports['quote_plus'] = Pyrb.defn(string: nil, safe: { default: "" }, encoding: { default: nil }, errors: { default: nil }) do |string, safe, encoding, errors|
    <<~__DOC__
    Like quote(), but also replace ' ' with '+', as required for quoting
    HTML form values. Plus signs in the original string are escaped unless
    they are included in safe. It also does not have safe default to '/'.
    __DOC__

    if !!(((Pyrb.isinstance.call([string, Pyrb::Str]).and { !(" ").in?(string) }).or { (Pyrb.isinstance.call([string, Pyrb::Bytes]).and { !(Pyrb::Bytes.call([" "])).in?(string) }) }))
      throw(:return, exports['quote'].call([string, safe, encoding, errors]))
    end

    if !!(Pyrb.isinstance.call([safe, Pyrb::Str]))
      space = " "
    else
      space = Pyrb::Bytes.call([" "])
    end

    string = exports['quote'].call([string, safe + space, encoding, errors])
    throw(:return, string.fcall('replace', [" ", "+"]))
  end

  exports['quote_from_bytes'] = Pyrb.defn(bs: nil, safe: { default: "/" }) do |bs, safe|
    <<~__DOC__
    Like quote(), but accepts a bytes object rather than a str, and does
    not perform string-to-bytes encoding.  It always returns an ASCII string.
    quote_from_bytes(b'abc def\x3f') -> 'abc%20def%3f'
    __DOC__

    if !!((Pyrb.isinstance.call([bs, [Pyrb::Bytes, Pyrb::Bytearray]])).not)
      Pyrb.raize(Pyrb::TypeError.call(["quote_from_bytes() expected bytes"]))
    end

    if !!((bs).not)
      throw(:return, "")
    end

    if !!(Pyrb.isinstance.call([safe, Pyrb::Str]))
      safe = safe.fcall('encode', ["ascii", "ignore"])
    else
      safe = Pyrb::Bytes.call([safe.fcall('__iter__').each_with_object([]) do |c, _ret_|
        if !!(c < 128)
          _ret_ << c
        end
      end
      ])
    end

    if !!((bs.fcall('rstrip', [exports['_ALWAYS_SAFE_BYTES'] + safe])).not)
      throw(:return, bs.fcall('decode'))
    end

    begin
      quoter = exports['_safe_quoters'][safe]
    rescue *Pyrb.rezcue(Pyrb::KeyError)
      exports['_safe_quoters'][safe] = quoter = exports['Quoter'].call([safe]).attr('__getitem__')
    end

    throw(:return, "".fcall('join', [bs.fcall('__iter__').map do |char|
      quoter.call([char])
    end
    ]))
  end

  exports['urlencode'] = Pyrb.defn(query: nil, doseq: { default: false }, safe: { default: "" }, encoding: { default: nil }, errors: { default: nil }, quote_via: { default: exports['quote_plus'] }) do |query, doseq, safe, encoding, errors, quote_via|
    x = nil

    <<~__DOC__
    Encode a dict or sequence of two-element tuples into a URL query string.

    If any values in the query arg are sequences and doseq is true, each
    sequence element is converted to a separate parameter.

    If the query arg is a sequence of two-element tuples, the order of the
    parameters in the output will match the order of parameters in the
    input.

    The components of a query arg may each be either a string or a bytes type.

    The safe, encoding, and errors parameters are passed down to the function
    specified by quote_via (encoding and errors only if a component is a str).
    __DOC__

    if !!(Pyrb.hasattr.call([query, "items"]))
      query = query.fcall('items')
    else
      begin
        if !!(Pyrb.len.call([query]).and { (Pyrb.isinstance.call([query[0], Pyrb::List])).not })
          Pyrb.raize(Pyrb::TypeError)
        end

      rescue *Pyrb.rezcue(Pyrb::TypeError)
        ty, va, tb = Pyrb.deconstruct(3, sys.fcall('exc_info'))
        Pyrb.raize(Pyrb::TypeError.call([("not a valid non-string sequence " + "or mapping object")]).fcall('with_traceback', [tb]))
      end

    end

    l = []
    if !!((doseq).not)
      (query).each do |k, v|
        if !!(Pyrb.isinstance.call([k, Pyrb::Bytes]))
          k = quote_via.call([k, safe])
        else
          k = quote_via.call([Pyrb::Str.call([k]), safe, encoding, errors])
        end

        if !!(Pyrb.isinstance.call([v, Pyrb::Bytes]))
          v = quote_via.call([v, safe])
        else
          v = quote_via.call([Pyrb::Str.call([v]), safe, encoding, errors])
        end

        l.fcall('append', [k + "=" + v])
      end
    else
      (query).each do |k, v|
        if !!(Pyrb.isinstance.call([k, Pyrb::Bytes]))
          k = quote_via.call([k, safe])
        else
          k = quote_via.call([Pyrb::Str.call([k]), safe, encoding, errors])
        end

        if !!(Pyrb.isinstance.call([v, Pyrb::Bytes]))
          v = quote_via.call([v, safe])
          l.fcall('append', [k + "=" + v])
        elsif !!(Pyrb.isinstance.call([v, Pyrb::Str]))
          v = quote_via.call([v, safe, encoding, errors])
          l.fcall('append', [k + "=" + v])
        else
          begin
            x = Pyrb.len.call([v])
          rescue *Pyrb.rezcue(Pyrb::TypeError)
            v = quote_via.call([Pyrb::Str.call([v]), safe, encoding, errors])
            l.fcall('append', [k + "=" + v])
          else
            (v).each do |elt|
              if !!(Pyrb.isinstance.call([elt, Pyrb::Bytes]))
                elt = quote_via.call([elt, safe])
              else
                elt = quote_via.call([Pyrb::Str.call([elt]), safe, encoding, errors])
              end

              l.fcall('append', [k + "=" + elt])
            end
          end

        end
      end
    end

    throw(:return, "&".fcall('join', [l]))
  end

  exports['to_bytes'] = Pyrb.defn(url: nil) do |url|
    warnings.fcall('warn', ["urllib.parse.to_bytes() is deprecated as of 3.8", exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_to_bytes'].call([url]))
  end

  exports['_to_bytes'] = Pyrb.defn(url: nil) do |url|
    "to_bytes(u\"URL\") --> 'URL'."
    if !!(Pyrb.isinstance.call([url, Pyrb::Str]))
      begin
        url = url.fcall('encode', ["ASCII"]).fcall('decode')
      rescue *Pyrb.rezcue(Pyrb::UnicodeError)
        Pyrb.raize(Pyrb::UnicodeError.call(["URL " + Pyrb.repr.call([url]) + " contains non-ASCII characters"]))
      end

    end

    throw(:return, url)
  end

  exports['unwrap'] = Pyrb.defn(url: nil) do |url|
    warnings.fcall('warn', ["urllib.parse.unwrap() is deprecated as of 3.8", exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_unwrap'].call([url]))
  end

  exports['_unwrap'] = Pyrb.defn(url: nil) do |url|
    "unwrap('<URL:type://host/path>') --> 'type://host/path'."
    url = Pyrb::Str.call([url]).fcall('strip')
    if !!((url[0...1] == "<").and { url[-1..-1] == ">" })
      url = url[1...-1].fcall('strip')
    end

    if !!(url[0...4] == "URL:")
      url = url[4..-1].fcall('strip')
    end

    throw(:return, url)
  end

  exports['splittype'] = Pyrb.defn(url: nil) do |url|
    warnings.fcall('warn', [("urllib.parse.splittype() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splittype'].call([url]))
  end

  exports['_typeprog'] = nil
  exports['_splittype'] = Pyrb.defn(url: nil) do |url|
    "splittype('type:opaquestring') --> 'type', 'opaquestring'."
    if !!(exports['_typeprog'].object_id == nil.object_id)
      exports['_typeprog'] = re.fcall('compile', ["([^/:]+):(.*)", re.attr('DOTALL')])
    end

    match = exports['_typeprog'].fcall('match', [url])
    if !!(match)
      scheme, data = Pyrb.deconstruct(2, match.fcall('groups'))
      throw(:return, [scheme.fcall('lower'), data])
    end

    throw(:return, [nil, url])
  end

  exports['splithost'] = Pyrb.defn(url: nil) do |url|
    warnings.fcall('warn', [("urllib.parse.splithost() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splithost'].call([url]))
  end

  exports['_hostprog'] = nil
  exports['_splithost'] = Pyrb.defn(url: nil) do |url|
    "splithost('//host[:port]/path') --> 'host[:port]', '/path'."
    if !!(exports['_hostprog'].object_id == nil.object_id)
      exports['_hostprog'] = re.fcall('compile', ["//([^/\#?]*)(.*)", re.attr('DOTALL')])
    end

    match = exports['_hostprog'].fcall('match', [url])
    if !!(match)
      host_port, path = Pyrb.deconstruct(2, match.fcall('groups'))
      if !!(path.and { path[0] != "/" })
        path = "/" + path
      end

      throw(:return, [host_port, path])
    end

    throw(:return, [nil, url])
  end

  exports['splituser'] = Pyrb.defn(host: nil) do |host|
    warnings.fcall('warn', [("urllib.parse.splituser() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splituser'].call([host]))
  end

  exports['_splituser'] = Pyrb.defn(host: nil) do |host|
    "splituser('user[:passwd]@host[:port]') --> 'user[:passwd]', 'host[:port]'."
    user, delim, host = Pyrb.deconstruct(3, host.fcall('rpartition', ["@"]))
    throw(:return, [(!!(delim) ? user : nil), host])
  end

  exports['splitpasswd'] = Pyrb.defn(user: nil) do |user|
    warnings.fcall('warn', [("urllib.parse.splitpasswd() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splitpasswd'].call([user]))
  end

  exports['_splitpasswd'] = Pyrb.defn(user: nil) do |user|
    "splitpasswd('user:passwd') -> 'user', 'passwd'."
    user, delim, passwd = Pyrb.deconstruct(3, user.fcall('partition', [":"]))
    throw(:return, [user, (!!(delim) ? passwd : nil)])
  end

  exports['splitport'] = Pyrb.defn(host: nil) do |host|
    warnings.fcall('warn', [("urllib.parse.splitport() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splitport'].call([host]))
  end

  exports['_portprog'] = nil
  exports['_splitport'] = Pyrb.defn(host: nil) do |host|
    "splitport('host:port') --> 'host', 'port'."
    if !!(exports['_portprog'].object_id == nil.object_id)
      exports['_portprog'] = re.fcall('compile', ["(.*):([0-9]*)$", re.attr('DOTALL')])
    end

    match = exports['_portprog'].fcall('match', [host])
    if !!(match)
      host, port = Pyrb.deconstruct(2, match.fcall('groups'))
      if !!(port)
        throw(:return, [host, port])
      end

    end

    throw(:return, [host, nil])
  end

  exports['splitnport'] = Pyrb.defn(host: nil, defport: { default: -1 }) do |host, defport|
    warnings.fcall('warn', [("urllib.parse.splitnport() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splitnport'].call([host, defport]))
  end

  exports['_splitnport'] = Pyrb.defn(host: nil, defport: { default: -1 }) do |host, defport|
    <<~__DOC__
    Split host and port, returning numeric port.
    Return given default port if no ':' found; defaults to -1.
    Return numerical port if a valid number are found after ':'.
    Return None if ':' but not a valid number.
    __DOC__

    host, delim, port = Pyrb.deconstruct(3, host.fcall('rpartition', [":"]))
    if !!((delim).not)
      host = port
    elsif !!(port)
      begin
        nport = Pyrb::Int.call([port])
      rescue *Pyrb.rezcue(Pyrb::ValueError)
        nport = nil
      end

      throw(:return, [host, nport])
    end

    throw(:return, [host, defport])
  end

  exports['splitquery'] = Pyrb.defn(url: nil) do |url|
    warnings.fcall('warn', [("urllib.parse.splitquery() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splitquery'].call([url]))
  end

  exports['_splitquery'] = Pyrb.defn(url: nil) do |url|
    "splitquery('/path?query') --> '/path', 'query'."
    path, delim, query = Pyrb.deconstruct(3, url.fcall('rpartition', ["?"]))
    if !!(delim)
      throw(:return, [path, query])
    end

    throw(:return, [url, nil])
  end

  exports['splittag'] = Pyrb.defn(url: nil) do |url|
    warnings.fcall('warn', [("urllib.parse.splittag() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splittag'].call([url]))
  end

  exports['_splittag'] = Pyrb.defn(url: nil) do |url|
    "splittag('/path\#tag') --> '/path', 'tag'."
    path, delim, tag = Pyrb.deconstruct(3, url.fcall('rpartition', ["#"]))
    if !!(delim)
      throw(:return, [path, tag])
    end

    throw(:return, [url, nil])
  end

  exports['splitattr'] = Pyrb.defn(url: nil) do |url|
    warnings.fcall('warn', [("urllib.parse.splitattr() is deprecated as of 3.8, " + "use urllib.parse.urlparse() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splitattr'].call([url]))
  end

  exports['_splitattr'] = Pyrb.defn(url: nil) do |url|
    <<~__DOC__
    splitattr('/path;attr1=value1;attr2=value2;...') ->
    '/path', ['attr1=value1', 'attr2=value2', ...].
    __DOC__

    words = url.fcall('split', [";"])
    throw(:return, [words[0], words[1..-1]])
  end

  exports['splitvalue'] = Pyrb.defn(attr: nil) do |attr|
    warnings.fcall('warn', [("urllib.parse.splitvalue() is deprecated as of 3.8, " + "use urllib.parse.parse_qsl() instead"), exports['DeprecationWarning'], ], { stacklevel: 2 })
    throw(:return, exports['_splitvalue'].call([attr]))
  end

  exports['_splitvalue'] = Pyrb.defn(attr: nil) do |attr|
    "splitvalue('attr=value') --> 'attr', 'value'."
    attr, delim, value = Pyrb.deconstruct(3, attr.fcall('partition', ["="]))
    throw(:return, [attr, (!!(delim) ? value : nil)])
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
