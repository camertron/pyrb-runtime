module Pyrb
  module Frozenset_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['frozenset'] = PythonClass.new('frozenset', [Pyrb::Set], {
      'add' => -> (args = [], **kwargs) do
        raise Pyrb::AttributeError.klass, "frozenset object has no attribute 'add'"
      end,

      'update' => -> (args = [], **kwargs) do
        raise Pyrb::AttributeError.klass, "frozenset object has no attribute 'update'"
      end
    })

    # @TODO: ugh
    Pyrb::Set.add_subclass(exports['frozenset'])
  end

  Frozenset = Frozenset_rb.exports['frozenset']
end
