module Pyrb
  ValueError = PythonClass.new('ValueError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  AttributeError = PythonClass.new('AttributeError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  ModuleNotFoundError = PythonClass.new('ModuleNotFoundError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  TypeError = PythonClass.new('TypeError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  KeyError = PythonClass.new('KeyError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  IndexError = PythonClass.new('IndexError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  ZeroDivisionError = PythonClass.new('ZeroDivisionError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  AssertionError = PythonClass.new('AssertionError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  LookupError = PythonClass.new('LookupError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  UnicodeError = PythonClass.new('UnicodeError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  UnicodeEncodeError = PythonClass.new('UnicodeEncodeError', [UnicodeError], {
    '__ruby_super__' => ::StandardError
  })

  OverflowError = PythonClass.new('OverflowError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  OSError = PythonClass.new('OSError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  NotImplementedError = PythonClass.new('NotImplementedError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  ImportError = PythonClass.new('ImportError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  IsADirectoryError = PythonClass.new('IsADirectoryError', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })

  StopIteration = PythonClass.new('StopIteration', [Pyrb::Exception], {
    '__ruby_super__' => ::StandardError
  })
end
