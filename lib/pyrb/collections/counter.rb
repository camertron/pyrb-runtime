module Pyrb
  module Collections
    module Counter_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['Counter'] = Pyrb::PythonClass.new('Counter', [Pyrb::Dict], {
        '__init__' => -> (args = [], **kwargs) do
          self_ = args.shift
          self_.ivars['value'] = {}

          return if args.empty? && kwargs.empty?

          if args.size == 1
            if args.first.is_a?(Hash)
              self_.value.merge!(args.first)
            else
              Pyrb.enumerate.call([args.first]).each do |_, item|
                self_.value[item] ||= 0
                self_.value[item] += 1
              end
            end
          elsif kwargs.size > 0
            kwargs.each_pair do |k, v|
              self_.value[k.to_s] ||= 0
              self_.value[k.to_s] += 1
            end
          end
        end,

        'elements' => -> (args = [], **kwargs) do
          self_ = args.get(0)

          Pyrb::Itertools::Chain.new(
            *self_.value.each_with_object({}) do |(obj, count), ret|
              next if count <= 0
              ret << Enumerator.new { |yielder| count.times { yielder << obj } }
            end
          )
        end
      })

      exports['Counter'].klass.class_eval do
        def value
          ivars['value']
        end
      end

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Counter = Counter_rb.exports['Counter']
  end
end

