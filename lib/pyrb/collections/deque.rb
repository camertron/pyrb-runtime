module Pyrb
  module Collections
    module Deque_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['deque'] = Pyrb::PythonClass.new('deque', [Pyrb::Object], {
        '__init__' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          value = args.get(1)
          self_.ivars['value'] = value
        end,

        '__len__' => -> (args = [], **kwargs) do
          args.get(0).ivars['value'].size
        end,

        '__iter__' => -> (args = [], **kwargs) do
          args.get(0).ivars['value'].each
        end,

        'append' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          item = args.get(1)
          self_.ivars['value'] << item
        end,

        'appendleft' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          item = args.get(1)
          self_.ivars['value'].unshift(item)
        end,

        'pop' => -> (args = [], **kwargs) do
          args.get(0).ivars['value'].pop
        end,

        'popleft' => -> (args = [], **kwargs) do
          args.get(0).ivars['value'].shift
        end,

        'clear' => -> (args = [], **kwargs) do
          args.get(0).ivars['value'].clear
        end,

        # this is not a python method; there's really no python equivalent,
        # as python does membership checks via the "in" and "not in" keywords
        'include?' => -> (self_, obj) do
          self_.ivars['value'].include?(obj)
        end
      })

      exports['deque'].klass.class_eval do
        def [](key)
          ivars['value'][key]
        end

        def []=(key, new_val)
          ivars['value'][key] = new_val
        end
      end

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Deque = Deque_rb.exports['deque']
  end
end
