module Pyrb
  module Collections
    module Namedtuple_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['namedtuple'] = Pyrb.defn(typename: nil, field_names: nil) do |typename, field_names|
        if field_names.is_a?(String)
          field_names = field_names.squeeze(' ').split(/[ ,]/)
        end

        field_names.map!(&:to_sym)

        nt = PythonClass.new(typename, [Pyrb::List]).tap do |klass|
          klass.attrs['_fields'] = field_names
          signature = { self: nil }.merge(Hash[field_names.zip([nil])])
          klass.attrs['__init__'] = Pyrb.defn(signature) do |self_, *args|
            # inheriting from Pyrb::List means we have to set the @value ivar
            self_.instance_variable_set(:@value, args)
          end

          field_names.each_with_index do |field_name, idx|
            klass.properties[field_name] = Pyrb::Property.new([
              Pyrb.defn(self: nil) do |self_|
                throw(:return, self_.value[idx])
              end
            ])

            klass.properties[field_name].setter =
              Pyrb.defn(self: nil, value: nil) do |self_, value|
                self_.value[idx] = value
              end
          end

          klass.attrs['__eq__'] = Pyrb.defn(self: nil, other: nil) do |self_, other|
            throw(:return, self_.value == other.value)
          end
        end

        nt.instance_module.class_eval do
          def inspect
            attrs = attr('_fields').map { |fn| "#{fn}=#{attr(fn).inspect}" }.join(', ')
            "#<#{self.class.prototype.class_name}:0x#{(object_id << 1).to_s(16).rjust(16, '0')} #{attrs}>"
          end
        end

        # @TODO: ugh
        Pyrb::List.add_subclass(nt)

        throw(:return, nt)
      end

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Namedtuple = Namedtuple_rb.exports['namedtuple']
  end
end
