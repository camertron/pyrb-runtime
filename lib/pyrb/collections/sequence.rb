module Pyrb
  module Collection
    module Sequence_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['Sequence'] = ::Array

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Sequence = Sequence_rb.exports['Sequence']
  end
end
