module Pyrb
  module Collections
    module Defaultdict_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['defaultdict'] = Pyrb::PythonClass.new('defaultdict', [Pyrb::Dict], {
        '__init__' => Pyrb.defn(self: nil, default_factory: { default: nil }) do |self_, default_factory|
          self_.ivars['default_factory'] = default_factory
        end,

        '__getitem__' => Pyrb.defn(self: nil, key: nil) do |self_, key|
          if self_.include?(key)
            # super
            throw(:return, Pyrb::Dict.attr('__getitem__').call([self_, key]))
          end

          self_.fcall('__missing__', [key])
        end,

        '__missing__' => Pyrb.defn(self: nil, key: nil) do |self_, key|
          if !self_.ivars['default_factory']
            raise Pyrb::KeyError.klass, "'#{key}'"
          end

          self_[key] = self_.ivars['default_factory'].call
        end
      })

      # @TODO: ugh
      Pyrb::Dict.add_subclass(exports['defaultdict'])

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Defaultdict = Namedtuple_rb.exports['defaultdict']
  end
end
