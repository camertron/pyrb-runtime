module Pyrb
  module Abc_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['Iterator'] = Pyrb::PythonClass.new('Iterator', [Pyrb::Object])
    exports['Iterable'] = Pyrb::PythonClass.new('Iterable', [Pyrb::Object])

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
