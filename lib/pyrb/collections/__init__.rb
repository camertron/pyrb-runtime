module Pyrb
  module Collections_init_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    import 'abc', from: '.'
    import 'deque', from: '.deque'
    import 'Sequence', from: '.sequence'
    import 'Counter', from: '.counter'
    import 'UserList', from: '.userlist'
    import 'namedtuple', from: '.namedtuple'
    import 'defaultdict', from: '.defaultdict'

    exports['__all__'] = %w(
      abc deque Sequence Counter UserList namedtuple defaultdict
    )

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
