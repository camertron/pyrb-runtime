module Pyrb
  module Collections
    module Userlist_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['UserList'] = ::Pyrb::List

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    UserList = Userlist_rb.exports['UserList']
  end
end
