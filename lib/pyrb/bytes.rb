require 'yaml'
require 'code-pages'

module Pyrb
  module Bytes_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    BASE_TRANS_TABLE = ''.b.tap { |s| (0..255).each { |b| s << b } }.freeze
    BASE_TRANS_TABLE_RB = BASE_TRANS_TABLE.sub('^', '\\^').sub('-', '\\-').freeze

    exports['bytes'] = PythonClass.new('bytes', [Pyrb::String], {
      '__init__' => Pyrb.defn(self: nil, bytes: nil) do |self_, bytes|
        self_.ivars['value'] = bytes.value
      end,

      'decode' => -> (args = [], **kwargs) do
        errors = kwargs.fetch(:errors, args.get(2)) || 'strict'
        codec = Encodings.find_codec(args.get(1) || 'utf-8')
        codec.decode(args.get(0).value, errors: errors)
      end,

      'join' => -> (args = [], **kwargs) do
        Pyrb::Bytes.new([Pyrb::String.fcall('join', args, **kwargs)])
      end,

      'rstrip' => -> (args = [], **kwargs) do
        Pyrb::Bytes.new([Pyrb::String.fcall('rstrip', args, **kwargs)])
      end,

      'lstrip' => -> (args = [], **kwargs) do
        Pyrb::Bytes.new([Pyrb::String.fcall('lstrip', args, **kwargs)])
      end,

      'upper' => -> (args = [], **kwargs) do
        Pyrb::Bytes.new([Pyrb::String.fcall('upper', args, **kwargs)])
      end,

      'lower' => -> (args = [], **kwargs) do
        Pyrb::Bytes.new([Pyrb::String.fcall('lower', args, **kwargs)])
      end,

      '__getitem__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        idx = args.get(1)

        case idx
          when ::Pyrb::Range.klass
            Pyrb::Bytes.new([''.b]).tap do |result|
              idx.apply_to(self_) { |i| result.value << self_.getbyte(i) }
            end
          when ::Range
            Pyrb::Bytes.new([self_.byteslice(idx)])
          else
            self_.getbyte(idx)
        end
      end,

      '__iter__' => -> (args = [], **kwargs) do
        args.get(0).value.each_byte
      end,

      '__setitem__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        idx = args.get(1)
        val = args.get(2).value
        # let's see how long we can get away with this lol
        self_[idx] = val
      end,

      '__mul__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        multiplier = args.get(1)
        Pyrb::Bytes.new([self_ * multiplier])
      end,

      '__contains__' => -> (args = [], **kwargs) do
        val = args.get(0).value
        chr = args.get(1).value

        return val.include?(chr.chr) if chr.is_a?(::Integer)
        val.include?(chr)
      end,

      '__repr__' => -> (args = [], **kwargs) do
        args.get(0).inspect
      end,

      'translate' => Pyrb.defn(self: nil, table: nil, delete: { default: '' }) do |self_, table, delete|
        Pyrb.check_buf(table)

        unless delete == ''
          Pyrb.check_buf(delete)
          delete = delete.to_contiguous
        end

        str = self_.value.delete(delete)

        if table
          table = table
            .to_contiguous
            .sub('^', '\\^')
            .sub('-', '\\-')

          str.tr!(BASE_TRANS_TABLE_RB, table)
        end

        Pyrb::Bytes.new([str])
      end,

      'splitlines' => Pyrb.defn(self: nil, keepends: { default: false }) do |self_, keepends|
        self_.value.split(/\r\n|\r|\n/).map do |s|
          Pyrb::Bytes.new([s])
        end
      end
    })

    exports['bytes'].tap do |klass|
      klass.attrs['fromhex'] = Pyrb.classmethod.call(
        klass, Pyrb.defn(cls: nil, str: nil) do |cls, str|
          result = ''.b

          (str.length / 2).floor.times do |i|
            result << str[(i * 2)..(i * 2) + 1].to_i(16)
          end

          throw(:return, klass.new([result]))
        end
      )

      klass.attrs['maketrans'] = Pyrb.classmethod.call(
        klass, Pyrb.defn(cls: nil, from: nil, to: nil) do |cls, from, to|
          Pyrb.check_buf(from)
          Pyrb.check_buf(to)

          if Pyrb.len.call([from]).neq(Pyrb.len.call([to]))
            raise Pyrb::ValueError.klass, 'maketrans arguments must have same length'
          end

          klass.new([BASE_TRANS_TABLE.tr(from.to_contiguous, to.to_contiguous)])
        end
      )
    end

    exports['bytes'].instance_module.class_eval do
      def +(other)
        Pyrb::Bytes.new([value + other.value])
      end

      def hash
        value.hash
      end

      alias_method :neg, :!

      def !
        value.nil? || value.eql?('')
      end

      def inspect
        return 'nil' if value.nil?
        "b'#{value.inspect[1..-2]}'"
      end

      def to_s
        inspect
      end

      def getbuffer
        @buffer ||= Pyrb::Buffer.new(value, self)
      end

      def offset_for(idx)
        idx
      end

      def to_contiguous
        value
      end
    end

    exports['bytes'].instance_eval do
      def call(args = [], **kwargs)
        # @TODO: handle zero-filling and the buffer protocol
        # (only supports strings and iterables for now)
        source = args.get(0)

        str = case Pyrb.type.call([source])
          when Pyrb::String
            source.b
          when Range, Pyrb::Range  # ...and other iterables of integers
            source.fcall('__iter__').to_a.pack('C*')
          else
            if Pyrb.hasattr.call([source, '__iter__'])
              source.fcall('__iter__').inject(''.force_encoding('ASCII')) do |ret, item|
                ret << item
              end
            end
        end

        new([str])
      end
    end
  end

  Bytes = Bytes_rb.exports['bytes']
end
