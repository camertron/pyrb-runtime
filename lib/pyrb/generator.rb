module Pyrb
  module Generator_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    # it's probably not correct for generator to inherit from iterator, but iterator
    # provides all the functionality we need, so let's leave it for now
    exports['generator'] = Pyrb::PythonClass.new('generator', [Pyrb::Iterator])
  end

  Generator = Generator_rb.exports['generator']
end
