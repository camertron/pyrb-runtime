module Pyrb
  module Repl
    class Workspace
      def self.location
        '(repl)'
      end

      include Pyrb::Runtime
      using Pyrb::Runtime
    end

    BANNER = <<~END.freeze
      Pyrb #{Pyrb::VERSION} (Python compat #{Pyrb::PYTHON_VERSION})
      [Ruby #{RUBY_VERSION} #{RbConfig::CONFIG['CC_VERSION_MESSAGE'].split("\n").first}] on #{Gem::Platform.local.os}
    END

    class << self
      def start
        require 'pyrb/transpiler'
        require 'readline'

        puts BANNER

        indent_level = 0
        session = Pyrb::Transpiler::Session.new(Workspace.new.__binding__)
        session.context.enter_scope(:repl)
        shutting_down = false
        input = ''

        Readline.pre_input_hook = proc do
          Readline.insert_text('  ' * indent_level)
          Readline.refresh_line
        end

        trap(:INT) do
          STDOUT.puts

          exit(1) if shutting_down

          Readline.delete_text
          Readline.refresh_line
          shutting_down = true
        end

        # true means add inputted line to history
        while buf = Readline.readline('>>> ', true)
          input += "#{buf}\n"

          if buf.strip.end_with?(':')
            indent_level += 1
          else
            if indent_level == 0 || (buf.strip.empty? && indent_level > 0)
              begin
                result = session.evaluate(input)
                puts "=> #{result.inspect}"
              rescue => e
                puts e.backtrace.join("\n")
                puts Pyrb.str.call([e])
              ensure
                input = ''
                indent_level = 0
                shutting_down = false
              end
            end
          end
        end
      end
    end
  end
end
