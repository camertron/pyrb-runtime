# encoding: utf-8

require 'pyrb'

module Lib_pyrb_bisect
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  "Bisection algorithms."
  exports['insort_right'] = Pyrb.defn(a: nil, x: nil, lo: { default: 0 }, hi: { default: nil }) do |a, x, lo, hi|
    <<~__DOC__
    Insert item x in list a, and keep it sorted assuming a is sorted.

    If x is already in a, insert it to the right of the rightmost x.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    __DOC__

    if !!(lo < 0)
      Pyrb.raize(Pyrb::ValueError.call(["lo must be non-negative"]))
    end

    if !!(hi.object_id == nil.object_id)
      hi = Pyrb.len.call([a])
    end

    while !!(lo < hi)
      mid = Pyrb.int_divide((lo + hi), 2)
      if !!(x < a[mid])
        hi = mid
      else
        lo = mid + 1
      end

    end

    a.fcall('insert', [lo, x])
  end

  exports['bisect_right'] = Pyrb.defn(a: nil, x: nil, lo: { default: 0 }, hi: { default: nil }) do |a, x, lo, hi|
    <<~__DOC__
    Return the index where to insert item x in list a, assuming a is sorted.

    The return value i is such that all e in a[:i] have e <= x, and all e in
    a[i:] have e > x.  So if x already appears in the list, a.insert(x) will
    insert just after the rightmost x already there.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    __DOC__

    if !!(lo < 0)
      Pyrb.raize(Pyrb::ValueError.call(["lo must be non-negative"]))
    end

    if !!(hi.object_id == nil.object_id)
      hi = Pyrb.len.call([a])
    end

    while !!(lo < hi)
      mid = Pyrb.int_divide((lo + hi), 2)
      if !!(x < a[mid])
        hi = mid
      else
        lo = mid + 1
      end

    end

    throw(:return, lo)
  end

  exports['insort_left'] = Pyrb.defn(a: nil, x: nil, lo: { default: 0 }, hi: { default: nil }) do |a, x, lo, hi|
    <<~__DOC__
    Insert item x in list a, and keep it sorted assuming a is sorted.

    If x is already in a, insert it to the left of the leftmost x.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    __DOC__

    if !!(lo < 0)
      Pyrb.raize(Pyrb::ValueError.call(["lo must be non-negative"]))
    end

    if !!(hi.object_id == nil.object_id)
      hi = Pyrb.len.call([a])
    end

    while !!(lo < hi)
      mid = Pyrb.int_divide((lo + hi), 2)
      if !!(a[mid] < x)
        lo = mid + 1
      else
        hi = mid
      end

    end

    a.fcall('insert', [lo, x])
  end

  exports['bisect_left'] = Pyrb.defn(a: nil, x: nil, lo: { default: 0 }, hi: { default: nil }) do |a, x, lo, hi|
    <<~__DOC__
    Return the index where to insert item x in list a, assuming a is sorted.

    The return value i is such that all e in a[:i] have e < x, and all e in
    a[i:] have e >= x.  So if x already appears in the list, a.insert(x) will
    insert just before the leftmost x already there.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    __DOC__

    if !!(lo < 0)
      Pyrb.raize(Pyrb::ValueError.call(["lo must be non-negative"]))
    end

    if !!(hi.object_id == nil.object_id)
      hi = Pyrb.len.call([a])
    end

    while !!(lo < hi)
      mid = Pyrb.int_divide((lo + hi), 2)
      if !!(a[mid] < x)
        lo = mid + 1
      else
        hi = mid
      end

    end

    throw(:return, lo)
  end

  exports['bisect'] = exports['bisect_right']
  exports['insort'] = exports['insort_right']
  Pyrb::Sys.exports['modules'][__FILE__] = self
end
