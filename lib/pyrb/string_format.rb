require 'strscan'

module Pyrb
  module StringFormat
    autoload :Conversion, 'pyrb/string_format/components'
    autoload :FieldName,  'pyrb/string_format/components'
    autoload :FormatSpec, 'pyrb/string_format/components'
    autoload :Parser,     'pyrb/string_format/parser'
    autoload :Pattern,    'pyrb/string_format/pattern'
    autoload :Token,      'pyrb/string_format/token'
    autoload :Tokenizer,  'pyrb/string_format/tokenizer'

    class << self
      FORMAT_START_RE = /(?<!\\)(?={)/.freeze
      FORMAT_END_RE   = /(?<!\\)\}/.freeze

      def format(str, args)
        scanner = StringScanner.new(str)
        result = ''
        idx = 0

        until scanner.eos?
          if prefix = scanner.scan_until(FORMAT_START_RE)
            result << prefix
          else
            result << scanner.rest
            break
          end

          break if scanner.eos?
          pattern_str = scanner.scan_until(FORMAT_END_RE)
          pattern = pattern_for(pattern_str)
          result << pattern.apply_to(args, idx)
          idx += 1
        end

        result
      end

      private

      def pattern_for(pattern_str)
        pattern_cache[pattern_str] ||= begin
          tok = Pyrb::StringFormat::Tokenizer.new(pattern_str)
          Pyrb::StringFormat::Parser.new(tok).parse
        end
      end

      def pattern_cache
        @pattern_cache ||= {}
      end
    end
  end
end
