# encoding: utf-8

require 'pyrb'

module Lib_pyrb_base64
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  re, _ = import('re')
  struct, _ = import('struct')
  binascii, _ = import('binascii')

  "Base16, Base32, Base64 (RFC 3548), Base85 and Ascii85 data encodings"
  exports['__all__'] = ["encode", "decode", "encodebytes", "decodebytes", "b64encode", "b64decode", "b32encode", "b32decode", "b16encode", "b16decode", "b85encode", "b85decode", "a85encode", "a85decode", "standard_b64encode", "standard_b64decode", "urlsafe_b64encode", "urlsafe_b64decode", ]
  exports['bytes_types'] = [Pyrb::Bytes, Pyrb::Bytearray]
  exports['_bytes_from_decode_data'] = Pyrb.defn(s: nil) do |s|
    if !!(Pyrb.isinstance.call([s, Pyrb::Str]))
      begin
        throw(:return, s.fcall('encode', ["ascii"]))
      rescue *Pyrb.rezcue(Pyrb::UnicodeEncodeError)
        Pyrb.raize(Pyrb::ValueError.call(["string argument should contain only ASCII characters"]))
      end

    end

    if !!(Pyrb.isinstance.call([s, exports['bytes_types']]))
      throw(:return, s)
    end

    begin
      throw(:return, Pyrb::Memoryview.call([s]).fcall('tobytes'))
    rescue *Pyrb.rezcue(Pyrb::TypeError)
      Pyrb.raize(Pyrb::TypeError.call([(("argument should be a bytes-like object or ASCII " + "string, not %r") % s.attr('__class__').attr('__name__'))]), from: nil)
    end
  end

  exports['b64encode'] = Pyrb.defn(s: nil, altchars: { default: nil }) do |s, altchars|
    <<~__DOC__
    Encode the bytes-like object s using Base64 and return a bytes object.

    Optional altchars should be a byte string of length 2 which specifies an
    alternative alphabet for the '+' and '/' characters.  This allows an
    application to e.g. generate url or filesystem safe Base64 strings.
    __DOC__

    encoded = binascii.fcall('b2a_base64', [s, ], { newline: false })
    if !!(altchars.object_id != nil.object_id)
      Pyrb.assert(Pyrb.len.call([altchars]) == 2, Pyrb.repr.call([altchars]))
      throw(:return, encoded.fcall('translate', [Pyrb::Bytes.fcall('maketrans', [Pyrb::Bytes.call(["+/"]), altchars])]))
    end

    throw(:return, encoded)
  end

  exports['b64decode'] = Pyrb.defn(s: nil, altchars: { default: nil }, validate: { default: false }) do |s, altchars, validate|
    <<~__DOC__
    Decode the Base64 encoded bytes-like object or ASCII string s.

    Optional altchars must be a bytes-like object or ASCII string of length 2
    which specifies the alternative alphabet used instead of the '+' and '/'
    characters.

    The result is returned as a bytes object.  A binascii.Error is raised if
    s is incorrectly padded.

    If validate is False (the default), characters that are neither in the
    normal base-64 alphabet nor the alternative alphabet are discarded prior
    to the padding check.  If validate is True, these non-alphabet characters
    in the input result in a binascii.Error.
    __DOC__

    s = exports['_bytes_from_decode_data'].call([s])
    if !!(altchars.object_id != nil.object_id)
      altchars = exports['_bytes_from_decode_data'].call([altchars])
      Pyrb.assert(Pyrb.len.call([altchars]) == 2, Pyrb.repr.call([altchars]))
      s = s.fcall('translate', [Pyrb::Bytes.fcall('maketrans', [altchars, Pyrb::Bytes.call(["+/"])])])
    end

    if !!(validate.and { (re.fcall('match', [Pyrb::Bytes.call(["^[A-Za-z0-9+/]*={0,2}$"]), s])).not })
      Pyrb.raize(binascii.fcall('Error', ["Non-base64 digit found"]))
    end

    throw(:return, binascii.fcall('a2b_base64', [s]))
  end

  exports['standard_b64encode'] = Pyrb.defn(s: nil) do |s|
    <<~__DOC__
    Encode bytes-like object s using the standard Base64 alphabet.

    The result is returned as a bytes object.
    __DOC__

    throw(:return, exports['b64encode'].call([s]))
  end

  exports['standard_b64decode'] = Pyrb.defn(s: nil) do |s|
    <<~__DOC__
    Decode bytes encoded with the standard Base64 alphabet.

    Argument s is a bytes-like object or ASCII string to decode.  The result
    is returned as a bytes object.  A binascii.Error is raised if the input
    is incorrectly padded.  Characters that are not in the standard alphabet
    are discarded prior to the padding check.
    __DOC__

    throw(:return, exports['b64decode'].call([s]))
  end

  exports['_urlsafe_encode_translation'] = Pyrb::Bytes.fcall('maketrans', [Pyrb::Bytes.call(["+/"]), Pyrb::Bytes.call(["-_"])])
  exports['_urlsafe_decode_translation'] = Pyrb::Bytes.fcall('maketrans', [Pyrb::Bytes.call(["-_"]), Pyrb::Bytes.call(["+/"])])
  exports['urlsafe_b64encode'] = Pyrb.defn(s: nil) do |s|
    <<~__DOC__
    Encode bytes using the URL- and filesystem-safe Base64 alphabet.

    Argument s is a bytes-like object to encode.  The result is returned as a
    bytes object.  The alphabet uses '-' instead of '+' and '_' instead of
    '/'.
    __DOC__

    throw(:return, exports['b64encode'].call([s]).fcall('translate', [exports['_urlsafe_encode_translation']]))
  end

  exports['urlsafe_b64decode'] = Pyrb.defn(s: nil) do |s|
    <<~__DOC__
    Decode bytes using the URL- and filesystem-safe Base64 alphabet.

    Argument s is a bytes-like object or ASCII string to decode.  The result
    is returned as a bytes object.  A binascii.Error is raised if the input
    is incorrectly padded.  Characters that are not in the URL-safe base-64
    alphabet, and are not a plus '+' or slash '/', are discarded prior to the
    padding check.

    The alphabet uses '-' instead of '+' and '_' instead of '/'.
    __DOC__

    s = exports['_bytes_from_decode_data'].call([s])
    s = s.fcall('translate', [exports['_urlsafe_decode_translation']])
    throw(:return, exports['b64decode'].call([s]))
  end

  exports['_b32alphabet'] = Pyrb::Bytes.call(["ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"])
  exports['_b32tab2'] = nil
  exports['_b32rev'] = nil
  exports['b32encode'] = Pyrb.defn(s: nil) do |s|
    c = nil

    <<~__DOC__
    Encode the bytes-like object s using Base32 and return a bytes object.
    __DOC__

    if !!(exports['_b32tab2'].object_id == nil.object_id)
      b32tab = exports['_b32alphabet'].fcall('__iter__').map do |i|
        Pyrb::Bytes.call([[i, ]])
      end

      exports['_b32tab2'] = b32tab.fcall('__iter__').flat_map do |a|
        b32tab.fcall('__iter__').map do |b|
          a + b
        end
      end

      b32tab = nil
    end

    if !!((Pyrb.isinstance.call([s, exports['bytes_types']])).not)
      s = Pyrb::Memoryview.call([s]).fcall('tobytes')
    end

    leftover = (Pyrb.len.call([s]) % 5)
    if !!(leftover)
      s = s + (Pyrb::Bytes.call(["\0"]) * (5 - leftover))
    end

    encoded = Pyrb::Bytearray.call()
    from_bytes = Pyrb::Int.attr('from_bytes')
    b32tab2 = exports['_b32tab2']
    (Pyrb.range.call([0, Pyrb.len.call([s]), 5])).each do |i|
      c = from_bytes.call([s[i...i + 5], "big"])
      encoded += (b32tab2[c >> 30] + b32tab2[(c >> 20) & 0x3ff] + b32tab2[(c >> 10) & 0x3ff] + b32tab2[c & 0x3ff])
    end
    if !!(leftover == 1)
      encoded[-6..-1] = Pyrb::Bytes.call(["======"])
    elsif !!(leftover == 2)
      encoded[-4..-1] = Pyrb::Bytes.call(["===="])
    elsif !!(leftover == 3)
      encoded[-3..-1] = Pyrb::Bytes.call(["==="])
    elsif !!(leftover == 4)
      encoded[-1..-1] = Pyrb::Bytes.call(["="])
    end

    throw(:return, Pyrb::Bytes.call([encoded]))
  end

  exports['b32decode'] = Pyrb.defn(s: nil, casefold: { default: false }, map01: { default: nil }) do |s, casefold, map01|
    acc, quanta = nil

    <<~__DOC__
    Decode the Base32 encoded bytes-like object or ASCII string s.

    Optional casefold is a flag specifying whether a lowercase alphabet is
    acceptable as input.  For security purposes, the default is False.

    RFC 3548 allows for optional mapping of the digit 0 (zero) to the
    letter O (oh), and for optional mapping of the digit 1 (one) to
    either the letter I (eye) or letter L (el).  The optional argument
    map01 when not None, specifies which letter the digit 1 should be
    mapped to (when map01 is not None, the digit 0 is always mapped to
    the letter O).  For security purposes the default is None, so that
    0 and 1 are not allowed in the input.

    The result is returned as a bytes object.  A binascii.Error is raised if
    the input is incorrectly padded or if there are non-alphabet
    characters present in the input.
    __DOC__

    if !!(exports['_b32rev'].object_id == nil.object_id)
      exports['_b32rev'] = Pyrb.enumerate.call([exports['_b32alphabet']]).fcall('__iter__').each_with_object(Pyrb::Dict.new) do |(k, v), _ret_|
        _ret_[v] = k
      end

    end

    s = exports['_bytes_from_decode_data'].call([s])
    if !!((Pyrb.len.call([s]) % 8))
      Pyrb.raize(binascii.fcall('Error', ["Incorrect padding"]))
    end

    if !!(map01.object_id != nil.object_id)
      map01 = exports['_bytes_from_decode_data'].call([map01])
      Pyrb.assert(Pyrb.len.call([map01]) == 1, Pyrb.repr.call([map01]))
      s = s.fcall('translate', [Pyrb::Bytes.fcall('maketrans', [Pyrb::Bytes.call(["01"]), Pyrb::Bytes.call(["O"]) + map01])])
    end

    if !!(casefold)
      s = s.fcall('upper')
    end

    l = Pyrb.len.call([s])
    s = s.fcall('rstrip', [Pyrb::Bytes.call(["="])])
    padchars = l - Pyrb.len.call([s])
    decoded = Pyrb::Bytearray.call()
    b32rev = exports['_b32rev']
    (Pyrb.range.call([0, Pyrb.len.call([s]), 8])).each do |i|
      quanta = s[i...i + 8]
      acc = 0
      begin
        (quanta).each do |c|
          acc = (acc << 5) + b32rev[c]
        end
      rescue *Pyrb.rezcue(Pyrb::KeyError)
        Pyrb.raize(binascii.fcall('Error', ["Non-base32 digit found"]), from: nil)
      end

      decoded += acc.fcall('to_bytes', [5, "big"])
    end
    if !!((l % 8).or { !(padchars).in?(Pyrb::Set.new([[0, 1, 3, 4, 6]])) })
      Pyrb.raize(binascii.fcall('Error', ["Incorrect padding"]))
    end

    if !!(padchars.and { decoded })
      acc <<= (5 * padchars)
      last = acc.fcall('to_bytes', [5, "big"])
      leftover = Pyrb.int_divide((43 - (5 * padchars)), 8)
      decoded[-5..-1] = last[0...leftover]
    end

    throw(:return, Pyrb::Bytes.call([decoded]))
  end

  exports['b16encode'] = Pyrb.defn(s: nil) do |s|
    <<~__DOC__
    Encode the bytes-like object s using Base16 and return a bytes object.
    __DOC__

    throw(:return, binascii.fcall('hexlify', [s]).fcall('upper'))
  end

  exports['b16decode'] = Pyrb.defn(s: nil, casefold: { default: false }) do |s, casefold|
    <<~__DOC__
    Decode the Base16 encoded bytes-like object or ASCII string s.

    Optional casefold is a flag specifying whether a lowercase alphabet is
    acceptable as input.  For security purposes, the default is False.

    The result is returned as a bytes object.  A binascii.Error is raised if
    s is incorrectly padded or if there are non-alphabet characters present
    in the input.
    __DOC__

    s = exports['_bytes_from_decode_data'].call([s])
    if !!(casefold)
      s = s.fcall('upper')
    end

    if !!(re.fcall('search', [Pyrb::Bytes.call(["[^0-9A-F]"]), s]))
      Pyrb.raize(binascii.fcall('Error', ["Non-base16 digit found"]))
    end

    throw(:return, binascii.fcall('unhexlify', [s]))
  end

  exports['_a85chars'] = nil
  exports['_a85chars2'] = nil
  exports['_A85START'] = Pyrb::Bytes.call(["<~"])
  exports['_A85END'] = Pyrb::Bytes.call(["~>"])
  exports['_85encode'] = Pyrb.defn(b: nil, chars: nil, chars2: nil, pad: { default: false }, foldnuls: { default: false }, foldspaces: { default: false }) do |b, chars, chars2, pad, foldnuls, foldspaces|
    if !!((Pyrb.isinstance.call([b, exports['bytes_types']])).not)
      b = Pyrb::Memoryview.call([b]).fcall('tobytes')
    end

    padding = ((-Pyrb.len.call([b])) % 4)
    if !!(padding)
      b = b + (Pyrb::Bytes.call(["\0"]) * padding)
    end

    words = struct.fcall('Struct', [("!%dI" % (Pyrb.int_divide(Pyrb.len.call([b]), 4)))]).fcall('unpack', [b])
    chunks = words.fcall('__iter__').map do |word|
      !!(foldnuls.and { (word).not }) ? Pyrb::Bytes.call(["z"]) : !!(foldspaces.and { word == 0x20202020 }) ? Pyrb::Bytes.call(["y"]) : (chars2[Pyrb.int_divide(word, 614125)] + chars2[Pyrb.int_divide(word, 85) % 7225] + chars[(word % 85)])
    end

    if !!(padding.and { (pad).not })
      if !!(chunks[-1] == Pyrb::Bytes.call(["z"]))
        chunks[-1] = (chars[0] * 5)
      end

      chunks[-1] = chunks[-1][0...-padding]
    end

    throw(:return, Pyrb::Bytes.call([""]).fcall('join', [chunks]))
  end

  exports['a85encode'] = Pyrb.defn(b: nil, _: { splat: true }, foldspaces: { default: false }, wrapcol: { default: 0 }, pad: { default: false }, adobe: { default: false }) do |b, _, foldspaces, wrapcol, pad, adobe|
    <<~__DOC__
    Encode bytes-like object b using Ascii85 and return a bytes object.

    foldspaces is an optional flag that uses the special short sequence 'y'
    instead of 4 consecutive spaces (ASCII 0x20) as supported by 'btoa'. This
    feature is not supported by the "standard" Adobe encoding.

    wrapcol controls whether the output should have newline (b'\\n') characters
    added to it. If this is non-zero, each output line will be at most this
    many characters long.

    pad controls whether the input is padded to a multiple of 4 before
    encoding. Note that the btoa implementation always pads.

    adobe controls whether the encoded byte sequence is framed with <~ and ~>,
    which is used by the Adobe implementation.
    __DOC__

    if !!(exports['_a85chars'].object_id == nil.object_id)
      exports['_a85chars'] = Pyrb.range.call([33, 118]).fcall('__iter__').map do |i|
        Pyrb::Bytes.call([[i, ]])
      end

      exports['_a85chars2'] = exports['_a85chars'].fcall('__iter__').flat_map do |a|
        exports['_a85chars'].fcall('__iter__').map do |b|
          (a + b)
        end
      end

    end

    result = exports['_85encode'].call([b, exports['_a85chars'], exports['_a85chars2'], pad, true, foldspaces])
    if !!(adobe)
      result = exports['_A85START'] + result
    end

    if !!(wrapcol)
      wrapcol = Pyrb.max.call([!!(adobe) ? 2 : 1, wrapcol])
      chunks = Pyrb.range.call([0, Pyrb.len.call([result]), wrapcol]).fcall('__iter__').map do |i|
        result[i...i + wrapcol]
      end

      if !!(adobe)
        if !!(Pyrb.len.call([chunks[-1]]) + 2 > wrapcol)
          chunks.fcall('append', [Pyrb::Bytes.call([""])])
        end

      end

      result = Pyrb::Bytes.call(["\n"]).fcall('join', [chunks])
    end

    if !!(adobe)
      result += exports['_A85END']
    end

    throw(:return, result)
  end

  exports['a85decode'] = Pyrb.defn(b: nil, _: { splat: true }, foldspaces: { default: false }, adobe: { default: false }, ignorechars: { default: Pyrb::Bytes.call([" \t\n\r\v"]) }) do |b, _, foldspaces, adobe, ignorechars|
    acc = nil

    <<~__DOC__
    Decode the Ascii85 encoded bytes-like object or ASCII string b.

    foldspaces is a flag that specifies whether the 'y' short sequence should be
    accepted as shorthand for 4 consecutive spaces (ASCII 0x20). This feature is
    not supported by the "standard" Adobe encoding.

    adobe controls whether the input sequence is in Adobe Ascii85 format (i.e.
    is framed with <~ and ~>).

    ignorechars should be a byte string containing characters to ignore from the
    input. This should only contain whitespace characters, and by default
    contains all whitespace characters in ASCII.

    The result is returned as a bytes object.
    __DOC__

    b = exports['_bytes_from_decode_data'].call([b])
    if !!(adobe)
      if !!((b.fcall('endswith', [exports['_A85END']])).not)
        Pyrb.raize(Pyrb::ValueError.call([("Ascii85 encoded byte sequences must end " + "with {!r}").fcall('format', [exports['_A85END']])]))
      end

      if !!(b.fcall('startswith', [exports['_A85START']]))
        b = b[2...-2]
      else
        b = b[0...-2]
      end

    end

    packI = struct.fcall('Struct', ["!I"]).attr('pack')
    decoded = []
    decoded_append = decoded.attr('append')
    curr = []
    curr_append = curr.attr('append')
    curr_clear = curr.attr('clear')
    (b + (Pyrb::Bytes.call(["u"]) * 4)).each do |x|
      if !!(Pyrb::Bytes.call(["!"])[0] <= x && x <= Pyrb::Bytes.call(["u"])[0])
        curr_append.call([x])
        if !!(Pyrb.len.call([curr]) == 5)
          acc = 0
          (curr).each do |x2|
            acc = (85 * acc) + (x2 - 33)
          end
          begin
            decoded_append.call([packI.call([acc])])
          rescue *Pyrb.rezcue(struct.attr('error'))
            Pyrb.raize(Pyrb::ValueError.call(["Ascii85 overflow"]), from: nil)
          end

          curr_clear.call()
        end

      elsif !!(x == Pyrb::Bytes.call(["z"])[0])
        if !!(curr)
          Pyrb.raize(Pyrb::ValueError.call(["z inside Ascii85 5-tuple"]))
        end

        decoded_append.call([Pyrb::Bytes.call(["\0\0\0\0"])])
      elsif !!(foldspaces.and { x == Pyrb::Bytes.call(["y"])[0] })
        if !!(curr)
          Pyrb.raize(Pyrb::ValueError.call(["y inside Ascii85 5-tuple"]))
        end

        decoded_append.call([Pyrb::Bytes.call([String.new([0x20, 0x20, 0x20, 0x20].pack('C*'), encoding: 'ASCII-8BIT')])])
      elsif !!((x).in?(ignorechars))
        next
      else
        Pyrb.raize(Pyrb::ValueError.call([("Non-Ascii85 digit found: %c" % x)]))
      end
    end
    result = Pyrb::Bytes.call([""]).fcall('join', [decoded])
    padding = 4 - Pyrb.len.call([curr])
    if !!(padding)
      result = result[0...-padding]
    end

    throw(:return, result)
  end

  exports['_b85alphabet'] = ((Pyrb::Bytes.call(["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"]) + Pyrb::Bytes.call(["abcdefghijklmnopqrstuvwxyz!\#$%&()*+-;<=>?@^_`{|}~"])))
  exports['_b85chars'] = nil
  exports['_b85chars2'] = nil
  exports['_b85dec'] = nil
  exports['b85encode'] = Pyrb.defn(b: nil, pad: { default: false }) do |b, pad|
    <<~__DOC__
    Encode bytes-like object b in base85 format and return a bytes object.

    If pad is true, the input is padded with b'\\0' so its length is a multiple of
    4 bytes before encoding.
    __DOC__

    if !!(exports['_b85chars'].object_id == nil.object_id)
      exports['_b85chars'] = exports['_b85alphabet'].fcall('__iter__').map do |i|
        Pyrb::Bytes.call([[i, ]])
      end

      exports['_b85chars2'] = exports['_b85chars'].fcall('__iter__').flat_map do |a|
        exports['_b85chars'].fcall('__iter__').map do |b|
          (a + b)
        end
      end

    end

    throw(:return, exports['_85encode'].call([b, exports['_b85chars'], exports['_b85chars2'], pad]))
  end

  exports['b85decode'] = Pyrb.defn(b: nil) do |b|
    acc, chunk = nil

    <<~__DOC__
    Decode the base85-encoded bytes-like object or ASCII string b

    The result is returned as a bytes object.
    __DOC__

    if !!(exports['_b85dec'].object_id == nil.object_id)
      exports['_b85dec'] = ([nil] * 256)
      (Pyrb.enumerate.call([exports['_b85alphabet']])).each do |i, c|
        exports['_b85dec'][c] = i
      end
    end

    b = exports['_bytes_from_decode_data'].call([b])
    padding = ((-Pyrb.len.call([b])) % 5)
    b = b + (Pyrb::Bytes.call(["~"]) * padding)
    out = []
    packI = struct.fcall('Struct', ["!I"]).attr('pack')
    (Pyrb.range.call([0, Pyrb.len.call([b]), 5])).each do |i|
      chunk = b[i...i + 5]
      acc = 0
      begin
        (chunk).each do |c|
          acc = (acc * 85) + exports['_b85dec'][c]
        end
      rescue *Pyrb.rezcue(Pyrb::TypeError)
        (Pyrb.enumerate.call([chunk])).each do |j, c|
          if !!(exports['_b85dec'][c].object_id == nil.object_id)
            Pyrb.raize(Pyrb::ValueError.call([("bad base85 character at position %d" % (i + j))]), from: nil)
          end
        end
        Pyrb.raize
      end

      begin
        out.fcall('append', [packI.call([acc])])
      rescue *Pyrb.rezcue(struct.attr('error'))
        Pyrb.raize(Pyrb::ValueError.call([("base85 overflow in hunk starting at byte %d" % i)]), from: nil)
      end
    end
    result = Pyrb::Bytes.call([""]).fcall('join', [out])
    if !!(padding)
      result = result[0...-padding]
    end

    throw(:return, result)
  end

  exports['MAXLINESIZE'] = 76
  exports['MAXBINSIZE'] = ((Pyrb.int_divide(exports['MAXLINESIZE'], 4)) * 3)
  exports['encode'] = Pyrb.defn(input: nil, output: nil) do |input, output|
    "Encode a file; input and output are binary files."
    while !!(true)
      s = input.fcall('read', [exports['MAXBINSIZE']])
      if !!((s).not)
        break
      end

      while !!(Pyrb.len.call([s]) < exports['MAXBINSIZE'])
        ns = input.fcall('read', [exports['MAXBINSIZE'] - Pyrb.len.call([s])])
        if !!((ns).not)
          break
        end

        s += ns
      end

      line = binascii.fcall('b2a_base64', [s])
      output.fcall('write', [line])
    end
  end

  exports['decode'] = Pyrb.defn(input: nil, output: nil) do |input, output|
    "Decode a file; input and output are binary files."
    while !!(true)
      line = input.fcall('readline')
      if !!((line).not)
        break
      end

      s = binascii.fcall('a2b_base64', [line])
      output.fcall('write', [s])
    end
  end

  exports['_input_type_check'] = Pyrb.defn(s: nil) do |s|
    begin
      m = Pyrb::Memoryview.call([s])
    rescue *Pyrb.rezcue(Pyrb::TypeError) => err
      msg = ("expected bytes-like object, not %s" % s.attr('__class__').attr('__name__'))
      Pyrb.raize(Pyrb::TypeError.call([msg]), from: err)
    end

    if !!(!(m.attr('format')).in?(["c", "b", "B"]))
      msg = (("expected single byte elements, not %r from %s" % [m.attr('format'), s.attr('__class__').attr('__name__')]))
      Pyrb.raize(Pyrb::TypeError.call([msg]))
    end

    if !!(m.attr('ndim') != 1)
      msg = (("expected 1-D data, not %d-D data from %s" % [m.attr('ndim'), s.attr('__class__').attr('__name__')]))
      Pyrb.raize(Pyrb::TypeError.call([msg]))
    end
  end

  exports['encodebytes'] = Pyrb.defn(s: nil) do |s|
    chunk = nil

    <<~__DOC__
    Encode a bytestring into a bytes object containing multiple lines
    of base-64 data.
    __DOC__

    exports['_input_type_check'].call([s])
    pieces = []
    (Pyrb.range.call([0, Pyrb.len.call([s]), exports['MAXBINSIZE']])).each do |i|
      chunk = s[i...i + exports['MAXBINSIZE']]
      pieces.fcall('append', [binascii.fcall('b2a_base64', [chunk])])
    end
    throw(:return, Pyrb::Bytes.call([""]).fcall('join', [pieces]))
  end

  exports['encodestring'] = Pyrb.defn(s: nil) do |s|
    "Legacy alias of encodebytes()."
    warnings, _ = import('warnings')
    warnings.fcall('warn', [("encodestring() is a deprecated alias since 3.1, " + "use encodebytes()"), Pyrb::DeprecationWarning, 2])
    throw(:return, exports['encodebytes'].call([s]))
  end

  exports['decodebytes'] = Pyrb.defn(s: nil) do |s|
    "Decode a bytestring of base-64 data into a bytes object."
    exports['_input_type_check'].call([s])
    throw(:return, binascii.fcall('a2b_base64', [s]))
  end

  exports['decodestring'] = Pyrb.defn(s: nil) do |s|
    "Legacy alias of decodebytes()."
    warnings, _ = import('warnings')
    warnings.fcall('warn', [("decodestring() is a deprecated alias since Python 3.1, " + "use decodebytes()"), Pyrb::DeprecationWarning, 2])
    throw(:return, exports['decodebytes'].call([s]))
  end

  exports['main'] = Pyrb.defn() do ||
    "Small main program"
    sys, getopt = import('sys', 'getopt')
    begin
      opts, args = Pyrb.deconstruct(2, getopt.fcall('getopt', [sys.attr('argv')[1..-1], "deut"]))
    rescue *Pyrb.rezcue(getopt.attr('error')) => msg
      sys.attrs['stdout'] = sys.attr('stderr')
      Pyrb.print.call([msg])
      Pyrb.print.call([(<<~__DOC__ % sys.attr('argv')[0])])
      usage: %s [-d|-e|-u|-t] [file|-]
      -d, -u: decode
      -e: encode (default)
      -t: encode and decode string 'Aladdin:open sesame'
      __DOC__

      sys.fcall('exit', [2])
    end

    func = exports['encode']
    (opts).each do |o, a|
      if !!(o == "-e")
        func = exports['encode']
      end

      if !!(o == "-d")
        func = exports['decode']
      end

      if !!(o == "-u")
        func = exports['decode']
      end

      if !!(o == "-t")
        exports['test'].call()
        throw(:return)
      end
    end
    if !!(args.and { args[0] != "-" })
      f = Pyrb.with.call([Pyrb.open.call([args[0], "rb"])]) do |f|
        func.call([f, sys.attr('stdout').attr('buffer')])
      end
    else
      func.call([sys.attr('stdin').attr('buffer'), sys.attr('stdout').attr('buffer')])
    end
  end

  exports['test'] = Pyrb.defn() do ||
    s0 = Pyrb::Bytes.call(["Aladdin:open sesame"])
    Pyrb.print.call([Pyrb.repr.call([s0])])
    s1 = exports['encodebytes'].call([s0])
    Pyrb.print.call([Pyrb.repr.call([s1])])
    s2 = exports['decodebytes'].call([s1])
    Pyrb.print.call([Pyrb.repr.call([s2])])
    Pyrb.assert(s0 == s2)
  end

  if !!(__name__ == "__main__")
    exports['main'].call()
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
