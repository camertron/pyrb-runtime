module Pyrb
  module Encodings
    autoload :RubyCodec,     'pyrb/encodings/ruby_codec'
    autoload :CodePageCodec, 'pyrb/encodings/code_page_codec'

    class << self
      def find_codec(encoding_name)
        enc_name = encoding_name.gsub('_', '-')
        enc_name = py_aliases.fetch(enc_name, enc_name)
        enc_name = py_to_rb_aliases.fetch(enc_name, enc_name)

        codecs[enc_name] ||= begin
          enc = Encoding.find(enc_name)
          RubyCodec.new(enc_name, enc)
        rescue ArgumentError
          code_page = CodePages[encoding_name] || CodePages[enc_name]

          unless code_page
            raise Pyrb::LookupError.klass, "unknown encoding: #{encoding_name}"
          end

          CodePageCodec.new(enc_name, code_page)
        end
      end

      private

      def codecs
        @codecs ||= {}
      end

      def py_aliases
        @py_aliases ||= YAML.load_file(
          File.expand_path(File.join('encodings', 'py_aliases.yml'), __dir__)
        )
      end

      def py_to_rb_aliases
        @py_to_rb_aliases ||= YAML.load_file(
          File.expand_path(File.join('encodings', 'py_to_rb_aliases.yml'), __dir__)
        )
      end
    end
  end
end
