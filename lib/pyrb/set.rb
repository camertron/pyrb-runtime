module Pyrb
  module Set_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['set'] = PythonClass.new('set', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        enum = args.get(1) || []

        self_.ivars['value'] = Pyrb::Dict.new

        Pyrb::Utils.unwrap_iter(enum).each do |item|
          self_.value[item] = true
        end
      end,

      '__iter__' => -> (args = [], **kwargs) do
        args.get(0).value.fcall('__iter__')
      end,

      '__len__' => -> (args = [], **kwargs) do
        args.get(0).value.fcall('__len__')
      end,

      '__eq__' => -> (args = [], **kwargs) do
        args.get(0).value == args.get(1).value
      end,

      '__ge__' => -> (args = [], **kwargs) do
        args.get(0).fcall('issuperset', [args.get(1)])
      end,

      '__le__' => -> (args = [], **kwargs) do
        args.get(0).fcall('issubset', [args.get(1)])
      end,

      '__sub__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other_set = args.get(1)
        other_set.value.fcall('__iter__').each { |k| self_.value.del(k) }
        self_
      end,

      'issuperset' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other = args.get(1)

        Pyrb.len.call([self_.value]) >= Pyrb.len.call([other]) &&
          other.fcall('__iter__').all? do |k|
            self_.value.include?(k)
          end
      end,

      'issubset' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other = args.get(1)

        Pyrb.len.call([self_.value]) <= Pyrb.len.call([other]) &&
          self_.value.fcall('__iter__').all? do |k|
            other.include?(k)
          end
      end,

      'add' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        item = args.get(1)
        self_.value[item] = true
      end,

      'update' => -> (args = [], **kwargs) do
        self_ = args.shift
        args.each { |other| self_.value.fcall('update', [other.value]) }
      end,

      'union' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other = args.get(1)

        self_.class.prototype.new.tap do |new_set|
          new_set.value.value.merge!(self_.value.value)
          new_set.value.key_hashes.merge!(self_.value.key_hashes)

          Utils.unwrap_iter(other).each do |item|
            new_set.value[item] = true
          end
        end
      end,

      '__contains__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        to_find = args.get(1)

        self_.fcall('keys').each do |key|
          return true if key.fcall('__eq__', [to_find])
        end

        false
      end
    })

    exports['set'].instance_module.class_eval do
      def value
        ivars['value']
      end

      def <<(item)
        fcall('add', [item])
      end

      def |(other_set)
        Pyrb::Set.new.tap do |new_set|
          new_set.fcall('update', [self])
          new_set.fcall('update', [other_set])
        end
      end

      def include?(obj)
        value.include?(obj)
      end
    end

    exports['set'].instance_eval do
      def call(args = [], **kwargs)
        obj = args.get(0)
        Pyrb::Set.new([Pyrb::List.call([obj])])
      end
    end
  end

  Set = Set_rb.exports['set']
end
