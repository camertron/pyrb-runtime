module Pyrb
  module Posixpath
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['exists'] = Pyrb.defn(path: nil) do |path|
      ::File.exist?(path.value)
    end
  end
end
