module Pyrb
  module Zip_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['zip'] = Pyrb::PythonClass.new('zip', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.shift
        enumerables = args

        self_.ivars['iterators'] = enumerables.map do |enum|
          enum.fcall('__iter__')
        end
      end,

      '__iter__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        Enumerator.new do |y|
          loop { y << self_.fcall('__next__') }
        end
      end,

      '__next__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        # this will raise ruby's StopIteration when finished,
        # which coincidentally is also python's name for the same thing
        raise ::StopIteration if self_.ivars['iterators'].empty?
        self_.ivars['iterators'].map { |iter| Pyrb.next_.call([iter]) }
      end
    })

    exports['zip'].klass.class_eval do
      include ::Enumerable

      def each(&block)
        fcall('__iter__').each(&block)
      end
    end

    exports['zip'].instance_eval do
      def call(args = [], **kwargs)
        new(args)
      end
    end
  end

  Zip = Zip_rb.exports['zip']
end
