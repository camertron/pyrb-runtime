module Pyrb
  module Unittest
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['TestCase'] = Pyrb::PythonClass.new('TestCase', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, methodName: { default: 'runTest' }) do |self_, method_name|
        self_.attrs['_testMethodName'] = method_name
      end
    })

    exports['main'] = -> (args = [], **kwargs) do
      puts "Please run unit tests via the rspec runner instead."
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
