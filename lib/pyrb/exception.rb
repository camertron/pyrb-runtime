module Pyrb
  module Exception_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['Exception'] = Pyrb::PythonClass.new('Exception', [Pyrb::Object], {
      '__name__' => 'Exception',
      '__ruby_super__' => ::Exception,

      '__init__' => -> (args = [], **kwargs) do
        args.get(0).attrs['message'] = args.get(1)
      end,

      '__str__' => -> (args = [], **kwargs) do
        args.get(0).attrs['message']
      end,

      'with_traceback' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.set_backtrace(args.get(1))
        self_
      end
    })

    exports['Exception'].instance_module.class_eval do
      def initialize(message = nil, *args)
        # in ruby, `raise Klass, 'msg'` will call `initialize' on Klass but
        # not `new'. As a workaround we have to override initialize here and
        # call __init__ manually
        init
        fcall('__init__', [message, *args])
      end

      def message
        attrs['message']
      end
    end
  end

  Exception = Exception_rb.exports['Exception']
end
