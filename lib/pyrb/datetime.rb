require 'date'

module Pyrb
  module Datetime_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['date'] = Pyrb::PythonClass.new('date', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        year = args.get(1)
        month = args.get(2)
        day = args.get(3)

        self_.ivars['value'] = ::Date.new(year, month, day)
        self_.send(:set_attrs)
      end,

      '__add__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other = args.get(1)

        # other is expected to be a timedelta
        self_.class.wrap(self_.value.next_day(other.attrs['days']))
      end,

      '__eq__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other = args.get(1)

        self_.attrs['day'] == other.attrs['day'] &&
          self_.attrs['month'] == other.attrs['month']
          self_.attrs['year'] == other.attrs['year']
      end,

      'strftime' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        pattern = args.get(1)
        self_.value.strftime(pattern)
      end
    })

    exports['date'].klass.class_eval do
      def self.wrap(obj)
        allocate.tap do |inst|
          inst.init
          inst.ivars['value'] = obj
          inst.send(:set_attrs)
        end
      end

      def value
        ivars['value']
      end

      def is_a?(type)
        type == ::Date || type == self.class
      end

      private

      def set_attrs
        attrs['month'] = value.month
        attrs['day'] = value.day
        attrs['year'] = value.year
      end
    end

    exports['date'].instance_eval do
      def call(args = [], **kwargs)
        new([args.get(0), args.get(1), args.get(2)])
      end
    end

    exports['timedelta'] = Pyrb::PythonClass.new('timedelta', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        microseconds = kwargs.fetch(:microseconds, 0)
        milliseconds = kwargs.fetch(:milliseconds, 0)
        seconds = kwargs.fetch(:seconds, 0)
        minutes = kwargs.fetch(:minutes, 0)
        hours = kwargs.fetch(:hours, 0)
        days = kwargs.fetch(:days, 0)
        weeks = kwargs.fetch(:weeks, 0)

        # only days, seconds and microseconds are stored internally
        ms = milliseconds + (microseconds * 1000)
        secs = seconds + (minutes * 60) + (hours * 3600)
        days = days + (weeks * 7)

        secs += ms / 1_000_000
        ms = ms % 1_000_000

        days += secs / (3600 * 24)
        secs = secs % (3600 * 24)

        self_.attrs['days'] = days
        self_.attrs['seconds'] = secs
        self_.attrs['microseconds'] = ms
      end,

      '__eq__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other = args.get(1)

        self_.attrs['days'] == other.attrs['days'] &&
          self_.attrs['seconds'] == other.attrs['seconds'] &&
          self_.attrs['microseconds'] == other.attrs['microseconds']
      end,

      '__add__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        other = args.get(1)

        exports['timedelta'].new(
          days: self_.attrs['days'] + other.attrs['days'],
          seconds: self_.attrs['seconds'] + other.attrs['seconds'],
          microseconds: self_.attrs['microseconds'] + other.attrs['microseconds']
        )
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Date = Datetime_rb.exports['date']
  TimeDelta = Datetime_rb.exports['timedelta']
end
