module Pyrb
  module Math_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['sqrt'] = -> (args = [], **kwargs) do
      ::Math.sqrt(args.get(0))
    end

    exports['gcd'] = -> (args = [], **kwargs) do
      args.get(0).gcd(args.get(1))
    end

    exports['ldexp'] = -> (args = [], **kwargs) do
      # x * (2^i)
      ::Math.ldexp(args.get(0), args.get(1))
    end

    exports['nan'] = ::Float::NAN

    exports['isnan'] = -> (args = [], **kwargs) do
      args.get(0).nan?
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
