module Pyrb
  class ClassAttrs
    include Enumerable

    def initialize(klass)
      @klass = klass
      @attrs = {}
    end

    def each(&block)
      @attrs.each(&block)
    end

    def del(name)
      @attrs.delete(name)
    end

    alias_method :each_pair, :each

    def hasattr(name)
      @attrs.include?(name) ||
        @klass.properties.include?(name) ||
        @klass.parents.any? { |parent| parent.attrs.include?(name) } ||
        name == '__dict__'
    end

    def [](name)
      if @attrs.include?(name)
        return @attrs[name]
      elsif @klass.properties.include?(name)
        return @klass.properties[name]
      end

      # this has to be done manually
      if name == '__dict__'
        return @klass.__dict__
      end

      @klass.parents.each do |parent|
        if parent.attrs.include?(name)
          return parent.attrs[name]
        end
      end

      # I replaced this with the loop above. Might have broken some stuff.
      # @klass.parents.each do |parent|
      #   if parent.attrs.self_include?(name)
      #     return parent.attrs.instance_variable_get(:@attrs)[name]
      #   end
      # end

      raise Pyrb::AttributeError.new(["#{@klass.class_name} object has no attribute #{name}"])
    end

    alias_method :get, :[]

    def []=(name, value)
      @attrs[name] = value
    end

    def self_include?(name)
      @attrs.include?(name)
    end

    def include?(name)
      return true if @attrs.include?(name)
      return true if @klass.properties.include?(name)

      @klass.parents.each do |parent|
        return true if parent.attrs.include?(name)
      end

      false
    end

    def to_h
      @attrs
    end
  end
end
