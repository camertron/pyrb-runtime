module Pyrb
  module Array_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    TYPECODES = 'bBhHiIlLqQfd'.freeze

    DATA_TYPES = {
      'b' => 'char',
      'B' => 'char',
      'h' => 'short',
      'H' => 'short',
      'i' => 'int',
      'I' => 'int',
      'l' => 'long',
      'L' => 'long',
      'q' => 'long long',
      'Q' => 'long long',
      'f' => 'float',
      'd' => 'double'
    }.freeze

    INVALID_TYPE_ERROR = "array() argument 1 must be a unicode character, not %{type}".freeze
    BAD_TYPECODE_ERROR = "bad typecode (must be #{TYPECODES[0..-2].chars.join(', ')}, or #{TYPECODES[-1]})".freeze

    struct, _ = import('struct')

    exports['array'] = PythonClass.new('array', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, typecode: nil, initializer: { default: nil }) do |self_, typecode, initializer|
        unless typecode.value.is_a?(::String)
          raise Pyrb::TypeError.klass, INVALID_TYPE_ERROR % { type: Pyrb.type.call([typecode]).attr('__name__') }
        end

        if !TYPECODES.include?(typecode)
          raise Pyrb::ValueError.klass, BAD_TYPECODE_ERROR
        end

        data = ''.b
        self_.ivars['buffer'] = Pyrb::Buffer.new(data, data, fmt: typecode)
        self_.ivars['metadata'] = struct.compile(typecode).patterns.first
        self_.ivars['typecode'] = typecode

        if initializer
          self_.fcall('extend', [initializer])
        end
      end,

      '__len__' => Pyrb.defn(self: nil) do |self_|
        self_.getbuffer.length
      end,

      '__getitem__' => Pyrb.defn(self: nil, idx: nil) do |self_, idx|
        self_.getbuffer[idx]
      end,

      'itemsize' => Pyrb.defn(self: nil) do |self_|
        self_.getbuffer.itemsize
      end,

      'extend' => Pyrb.defn(self: nil, iterable: nil) do |self_, iterable|
        iterable.each do |val|
          self_.fcall('append', [val])
        end
      end,

      'append' => Pyrb.defn(self: nil, x: nil) do |self_, x|
        types_agree =
          (self_.metadata.type == :integer && x.is_a?(::Integer)) ||
          (self_.metadata.type == :float && x.is_a?(::Float))

        unless types_agree
          raise Pyrb::TypeError.klass,
            "#{self_.metadata.type} argument expected, got #{Pyrb.type.call([x]).attr('__name__')}"
        end

        if x < self_.metadata.min_value
          raise Pyrb::OverflowError.klass,
            "#{self_.metadata.signed? ? 'signed' : 'unsigned'} #{self_.type_name} is less than minimum"
        end

        if x > self_.metadata.max_value
          raise Pyrb::OverflowError.klass,
            "#{self_.metadata.signed? ? 'signed' : 'unsigned'} #{self_.type_name} is greater than maximum"
        end

        self_.getbuffer.data << struct.fcall('pack', [self_.typecode, x]).value
        self_.getbuffer.length += 1
      end,

      'tobytes' => Pyrb.defn(self: nil) do |self_|
        Pyrb::Bytes.call([self_.getbuffer.data])
      end
    })

    exports['array'].instance_module.class_eval do
      def metadata
        ivars['metadata']
      end

      def getbuffer
        ivars['buffer']
      end

      def write(data, offset)
        getbuffer.data[offset..(offset + data.length)] = data
      end

      def typecode
        ivars['typecode']
      end

      def type_name
        Array_rb::DATA_TYPES[typecode]
      end

      def to_contiguous
        getbuffer.to_contiguous
      end
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Array = Array_rb.exports['array']
end
