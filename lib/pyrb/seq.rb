module Pyrb
  module Seq_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['seq'] = PythonClass.new('seq', [Pyrb::Object], {
      '__iter__' => -> (args = [], **kwargs) do
        args.get(0)
      end,

      '__reversed__' => -> (args = [], **kwargs) do
        args.get(0).reverse_each
      end
    })
  end

  Seq = Seq_rb.exports['seq']
end
