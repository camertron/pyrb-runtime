module Pyrb
  module Filter_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['filter'] = PythonClass.new('filter', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.ivars['callable'] = args.get(1)
        self_.ivars['enumerable'] = args.get(2)
      end,

      '__iter__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        Enumerator.new do |y|
          nx = self_.attrs['__next__']
          loop { y << nx.call }
        end
      end,

      '__next__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        loop do
          val = self_.ivars['enumerable'].next

          if self_.ivars['callable'].nil?
            # "identity" function, i.e. return val if it isn't falsey
            return val if !!val
          elsif !!(self_.ivars['callable'].call([val]))
            return val
          end
        end

        raise ::StopIteration
      end
    })

    exports['filter'].klass.class_eval do
      include ::Enumerable

      def each(&block)
        fcall('__iter__').each(&block)
      end
    end

    exports['filter'].instance_eval do
      def call(args = [], **kwargs)
        callable = args.get(0)
        enumerable = args.get(1)
        new([callable, Utils.unwrap_iter(enumerable)])
      end
    end
  end

  Filter = Filter_rb.exports['filter']
end
