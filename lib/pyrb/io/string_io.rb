module Pyrb
  module StringIO_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    TextIOBase, _ = import('TextIOBase', from: '.text_io_base')

    exports['StringIO'] = Pyrb::PythonClass.new('StringIO', [TextIOBase], {
      '__init__' => Pyrb.defn(self: nil, initial_value: { default: '' }, newline: { default: "\n" }) do |self_, initial_value, newline|
        self_.ivars['io'] = ::StringIO.new(initial_value.value)
        self_.ivars['newline'] = newline
      end,

      'read' => Pyrb.defn(self: nil, size: nil) do |self_, size|
        self_.ivars['io'].read(size) || ''
      end,

      'write' => Pyrb.defn(self: nil, b: nil) do |self_, b|
        case b
          when ::String, ::Pyrb::String.klass
            self_.ivars['io'].write(b.value)
          else
            raise Pyrb::TypeError.klass, "string argument expected, not #{b.attr('__name__')}"
        end
      end,

      'getvalue' => Pyrb.defn(self: nil) do |self_|
        self_.ivars['io'].string
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  StringIO = StringIO_rb.exports['StringIO']
end
