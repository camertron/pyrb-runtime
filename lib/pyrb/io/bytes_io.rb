module Pyrb
  module BytesIO_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    TextIOBase, _ = import('TextIOBase', from: '.text_io_base')

    exports['BytesIO'] = Pyrb::PythonClass.new('BytesIO', [TextIOBase], {
      '__init__' => Pyrb.defn(self: nil, initial_value: { default: false }, newline: { default: false }) do |self_, initial_value, newline|
        initial_value ||= ''
        newline ||= "\n"
        self_.ivars['io'] = ::StringIO.new(initial_value.value).binmode
        self_.ivars['newline'] = newline
      end,

      'read' => Pyrb.defn(self: nil, size: nil) do |self_, size|
        Pyrb::Bytes.call([self_.ivars['io'].read(size) || ''])
      end,

      'readline' => Pyrb.defn(self: nil, size: { default: -1 }) do |self_, size|
        str = self_.ivars['io'].string
        newline_idx = str.index(self_.ivars['newline'], self_.ivars['io'].pos) || str.size
        size = str.size if size == -1
        len = size < newline_idx ? size : newline_idx
        Pyrb::Bytes.call([self_.ivars['io'].read(len) || ''])
      end,

      'write' => Pyrb.defn(self: nil, b: nil) do |self_, b|
        case b
          when ::Pyrb::Bytes.klass
            self_.ivars['io'].write(b.value)
          else
            raise Pyrb::TypeError.klass, "a bytes-like object is required, not #{b.attr('__name__')}"
        end
      end,

      'getvalue' => Pyrb.defn(self: nil) do |self_|
        self_.ivars['io'].string
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  BytesIO = BytesIO_rb.exports['BytesIO']
end
