module Pyrb
  module BufferedRandom_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    # using these ruby constants is totally cheating, but I haven't
    # figured out how we're going to support circular dependencies yet :(
    exports['BufferedRandom'] = Pyrb::PythonClass.new('BufferedRandom', [Pyrb::BufferedReader, Pyrb::BufferedWriter], {
      # BufferedRandom supports random-access methods like seek() and tell()
      # which haven't been implemented yet.
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  BufferedRandom = BufferedRandom_rb.exports['BufferedRandom']
end
