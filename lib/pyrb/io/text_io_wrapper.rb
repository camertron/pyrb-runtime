module Pyrb
  module TextIOWrapper_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    # this thing is woefully incomplete
    exports['TextIOWrapper'] = Pyrb::PythonClass.new('TextIOWrapper', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, buffer: nil) do |self_, buffer|
        self_.attrs['buffer'] = buffer
      end,

      '__enter__' => Pyrb.defn(self: nil) do |self_|
      end,

      '__exit__' => Pyrb.defn(self: nil) do |self_|
        self_.attrs['buffer'].fcall('close')
      end,

      'write' => Pyrb.defn(self: nil, text: nil) do |self_, text|
        self_.attrs['buffer'].fcall('write', [text])
      end,

      'read' => Pyrb.defn(self: nil, length: nil) do |self_, length|
        self_.attrs['buffer'].fcall('read', [length])
      end,

      'readline' => Pyrb.defn(self: nil, size: { default: -1 }) do |self_, size|
        self_.attrs['buffer'].fcall('readline', [size])
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  TextIOWrapper = TextIOWrapper_rb.exports['TextIOWrapper']
end
