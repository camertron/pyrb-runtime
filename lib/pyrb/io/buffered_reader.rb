module Pyrb
  module BufferedReader_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    io, _ = import('io')

    exports['BufferedReader'] = Pyrb::PythonClass.new('BufferedReader', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, raw: nil, buffer_size: { default: io.attr('DEFAULT_BUFFER_SIZE') }) do |self_, raw, buffer_size|
        self_.ivars['io'] = raw
        self_.attrs['buffer_size'] = buffer_size
      end,

      'read' => Pyrb.defn(self: nil, length: nil) do |self_, length|
        Pyrb::Bytes.new([self_.ivars['io'].read(length)])
      end,

      'readline' => Pyrb.defn(self: nil, size: { default: -1 }) do |self_, size|
        Pyrb::Bytes.new([self_.ivars['io'].readline(size)])
      rescue ::EOFError
      end,

      'close' => Pyrb.defn(self: nil) do |self_|
        self_.ivars['io'].close
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  BufferedReader = BufferedReader_rb.exports['BufferedReader']
end
