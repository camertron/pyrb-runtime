module Pyrb
  module TextIOBase_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['TextIOBase'] = Pyrb::PythonClass.new('TextIOBase', [Pyrb::Object])

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  TextIOBase = TextIOBase_rb.exports['TextIOBase']
end
