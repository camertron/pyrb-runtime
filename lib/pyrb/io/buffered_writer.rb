module Pyrb
  module BufferedWriter_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    io, _ = import('io')

    exports['BufferedWriter'] = Pyrb::PythonClass.new('BufferedWriter', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, raw: nil, buffer_size: { default: io.attr('DEFAULT_BUFFER_SIZE') }) do |self_, raw, buffer_size|
        self_.ivars['io'] = raw
        self_.attrs['buffer_size'] = buffer_size
      end,

      'write' => Pyrb.defn(self: nil, b: nil) do |self_, b|
        self_.ivars['io'].write(b.value)
      end,

      'close' => Pyrb.defn(self: nil) do |self_|
        self_.ivars['io'].close
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  BufferedWriter = BufferedWriter_rb.exports['BufferedWriter']
end
