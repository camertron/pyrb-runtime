module Pyrb
  module IO_init_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    import 'BufferedReader', from: '.buffered_reader'
    import 'BufferedWriter', from: '.buffered_writer'
    import 'BufferedRandom', from: '.buffered_random'
    import 'TextIOBase', from: '.text_io_base'
    import 'TextIOWrapper', from: '.text_io_wrapper'
    import 'BytesIO', from: '.bytes_io'
    import 'StringIO', from: '.string_io'

    exports['DEFAULT_BUFFER_SIZE'] = 8192

    exports['__all__'] = %w(
      BufferedReader BufferedWriter BufferedRandom TextIOBase TextIOWrapper BytesIO StringIO
    )

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
