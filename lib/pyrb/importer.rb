require 'thread'

module Pyrb
  class Importer
    class << self
      def resolve_module_path(module_path, origin)
        components = module_path.split(/(\.+)/).reject(&:empty?)
        real_path, sys_path = expand_path(components.first, origin)
        return real_path if File.file?(real_path || '')
      end

      def import(module_path, origin)
        components = module_path.split(/(\.+)/).reject(&:empty?)
        real_path, sys_path = expand_path(components.first, origin)

        current = if File.directory?(real_path)
          load_package(real_path, File.basename(real_path))
        elsif File.file?(real_path)
          real_module_path = real_path[(sys_path.size + 1)..-1]
            .chomp('.rb')
            .gsub(File::SEPARATOR, '.')

          module_stack.push(real_module_path)
          load_module(real_path).tap { module_stack.pop }
        else
          raise Pyrb::ModuleNotFoundError.klass, "No module named '#{current}'"
        end

        components.shift

        components.each do |component|
          next if component == '.'
          current = current.attrs[component]
        end

        return current if components.empty?

        if current.is_a?(Pyrb::PackageAttrs)
          current.attrs[components.last] || current
        elsif current.respond_to?(:exports)
          current.exports[components.last] || current
        else
          current
        end
      end

      private

      def module_stack
        Pyrb::Runtime.module_stack
      end

      def expand_path(package_or_module, origin_file)
        if package_or_module.start_with?('.')
          find_relative_to_origin(package_or_module, origin_file)
        else
          find_in_sys_path(package_or_module)
        end
      end

      def find_relative_to_origin(package_or_module, origin_file)
        idx = package_or_module.index(/\A[^\.]/) || package_or_module.length
        path = File.join(*(['..'] * (idx - 1)))
        File.expand_path("./#{path[idx..-1]}", File.dirname(origin_file))
      end

      def find_in_sys_path(package_or_module)
        real_path = nil
        found_sys_path = nil

        Pyrb::Sys.exports['path'].each do |sys_path|
          sys_path = './' if sys_path == ''
          path = File.join(sys_path, package_or_module)

          if File.exist?("#{path}.rb")
            real_path = "#{path}.rb"
            found_sys_path = sys_path
            break
          elsif File.exist?(path)
            real_path = path
            found_sys_path = sys_path
            break
          end
        end

        if real_path
          [File.expand_path(real_path, Dir.getwd), found_sys_path]
        end
      end

      def load_module(path)
        require path
        Pyrb::Sys.exports['modules'][path]
      end

      def load_package(path, name)
        PythonPackage.new(path, name)
      end
    end
  end
end
