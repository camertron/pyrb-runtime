module Pyrb
  class Property_rb
    extend Pyrb::Runtime
    using Pyrb::Runtime

    def self.location
      __FILE__
    end

    exports['property'] = PythonClass.new('property', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self_: nil, getter: nil) do |self_, getter|
        self_.getter = getter
      end
    })

    exports['property'].klass.class_eval do
      attr_accessor :getter, :setter

      def get(args)
        getter.call(args)
      end

      def set(args)
        setter.call(args)
      end
    end
  end

  Property = Property_rb.exports['property']
end
