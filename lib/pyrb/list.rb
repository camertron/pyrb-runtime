module Pyrb
  module List_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['list'] = Pyrb::PythonClass.new('list', [Pyrb::Object], {
      '__name__' => 'list',

      '__len__' => -> (args = [], **kwargs) do
        args.get(0).value.size
      end,

      'append' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        elem = args.get(1)
        self_ << elem
      end,

      'extend' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        arr = args.get(1)
        self_.concat(arr)
      end,

      'insert' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        obj = args.get(1)
        idx = args.get(2)
        self_.insert(obj, idx)
      end,

      'remove' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        elem = args.get(1)
        self_.delete(elem)
      end,

      'clear' => -> (args = [], **kwargs) do
        args.get(0).value.clear
      end,

      '__eq__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1).value
        return false if self_.size != Pyrb.len.call([other])
        self_.each_with_index.all? { |e, i| e == other[i] }
      end,

      '__ne__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1).value
        return true if self_.size != Pyrb.len.call([other])
        self_.each_with_index.any? { |e, i| e != other[i] }
      end,

      '__lt__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1)

        self_.each_with_index do |e, idx|
          next if self_[idx].fcall('__eq__', [other[idx]])
          next unless Pyrb.hasattr.call([self_[idx], '__lt__'])
          return self_[idx].fcall('__lt__', [other[idx]])
        end

        false
      end,

      '__le__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1)

        self_.each_with_index do |e, idx|
          next if self_[idx].fcall('__eq__', [other[idx]])
          next unless Pyrb.hasattr.call([self_[idx], '__le__'])
          return self_[idx].fcall('__le__', [other[idx]])
        end

        false
      end,

      '__gt__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1)

        self_.each_with_index do |e, idx|
          next if self_[idx].fcall('__eq__', [other[idx]])
          next unless Pyrb.hasattr.call([self_[idx], '__gt__'])
          return self_[idx].fcall('__gt__', [other[idx]])
        end

        false
      end,

      '__ge__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        other = args.get(1)

        self_.each_with_index do |e, idx|
          next if self_[idx].fcall('__eq__', [other[idx]])
          next unless Pyrb.hasattr.call([self_[idx], '__ge__'])
          return self_[idx].fcall('__ge__', [other[idx]])
        end

        false
      end,

      '__str__' => -> (args = [], **kwargs) do
        args.get(0).value.inspect
      end,

      '__add__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        elem = args.get(1)
        self_ + elem
      end,

      '__iter__' => -> (args = [], **kwargs) do
        Pyrb::ListIterator.new([args.get(0).value.each])
      end,

      'push' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        val = args.get(1)
        self_.push(val)
      end,

      'pop' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        idx = args.get(1) || -1
        raise Pyrb::IndexError.klass, 'pop from an empty list' if self_.empty?
        self_.delete_at(idx)
      end,

      'index' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        to_find = args.get(1)
        self_.index(to_find) || raise(Pyrb::ValueError.klass, "#{to_find} is not in list")
      end,

      'count' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        to_find = args.get(1)
        self_.count { |elem| elem == to_find }
      end,

      '__reversed__' => -> (args = [], **kwargs) do
        args.get(0).value.reverse_each
      end,

      'sort' => -> (args = [], **kwargs) do
        reverse = kwargs.fetch(:reverse, args.get(2))

        if reverse
          args.get(0).value.sort! { |a, b| b <=> a }
        else
          args.get(0).value.sort! { |a, b| a <=> b }
        end
      end,

      '__getitem__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        idx = args.get(1)
        raise IndexError.klass, 'list index out of range' if idx >= self_.size
        self_.get(idx)
      end,

      '__contains__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        to_find = args.get(1)

        self_.each do |item|
          return true if item.fcall('__eq__', [to_find])
        end

        false
      end
    })

    exports['list'].instance_module.class_eval do
      def value
        @value ||= []
      end
    end

    exports['list'].instance_eval do
      def call(args = [], **kwargs)
        obj = args.get(0)

        case obj
          when NilClass
            return []
          when ::Enumerable
            return obj.to_a
          when ::String
            return obj.chars
        end

        iter = obj.fcall('__iter__')

        [].tap do |arr|
          # loop implicitly rescues StopIteration
          loop { arr << Pyrb.next_.call([iter]) }
        end
      end
    end
  end

  List = List_rb.exports['list']
end
