require 'fiddle'

module Pyrb
  module Struct_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    PATTERN_RE = /([@=<>!]?)(\d+)?([\w?])/

    DEFAULT_ORDER_CHAR = '@'

    BYTE_ORDER_CHARS = {
      '@' => Pyrb::Sys.attr('byteorder') == 'big' ? :be : :le,
      '=' => Pyrb::Sys.attr('byteorder') == 'big' ? :be : :le,
      '<' => :le,
      '>' => :be,
      '!' => :be
    }.freeze

    ALIGNMENT_CHARS = {
      '@' => :native,
      '=' => :none,
      '<' => :none,
      '>' => :none,
      '!' => :none
    }.freeze

    SIZE_CHARS = {
      '@' => :native_size,
      '=' => :standard_size,
      '<' => :standard_size,
      '>' => :standard_size,
      '!' => :standard_size
    }.freeze

    # type => unit width => byte order => sign
    FORMAT_MATRIX = {
      integer: {
        # single char/byte
        1 => {
          native: {
            signed: 'c',
            unsigned: 'C'
          },

          be: {
            signed: 'c',
            unsigned: 'C'
          },

          le: {
            signed: 'c',
            unsigned: 'C'
          }
        },

        # short
        2 => {
          native: {
            signed: 's!',
            unsigned: 'S!'
          },

          be: {
            signed: 's!>',
            unsigned: 'S!>'
          },

          le: {
            signed: 's!<',
            unsigned: 'S!<'
          }
        },

        # int/long
        4 => {
          native: {
            signed: 'i!',
            unsigned: 'I!'
          },

          be: {
            signed: 'i!>',
            unsigned: 'I!>'
          },

          le: {
            signed: 'i!<',
            unsigned: 'I!<'
          }
        },

        # long/long_long
        8 => {
          native: {
            signed: 'l!',
            unsigned: 'L!'
          },

          be: {
            signed: 'l!>',
            unsigned: 'L!>'
          },

          le: {
            signed: 'l!<',
            unsigned: 'L!<'
          }
        }
      },

      float: {
        # IEEE half-precision float, we pack it like a short
        2 => {
          native: {
            signed: 's!',
            unsigned: 'S!'
          },

          be: {
            signed: 's!>',
            unsigned: 'S!>'
          },

          le: {
            signed: 's!<',
            unsigned: 'S!<'
          }
        },

        4 => {
          native: {
            signed: 'F',
            unsigned: 'F'
          },

          be: {
            signed: 'g',
            unsigned: 'g'
          },

          le: {
            signed: 'e',
            unsigned: 'e'
          }
        }
      },

      double: {
        8 => {
          native: {
            signed: 'D',
            unsigned: 'D'
          },

          be: {
            signed: 'G',
            unsigned: 'G',
          },

          le: {
            signed: 'E',
            unsigned: 'E'
          }
        }
      },

      string: 'a',
      pascal_string: 'Ca',
      char: 'a',
      bool: 'C',
      null: 'C'
    }.freeze

    PYTHON_FORMAT_CHARS = {
      'x' => { type: :null, standard_size: 1, native_size: 1, valcount: 0 },
      'c' => { type: :char, standard_size: 1, native_size: 1 },
      '?' => { type: :bool, standard_size: 1, native_size: 1 },

      'b' => {
        type: :integer,
        signed: true,
        standard_size: 1,
        native_size: 1
      },

      'B' => {
        type: :integer,
        signed: false,
        standard_size: 1,
        native_size: 1
      },

      'h' => {
        type: :integer,
        signed: true,
        standard_size: 2,
        native_size: Fiddle::SIZEOF_SHORT
      },

      'H' => {
        type: :integer,
        signed: false,
        standard_size: 2,
        native_size: Fiddle::SIZEOF_SHORT
      },

      'i' => {
        type: :integer,
        signed: true,
        standard_size: 4,
        native_size: Fiddle::SIZEOF_INT
      },

      'I' => {
        type: :integer,
        signed: false,
        standard_size: 4,
        native_size: Fiddle::SIZEOF_INT
      },

      'l' => {
        type: :integer,
        signed: true,
        standard_size: 4,
        native_size: Fiddle::SIZEOF_LONG
      },

      'L' => {
        type: :integer,
        signed: false,
        standard_size: 4,
        native_size: Fiddle::SIZEOF_LONG
      },

      'q' => {
        type: :integer,
        signed: true,
        standard_size: 8,
        native_size: Fiddle::SIZEOF_LONG_LONG
      },

      'Q' => {
        type: :integer,
        signed: false,
        standard_size: 8,
        native_size: Fiddle::SIZEOF_LONG_LONG
      },

      'f' => {
        type: :float,
        signed: true,
        standard_size: 4,
        native_size: Fiddle::SIZEOF_FLOAT,
        min_value: ((1 << 24) - 1) * 2**104 * -1,
        max_value: ((1 << 24) - 1) * 2**104
      },

      'e' => {
        type: :float,
        signed: true,
        standard_size: 2,
        native_size: 2,
        min_value: -65519,
        max_value: 65519
      },

      'd' => {
        type: :double,
        signed: true,
        standard_size: 8,
        native_size: Fiddle::SIZEOF_DOUBLE
      },

      's' => {
        type: :string,
        valcount: 1,
        standard_size: :count,
        native_size: :count
      },

      'p' => {
        type: :pascal_string,
        valcount: 1,
        standard_size: :count,
        native_size: :count
      },

      'n' => {
        type: :integer,
        signed: true,
        native_size: Fiddle::SIZEOF_SSIZE_T
      },

      'N' => {
        type: :integer,
        signed: false,
        native_size: Fiddle::SIZEOF_SIZE_T
      },

      'P' => {
        type: :integer,
        signed: true,
        native_size: Fiddle::SIZEOF_VOIDP
      }
    }.freeze

    NULL_CHAR = "\0".b.freeze
    NULL_BYTE_ARRAY = [0].freeze

    def self.compile(fmt)
      BinFormat.compile(fmt)
    end

    # generic struct error
    exports['error'] = Pyrb::PythonClass.new('error', [Pyrb::Exception], {
      '__ruby_super__' => ::StandardError
    })

    # error that means Pyrb doesn't support some struct things it probably should
    exports['UnsupportedFormatError'] = Pyrb::PythonClass.new('UnsupportedFormatError', [Pyrb::Exception])

    unpack_iterator = PythonClass.new('unpack_iterator', [Pyrb::Iterator], {
      '__init__' => Pyrb.defn(self: nil, struct: nil, buffer: nil) do |self_, struct, buffer|
        struct_size = struct.attr('size')

        if struct_size == 0
          raise(
            Pyrb::Struct.exports['error'].klass,
            'cannot iteratively unpack with a struct of length 0'
          )
        end

        if Pyrb.len.call([buffer]) % struct_size > 0
          raise(
            Pyrb::Struct.exports['error'].klass,
            "iterative unpacking requires a buffer of a multiple of #{struct_size} bytes"
          )
        end

        self_.ivars['struct'] = struct
        self_.ivars['buffer'] = buffer
        self_.ivars['pos'] = 0

        self_.ivars['iter'] = Enumerator.new do |y|
          Pyrb.len.call([buffer]).div(struct.attr('size')).times do |i|
            self_.ivars['pos'] += 1
            y << struct.fcall('unpack_from', [buffer, i * struct.attr('size')])
          end
        end
      end,

      '__length_hint__' => Pyrb.defn(self: nil) do |self_|
        size = self_.ivars['struct'].attr('size')
        consumed = self_.ivars['pos'] * size
        (Pyrb.len.call([self_.ivars['buffer']]) - consumed).div(size)
      end
    })

    exports['pack'] = Pyrb.defn(format: nil, values: { splat: true }) do |fmt, values|
      exports['Struct'].call([fmt]).fcall('pack', values)
    end

    exports['unpack'] = Pyrb.defn(format: nil, buffer: nil) do |fmt, buffer|
      exports['Struct'].call([fmt]).fcall('unpack', [buffer])
    end

    exports['unpack_from'] = Pyrb.defn(format: nil, buffer: nil, offset: { default: 0 }) do |fmt, buffer, offset|
      exports['Struct'].call([fmt]).fcall('unpack_from', [buffer, offset])
    end

    exports['iter_unpack'] = Pyrb.defn(format: nil, buffer: nil) do |fmt, buffer|
      exports['Struct'].call([fmt]).fcall('iter_unpack', [buffer])
    end

    exports['pack_into'] = Pyrb.defn(format: nil, buffer: nil, offset: nil, values: { splat: true }) do |fmt, buffer, offset, values|
      exports['Struct'].call([fmt]).fcall('pack_into', [buffer, offset, *values])
    end

    exports['calcsize'] = Pyrb.defn(format: nil) do |fmt|
      exports['Struct'].call([fmt]).attr('size')
    end

    exports['Struct'] = Pyrb::PythonClass.new('Struct', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, format: nil) do |self_, format|
        self_.attrs['format'] = format
        self_.ivars['compiled'] = compile(format.value)
        self_.attrs['size'] = self_.ivars['compiled'].size
      end,

      'pack' => Pyrb.defn(self: nil, values: { splat: true }) do |self_, values|
        self_.ivars['compiled'].pack(values)
      end,

      'pack_into' => Pyrb.defn(self: nil, buffer: nil, offset: nil, values: { splat: true }) do |self_, buffer, offset, values|
        self_.ivars['compiled'].pack_into(buffer, offset, *values)
      end,

      'unpack' => Pyrb.defn(self: nil, buffer: nil) do |self_, buffer|
        self_.ivars['compiled'].unpack(buffer)
      end,

      'unpack_from' => Pyrb.defn(self: nil, buffer: nil, offset: { default: 0 }) do |self_, buffer, offset|
        self_.ivars['compiled'].unpack_from(buffer, offset)
      end,

      'iter_unpack' => Pyrb.defn(self: nil, buffer: nil) do |self_, buffer|
        unpack_iterator.call([self_, buffer])
      end
    })

    class BinFormat
      BYTES_ERROR_MSG = 'char format requires a bytes object of length 1'.freeze
      INTEGER_ERROR_MSG = 'required argument is not an integer'.freeze
      FLOAT_ERROR_MSG = 'required argument is not a float'.freeze
      FLOAT_DOMAIN_ERROR_MSG = 'float too large to pack with %s format'.freeze

      class << self
        def compile(pattern_str)
          new(
            pattern_str.scan(PATTERN_RE).map do |order_chr, count, py_fmt|
              key = "#{order_chr}#{count}#{py_fmt}"
              pattern_cache[key] ||= BinPattern.new(order_chr, count, py_fmt)
            end
          )
        end

        private

        def pattern_cache
          @pattern_cache ||= {}
        end
      end

      attr_reader :patterns

      def initialize(patterns)
        @patterns = patterns

        if size > Pyrb::Sys.attr('maxsize')
          raise(
            Pyrb::Struct.exports['error'].klass,
            'total struct size is too long'
          )
        end
      end

      def size
        @size ||= patterns.inject(0) { |sum, pat| sum + pat.size }
      end

      def valcount
        @valcount ||= patterns.inject(0) { |sum, pat| sum + pat.valcount }
      end

      def count
        @count ||= patterns.inject(0) { |sum, pat| sum + pat.count }
      end

      def pack_str
        @pack_str ||= patterns.each_with_object('') do |pat, ret|
          ret << pat.pack_str
        end
      end

      def pack(values)
        if values.size != valcount
          raise(
            Pyrb::Struct.exports['error'].klass,
            "pack expected #{valcount} items for packing (got #{values.size})"
          )
        end

        value_idx = 0
        ready_values = []

        patterns.each do |pattern|
          value_idx += validate_and_coerce(pattern, values, value_idx, ready_values)
        end

        buffer = ::String.new(capacity: size)
        ready_values.pack(pack_str, buffer: buffer)
        Pyrb::Bytes.call([buffer])
      end

      def pack_into(buffer, offset, *values)
        if values.size != valcount
          raise(
            Pyrb::Struct.exports['error'].klass,
            "pack expected #{valcount} items for packing (got #{values.size})"
          )
        end

        # TODO: what other non-int things can we pass as the offset?
        if offset.nil?
          raise Pyrb::TypeError.klass, "'NoneType' object cannot be interpreted as an integer"
        end

        if Pyrb.len.call([buffer]) - offset < size
          raise(
            Pyrb::Struct.exports['error'].klass,
            "pack_into requires a buffer of at least #{size} bytes"
          )
        end

        value_idx = 0
        ready_values = []

        patterns.each do |pattern|
          ready_values.clear
          value_idx += validate_and_coerce(pattern, values, value_idx, ready_values)
          buffer.write(ready_values.pack(pattern.pack_str), offset)
        end
      end

      def unpack(buffer)
        if Pyrb.len.call([buffer]) != size
          raise(
            Pyrb::Struct.exports['error'].klass,
            "unpack requires a buffer of #{size} bytes"
          )
        end

        data = coerce_buffer(buffer)
        unpack_impl(data, 0)
      end

      def unpack_from(buffer, offset = 0)
        data = coerce_buffer(buffer)

        if (data.length - offset) < size
          raise(
            Pyrb::Struct.exports['error'].klass,
            "unpack_from requires a buffer of at least #{size} bytes"
          )
        end

        unpack_impl(data, offset)
      end

      private

      def unpack_impl(data, offset)
        values = data.unpack("@#{offset}#{pack_str}")
        ready_values = []
        value_idx = 0

        patterns.each do |pattern|
          if pattern.type == :string
            ready_values << values.get(value_idx)
            value_idx += 1
          else
            pattern.count.times do
              val = values.get(value_idx)
              break unless val

              case pattern.type
                when :null
                  # do nothing
                when :bool
                  ready_values << unpack_bool(pattern, val)
                when :pascal_string
                  ready_values << Pyrb::Bytes.new([values[value_idx + 1][0...val]])
                  value_idx += 1
                when :float
                  ready_values << if pattern.py_fmt == 'e'
                    short_to_half_precision_float(val)
                  else
                    val
                  end
                else
                  ready_values << val
              end

              value_idx += 1
            end
          end
        end

        ready_values
      end

      # All of this complicated logic exists to handle the fact
      # that Python will consider the endianness within a single
      # byte to convert from a number to a boolean value. For example:
      #
      # struct.unpack('<?', b'\xF0') => (False,)
      # struct.unpack('>?', b'\xF0') => (True,)
      #
      # In the first case (little endian), there is a 0 in the MSB
      # so a false value is unpacked. In the second case (big
      # endian), there is a 1 in the MSB so a true value is unpacked.
      # Note that it's unusual for endinanness to be considered at
      # the bit level, which is what's happening here. The MSB
      # changes depending on LE or BE, but within the byte itself
      # as opposed to its position in a logical group of bytes.
      #
      # Oh, and 1 is a special case for which endinanness is ignored.
      # No you're not crazy, this is all pretty weird behavior.
      def unpack_bool(pattern, val)
        case val
          when ::Integer
            if val == 1
              true
            elsif val == 0
              false
            else
              if pattern.order == :le
                (val & 0x1) > 0
              else
                # calculate the position of the MSB to create the mask
                (val & 2**Math.log2(val).floor) > 0
              end
            end
          else
            !!val
        end
      end

      def coerce_buffer(obj)
        case obj
          when ::String
            obj
          else
            obj.getbuffer.data
        end
      end

      def validate_and_coerce(pattern, values, value_idx, ready_values)
        case pattern.type
          when :integer
            validate_and_coerce_integers(pattern, values, value_idx, ready_values)
          when :float, :double
            validate_and_coerce_floats(pattern, values, value_idx, ready_values)
          when :bool
            validate_and_coerce_bools(pattern, values, value_idx, ready_values)
          when :char
            validate_and_coerce_chars(pattern, values, value_idx, ready_values)
          when :null
            validate_and_coerce_nulls(pattern, ready_values)
          when :string, :pascal_string
            validate_and_coerce_string(pattern, values, value_idx, ready_values)
        end
      end

      def validate_and_coerce_integers(pattern, values, value_idx, ready_values)
        value_idx.upto(value_idx + pattern.count - 1) do |i|
          ready_val = coerce_integer(values[i])

          if ready_val < pattern.min_value || ready_val > pattern.max_value
            raise(
              Pyrb::Struct.exports['error'].klass,
              "'#{pattern.py_fmt}' format requires #{pattern.min_value} <= number <= #{pattern.max_value}"
            )
          end

          ready_values << ready_val
        end

        pattern.count
      end

      def coerce_integer(obj)
        case obj
          when ::Integer
            obj
          when ::TrueClass
            1
          when ::FalseClass
            0
          else
            if Pyrb.hasattr.call([obj, '__index__'])
              coerce_integer(obj.fcall('__index__'))
            else
              raise Pyrb::Struct.exports['error'].klass, INTEGER_ERROR_MSG
            end
        end
      end

      def validate_and_coerce_floats(pattern, values, value_idx, ready_values)
        value_idx.upto(value_idx + pattern.count - 1) do |i|
          cur = values[i]

          ready_values << case cur
            when ::Integer, ::Float, ::Rational
              if pattern.py_fmt == 'e'
                half_precision_float_to_short(pattern, cur)
              else
                # can be a float or a double
                if pattern.type == :float
                  if cur < pattern.min_value || cur > pattern.max_value
                    # for some reason Python raises an overflow error in this case
                    # instead of the usual struct error
                    raise Pyrb::OverflowError.klass, FLOAT_DOMAIN_ERROR_MSG % pattern.py_fmt
                  end
                end

                cur
              end
            when ::TrueClass
              1.0
            when ::FalseClass
              0.0
            else
              raise Pyrb::Struct.exports['error'].klass, FLOAT_ERROR_MSG
          end
        end

        pattern.count
      end

      def validate_and_coerce_bools(pattern, values, value_idx, ready_values)
        value_idx.upto(value_idx + pattern.count - 1) do |i|
          ready_values << coerce_bool(values[i])
        end

        pattern.count
      end

      def coerce_bool(obj)
        case obj
          when ::TrueClass, ::FalseClass
            obj ? 1 : 0
          when ::Integer, ::Float
            obj != 0 ? 1 : 0
          else
            if Pyrb.hasattr.call([obj, '__bool__'])
              coerce_bool(obj.fcall('__bool__'))
            else
              !!obj ? 1 : 0
            end
        end
      end

      def validate_and_coerce_chars(pattern, values, value_idx, ready_values)
        value_idx.upto(value_idx + pattern.count - 1) do |i|
          cur = values[i]

          ready_values << case cur
            when Pyrb::Bytes.klass
              if cur.value.size > 1
                raise Pyrb::Struct.exports['error'].klass, BYTES_ERROR_MSG
              end

              cur.value[0]
            else
              raise Pyrb::Struct.exports['error'].klass, BYTES_ERROR_MSG
          end
        end

        pattern.count
      end

      def validate_and_coerce_nulls(pattern, ready_values)
        ready_values.concat(NULL_BYTE_ARRAY * pattern.count)
        0
      end

      # @TODO: values have to be bytes objects
      def validate_and_coerce_string(pattern, values, value_idx, ready_values)
        len = pattern.count
        val = values[value_idx]

        if pattern.type == :pascal_string
          # the first byte is the number of subsequent characters
          count = [val.value.length, pattern.count - 1].min
          count = 255 if count > 255
          ready_values << count
          len -= 1
        end

        ready_values << val.value[0...len]

        1
      end

      # Adapted from cpython's source code:
      # https://github.com/python/cpython/blob/bdbad71b9def0b86433de12cecca022eee91bd9f/Objects/floatobject.c#L2061
      def half_precision_float_to_short(pattern, flt)
        return 0x7C00 if flt == ::Float::INFINITY
        return 0xFC00 if flt == -::Float::INFINITY

        # This could be any value with an exponent of 31 and a significand
        # greater than zero. The python tests seem to want a sig of 0.5
        # (i.e. 0x0200), likely for some mathematical reason.
        return 0x7E00 if flt.to_f.nan?

        # In ruby, 0.0 == -0.0, but 0.0.angle == 0 and -0.0.angle == Math::PI
        return 0x8000 if flt == 0.0 && flt.angle > 0  # i.e. -0.0
        return 0x0000 if flt == 0.0

        if flt < pattern.min_value || flt > pattern.max_value
          raise Pyrb::OverflowError.klass, FLOAT_DOMAIN_ERROR_MSG % pattern.py_fmt
        end

        bits = 0
        sign = flt < 0.0 ? 1 : 0

        f, e = Math.frexp(flt)
        f = f.abs

        # Normalize f to be in the range [1.0, 2.0)
        f *= 2.0
        e -= 1

        if e < -25
          # |x| < 2**-25. Underflow to zero.
          f = 0.0
          e = 0
        elsif e < -14
          # |x| < 2**-14. Gradual underflow
          f = Math.ldexp(f, 14 + e)
          e = 0
        else # if (!(e == 0 && f == 0.0)
          e += 15
          f -= 1.0 # Get rid of leading 1
        end

        f *= 1024.0 # 2**10

        # Round to even
        bits = f.to_i

        if (f - bits > 0.5) || ((f - bits == 0.5) && (bits % 2 == 1))
          bits += 1

          if bits == 1024
            # The carry propagated out of a string of 10 1 bits.
            bits = 0
            e += 1

            if e == 31
              raise Pyrb::OverflowError.klass, FLOAT_DOMAIN_ERROR_MSG % pattern.py_fmt
            end
          end
        end

        bits | (e << 10) | (sign << 15)
      end

      def short_to_half_precision_float(val)
        sig = val & ((2**10) - 1)
        exp = (val & 0x7C00) >> 10
        sign = (val & 0x8000) >> 15

        if exp == 0
          if sig == 0
            (-1.0)**sign * 0.0
          else
            (-1.0)**sign * (2**-14) * ((2**-10) * sig)
          end
        elsif exp == 31
          if sig == 0
            (-1.0)**sign * ::Float::INFINITY
          else
            ::Float::NAN
          end
        else
          (-1.0)**sign * 2**(exp - 15) * (1 + (2**(-10)) * sig)
        end
      end
    end

    class BinPattern
      attr_reader :order_chr, :count, :py_fmt

      def initialize(order_chr, count, py_fmt)
        @order_chr = order_chr
        @count = count ? count.to_i : 1
        @py_fmt = py_fmt

        @order_chr = DEFAULT_ORDER_CHAR if order_chr.empty?

        validate!
      end

      def order
        @order ||= BYTE_ORDER_CHARS[order_chr]
      end

      def alignment
        @alignment ||= ALIGNMENT_CHARS[order_chr]
      end

      def size
        @size ||= begin
          if s = metadata[SIZE_CHARS[order_chr]]
            s == :count ? count : (s * count)
          else
            raise Pyrb::Struct.exports['error'].klass, 'bad char in struct format'
          end
        end
      end

      def unit_size
        @unit_size ||= begin
          if s = metadata[SIZE_CHARS[order_chr]]
            s == :count ? count : s
          else
            raise Pyrb::Struct.exports['error'].klass, 'bad char in struct format'
          end
        end
      end

      def unit_bitsize
        @unit_bitsize ||= unit_size * 8
      end

      def valcount
        @valcount ||= metadata[:valcount] || count
      end

      def pack_str
        @pack_str ||= if type == :pascal_string
          "#{chr}#{count - 1}"
        else
          "#{chr}#{count}"
        end
      end

      def signed?
        @signed ||= metadata.fetch(:signed)
      end

      def type
        @type ||= metadata.fetch(:type)
      end

      def min_value
        @min_value = metadata[:min_value] || if signed?
          -(2**(unit_bitsize - 1))
        else
          0
        end
      end

      def max_value
        @max_value ||= metadata[:max_value] || if signed?
          2**(unit_bitsize - 1) - 1
        else
          2**unit_bitsize - 1
        end
      end

      private

      def chr
        @chr ||= case type
          when :integer, :float, :double
            FORMAT_MATRIX[type][unit_size][order][signed? ? :signed : :unsigned]
          when :string, :pascal_string, :char, :bool, :null
            FORMAT_MATRIX[type]
        end
      end

      def validate!
        PYTHON_FORMAT_CHARS.fetch(py_fmt) do
          raise(Pyrb::Struct.exports['error'].klass, 'bad char in struct format')
        end
      end

      def metadata
        @metadata ||= PYTHON_FORMAT_CHARS.fetch(py_fmt) do
          raise(
            exports['UnsupportedFormatError'].klass,
            "Pyrb doesn't yet support the '#{py_fmt}' struct format"
          )
        end
      end
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Struct = Struct_rb
end
