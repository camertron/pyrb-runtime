require 'open3'

module Pyrb
  module Subprocess
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    init_params = {
      self: nil,
      args: { splat: true },
      bufsize: { default: -1 },
      executable: { default: nil },
      stdin: { default: nil },
      stdout: { default: nil },
      stderr: { default: nil },
      preexec_fn: { default: nil },
      close_fds: { default: true },
      shell: { default: false },
      cwd: { default: nil },
      env: { default: nil },
      universal_newlines: { default: nil },
      startupinfo: { default: nil },
      creationflags: { default: 0 },
      restore_signals: { default: true },
      start_new_session: { default: false },
      pass_fds: { default: () },
      encoding: { default: nil },
      errors: { default: nil },
      text: { default: nil }
    }

    sys, _ = import('sys')

    exports['Popen'] = Pyrb::PythonClass.new('Popen', [Pyrb::Object], {
      '__init__' => Pyrb.defn(init_params) do |self_, args, bufsize, executable, stdin, stdout, stderr, preexec_fn, close_fds, shell, cwd, env, universal_newlines, startupinfo, creationflags, restore_signals, start_new_session, pass_fds, encoding, errors, text|
        # the remaining arguments (the number of which, by the way, is INSANE)
        # are not currently supported
        self_.ivars['stdin'], self_.ivars['stdout'], self_.ivars['stderr'] = Open3.popen3(*[sys.attr('executable'), *args])
      end,

      '__enter__' => Pyrb.defn(self: nil) do |self_|
      end,

      '__exit__' => Pyrb.defn(self: nil) do |self_|
        self_.ivars['stdin'].close
        self_.ivars['stdout'].close
        self_.ivars['stderr'].close
      end,

      'communicate' => Pyrb.defn(self: nil, input: { default: nil }, timeout: { default: nil }) do |self_, input, timeout|
        self_.ivars['stdin'].write(input.value) if input
        self_.ivars['stdin'].close_write  # otherwise the pipe will think more input is forthcoming

        out = self_.ivars['stdout'].read
        err = self_.ivars['stderr'].read

        out = nil if out.strip.empty?
        err = nil if err.strip.empty?

        out = Pyrb::Bytes.call([out]) if out
        err = Pyrb::Bytes.call([err]) if err

        [out, err]
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
