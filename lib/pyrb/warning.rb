module Pyrb
  module Warning_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['Warning'] = Pyrb::PythonClass.new('Warning', [Pyrb::Object] , {
      '__init__' => Pyrb.defn(self: nil, message: '') do |self_, message|
        self_.ivars['message'] = message
      end,

      '__str__' => Pyrb.defn(self: nil) do |self_|
        self_.ivars['message']
      end
    })

    exports['UserWarning']        = Pyrb::PythonClass.new('UserWarning', [exports['Warning']])
    exports['DeprecationWarning'] = Pyrb::PythonClass.new('DeprecationWarning', [exports['Warning']])
  end

  Warning = Warning_rb.exports['Warning']
  UserWarning = Warning_rb.exports['UserWarning']
  DeprecationWarning = Warning_rb.exports['DeprecationWarning']
end
