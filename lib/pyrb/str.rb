module Pyrb
  module Str_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['str'] = Pyrb::PythonClass.new('str', [Pyrb::Object]).tap do |klass|
      klass.attrs['isalpha'] = Pyrb.classmethod.call(
        klass, -> (args = [], **kwargs) do
          !(args.get(1) =~ /\A[A-Za-z]*\z/).nil?
        end
      )
    end

    exports['str'].instance_eval do
      def call(args = [], **kwargs)
        args.get(0).fcall('__str__')
      end
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Str = Str_rb.exports['str']
end
