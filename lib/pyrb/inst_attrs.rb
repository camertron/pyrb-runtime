module Pyrb
  class InstAttrs
    attr_reader :attrs

    def initialize(inst)
      @inst = inst
      @attrs = {}
    end

    def [](name)
      attr = get(name)

      if attr.is_a?(Proc)
        Pyrb::MethodDescriptor.wrap(@inst, attr)
      else
        attr
      end
    end

    def hasattr(name)
      @attrs.include?(name) ||
        @inst.prototype.properties.include?(name.to_sym) ||
        @inst.prototype.attrs.include?(name) ||
        name == '__dict__'
    end

    def get(name)
      if @attrs.include?(name)
        @attrs[name]
      elsif name == '__dict__'
        @inst.__dict__
      elsif @inst.prototype.properties.include?(name.to_sym)
        @inst.prototype.properties[name.to_sym].get([@inst])
      elsif @inst.prototype.attrs.include?(name)
        @inst.prototype.attrs[name]
      else
        raise Pyrb::AttributeError.new(["#{@inst.prototype.class_name} object has no attribute #{name}"])
      end
    end

    def del(name)
      if @attrs.include?(name)
        @attrs.delete(name)
      elsif @inst.prototype.properties.include?(name.to_sym)
        @inst.prototype.properties.del(name)
      elsif @inst.prototype.attrs.include?(name)
        @inst.prototype.attrs.del(name)
      else
        raise Pyrb::AttributeError.klass, "#{@inst.prototype.class_name} object has no attribute #{name}"
      end
    end

    def []=(name, value)
      if @inst.prototype.properties.include?(name.to_sym)
        @inst.prototype.properties[name.to_sym].set([@inst, value])
        return
      end

      @attrs[name] = if value.is_a?(Pyrb::MethodDescriptor.klass)
        value.ivars['method']
      else
        value
      end
    end

    def to_h
      @attrs
    end
  end
end
