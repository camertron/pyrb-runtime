module Pyrb
  module NativeRange_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['NativeRange'] = Pyrb::PythonClass.new('NativeRange', [Pyrb::Object], {
      '__len__' => Pyrb.defn(self: nil) do |self_|
        self_.size
      end,

      '__getitem__' => Pyrb.defn(self: nil, idx: nil) do |self_, idx|
        self_.first + idx
      end
    })
  end

  NativeRange = NativeRange_rb.exports['NativeRange']
end
