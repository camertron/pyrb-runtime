module Pyrb
  module PythonInstance
    attr_reader :attrs, :ivars, :prototype

    def initialize(*)
      init
      super
    end

    def init
      @attrs = InstAttrs.new(self)

      # these are supposed to be visible to ruby only
      @ivars = {}
      @prototype = self.class.prototype

      @attrs['__class__'] = prototype
    end

    def __dict__
      Pyrb::Dict.new.tap do |dict|
        dict.fcall('update', [@prototype.__dict__])
        dict.fcall('update', [@attrs.to_h])
      end
    end

    def value
      @ivars.fetch('value', self)
    end

    def inspect
      "#<#{@prototype.class_name}:0x#{(object_id << 1).to_s(16).rjust(16, '0')}>"
    end

    def attr(name)
      @attrs[name]
    end

    def hasattr(name)
      @attrs.hasattr(name)
    end

    def fcall(func_name, args = [], **kwargs, &block)
      attr = @attrs.get(func_name)

      if attr.is_a?(Proc)
        attr.call([self] + args, **kwargs, &block)
      else
        attr.call(args, **kwargs, &block)
      end
    end

    def del(name)
      @attrs.del(name)
    end

    def method_missing(func_name, *args, **kwargs, &block)
      if @attrs.get(func_name.to_s).is_a?(Proc)
        prototype.klass.define_method(func_name) do |*args, **kwargs, &block|
          fcall(func_name.to_s, args, **kwargs, &block)
        end
      else
        prototype.klass.define_method(func_name) do |*_args, **_kwargs, &_block|
          attr(func_name.to_s)
        end
      end

      send(func_name, *args, **kwargs, &block)
    end

    def respond_to?(func_name, _include_all = true)
      Pyrb.hasattr.call([self, func_name]) || super
    end
  end
end
