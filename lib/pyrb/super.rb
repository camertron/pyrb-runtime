module Pyrb
  module Super_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['super'] = PythonClass.new('super', [Pyrb::Object])

    exports['super'].instance_module.class_eval do
      def fcall(func_name, args = [], **kwargs)
        # Not sure if this lookup is 100% accurate.
        # Basically we're trying to find thisclass's super, which I think
        # is it's first parent class. The python docs say it follows the
        # same lookup rules as getattr, but then doesn't explain what those
        # rules are in either docs for getattr() or super(). Wah wah.
        receiver = attr('__thisclass__').parents.first
        receiver.fcall(func_name, [attr('__self__')] + args, **kwargs)
      end
    end
  end

  Super = Super_rb.exports['super']
end
