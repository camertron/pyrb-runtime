module Pyrb
  module Utils
    using Pyrb::Runtime

    def self.unwrap_iter(iterator)
      case iterator
        when ::Enumerator
          iterator
        else
          iterator.fcall('__iter__')
      end
    end
  end
end
