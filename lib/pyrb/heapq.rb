# encoding: utf-8

require 'pyrb'

module Lib_pyrb_heapq
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  <<~__DOC__
  Heap queue algorithm (a.k.a. priority queue).

  Heaps are arrays for which a[k] <= a[2*k+1] and a[k] <= a[2*k+2] for
  all k, counting elements from 0.  For the sake of comparison,
  non-existing elements are considered to be infinite.  The interesting
  property of a heap is that a[0] is always its smallest element.

  Usage:

  heap = []            # creates an empty heap
  heappush(heap, item) # pushes a new item on the heap
  item = heappop(heap) # pops the smallest item from the heap
  item = heap[0]       # smallest item on the heap without popping it
  heapify(x)           # transforms list into a heap, in-place, in linear time
  item = heapreplace(heap, item) # pops and returns smallest item, and adds
                                 # new item; the heap size is unchanged

  Our API differs from textbook heap algorithms as follows:

  - We use 0-based indexing.  This makes the relationship between the
    index for a node and the indexes for its children slightly less
    obvious, but is more suitable since Python uses 0-based indexing.

  - Our heappop() method returns the smallest item, not the largest.

  These two make it possible to view the heap as a regular Python list
  without surprises: heap[0] is the smallest item, and heap.sort()
  maintains the heap invariant!
  __DOC__

  exports['__about__'] = <<~__DOC__
  Heap queues

  [explanation by François Pinard]

  Heaps are arrays for which a[k] <= a[2*k+1] and a[k] <= a[2*k+2] for
  all k, counting elements from 0.  For the sake of comparison,
  non-existing elements are considered to be infinite.  The interesting
  property of a heap is that a[0] is always its smallest element.

  The strange invariant above is meant to be an efficient memory
  representation for a tournament.  The numbers below are `k', not a[k]:

                                     0

                    1                                 2

            3               4                5               6

        7       8       9       10      11      12      13      14

      15 16   17 18   19 20   21 22   23 24   25 26   27 28   29 30

  In the tree above, each cell `k' is topping `2*k+1' and `2*k+2'.  In
  a usual binary tournament we see in sports, each cell is the winner
  over the two cells it tops, and we can trace the winner down the tree
  to see all opponents s/he had.  However, in many computer applications
  of such tournaments, we do not need to trace the history of a winner.
  To be more memory efficient, when a winner is promoted, we try to
  replace it by something else at a lower level, and the rule becomes
  that a cell and the two cells it tops contain three different items,
  but the top cell "wins" over the two topped cells.

  If this heap invariant is protected at all time, index 0 is clearly
  the overall winner.  The simplest algorithmic way to remove it and
  find the "next" winner is to move some loser (let's say cell 30 in the
  diagram above) into the 0 position, and then percolate this new 0 down
  the tree, exchanging values, until the invariant is re-established.
  This is clearly logarithmic on the total number of items in the tree.
  By iterating over all items, you get an O(n ln n) sort.

  A nice feature of this sort is that you can efficiently insert new
  items while the sort is going on, provided that the inserted items are
  not "better" than the last 0'th element you extracted.  This is
  especially useful in simulation contexts, where the tree holds all
  incoming events, and the "win" condition means the smallest scheduled
  time.  When an event schedule other events for execution, they are
  scheduled into the future, so they can easily go into the heap.  So, a
  heap is a good structure for implementing schedulers (this is what I
  used for my MIDI sequencer :-).

  Various structures for implementing schedulers have been extensively
  studied, and heaps are good for this, as they are reasonably speedy,
  the speed is almost constant, and the worst case is not much different
  than the average case.  However, there are other representations which
  are more efficient overall, yet the worst cases might be terrible.

  Heaps are also very useful in big disk sorts.  You most probably all
  know that a big sort implies producing "runs" (which are pre-sorted
  sequences, which size is usually related to the amount of CPU memory),
  followed by a merging passes for these runs, which merging is often
  very cleverly organised[1].  It is very important that the initial
  sort produces the longest runs possible.  Tournaments are a good way
  to that.  If, using all the memory available to hold a tournament, you
  replace and percolate items that happen to fit the current run, you'll
  produce runs which are twice the size of the memory for random input,
  and much better for input fuzzily ordered.

  Moreover, if you output the 0'th item on disk and get an input which
  may not fit in the current tournament (because the value "wins" over
  the last output value), it cannot fit in the heap, so the size of the
  heap decreases.  The freed memory could be cleverly reused immediately
  for progressively building a second heap, which grows at exactly the
  same rate the first heap is melting.  When the first heap completely
  vanishes, you switch heaps and start a new run.  Clever and quite
  effective!

  In a word, heaps are useful memory structures to know.  I use them in
  a few applications, and I think it is good to keep a `heap' module
  around. :-)

  --------------------
  [1] The disk balancing algorithms which are current, nowadays, are
  more annoying than clever, and this is a consequence of the seeking
  capabilities of the disks.  On devices which cannot seek, like big
  tape drives, the story was quite different, and one had to be very
  clever to ensure (far in advance) that each tape movement will be the
  most effective possible (that is, will best participate at
  "progressing" the merge).  Some tapes were even able to read
  backwards, and this was also used to avoid the rewinding time.
  Believe me, real good tape sorts were quite spectacular to watch!
  From all times, sorting has always been a Great Art! :-)
  __DOC__

  exports['__all__'] = ["heappush", "heappop", "heapify", "heapreplace", "merge", "nlargest", "nsmallest", "heappushpop"]
  exports['heappush'] = Pyrb.defn(heap: nil, item: nil) do |heap, item|
    "Push item onto heap, maintaining the heap invariant."
    heap.fcall('append', [item])
    exports['_siftdown'].call([heap, 0, Pyrb.len.call([heap]) - 1])
  end

  exports['heappop'] = Pyrb.defn(heap: nil) do |heap|
    "Pop the smallest item off the heap, maintaining the heap invariant."
    lastelt = heap.fcall('pop')
    if !!(heap)
      returnitem = heap[0]
      heap[0] = lastelt
      exports['_siftup'].call([heap, 0])
      throw(:return, returnitem)
    end

    throw(:return, lastelt)
  end

  exports['heapreplace'] = Pyrb.defn(heap: nil, item: nil) do |heap, item|
    <<~__DOC__
    Pop and return the current smallest value, and add the new item.

    This is more efficient than heappop() followed by heappush(), and can be
    more appropriate when using a fixed-size heap.  Note that the value
    returned may be larger than item!  That constrains reasonable uses of
    this routine unless written as part of a conditional replacement:

        if item > heap[0]:
            item = heapreplace(heap, item)
    __DOC__

    returnitem = heap[0]
    heap[0] = item
    exports['_siftup'].call([heap, 0])
    throw(:return, returnitem)
  end

  exports['heappushpop'] = Pyrb.defn(heap: nil, item: nil) do |heap, item|
    "Fast version of a heappush followed by a heappop."
    if !!(heap.and { heap[0] < item })
      item, heap[0] = Pyrb.deconstruct(2, heap[0], item)
      exports['_siftup'].call([heap, 0])
    end

    throw(:return, item)
  end

  exports['heapify'] = Pyrb.defn(x: nil) do |x|
    "Transform list into a heap, in-place, in O(len(x)) time."
    n = Pyrb.len.call([x])
    (Pyrb.reversed.call([Pyrb.range.call([Pyrb.int_divide(n, 2)])])).each do |i|
      exports['_siftup'].call([x, i])
    end
  end

  exports['_heappop_max'] = Pyrb.defn(heap: nil) do |heap|
    "Maxheap version of a heappop."
    lastelt = heap.fcall('pop')
    if !!(heap)
      returnitem = heap[0]
      heap[0] = lastelt
      exports['_siftup_max'].call([heap, 0])
      throw(:return, returnitem)
    end

    throw(:return, lastelt)
  end

  exports['_heapreplace_max'] = Pyrb.defn(heap: nil, item: nil) do |heap, item|
    "Maxheap version of a heappop followed by a heappush."
    returnitem = heap[0]
    heap[0] = item
    exports['_siftup_max'].call([heap, 0])
    throw(:return, returnitem)
  end

  exports['_heapify_max'] = Pyrb.defn(x: nil) do |x|
    "Transform list into a maxheap, in-place, in O(len(x)) time."
    n = Pyrb.len.call([x])
    (Pyrb.reversed.call([Pyrb.range.call([Pyrb.int_divide(n, 2)])])).each do |i|
      exports['_siftup_max'].call([x, i])
    end
  end

  exports['_siftdown'] = Pyrb.defn(heap: nil, startpos: nil, pos: nil) do |heap, startpos, pos|
    newitem = heap[pos]
    while !!(pos > startpos)
      parentpos = (pos - 1) >> 1
      parent = heap[parentpos]
      if !!(newitem < parent)
        heap[pos] = parent
        pos = parentpos
        next
      end

      break
    end

    heap[pos] = newitem
  end

  exports['_siftup'] = Pyrb.defn(heap: nil, pos: nil) do |heap, pos|
    endpos = Pyrb.len.call([heap])
    startpos = pos
    newitem = heap[pos]
    childpos = (2 * pos) + 1
    while !!(childpos < endpos)
      rightpos = childpos + 1
      if !!((rightpos < endpos).and { (heap[childpos] < heap[rightpos]).not })
        childpos = rightpos
      end

      heap[pos] = heap[childpos]
      pos = childpos
      childpos = (2 * pos) + 1
    end

    heap[pos] = newitem
    exports['_siftdown'].call([heap, startpos, pos])
  end

  exports['_siftdown_max'] = Pyrb.defn(heap: nil, startpos: nil, pos: nil) do |heap, startpos, pos|
    "Maxheap variant of _siftdown"
    newitem = heap[pos]
    while !!(pos > startpos)
      parentpos = (pos - 1) >> 1
      parent = heap[parentpos]
      if !!(parent < newitem)
        heap[pos] = parent
        pos = parentpos
        next
      end

      break
    end

    heap[pos] = newitem
  end

  exports['_siftup_max'] = Pyrb.defn(heap: nil, pos: nil) do |heap, pos|
    "Maxheap variant of _siftup"
    endpos = Pyrb.len.call([heap])
    startpos = pos
    newitem = heap[pos]
    childpos = (2 * pos) + 1
    while !!(childpos < endpos)
      rightpos = childpos + 1
      if !!((rightpos < endpos).and { (heap[rightpos] < heap[childpos]).not })
        childpos = rightpos
      end

      heap[pos] = heap[childpos]
      pos = childpos
      childpos = (2 * pos) + 1
    end

    heap[pos] = newitem
    exports['_siftdown_max'].call([heap, startpos, pos])
  end

  exports['merge'] = Pyrb.defn(iterables: { splat: true }, key: { default: nil }, reverse: { default: false }) do |iterables, key, reverse|
    next_, value = nil

    Pyrb::Generator.new([
      Enumerator.new do |__pyrb_gen__|
        <<~__DOC__
        Merge multiple sorted inputs into a single sorted output.

        Similar to sorted(itertools.chain(*iterables)) but returns a generator,
        does not pull the data into memory all at once, and assumes that each of
        the input streams is already sorted (smallest to largest).

        >>> list(merge([1,3,5,7], [0,2,4,8], [5,10,15,20], [], [25]))
        [0, 1, 2, 3, 4, 5, 5, 7, 8, 10, 15, 20, 25]

        If *key* is not None, applies a key function to each element to determine
        its sort order.

        >>> list(merge(['dog', 'horse'], ['cat', 'fish', 'kangaroo'], key=len))
        ['dog', 'cat', 'fish', 'horse', 'kangaroo']
        __DOC__

        h = []
        h_append = h.attr('append')
        if !!(reverse)
          _heapify = exports['_heapify_max']
          _heappop = exports['_heappop_max']
          _heapreplace = exports['_heapreplace_max']
          direction = -1
        else
          _heapify = exports['heapify']
          _heappop = exports['heappop']
          _heapreplace = exports['heapreplace']
          direction = 1
        end

        if !!(key.object_id == nil.object_id)
          (Pyrb.enumerate.call([Pyrb::Map.call([Pyrb.iter, iterables])])).each do |order, it|
            begin
              next_ = it.attr('__next__')
              h_append.call([[next_.call(), (order * direction), next_]])
            rescue *Pyrb.rezcue(::StopIteration)
            end
          end
          _heapify.call([h])
          while !!(Pyrb.len.call([h]) > 1)
            begin
              while !!(true)
                value, order, next_ = (s = Pyrb.deconstruct(3, h[0]))
                __pyrb_gen__ << value
                s[0] = next_.call()
                _heapreplace.call([h, s])
              end

            rescue *Pyrb.rezcue(::StopIteration)
              _heappop.call([h])
            end

          end

          if !!(h)
            value, order, next_ = Pyrb.deconstruct(3, h[0])
            __pyrb_gen__ << value
            next_.attr('__self__').each { |item| __pyrb_gen__ << item }
          end

          next
        end

        (Pyrb.enumerate.call([Pyrb::Map.call([Pyrb.iter, iterables])])).each do |order2, it|
          begin
            next_ = it.attr('__next__')
            value = next_.call()
            h_append.call([[key.call([value]), (order2 * direction), value, next_]])
          rescue *Pyrb.rezcue(::StopIteration)
          end
        end
        _heapify.call([h])
        while !!(Pyrb.len.call([h]) > 1)
          begin
            while !!(true)
              key_value, order, value, next_ = (s = Pyrb.deconstruct(4, h[0]))
              __pyrb_gen__ << value
              value = next_.call()
              s[0] = key.call([value])
              s[2] = value
              _heapreplace.call([h, s])
            end

          rescue *Pyrb.rezcue(::StopIteration)
            _heappop.call([h])
          end

        end

        if !!(h)
          key_value, order, value, next_ = Pyrb.deconstruct(4, h[0])
          __pyrb_gen__ << value
          next_.attr('__self__').each { |item| __pyrb_gen__ << item }
        end

      end
    ])
  end

  exports['nsmallest'] = Pyrb.defn(n: nil, iterable: nil, key: { default: nil }) do |n, iterable, key|
    k = nil

    <<~__DOC__
    Find the n smallest elements in a dataset.

    Equivalent to:  sorted(iterable, key=key)[:n]
    __DOC__

    if !!(n == 1)
      it = Pyrb.iter.call([iterable])
      sentinel = Pyrb::Object.call()
      if !!(key.object_id == nil.object_id)
        result = Pyrb.min.call([it, ], { default: sentinel })
      else
        result = Pyrb.min.call([it, ], { default: sentinel, key: key })
      end

      throw(:return, !!(result.object_id == sentinel.object_id) ? [] : [result])
    end

    begin
      size = Pyrb.len.call([iterable])
    rescue *Pyrb.rezcue([Pyrb::TypeError, Pyrb::AttributeError])
    else
      if !!(n >= size)
        throw(:return, Pyrb.sorted.call([iterable, ], { key: key })[0...n])
      end

    end

    if !!(key.object_id == nil.object_id)
      it = Pyrb.iter.call([iterable])
      result = Pyrb::Zip.call([Pyrb.range.call([n]), it]).fcall('__iter__').map do |(i, elem)|
        [elem, i]
      end

      if !!((result).not)
        throw(:return, result)
      end

      exports['_heapify_max'].call([result])
      top = result[0][0]
      order = n
      _heapreplace = exports['_heapreplace_max']
      (it).each do |elem|
        if !!(elem < top)
          _heapreplace.call([result, [elem, order]])
          top = result[0][0]
          order += 1
        end
      end
      result.fcall('sort')
      throw(:return, result.fcall('__iter__').map do |r|
        r[0]
      end
      )
    end

    it = Pyrb.iter.call([iterable])
    result = Pyrb::Zip.call([Pyrb.range.call([n]), it]).fcall('__iter__').map do |(i, elem)|
      [key.call([elem]), i, elem]
    end

    if !!((result).not)
      throw(:return, result)
    end

    exports['_heapify_max'].call([result])
    top = result[0][0]
    order = n
    _heapreplace = exports['_heapreplace_max']
    (it).each do |elem|
      k = key.call([elem])
      if !!(k < top)
        _heapreplace.call([result, [k, order, elem]])
        top = result[0][0]
        order += 1
      end
    end
    result.fcall('sort')
    throw(:return, result.fcall('__iter__').map do |r|
      r[2]
    end
    )
  end

  exports['nlargest'] = Pyrb.defn(n: nil, iterable: nil, key: { default: nil }) do |n, iterable, key|
    k = nil

    <<~__DOC__
    Find the n largest elements in a dataset.

    Equivalent to:  sorted(iterable, key=key, reverse=True)[:n]
    __DOC__

    if !!(n == 1)
      it = Pyrb.iter.call([iterable])
      sentinel = Pyrb::Object.call()
      if !!(key.object_id == nil.object_id)
        result = Pyrb.max.call([it, ], { default: sentinel })
      else
        result = Pyrb.max.call([it, ], { default: sentinel, key: key })
      end

      throw(:return, !!(result.object_id == sentinel.object_id) ? [] : [result])
    end

    begin
      size = Pyrb.len.call([iterable])
    rescue *Pyrb.rezcue([Pyrb::TypeError, Pyrb::AttributeError])
    else
      if !!(n >= size)
        throw(:return, Pyrb.sorted.call([iterable, ], { key: key, reverse: true })[0...n])
      end

    end

    if !!(key.object_id == nil.object_id)
      it = Pyrb.iter.call([iterable])
      result = Pyrb::Zip.call([Pyrb.range.call([0, -n, -1]), it]).fcall('__iter__').map do |(i, elem)|
        [elem, i]
      end

      if !!((result).not)
        throw(:return, result)
      end

      exports['heapify'].call([result])
      top = result[0][0]
      order = -n
      _heapreplace = exports['heapreplace']
      (it).each do |elem|
        if !!(top < elem)
          _heapreplace.call([result, [elem, order]])
          top = result[0][0]
          order -= 1
        end
      end
      result.fcall('sort', [], { reverse: true })
      throw(:return, result.fcall('__iter__').map do |r|
        r[0]
      end
      )
    end

    it = Pyrb.iter.call([iterable])
    result = Pyrb::Zip.call([Pyrb.range.call([0, -n, -1]), it]).fcall('__iter__').map do |(i, elem)|
      [key.call([elem]), i, elem]
    end

    if !!((result).not)
      throw(:return, result)
    end

    exports['heapify'].call([result])
    top = result[0][0]
    order = -n
    _heapreplace = exports['heapreplace']
    (it).each do |elem|
      k = key.call([elem])
      if !!(top < k)
        _heapreplace.call([result, [k, order, elem]])
        top = result[0][0]
        order -= 1
      end
    end
    result.fcall('sort', [], { reverse: true })
    throw(:return, result.fcall('__iter__').map do |r|
      r[2]
    end
    )
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
