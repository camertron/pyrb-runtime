module Pyrb
  module Encodings
    class CodePageCodec
      attr_reader :name, :code_page

      def initialize(name, code_page)
        @name = name
        @code_page = code_page
      end

      def decode(str, options = {})
        code_page.to_utf8(str)
      end
    end
  end
end
