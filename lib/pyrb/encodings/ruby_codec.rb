module Pyrb
  module Encodings
    class RubyCodec
      REPLACE_OPTIONS = { invalid: :replace }.freeze
      IGNORE_OPTIONS = { invalid: :replace, replace: '' }.freeze
      DEFAULT_OPTIONS = {}.freeze

      attr_reader :name, :encoding, :options

      def initialize(name, encoding)
        @name = name
        @encoding = encoding
        @options = options
      end

      def decode(str, options = {})
        options = case options[:errors]
          when 'replace'
            REPLACE_OPTIONS
          when 'ignore'
            IGNORE_OPTIONS
          else
            {}
        end

        begin
          str.force_encoding(encoding).encode('UTF-8', options)
        rescue ::Encoding::UndefinedConversionError => e
          raise Pyrb::UnicodeEncodeError.klass, e.message
        end
      end
    end
  end
end
