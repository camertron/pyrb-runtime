module Pyrb
  module Warnings_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['Warning'] = Pyrb::PythonClass.new('Warning', [Pyrb::Object] , {
      '__init__' => Pyrb.defn(self: nil, message: '') do |self_, message|
        self_.ivars['message'] = message
      end,

      '__str__' => Pyrb.defn(self: nil) do |self_|
        throw :return, self_.ivars['message']
      end
    })

    exports['UserWarning']        = Pyrb::PythonClass.new('UserWarning', [exports['Warning']])
    exports['DeprecationWarning'] = Pyrb::PythonClass.new('DeprecationWarning', [exports['Warning']])

    exports['WarningMessage'] = Pyrb::PythonClass.new('WarningMessage', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, message: nil, category: nil, filename: nil, lineno: nil, file: { default: nil }, line: { default: nil }, source: { default: nil }) do |self_, message, category, filename, lineno, file, line, source|
        self_.attrs['message'] = message
        self_.attrs['category'] = category
        self_.attrs['_category_name'] = category ? category.attr('__name__') : nil
        self_.attrs['filename'] = filename
        self_.attrs['lineno'] = lineno
        self_.attrs['file'] = file  # this is an IO-like object, eg. stdout, stderr
        self_.attrs['line'] = line
        self_.attrs['source'] = source
      end,

      '__str__' => Pyrb.defn(self: nil) do |self_|
        throw :return, "{message : %r, category : %r, filename : %r, lineno : %s, line : %r}" % [
          self_.attrs['message'], self_.attrs['_category_name'], self_.attrs['filename'],
          self_.attrs['lineno'], self_.attrs['line']
        ]
      end
    })

    exports['warn'] = Pyrb.defn(message: nil, category: { default: exports['UserWarning'] }, stacklevel: { default: 1 }, source: { default: nil }) do |message, category, stacklevel, source|
      trace = Pyrb.clean_backtrace(Kernel.caller)[stacklevel]
      idx = trace.index(':')
      filename = trace[0...idx]
      lineno = trace[(idx + 1)..-1].to_i

      if message.is_a?(exports['Warning'].klass)
        category = message.attr('__class__')
        message = Pyrb.str.call(message)
      end

      # not sure what "source" is supposed to be
      exports['warn_explicit'].call([message, category, filename, lineno, nil, nil, nil, source])
    end

    exports['warn_explicit'] = Pyrb.defn(message: nil, category: nil, filename: nil, lineno: nil, module: { default: nil }, registry: { default: nil }, module_globals: { default: nil }, source: { default: nil }) do |message, category, filename, lineno, module_, registry, module_globals, source|
      msg = exports['WarningMessage'].call([message, category, filename, lineno, source])
      exports['_showwarnmsg'].call([msg])
    end

    exports['_showwarnmsg'] = Pyrb.defn(msg: nil) do |msg|
      exports['_showwarnmsg_impl'].call([msg])
    end

    exports['_showwarnmsg_impl'] = Pyrb.defn(msg: nil) do |msg|
      file = msg.attr('file') || Pyrb::Sys.attr('stderr')
      file.fcall('write', [exports['_formatwarnmsg'].call([msg])])
    end

    original_showwarnmsg_impl = exports['_showwarnmsg_impl']

    exports['_formatwarnmsg'] = Pyrb.defn(msg: nil) do |msg|
      throw :return, exports['_formatwarnmsg_impl'].call([msg])
    end

    exports['_formatwarnmsg_impl'] = Pyrb.defn(msg: nil) do |msg|
      # there's a lot more to this method in cpython, but this should be enough for now
      category = msg.attr('category').attr('__name__')
      throw :return, "#{msg.attr('filename')}:{msg.attr('lineno')}: {category}: {msg.attr('message')}\n"
    end

    exports['catch_warnings'] = Pyrb::PythonClass.new('catch_warnings', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, record: { default: false }, module: { default: nil }) do |self_, record, module_|
        self_.attrs['record'] = record
        # ignore module arg for now
      end,

      '__enter__' => Pyrb.defn(self: nil) do |self_|
        if self_.attrs['record']
          log = []
          exports['_showwarnmsg_impl'] = log.attr('append')
          throw :return, log
        else
          throw :return, nil
        end
      end,

      '__exit__' => Pyrb.defn(self: nil, exc_info: { splat: true }) do |self_, _exc_info|
        exports['_showwarnmsg_impl'] = original_showwarnmsg_impl
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Warnings = Warnings_rb
end
