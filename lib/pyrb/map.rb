module Pyrb
  module Map_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['map'] = PythonClass.new('map', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.ivars['callable'] = args.get(1)
        self_.ivars['enumerable'] = args.get(2)
      end,

      '__iter__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        Enumerator.new do |y|
          loop { y << self_.fcall('__next__') }
        end
      end,

      '__next__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        # this will raise ruby's StopIteration when finished,
        # which coincidentally is also python's name for the same thing
        self_.ivars['callable'].call([
          Pyrb.next_.call([self_.ivars['enumerable']])
        ])
      end
    })

    exports['map'].klass.class_eval do
      include ::Enumerable

      def +(other)
        Pyrb::List.call([self]) + other
      end

      def each(&block)
        fcall('__iter__').each(&block)
      end
    end

    exports['map'].instance_eval do
      def call(args = [], **kwargs)
        callable = args.get(0)
        enumerable = args.get(1)
        new([callable, Utils.unwrap_iter(enumerable)])
      end
    end
  end

  Map = Map_rb.exports['map']
end
