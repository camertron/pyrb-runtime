module Pyrb
  module Itertools
    module Chain_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['chain'] = Pyrb::PythonClass.new('chain', [Pyrb::Object], {
        '__init__' => -> (args = [], **kwargs) do
          self_ = args.shift
          self_.ivars['iterables'] = args.map { |arg| Pyrb.iter.call([arg]) }
          self_.ivars['current_idx'] = 0
        end,

        '__iter__' => -> (args = [], **kwargs) do
          # same behavior as python
          args.get(0)
        end,

        '__next__' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          begin
            idx = self_.ivars['current_idx']
            Pyrb.next_.call([self_.ivars['iterables'][idx]])
          rescue ::StopIteration
            if self_.ivars['iterables'].size - 1 == idx
              raise
            else
              self_.ivars['current_idx'] += 1
              redo
            end
          end
        end
      })

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Chain = Chain_rb.exports['chain']
  end
end
