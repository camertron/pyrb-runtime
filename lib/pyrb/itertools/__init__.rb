module Pyrb
  module Itertools_init_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    import 'chain', from: '.chain'
    import 'combinations', from: '.combinations'
    import 'groupby', from: '.groupby'

    exports['__all__'] = %w(chain combinations groupby)

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
