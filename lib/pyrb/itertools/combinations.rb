module Pyrb
  module Itertools
    module Combinations_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['combinations'] = Pyrb::PythonClass.new('combinations', [Pyrb::Object], {
        '__init__' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          iterable = args.get(1)
          n = args.get(2)
          self_.ivars['enum'] = Pyrb::List.call([iterable]).combination(n)
        end,

        '__iter__' => -> (args = [], **kwargs) do
          args.get(0).ivars['enum']
        end,

        '__next__' => -> (args = [], **kwargs) do
          args.get(0).ivars['enum'].next
        end
      })

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Combinations = Combinations_rb.exports['combinations']
  end
end
