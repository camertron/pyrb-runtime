module Pyrb
  module Itertools
    module Groupby_rb
      def self.location
        __FILE__
      end

      extend Pyrb::Runtime
      using Pyrb::Runtime

      DEFAULT_KEY_FUNC = -> (args = [], **kwargs) { args.get(0) }

      exports['groupby'] = Pyrb::PythonClass.new('groupby', [Pyrb::Object], {
        '__init__' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          iterable = args.get(1)
          key = kwargs.fetch(:key, args.get(2)) || DEFAULT_KEY_FUNC

          self_.ivars['enum'] =
            Pyrb::Utils.unwrap_iter(iterable).chunk { |n| key.call(n) }
        end,

        '__iter__' => -> (args = [], **kwargs) do
          args.get(0).ivars['enum']
        end,

        '__next__' => -> (args = [], **kwargs) do
          args.get(0).ivars['enum'].next
        end
      })

      Pyrb::Sys.exports['modules'][__FILE__] = self
    end

    Groupby = Groupby_rb.exports['groupby']
  end
end
