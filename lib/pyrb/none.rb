module Pyrb
  module None_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['NoneType'] = PythonClass.new('NoneType', [Pyrb::Object], {
      '__name__' => 'NoneType'
    })
  end

  None = None_rb.exports['NoneType'].call
end
