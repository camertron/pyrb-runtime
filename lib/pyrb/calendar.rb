require 'date'

module Pyrb
  module Calendar_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['Calendar'] = Pyrb::PythonClass.new('Calendar', [Pyrb::Object], {
      'itermonthdates' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        year = args.get(1)
        month = args.get(2)

        start_date = ::Date.new(year, month, 1)
        end_date = start_date.next_month.prev_day

        # python goes from Monday (wday == 1) to Sunday (wday == 0)
        while start_date.wday != 1
          start_date = start_date.prev_day
        end

        while end_date.wday != 0
          end_date = end_date.next_day
        end

        Enumerator.new do |y|
          start_date.upto(end_date) { |d| y << Pyrb::Date.wrap(d) }
        end
      end
    })

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Calendar = Calendar_rb.exports['Calendar']
end
