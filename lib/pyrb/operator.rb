module Pyrb
  module Operator_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['mul'] = -> (args = [], **kwargs) do
      args.get(0) * args.get(1)
    end

    exports['__mul__'] = -> (args = [], **kwargs) do
      args.get(0) * args.get(1)
    end

    exports['length_hint'] = -> (args = [], **kwargs) do
      obj = args.get(0)
      default = args.get(1) || 0

      if Pyrb.hasattr.call([obj, '__len__'])
        obj.fcall('__len__')
      elsif Pyrb.hasattr.call([obj, '__length_hint__'])
        obj.fcall('__length_hint__')
      else
        default
      end
    end

    exports['itemgetter'] = PythonClass.new('itemgetter', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.ivars['items'] = Array(args.get(1))
      end
    })

    exports['itemgetter'].klass.class_eval do
      def call(args = [], **kwargs)
        obj = args.get(0)

        if ivars['items'].size > 1
          ivars['items'].map { |idx| obj.fcall('__getattr__', [idx]) }
        else
          obj.fcall('__getitem__', [ivars['items'].first])
        end
      end
    end

    exports['itemgetter'].instance_eval do
      def call(args = [], **kwargs)
        new(args, **kwargs)
      end
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
