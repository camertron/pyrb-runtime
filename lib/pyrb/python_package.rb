module Pyrb
  class PythonPackage
    attr_reader :path, :package_name, :attrs

    def initialize(path, package_name)
      @path = path
      @package_name = package_name
      @attrs = PackageAttrs.new(self)
    end

    def attr(name)
      attrs[name]
    end
  end
end
