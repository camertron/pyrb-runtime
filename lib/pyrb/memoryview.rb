module Pyrb
  module Memoryview_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    struct, _ = import('struct')

    exports['memoryview'] = PythonClass.new('memoryview', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, obj: nil) do |self_, obj|
        Pyrb.check_buf(obj)

        self_.ivars['buffer'] = obj.getbuffer
        self_.attrs['format'] = obj.getbuffer.fmt
        self_.attrs['ndim'] = obj.getbuffer.ndim
        self_.attrs['shape'] = obj.getbuffer.shape
      end,

      '__len__' => Pyrb.defn(self: nil) do |self_|
        self_.getbuffer.length
      end,

      '__getitem__' => Pyrb.defn(self: nil, index: nil) do |self_, idx|
        case idx
          when ::Range, Pyrb::Range.klass
            if self_.getbuffer.ndim > 1
              # Python also raises this
              raise Pyrb::NotImplementedError, 'multi-dimensional sub-views are not implemented'
            end

            exports['memoryview'].new([self_.getbuffer[idx]])
          else
            self_.getbuffer[idx]
        end
      end,

      'tobytes' => Pyrb.defn(self: nil) do |self_|
        Pyrb::Bytes.new([self_.getbuffer.data])
      end,

      'tolist' => Pyrb.defn(self: nil) do |self_|
        self_.tolist_helper(0, 0)
      end,

      'cast' => Pyrb.defn(self: nil, format: nil, shape: { default: nil }) do |self_, format_, shape|
        buf = self_.getbuffer

        new_buf = Pyrb::Buffer.new(
          self_.getbuffer.data, self_,
          range: buf.range,
          fmt: format_,
          shape: shape,
          readonly: buf.readonly
        )

        exports['memoryview'].new([new_buf])
      end
    })

    exports['memoryview'].properties['obj'] = Pyrb::Property.new([Pyrb.defn(self: nil) do |self_|
      self_.getbuffer.exporter
    end])

    exports['memoryview'].instance_module.class_eval do
      def getbuffer
        ivars['buffer']
      end

      def offset_for(idx)
        getbuffer.offset_for(idx)
      end

      def to_contiguous
        getbuffer.to_contiguous
      end

      def tolist_helper(shape_idx, idx)
        shape = getbuffer.shape
        itemsize = getbuffer.itemsize

        Array.new(shape[shape_idx]) do |i|
          if shape_idx.lt(shape.size - 1)
            n = (shape_idx + 1).upto(shape.size - 1).inject(i) { |prod, dim| prod * shape[dim] }
            tolist_helper(shape_idx + 1, idx + n)
          else
            self[idx + i]
          end
        end
      end
    end
  end

  Memoryview = Memoryview_rb.exports['memoryview']
end
