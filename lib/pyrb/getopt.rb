# encoding: utf-8

require 'pyrb'

module Lib_pyrb_getopt
  def self.location
    __FILE__
  end

  extend Pyrb::Runtime
  using Pyrb::Runtime

  os, _ = import('os')
  sys, _ = import('sys')

  <<~__DOC__
  Parser for command line options.

  This module helps scripts to parse the command line arguments in
  sys.argv.  It supports the same conventions as the Unix getopt()
  function (including the special meanings of arguments of the form `-'
  and `--').  Long options similar to those supported by GNU software
  may be used as well via an optional third argument.  This module
  provides two functions and an exception:

  getopt() -- Parse command line options
  gnu_getopt() -- Like getopt(), but allow option and non-option arguments
  to be intermixed.
  GetoptError -- exception (class) raised with 'opt' attribute, which is the
  option involved with the exception.
  __DOC__

  exports['__all__'] = ["GetoptError", "error", "getopt", "gnu_getopt"]
  begin
    _tmp_, _ = import('gettext', from: 'gettext')
    _ = _tmp_
  rescue *Pyrb.rezcue(Pyrb::ImportError)
    _ = Pyrb.defn(s: nil) do |s|
      throw(:return, s)
    end

  end

  exports['GetoptError'] = Pyrb::PythonClass.new('GetoptError', [Pyrb::Exception]).tap do |klass|
    klass.attrs['opt'] = ""
    klass.attrs['msg'] = ""
    klass.attrs['__init__'] = Pyrb.defn(self: nil, msg: nil, opt: { default: "" }) do |self_, msg, opt|
      self_.attrs['msg'] = msg
      self_.attrs['opt'] = opt
      Pyrb::Exception.fcall('__init__', [self_, msg, opt])
    end

    klass.attrs['__str__'] = Pyrb.defn(self: nil) do |self_|
      throw(:return, self_.attr('msg'))
    end
  end

  GetoptError = exports['GetoptError']

  exports['error'] = exports['GetoptError']
  exports['getopt'] = Pyrb.defn(args: nil, shortopts: nil, longopts: { default: [] }) do |args, shortopts, longopts|
    <<~__DOC__
    getopt(args, options[, long_options]) -> opts, args

    Parses command line options and parameter list.  args is the
    argument list to be parsed, without the leading reference to the
    running program.  Typically, this means "sys.argv[1:]".  shortopts
    is the string of option letters that the script wants to
    recognize, with options that require an argument followed by a
    colon (i.e., the same format that Unix getopt() uses).  If
    specified, longopts is a list of strings with the names of the
    long options which should be supported.  The leading '--'
    characters should not be included in the option name.  Options
    which require an argument should be followed by an equal sign
    ('=').

    The return value consists of two elements: the first is a list of
    (option, value) pairs; the second is the list of program arguments
    left after the option list was stripped (this is a trailing slice
    of the first argument).  Each option-and-value pair returned has
    the option as its first element, prefixed with a hyphen (e.g.,
    '-x'), and the option argument as its second element, or an empty
    string if the option has no argument.  The options occur in the
    list in the same order in which they were found, thus allowing
    multiple occurrences.  Long and short options may be mixed.
    __DOC__

    opts = []
    if !!(Pyrb.type.call([longopts]) == Pyrb.type.call([""]))
      longopts = [longopts]
    else
      longopts = Pyrb::List.call([longopts])
    end

    while !!(args.and { args[0].fcall('startswith', ["-"]) }.and { args[0] != "-" })
      if !!(args[0] == "--")
        args = args[1..-1]
        break
      end

      if !!(args[0].fcall('startswith', ["--"]))
        opts, args = Pyrb.deconstruct(2, exports['do_longs'].call([opts, args[0][2..-1], longopts, args[1..-1]]))
      else
        opts, args = Pyrb.deconstruct(2, exports['do_shorts'].call([opts, args[0][1..-1], shortopts, args[1..-1]]))
      end

    end

    throw(:return, [opts, args])
  end

  exports['gnu_getopt'] = Pyrb.defn(args: nil, shortopts: nil, longopts: { default: [] }) do |args, shortopts, longopts|
    <<~__DOC__
    getopt(args, options[, long_options]) -> opts, args

    This function works like getopt(), except that GNU style scanning
    mode is used by default. This means that option and non-option
    arguments may be intermixed. The getopt() function stops
    processing options as soon as a non-option argument is
    encountered.

    If the first character of the option string is `+', or if the
    environment variable POSIXLY_CORRECT is set, then option
    processing stops as soon as a non-option argument is encountered.
    __DOC__

    opts = []
    prog_args = []
    if !!(Pyrb.isinstance.call([longopts, Pyrb::Str]))
      longopts = [longopts]
    else
      longopts = Pyrb::List.call([longopts])
    end

    if !!(shortopts.fcall('startswith', ["+"]))
      shortopts = shortopts[1..-1]
      all_options_first = true
    elsif !!(os.attr('environ').fcall('get', ["POSIXLY_CORRECT"]))
      all_options_first = true
    else
      all_options_first = false
    end

    while !!(args)
      if !!(args[0] == "--")
        prog_args += args[1..-1]
        break
      end

      if !!(args[0][0...2] == "--")
        opts, args = Pyrb.deconstruct(2, exports['do_longs'].call([opts, args[0][2..-1], longopts, args[1..-1]]))
      elsif !!((args[0][0...1] == "-").and { args[0] != "-" })
        opts, args = Pyrb.deconstruct(2, exports['do_shorts'].call([opts, args[0][1..-1], shortopts, args[1..-1]]))
      else
        if !!(all_options_first)
          prog_args += args
          break
        else
          prog_args.fcall('append', [args[0]])
          args = args[1..-1]
        end

      end

    end

    throw(:return, [opts, prog_args])
  end

  exports['do_longs'] = Pyrb.defn(opts: nil, opt: nil, longopts: nil, args: nil) do |opts, opt, longopts, args|
    begin
      i = opt.fcall('index', ["="])
    rescue *Pyrb.rezcue(Pyrb::ValueError)
      optarg = nil
    else
      opt, optarg = Pyrb.deconstruct(2, opt[0...i], opt[i + 1..-1])
    end

    has_arg, opt = Pyrb.deconstruct(2, exports['long_has_args'].call([opt, longopts]))
    if !!(has_arg)
      if !!(optarg.object_id == nil.object_id)
        if !!((args).not)
          Pyrb.raize(exports['GetoptError'].call([(_.call(["option --%s requires argument"]) % opt), opt]))
        end

        optarg, args = Pyrb.deconstruct(2, args[0], args[1..-1])
      end

    elsif !!(optarg.object_id != nil.object_id)
      Pyrb.raize(exports['GetoptError'].call([(_.call(["option --%s must not have an argument"]) % opt), opt]))
    end

    opts.fcall('append', [["--" + opt, optarg.or { "" }]])
    throw(:return, [opts, args])
  end

  exports['long_has_args'] = Pyrb.defn(opt: nil, longopts: nil) do |opt, longopts|
    possibilities = longopts.fcall('__iter__').each_with_object([]) do |o, _ret_|
      if !!(o.fcall('startswith', [opt]))
        _ret_ << o
      end
    end

    if !!((possibilities).not)
      Pyrb.raize(exports['GetoptError'].call([(_.call(["option --%s not recognized"]) % opt), opt]))
    end

    if !!((opt).in?(possibilities))
      throw(:return, [false, opt])
    elsif !!((opt + "=").in?(possibilities))
      throw(:return, [true, opt])
    end

    if !!(Pyrb.len.call([possibilities]) > 1)
      Pyrb.raize(exports['GetoptError'].call([(_.call(["option --%s not a unique prefix"]) % opt), opt]))
    end

    Pyrb.assert(Pyrb.len.call([possibilities]) == 1)
    unique_match = possibilities[0]
    has_arg = unique_match.fcall('endswith', ["="])
    if !!(has_arg)
      unique_match = unique_match[0...-1]
    end

    throw(:return, [has_arg, unique_match])
  end

  exports['do_shorts'] = Pyrb.defn(opts: nil, optstring: nil, shortopts: nil, args: nil) do |opts, optstring, shortopts, args|
    while !!(optstring != "")
      opt, optstring = Pyrb.deconstruct(2, optstring[0], optstring[1..-1])
      if !!(exports['short_has_arg'].call([opt, shortopts]))
        if !!(optstring == "")
          if !!((args).not)
            Pyrb.raize(exports['GetoptError'].call([(_.call(["option -%s requires argument"]) % opt), opt]))
          end

          optstring, args = Pyrb.deconstruct(2, args[0], args[1..-1])
        end

        optarg, optstring = Pyrb.deconstruct(2, optstring, "")
      else
        optarg = ""
      end

      opts.fcall('append', [["-" + opt, optarg]])
    end

    throw(:return, [opts, args])
  end

  exports['short_has_arg'] = Pyrb.defn(opt: nil, shortopts: nil) do |opt, shortopts|
    (Pyrb.range.call([Pyrb.len.call([shortopts])])).each do |i|
      if !!(opt == shortopts[i] && shortopts[i] != ":")
        throw(:return, shortopts.fcall('startswith', [":", i + 1]))
      end
    end
    Pyrb.raize(exports['GetoptError'].call([(_.call(["option -%s not recognized"]) % opt), opt]))
  end

  if !!(__name__ == "__main__")
    Pyrb.print.call([exports['getopt'].call([sys.attr('argv')[1..-1], "a:b", ["alpha=", "beta"]])])
  end

  Pyrb::Sys.exports['modules'][__FILE__] = self
end
