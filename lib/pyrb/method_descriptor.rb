module Pyrb
  module MethodDescriptor_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['method_descriptor'] = PythonClass.new('method_descriptor', [Pyrb::Object])

    exports['method_descriptor'].klass.class_eval do
      def self.wrap(ref, methd)
        allocate.tap do |inst|
          inst.init
          inst.attrs['__self__'] = ref
          inst.attrs['__func__'] = methd
        end
      end

      def call(args = [], **kwargs)
        # use #get here to prevent the function from being recursively wrapped
        # (because it's a proc)
        attrs.get('__func__').call([attrs.get('__self__')] + args, **kwargs)
      end
    end
  end

  MethodDescriptor = MethodDescriptor_rb.exports['method_descriptor']
end
