module Pyrb
  # adapted from https://stackoverflow.com/a/15813687/498080
  class NamedModule < Module
    attr_accessor :name

    def initialize(name, &block)
      super(&block)
      @name = name
    end

    def inspect
      "#<#{name}:0x#{(object_id << 1).to_s(16).rjust(16, '0')}>"
    end
  end
end
