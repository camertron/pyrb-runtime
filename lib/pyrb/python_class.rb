require 'thread'

module Pyrb
  class PythonClass
    attr_reader :attrs, :class_name, :parents, :ivars, :__subclasses__, :source_location

    # NOTE: you can only define instance methods via init_attrs
    def initialize(class_name, parents, init_attrs = {})
      @class_name = class_name
      @parents = parents
      @attrs = ClassAttrs.new(self)

      init_attrs.each do |name, init_prop|
        @attrs[name] = init_prop
      end

      @ivars = {}

      # stuff accessed by (converted) python code
      @__subclasses__ = -> (args = [], **kwargs) { @subclass_list ||= [] }
      attrs['__subclasses__'] = @__subclasses__
      attrs['__name__'] = class_name
      attrs['__module__'] = (Thread.current[:pyrb_current_module] || []).last
      attrs['__class__'] = self

      singleton_class.instance_eval do
        def attrs
          @attrs
        end

        def fcall(func_name, args = [], **kwargs, &block)
          @attrs[func_name].call(args, **kwargs, &block)
        end

        def attr(name)
          @attrs[name]
        end
      end

      singleton_class.instance_variable_set(:@attrs, @attrs)
      @source_location = caller[1].split(':in ').first

      parents.each do |parent|
        parent.add_subclass(self)
      end
    end

    def parent_of?(subclass)
      @subclass_list ||= []
      @subclass_list.include?(subclass)
    end

    def add_subclass(subclass)
      @subclass_list ||= []
      @subclass_list << subclass
    end

    def instance_module
      @instance_module ||= begin
        proto = self

        NamedModule.new("#{class_name}.InstMod") do
          proto.parents.each { |p| include(p.instance_module) }
        end
      end
    end

    def new(args = [], **kwargs, &block)
      klass.new.tap do |instance|
        if instance.hasattr('__init__')
        # if Pyrb.hasattr.call([instance, '__init__'])
          instance.fcall('__init__', args, **kwargs)
        end
      end
    end

    def properties
      @properties ||= Properties.new(self)
    end

    def attr(name)
      return @properties[name] if @properties && @properties.include?(name)
      @attrs[name]
    end

    def wrap(*args)
      klass.wrap(*args)
    end

    alias_method :eq, :==

    def ==(other)
      eq(other)
    end

    alias_method :neq, :!=

    def !=(other)
      neq(other)
    end

    def __dict__
      # this can't be done in the constructor because Pyrb::Dict is itself a
      # PythonClass and therefore causes circular dependency issues when required
      Pyrb::Dict.new.tap do |dict|
        @parents.each do |parent|
          dict.fcall('update', [parent.attrs['__dict__']])
        end

        dict.fcall('update', [@attrs.to_h])
        dict.fcall('update', [properties])
      end
    end

    def call(args = [], **kwargs)
      new(args, **kwargs)
    end

    def klass
      @klass ||= begin
        proto = self

        Class.new(ruby_super) do
          include PythonInstance
          include proto.instance_module

          @prototype = proto

          class << self
            attr_reader :prototype

            def inspect
              prototype.class_name
            end

            def hasattr(name)
              prototype.hasattr(name)
            end
          end
        end
      end
    end

    def inspect
      class_name
    end

    def fcall(func_name, args = [], **kwargs, &block)
      @attrs[func_name].call(args, **kwargs, &block)
    end

    def method_missing(func_name, *args, **kwargs, &block)
      if klass.respond_to?(func_name.to_s)
        return klass.send(func_name, *args, **kwargs, &block)
      end

      if attr(func_name.to_s).respond_to?(:call)
        klass.define_singleton_method(func_name) do |*args, **kwargs, &block|
          prototype.fcall(func_name.to_s, args, **kwargs, &block)
        end
      else
        klass.define_singleton_method(func_name) do |*_args, **_kwargs, &_block|
          prototype.attr(func_name.to_s)
        end
      end

      klass.send(func_name, *args, **kwargs, &block)
    end

    def respond_to?(func_name, _include_all = true)
      Pyrb.hasattr.call([self, func_name]) || super
    end

    def hasattr(name)
      @attrs.hasattr(name)
    end

    private

    def ruby_super
      if attrs.self_include?('__ruby_super__')
        attrs['__ruby_super__']
      elsif parents.include?(Pyrb::Exception_rb.exports['Exception'])
        ::Exception
      else
        ::Object
      end
    end
  end
end
