module Pyrb
  module StringFormat
    class Parser
      attr_reader :tokenizer, :current_token

      def initialize(tokenizer)
        @tokenizer = tokenizer
        @current_token = tokenizer.get
      end

      def parse
        Pattern.new.tap do |pattern|
          consume(:OPEN_CURLY)
          pattern.field_name = field_name
          pattern.conversion = conversion
          pattern.format_spec = format_spec
          consume(:CLOSE_CURLY)
        end
      end

      def field_name
        case current_token.type
          when :EXCLAM, :COLON
            nil
          else
            name = arg_name

            # usually happens if we encounter a completely empty pattern (i.e. '{}')
            unless name.to_s.empty?
              FieldName.new.tap do |fn|
                fn.arg_name = name
                fn.trailers.concat(trailers)
              end
            end
        end
      end

      def conversion
        if current_token.type == :EXCLAM
          return Conversion.new.tap do |conv|
            consume(:EXCLAM)

            case current_token.value
              when 's'
                conv.type = :STR
              when 'r'
                conv.type = :REPR
              when 'a'
                conv.type = :ASCII
            end

            consume(:LITERAL)
          end
        end

        nil
      end

      def format_spec
        FormatSpec.new.tap do |spec|
          if current_token.type == :COLON
            consume(:COLON)

            if current_token.type == :ALIGN
              spec.align = align
              consume(:ALIGN)
            elsif tokenizer.peek.type == :ALIGN
              spec.fill = current_token.value
              consume(current_token.type)
              spec.align = align
              consume(:ALIGN)
            end

            if current_token.type == :SIGN
              spec.sign = sign
              consume(:SIGN)
            end

            if current_token.type == :POUND
              spec.alternate_form = true
              consume(:POUND)
            end

            if current_token.type == :DIGITS
              if current_token.value.start_with?('0')
                spec.zero = true

                if current_token.value.size > 1
                  spec.width = current_token.value[1..-1].to_i
                end
              else
                spec.width = current_token.value.to_i
              end

              consume(:DIGITS)
            end

            if current_token.type == :GROUPING
              spec.grouping = current_token.value
              consume(:GROUPING)
            end

            if current_token.type == :PERIOD
              consume(:PERIOD)
              spec.precision = current_token.value.to_i
              consume(:DIGITS)
            end

            if current_token.type == :LITERAL
              spec.type = current_token.value
              consume(:LITERAL)
            end
          end
        end
      end

      def align
        case current_token.value
          when '<'
            :LEFT
          when '>'
            :RIGHT
          when '^'
            :CENTER
          else
            :NUMERIC_PADDING
        end
      end

      def sign
        case current_token.value
          when '-'
            :NEGATIVE
          when '+'
            :POSITIVE
          else
            :SPACE
        end
      end

      def trailers
        [].tap do |result|
          loop do
            case current_token.type
              when :PERIOD
                consume(:PERIOD)
                result << DotTrailer.new(identifier)

              when :OPEN_BRACKET
                consume(:OPEN_BRACKET)
                result << SubscriptTrailer.new(elementIndex)
                consume(:CLOSE_BRACKET)

              else
                break
            end
          end
        end
      end

      def element_index
        if current_token.type == :DIGITS
          return current_token.value.tap { consume(:DIGITS) }
        end

        # <any source character except "]"> +
        ''.tap do |result|
          until current_token.value == ']'
            result << current_token.value
            consume(current_token.type)
          end
        end
      end

      def arg_name
        if current_token.type == :DIGITS
          return current_token.value.to_i.tap { consume(:DIGITS) }
        end

        identifier
      end

      def identifier
        ''.tap do |result|
          while current_token.type == :LITERAL
            result << current_token.value
            consume(:LITERAL)
          end
        end
      end

      def consume(token_type)
        if current_token.type == token_type
          @current_token = tokenizer.get
        else
          raise RuntimeError,
            "Unexpected token '" + current_token.type.to_s + "', expected '" + token_type.to_s + "' instead"
        end
      end
    end
  end
end
