module Pyrb
  module StringFormat
    class FieldName
      attr_accessor :arg_name, :trailers

      def initialize
        @trailers = []
      end

      def arg_for(args)
        # TODO: handle trailers
        args[arg_name]
      end
    end

    class FormatSpec
      attr_accessor :fill, :sign, :align, :type, :alternate_form
      attr_accessor :zero, :width, :grouping, :precision

      alias_method :alternate_form?, :alternate_form
      alias_method :zero?, :zero

      def initialize
        @alternate_form = false
        @zero = false
      end
    end

    class Conversion
      attr_accessor :type
    end
  end
end
