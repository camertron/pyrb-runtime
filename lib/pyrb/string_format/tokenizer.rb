require 'strscan'

module Pyrb
  module StringFormat
    class Tokenizer
      TOKENS = {
        OPEN_CURLY: /\{/,
        CLOSE_CURLY: /\}/,
        OPEN_BRACKET: /\[/,
        CLOSE_BRACKET: /\]/,
        EXCLAM: /!/,
        POUND: /#/,
        COLON: /:/,
        PERIOD: /\./,
        ALIGN: /[<>=^]/,
        SIGN: /[-+ ]/,
        DIGITS: /\d+/,
        GROUPING: /[_,]/,
        LITERAL: /./
      }

      def initialize(input)
        @scanner = StringScanner.new(input)
      end

      def has_next?
        TOKENS.any? { |_, re| @scanner.exist?(re) }
      end

      def get
        if @next_token
          @scanner.pos += @next_token.value.length
          t = @next_token
          @next_token = nil
          return t
        end

        type, re = TOKENS.find { |_, re| @scanner.match?(re) }
        return nil unless type
        value = @scanner.scan(re)
        Token.new(type, value)
      end

      def peek
        return @next_token if @next_token
        type, re = TOKENS.find { |_, re| @scanner.match?(re) }
        return nil unless type
        value = @scanner.matched
        @next_token = Token.new(type, value)
      end
    end
  end
end
