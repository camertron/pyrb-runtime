module Pyrb
  module StringFormat
    module Converter
      class << self
        using Pyrb::Runtime

        def convert(arg, type)
          case type
            when :STR
              arg.fcall('__str__')
            when :REPR
              arg.fcall('__repr__')
            else
              # ascii not supported at the moment
              raise RuntimeError, "'#{type}' conversion not supported"
          end
        end
      end
    end

    class Pattern
      DEFAULT_PRECISION = 6

      DEFAULT_STRING_TYPE = 's'.freeze
      DEFAULT_INTEGER_TYPE = 'd'.freeze
      DEFAULT_FLOAT_TYPE = 'fd'.freeze  # float default (I made this up)

      DEFAULT_STRING_ALIGNMENT = :LEFT
      DEFAULT_INTEGER_ALIGNMENT = :RIGHT
      DEFAULT_FLOAT_ALIGNMENT = :RIGHT

      attr_accessor :field_name, :conversion, :format_spec

      def apply_to(args, idx)
        arg = arg_for(args, idx)
        arg = convert(arg)
        type = type_for(arg)
        format_as_type(arg, type)
      end

      private

      def arg_for(args, idx)
        if field_name
          field_name.arg_for(args)
        else
          args[idx]
        end
      end

      def convert(arg)
        return arg unless conversion
        Converter.convert(arg, conversion.type)
      end

      def type_for(arg)
        format_spec.type ||
          case arg
            when ::String then DEFAULT_STRING_TYPE
            when ::Integer then DEFAULT_INTEGER_TYPE
            when ::Float then DEFAULT_FLOAT_TYPE
          end
      end

      def alignment_for(arg)
        format_spec.align ||
          case arg
            when ::String then DEFAULT_STRING_ALIGNMENT
            when ::Integer then DEFAULT_INTEGER_ALIGNMENT
            when ::Float then DEFAULT_FLOAT_ALIGNMENT
          end
      end

      def sign_for(arg)
        if arg < 0
          '-'
        else
          case format_spec.sign
            when :POSITIVE then '+'
            when :SPACE then ' '
            else
              ''
          end
        end
      end

      def precision
        format_spec.precision || DEFAULT_PRECISION
      end

      def grouping
        format_spec.grouping || ''
      end

      def alternate_form?
        format_spec.alternate_form?
      end

      def zero?
        format_spec.zero?
      end

      def width
        format_spec.width || 0
      end

      def fill
        format_spec.fill || ' '
      end

      def format_as_type(arg, type)
        case type
          when 's'
            arg.to_s
          when 'b'
            format_binary(arg)
          when 'c'
            format_char(arg)
          when 'd'
            format_decimal(arg)
          when 'o'
            format_octal(arg)
          when 'x'
            format_hex(arg, :lower)
          when 'X'
            format_hex(arg, :upper)
          when 'n'
            format_number(arg)
          when 'e'
            format_sci(arg, :lower)
          when 'E'
            format_sci(arg, :upper)
          when 'f'
            format_fixed(arg, :lower)
          when 'F'
            format_fixed(arg, :upper)
          when 'g'
            format_general(arg, :lower)
          when 'G'
            format_general(arg, :upper)
          when '%'
            format_percent(arg)
          when 'fd'
            format_float_default(arg)
        end
      end

      def format_binary(arg)
        bin = group(arg.abs.to_s(2), 4)
        sign = sign_for(arg)
        prefix = alternate_form? ? "#{sign}0b" : sign
        format_number_from_parts(arg, prefix, bin)
      end

      def format_char(arg)
        [arg].pack('U*')
      end

      def format_decimal(arg)
        num = group(arg.to_i.abs.to_s)
        sign = sign_for(arg)
        format_number_from_parts(arg, sign, num)
      end

      def format_octal(arg)
        oct = group(arg.abs.to_s(8), 4)
        sign = sign_for(arg)
        prefix = alternate_form? ? "#{sign}0o" : sign
        format_number_from_parts(arg, prefix, oct)
      end

      def format_hex(arg, flavor)
        hex = arg.abs.to_s(16)
        hex = group(flavor == :upper ? hex.upcase : hex, 4)
        sign = sign_for(arg)
        prefix = alternate_form? ? "#{sign}0x" : sign
        format_number_from_parts(arg, prefix, hex)
      end

      def format_number_from_parts(arg, prefix, suffix, align_pad = true)
        return "#{prefix}#{suffix}" unless align_pad
        alignment = alignment_for(arg)

        if zero? || alignment == :NUMERIC_PADDING
          "#{prefix}#{align_and_pad(suffix, alignment, width - prefix.size)}"
        else
          align_and_pad("#{prefix}#{suffix}", alignment, width)
        end
      end

      def format_number(arg)
        # TODO: use locale-specific separators
        case arg
          when ::Float
            format_general(arg, :lower)
          when ::Integer
            format_decimal(arg)
        end
      end

      def format_sci(arg, flavor, pre = precision, remove_trailing = false)
        exp = Math.log10(arg.abs).floor
        mantissa = arg.to_f / (10 ** exp)
        mantissa = format_fixed(mantissa, flavor, pre, remove_trailing, false, false)
        exp = "#{exp < 0 ? '-' : '+'}#{exp.abs.to_s.rjust(2, '0')}"
        e = flavor == :upper ? 'E' : 'e'
        format_number_from_parts(arg, sign_for(arg), "#{mantissa}#{e}#{exp}")
      end

      def format_fixed(arg, flavor, pre = precision, remove_trailing = false, align_pad = true, include_sign = true)
        if arg.infinite?
          flavor == :lower ? 'inf' : 'INF'
        elsif arg.to_f.nan?
          flavor == :lower ? 'nan' : 'NAN'
        else
          res = arg.to_f.abs.round(pre).to_s

          if pre > 0
            int, dec = res.split('.')

            if remove_trailing
              dec.sub!(/0+\z/, '')
              res = "#{group(int)}#{dec}"
            else
              res = "#{group(int)}.#{dec.ljust(pre, '0')}"
            end
          end

          sign = include_sign ? sign_for(arg) : ''
          format_number_from_parts(arg, sign, res, align_pad)
        end
      end

      def format_general(arg, flavor)
        # precision if 0 treated the same as a precision of 1
        precision = 1 if precision == 0
        exp = Math.log10(arg.abs).floor

        if exp >= -4 && exp < precision
          format_fixed(arg, flavor, precision - 1 - exp, true)
        else
          format_sci(arg, flavor, precision - 1, true)
        end
      end

      def format_percent(arg)
        "#{sign_for(arg)}#{format_fixed(arg * 100)}%"
      end

      def format_float_default(arg, flavor)
        int, dec = arg.to_s.split('.')
        pre = (dec || '0').length
        format_general(arg, :lower, pre)
      end

      def group(str, chunk_size = 3)
        return str if str.length <= chunk_size || grouping == ''

        ''.tap do |result|
          first_chunk_len = str.length % chunk_size

          unless first_chunk_len == 0
            result << str[0...first_chunk_len]
            result << grouping
          end

          (first_chunk_len...str.length).step(chunk_size).with_index do |i, idx|
            result << grouping if idx > 0
            result << str[i...(i + chunk_size)]
          end
        end
      end

      def align_and_pad(str, alignment, len)
        case alignment
          when :LEFT
            str.ljust(len, fill)
          when :RIGHT, :NUMERIC_PADDING
            str.rjust(len, fill)
          when :CENTER
            str.center(len, fill)
          else
            str
        end
      end
    end
  end
end
