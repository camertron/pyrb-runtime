require 'pyrb'
require 'absolver'

module Pyrb
  module Runtime
    FORMAT_RE    = /(?<!%)(%(?:[a-z]|\([^)]+\)[a-z]))/.freeze
    ANON_FMT_RE  = /%([a-z])/.freeze
    NAMED_FMT_RE = /%\(([^)]+)\)([a-z])/.freeze

    refine ::String do
      alias_method :each, :each_char

      def attrs
        ::Pyrb::String.attrs
      end

      def is_a?(type)
        type == ::Pyrb::String ||
          type == ::Pyrb::String.klass ||
          type == ::Pyrb::Str ||
          type == ::Pyrb::Str.klass ||
          super
      end

      alias_method :get, :[]

      def [](idx)
        case idx
          when ::Integer
            raise Pyrb::IndexError.klass, 'string index out of range' if idx >= size
            get(idx)
          when ::Range
            get(idx)
          when ::Pyrb::Range.klass
            ''.tap { |str| idx.apply_to(self) { |i| str << get(i) } }
        end
      end

      alias_method :interp, :%

      def %(replacements)
        if !replacements.is_a?(Pyrb::Dict.klass)
          replacements = Array(replacements)
        end

        split(FORMAT_RE)
          .each_slice(2)
          .with_index
          .each_with_object('') do |((str, var), idx), ret|
            ret << str
            next unless var

            replacement, fmt = case var
              when NAMED_FMT_RE
                [replacements[$1], $2]
              when ANON_FMT_RE
                [replacements[idx], $1]
              else
                raise Pyrb::AttributeError.klass, "unexpected string format #{var}"
            end

            ret << case fmt
              when 'r'
                replacement.fcall('__repr__')
              when 's'
                replacement.fcall('__str__')
              else
                sprintf(var, replacement)
            end
          end
      end

      def !
        eql?('')
      end

      alias_method :not, :!

      alias_method :eq, :==

      def ==(other)
        fcall('__eq__', [other])
      end

      alias_method :neq, :!=

      def !=(other)
        fcall('__ne__', [other])
      end

      alias_method :lt, :<
      alias_method :gt, :>
      alias_method :le, :<=
      alias_method :ge, :>=
    end

    INT_OP_MSG = "unsupported operand type(s) for +: 'int' and '%s'".freeze

    module IntUtils
      def self.check_int_op_type(other)
        unless other.is_a?(::Numeric)
          raise Pyrb::TypeError.klass, INT_OP_MSG % (Pyrb.type.call([other]).prototype.class_name)
        end
      end
    end

    refine ::Integer do
      def attrs
        ::Pyrb::Int.attrs
      end

      def is_a?(type)
        type == ::Pyrb::Int || type == ::Pyrb::Int.klass || super
      end

      def !
        eql?(0)
      end

      alias_method :not, :!
      alias_method :div, :/

      # In python, dividing two integers produces a float.
      #
      # In ruby, dividing a float by zero returns Infinity. This is inconsistent
      # with python, which blows up with a ZeroDivisionError.
      #
      def /(other)
        IntUtils.check_int_op_type(other)
        raise(Pyrb::ZeroDivisionError.klass, 'division by zero') if other == 0
        to_f / other
      end

      alias_method :mul, :*

      def *(other)
        case other
          when ::String, ::Pyrb::Bytes.klass
            # ruby wants this the other way around, i.e. string * int, not int * string
            other.value * value
          else
            IntUtils.check_int_op_type(other)
            super(other.value)
        end
      end

      # we do this because in python, hash(6.0) == hash(6)
      def eql?(other)
        value == other
      end

      # we do this because in python, hash(6.0) == hash(6)
      def hash
        to_f.hash
      end

      def call(args = [], **kwargs)
        Pyrb::Int.call(args, **kwargs)
      end

      alias_method :eq, :==

      def ==(other)
        fcall('__eq__', [other])
      end

      alias_method :neq, :!=

      def !=(other)
        fcall('__ne__', [other])
      end

      alias_method :gt, :>

      def >(other)
        fcall('__gt__', [other])
      end

      alias_method :lt, :<

      def <(other)
        fcall('__lt__', [other])
      end

      alias_method :le, :<=

      def <=(other)
        fcall('__le__', [other])
      end

      alias_method :ge, :>=

      def >=(other)
        fcall('__ge__', [other])
      end

      alias_method :add, :+

      def +(other)
        IntUtils.check_int_op_type(other)
        super
      end

      alias_method :sub, :-

      def -(other)
        IntUtils.check_int_op_type(other)
        super
      end
    end

    refine ::Integer.singleton_class do
      def attrs
        Pyrb::Int.attrs
      end

      def attr(key)
        attrs[key]
      end
    end

    refine ::Float do
      def attrs
        ::Pyrb::Float.attrs
      end

      def is_a?(type)
        type == ::Pyrb::Float || type == ::Pyrb::Float.klass || super
      end

      def !
        eql?(0.0)
      end

      alias_method :not, :!

      # we do this because in python, hash(6.0) == hash(6)
      def eql?(other)
        self == other
      end

      alias_method :add, :+
      alias_method :sub, :-
      alias_method :mul, :*
      alias_method :div, :/
      alias_method :lt, :<
      alias_method :gt, :>
      alias_method :le, :<=
      alias_method :ge, :>=
    end

    refine ::Array do
      def attrs
        Pyrb::List.attrs
      end

      def is_a?(type)
        type == ::Pyrb::List || type == ::Pyrb::List.klass || super
      end

      def !
        empty?
      end

      alias_method :get, :[]

      def [](idx)
        case idx
          when ::Integer
            raise Pyrb::IndexError.klass, 'list index out of range' if idx >= size
            get(idx)
          when ::Range
            get(idx)
          when ::Pyrb::Range.klass
            [].tap { |arr| idx.apply_to(self) { |i| arr << get(i) } }
        end
      end

      alias_method :set, :[]=

      def []=(idx, val)
        case idx
          when ::Integer
            raise Pyrb::IndexError.klass, 'list assignment index out of range' if idx >= size
            set(idx, val)
          when ::Range
            # can only assign an iterable via range key
            set(idx, Pyrb::List.call([val]))
          when ::Pyrb::Range.klass
            # apparently this is possible but I don't understand how it works
            raise ArgumentError, 'WTF python'
        end
      end

      def del(idx)
        case idx
          when ::Integer
            delete_at(idx)
          when ::Range
            idx.reverse_each { |i| delete_at(i) }
          when ::Pyrb::Range
            # always iterate from max index to min
            if idx.ivars['step'] < 0
              idx.apply_to(self).reverse_each { |i| delete_at(i) }
            else
              idx.apply_to(self) { |i| delete_at(i) }
            end
        end
      end

      alias_method :eq, :==

      def ==(other)
        fcall('__eq__', [other])
      end

      alias_method :neq, :!=

      def !=(other)
        fcall('__ne__', [other])
      end
    end

    refine ::Array.singleton_class do
      def attrs
        Pyrb::List.attrs
      end

      def attr(key)
        attrs[key]
      end

      def call(args = [])
        args.first
      end
    end

    refine ::Hash do
      alias_method :del, :delete
    end

    refine ::Regexp do
      def attrs
        ::Pyrb::Pattern.attrs
      end
    end

    # is it really correct to equate Enumerator and seq?
    refine ::Enumerator do
      def attrs
        ::Pyrb::Seq.attrs
      end

      def call
        self
      end
    end

    refine ::TrueClass do
      def attrs
        Pyrb::TRUE.attrs
      end

      alias_method :eq, :==

      def ==(other)
        # in python, 0 == False and 1 == True
        other.is_a?(self.class) || other.eq(1)
      end

      alias_method :ne, :!=

      def !=(other)
        !(self == other)
      end

      def is_a?(type)
        type == Pyrb::Bool ||
          type == Pyrb::Bool.klass ||
          super
      end
    end

    refine ::FalseClass do
      def attrs
        Pyrb::FALSE.attrs
      end

      alias_method :eq, :==

      def ==(other)
        # in python, 0 == False and 1 == True
        other.is_a?(self.class) || other.eq(0)
      end

      alias_method :ne, :!=

      def !=(other)
        !(self == other)
      end

      def is_a?(type)
        type == Pyrb::Bool ||
          type == Pyrb::Bool.klass ||
          super
      end
    end

    refine ::NilClass do
      def attrs
        Pyrb::None.attrs
      end

      def fcall(func_name, args = [], **kwargs, &block)
        if attr = attrs.get(func_name)
          attr.call([self] + args, **kwargs, &block)
        else
          raise Pyrb::AttributeError.klass, "object '#{self}' has no attribute '#{func_name}'"
        end
      end

      def value
        self
      end

      def !
        true
      end

      alias_method :not, :!
    end

    refine ::Exception do
      def attrs
        ::Pyrb::Exception.attrs
      end

      def prototype
        self.class
      end
    end

    refine ::Exception.singleton_class do
      def attrs
        ::Pyrb::Exception.attrs
      end

      def prototype
        self
      end
    end

    refine ::StopIteration do
      def prototype
        ::Pyrb::StopIteration
      end
    end

    refine ::StopIteration.singleton_class do
      def prototype
        ::Pyrb::StopIteration
      end
    end

    refine ::Class do
      alias_method :class_name, :name
    end

    refine ::Proc do
      attr_accessor :__pyrb_location__

      def attrs
        @attrs ||= {}
      end

      def fcall(name, args = [], **kwargs)
        case name
          when '__str__', '__repr__'
            inspect
        end
      end

      def source_location_with_python
        @__pyrb_location__ || source_location_without_python
      end

      alias_method :source_location_without_python, :source_location
      alias_method :source_location, :source_location_with_python
    end

    refine ::Range do
      def attrs
        Pyrb::NativeRange.attrs
      end
    end

    refine ::Object do
      def fcall(func_name, args = [], **kwargs, &block)
        if attr = attrs.get(func_name)
          attr.call([self] + args, **kwargs, &block)
        else
          raise Pyrb::AttributeError.klass, "object '#{self}' has no attribute '#{func_name}'"
        end
      end

      def value
        self
      end

      def attr(name)
        a = attrs.get(name)

        if a.is_a?(Proc)
          Pyrb::MethodDescriptor.wrap(self, a)
        else
          a
        end
      end

      def hasattr(name)
        attrs.hasattr(name)
      end

      alias_method :eq, :==

      def ==(other)
        if Pyrb.hasattr.call([self, '__eq__'])
          fcall('__eq__', [other])
        else
          eq(other)
        end
      end

      alias_method :neq, :!=

      def !=(other)
        if Pyrb.hasattr.call([self, '__ne__'])
          fcall('__ne__', [other])
        else
          neq(other)
        end
      end

      def +(other)
        if Pyrb.hasattr.call([self, '__add__'])
          fcall('__add__', [other])
        else
          super
        end
      end

      def -(other)
        if Pyrb.hasattr.call([self, '__sub__'])
          fcall('__sub__', [other])
        else
          super
        end
      end

      def <=>(other)
        if fcall('__gt__', [other])
          1
        elsif fcall('__lt__', [other])
          -1
        else
          0
        end
      end

      def <(other)
        if Pyrb.hasattr.call([self, '__lt__'])
          fcall('__lt__', [other])
        else
          super
        end
      end

      def >(other)
        if Pyrb.hasattr.call([self, '__gt__'])
          fcall('__gt__', [other])
        else
          super
        end
      end

      def <=(other)
        if Pyrb.hasattr.call([self, '__le__'])
          fcall('__le__', [other])
        else
          super
        end
      end

      def >=(other)
        if Pyrb.hasattr.call([self, '__ge__'])
          fcall('__ge__', [other])
        else
          super
        end
      end

      def *(other)
        if Pyrb.hasattr.call([self, '__mul__'])
          fcall('__mul__', [other])
        else
          super
        end
      end

      def [](subscript)
        if Pyrb.hasattr.call([self, '__getitem__'])
          fcall('__getitem__', [subscript])
        else
          raise Pyrb::TypeError.klass, "object '#{self}' is not subscriptable"
        end
      end

      def []=(subscript, new_value)
        if Pyrb.hasattr.call([self, '__setitem__'])
          fcall('__setitem__', [subscript, new_value])
        else
          raise Pyrb::TypeError.klass, "object '#{self}' is not subscriptable"
        end
      end

      def not
        !self
      end

      def or(&block)
        return(block.call) if !self
        self
      end

      def and(&block)
        return self if !self
        block.call
      end

      def in?(seq)
        if Pyrb.hasattr.call([seq, '__contains__'])
          seq.fcall('__contains__', [self])
        else
          seq.include?(self)
        end
      end

      def each(&block)
        Pyrb.iter.call([self]).each(&block)
      end
    end

    # import unittest
    # from hamming import distance
    def import(*exportables, from: nil)
      exportables.map do |exportable|
        module_path = if from
          "#{from}.#{exportable}"
        else
          exportable
        end

        imports[exportable] = Pyrb::Importer.import(module_path, location)
      end
    end

    def exports
      @exports ||= {}
    end

    alias :attrs :exports

    def attr(name)
      return __dict__ if name == '__dict__'
      exports[name]
    end

    def hasattr(name)
      exports.include?(name) || name == '__dict__'
    end

    def imports
      @imports ||= {}
    end

    def __name__
      if $0 == location
        '__main__'
      end
    end

    def fcall(func_name, args = [], **kwargs, &block)
      return __dict__.call if func_name == '__dict__'

      if export = exports[func_name]
        export.call(args, **kwargs, &block)
      else
        raise Pyrb::AttributeError.klass,
          "module '#{File.basename(location).chomp('.rb')}' has no attribute '#{func_name}'"
      end
    end

    def method_missing(func_name, *args, **kwargs, &block)
      if exports[func_name.to_s].is_a?(Proc)
        define_singleton_method(func_name) do |*args, **kwargs, &block|
          fcall(func_name.to_s, args, **kwargs, &block)
        end
      else
        define_singleton_method(func_name) do |*_args, **_kwargs, &_block|
          attr(func_name.to_s)
        end
      end

      send(func_name, *args, **kwargs, &block)
    end

    def respond_to?(func_name, _include_all = true)
      exports.include?(func_name.to_s) || super
    end

    def __dict__
      @__dict__ ||= -> (args = [], **kwargs) do
        exports.keys
      end
    end

    def self.module_stack
      Thread.current[:pyrb_current_module] ||= []
    end
  end

  class << self
    using Runtime

    def clean_backtrace(backtrace)
      backtrace.reject do |frame|
        frame.include?(__FILE__)
      end
    end

    def import(*exportables, from: nil)
      exportables.map do |exportable|
        module_path = if from
          "#{from}.#{exportable}"
        else
          exportable
        end

        Pyrb::Importer.import(module_path, Dir.getwd)
      end
    end

    # this is called directly in ruby, no need to make it a proc
    def int_divide(first, second)
      # can't use normal / here because it's refined
      raise(Pyrb::ZeroDivisionError.klass, 'division by zero') if second == 0
      first.div(second).floor
    end

    def len
      @len ||= -> (args = [], **kwargs) do
        obj = args.get(0)

        if Pyrb.hasattr.call([obj, '__len__'])
          obj.fcall('__len__')
        else
          raise Pyrb::TypeError.klass, "object '#{obj}' has no len()"
        end
      end
    end

    def check_buf(buf)
      unless buf.respond_to?(:getbuffer)
        raise Pyrb::TypeError.klass, "a bytes-like object is required, not '#{buf.attr('__name__')}'"
      end
    end

    def max
      @max ||= -> (args = [], **kwargs) do
        seq = if args.size > 1
          args
        else
          # first arg must be an iterable
          Utils.unwrap_iter(args.first)
        end

        if key_func = kwargs[:key]
          return seq.max_by { |elem| key_func.call([elem]) }
        end

        seq.max
      end
    end

    def min
      @min ||= -> (args = [], **kwargs) do
        seq = if args.size > 1
          args
        else
          # first arg must be an iterable
          Utils.unwrap_iter(args.first)
        end

        if key_func = kwargs[:key]
          return seq.min_by { |elem| key_func.call([elem]) }
        end

        seq.min
      end
    end

    def range
      @range ||= -> (args = [], **kwargs) { Pyrb::Range.new(args, **kwargs) }
    end

    def with
      @with ||= -> (args = [], **kwargs, &block) do
        obj = args.get(0)

        # __around__ is a special additional method that can be
        # defined by PythonClasses in cases where the block
        # needs to be evaluated by obj instead of right here
        # in 'with'. It can be useful for example to run the
        # block in some wrapping context, which isn't really
        # possible with vanilla Python semantics.
        if hasattr.call([obj, '__around__'])
          obj.fcall('__around__', [block])
          return obj
        end

        begin
          obj.fcall('__enter__')
          block.call(obj)
        rescue ::Exception => e
          if !obj.fcall('__exit__', [e.class, e, e.backtrace])
            raise e
          end
        else
          obj.fcall('__exit__', [nil, nil, nil])
        end

        obj
      end
    end

    def isinstance
      @isinstance ||= -> (args = [], **kwargs) do
        obj = args.get(0)
        types = Array(args.get(1))

        types.any? do |type|
          case type
            when PythonClass
              obj.is_a?(type.klass) || obj.is_a?(type.instance_module)
            when ::Class
              obj.is_a?(type)
            else
              raise Pyrb::TypeError.klass, 'isinstance() arg 2 must be a type or tuple of types'
          end
        end
      end
    end

    def raize(err, from: :default)
      e = case err
        when PythonClass
          err.klass.new
        else
          err
      end

      Absolver.set_cause(e, from) unless from == :default
      raise e
    end

    def rezcue(err)
      case err
        when ::Array
          err.flat_map { |e| rezcue(e) }
        when PythonClass
          [err.klass]
        else
          [err]
      end
    end

    def print
      @print ||= -> (args = [], **kwargs) do
        str = args.get(0) || ''
        end_ = kwargs.fetch(:end_, args.get(1)) || "\n"
        STDOUT.write(str)
        STDOUT.write(end_)
      end
    end

    def type
      @type ||= -> (args = [], **kwargs) do
        obj = args.get(0)

        case obj
          when ::Proc
            Pyrb::Types::FunctionType
          when ::Array
            Pyrb::List
          when ::Enumerable
            Pyrb::Types::GeneratorType
          when ::Integer
            Pyrb::Int
          when ::Float
            Pyrb::Float
          when ::String
            Pyrb::String
          when ::TrueClass, ::FalseClass
            Pyrb::Bool
          when ::NilClass
            Pyrb::None
          when PythonInstance
            obj.prototype
        end
      end
    end

    def hasattr
      @has_attr ||= -> (args = [], **kwargs) do
        obj = args.get(0)
        name = args.get(1)

        begin
          case obj.attrs
            when ::Hash
              obj.attrs.include?(name)
            else
              return obj.hasattr(name)
          end
        rescue ::NoMethodError
          # Rescue NoMethodError here because refined objects (i.e. array,
          # hash, etc), although they respond to #attrs, will return false
          # when asked
          false
        end
      end
    end

    def sorted
      @sorted ||= -> (args = [], **kwargs) do
        seq = iter.call([args.get(0)])
        reverse = kwargs.fetch(:reverse, false)
        key = kwargs[:key]

        # All this madness is to implement a stable sort that can
        # also perform reversed comparisions. Normally, stable sorting
        # can be done in ruby via with_index.sort_by { |elem, i| [elem, i] },
        # but that unfortunately doesn't let us perform the comparison
        # between to elements ourselves, and therefore takes away the
        # ability to perform b <=> a instead of a <=> b. Instead we
        # have to attach indices first via map, then call regular #sort,
        # comparing normally or in reverse as necessary. Since the map
        # adds an index to each element, we have to remove it before
        # returning the result.

        n = 0

        result = seq
          .map { |elem| [elem, n += 1] }
          .sort do |a, b|
            a, a_idx = a
            b, b_idx = b
            a = key ? key.call([a]) : a
            b = key ? key.call([b]) : b
            next a_idx <=> b_idx if a == b
            reverse ? b <=> a : a <=> b
          end

        result.map!(&:first)
        result
      end
    end

    def sum
      @sum ||= -> (args = [], **kwargs) do
        enumerable = args.get(0)
        enumerable.fcall('__iter__').inject(0) { |accum, item| accum + item }
      end
    end

    def next_
      @next ||= -> (args = [], **kwargs) do
        iterator = args.get(0)

        case iterator
          when ::Enumerator
            iterator.next
          else
            iterator.fcall('__next__')
        end
      end
    end

    def reversed
      @reversed ||= -> (args = [], **kwargs) do
        iterator = args.get(0)
        iter = Utils.unwrap_iter(iterator)
        iter.fcall('__reversed__')
      end
    end

    def enumerate
      @enumerate ||= -> (args = [], **kwargs) do
        iterator = args.get(0)
        start = args.get(1) || 0

        iter = Utils.unwrap_iter(iterator)

        Enumerator.new do |yielder|
          iter.with_index { |item, idx| yielder << [idx, item] }
        end
      end
    end

    def any
      @any ||= -> (args = [], **kwargs) do
        iterator = args.get(0)
        Utils.unwrap_iter(iterator).any?
      end
    end

    def all
      @all ||= -> (args = [], **kwargs) do
        iterator = args.get(0)
        Utils.unwrap_iter(iterator).all?
      end
    end

    def getattr
      @getattr ||= -> (args = [], **kwargs) do
        obj = args.get(0)
        name = args.get(1)

        if Pyrb.hasattr.call([obj, name])
          obj.attrs[name]
        elsif args.size > 2
          args[2]
        else
          raise Pyrb::AttributeError.klass, "#{obj} object has no attribute #{name}"
        end
      end
    end

    def dir
      @dir ||= -> (args = [], **kwargs) do
        obj = args.get(0)

        if Pyrb.hasattr.call([obj, '__dir__'])
          Pyrb.hasattr.call([obj, '__dir__'])
          obj.fcall('__dir__')
        else
          obj.fcall('__dict__')
        end
      end
    end

    def abs
      @abs ||= -> (args = [], **kwargs) do
        args.get(0).abs
      end
    end

    def divmod
      @divmod ||= -> (args = [], **kwargs) do
        a = args.get(0)
        b = args.get(1)
        [(a / b).floor, a % b]
      end
    end

    def callable
      @callable ||= -> (args = [], **kwargs) do
        obj = args.get(0)
        obj.respond_to?(:call)
      end
    end

    def chr
      @chr ||= -> (args = [], **kwargs) do
        args.get(0).chr
      end
    end

    def ord
      @ord ||= -> (args = [], **kwargs) do
        args.get(0).ord
      end
    end

    def setattr
      @setattr ||= -> (args = [], **kwargs) do
        object = args.get(0)
        name = args.get(1)
        value = args.get(2)
        object.attrs[name] = value
      end
    end

    def round
      @round ||= -> (args = [], **kwargs) do
        args.get(0).round(args.get(1) || 0)
      end
    end

    # lord jesus forgive me
    def eeval
      @eeval ||= -> (args = [], **kwargs) do
        eval(args.get(0))
      end
    end

    def iter
      @iter ||= -> (args = [], **kwargs) do
        object = args.get(0)
        has_sentinel = args.size > 1

        if has_sentinel
          sentinel = args.get(1)

          Pyrb::Iterator.new([
            Enumerator.new do |y|
              loop do
                nxt = object.call
                break if nxt == sentinel
                y << nxt
              end
            end
          ])
        else
          return object if object.is_a?(::Enumerator)
          return Pyrb::Iterator.new([object.each]) if object.class < ::Enumerable

          if hasattr.call([object, '__iter__'])
            iter = object.fcall('__iter__')

            return iter if iter.is_a?(::Enumerator)
            return Pyrb::Iterator.new([iter.each]) if iter.class < ::Enumerable

            if hasattr.call([object, '__next__'])
              return Pyrb::Iterator.new([
                Enumerator.new do |y|
                  loop { y << object.fcall('__next__') }
                end
              ])
            end
          elsif hasattr.call([object, '__getitem__'])
            return Pyrb::Iterator.new([
              Enumerator.new do |y|
                i = 0

                loop do
                  begin
                    nxt = object.fcall('__getitem__', [i])
                    y << nxt
                  rescue Pyrb::IndexError.klass
                    break
                  end

                  i += 1
                end
              end
            ])
          end

          raise Pyrb::TypeError.klass, "'#{object}' is not iterable"
        end
      end
    end

    def repr
      @repr ||= -> (args = [], **kwargs) do
        args.get(0).fcall('__repr__')
      end
    end

    def str
      @str ||= -> (args = [], **kwargs) do
        args.get(0).fcall('__str__')
      end
    end

    def splat(obj)
      case obj
        when ::Array
          obj
        else
          Pyrb::List.call([obj])
      end
    end

    def deconstruct(term_count, *objs)
      terms = if objs.size == 1
        splat(objs.first)
      else
        splat(objs)
      end

      if terms.size != term_count
        raise Pyrb::ValueError.klass, "not enough values to unpack (expected #{term_count}, got #{terms.size})"
      end

      terms
    end

    def assert(expr, msg = '')
      if __debug__
        raise Pyrb::AssertionError.new([msg]) if !expr
      end
    end

    def defn(arg_meta = {}, &block)
      mtd = -> (args = [], **kwargs) do
        idx = 0

        result = arg_meta.map do |name, props|
          if props
            if props[:splat]
              next args.get(idx..-1).tap { idx = args.size }
            elsif props[:kwsplat]
              next kwargs
            end
          end

          kwargs.fetch(name) do
            if idx >= args.size
              # dup here to prevent default value from being mutated by method
              # next props[:default].dup if props && props.include?(:default)
              next props[:default] if props && props.include?(:default)
              raise Pyrb::TypeError.klass, "missing #{arg_meta.size - idx} positional argument(s)"
            end

            args.get(idx)
          end.tap { idx += 1 }
        end

        # we have to use throw/catch here because it's impossible to
        # return from a block (next/break don't work because of
        # surrounding block contexts like each, etc)
        catch :return do
          block.call(*result)
        end
      end

      mtd.__pyrb_location__ = caller[0].split(':in ').first
      mtd.attrs['__module__'] = Runtime.module_stack.last
      mtd
    end

    def zuper
      @zuper ||= -> (args = [], **kwargs) do
        Pyrb::Super.new.tap do |sup|
          sup.attrs['__thisclass__'] = args.get(0)
          sup.attrs['__self__'] = args.get(1)
        end
      end
    end

    def issubclass
      @issubclass ||= -> (args = [], **kwargs) do
        klass = args.get(0)
        classinfo = Array(args.get(1))

        classinfo.any? do |candidate|
          candidate.parent_of?(klass)
        end
      end
    end

    def exit
      @exit ||= -> (args = [], **kwargs) do
        code = args.empty? ? 0 : args.get(0)
        ::Kernel.exit(code)
      end
    end

    def open
      @open ||= defn(file: nil, mode: { default: 'r' }, buffering: { default: -1 }, encoding: { default: nil }, errors: { default: nil }, newline: { default: nil }, closefd: { default: true }, opener: { default: nil }) do |file, mode, buffering, encoding, errors, newline, closefd, opener|
        # Ignore the rest of these arguments for now. I mean what even is going on here, Python. Christ.
        Pyrb::TextIOWrapper.new([Pyrb::BufferedRandom.new([File.open(file, mode)])])
      end
    end

    # ============ Annotations ============

    def classmethod
      @classmethod ||= -> (context, callable) do
        Pyrb::Classmethod.new(context, callable)
      end
    end

    def staticmethod
      @staticmethod ||= -> (context, callable) do
        Pyrb::Staticmethod.new(context, callable)
      end
    end
  end
end
