module Pyrb
  class Properties
    # TODO: all the .to_symming in here sucks
    def initialize(klass)
      @klass = klass
      @props = {}
    end

    def [](name)
      name = name.to_sym
      return @props[name] if @props.include?(name)

      @klass.parents.each do |parent|
        if parent.properties.include?(name)
          return parent.properties[name]
        end
      end

      nil
    end

    def []=(name, prop)
      @props[name.to_sym] = prop
    end

    def include?(name)
      name = name.to_sym
      return true if @props.include?(name)

      @klass.parents.each do |parent|
        return true if parent.properties.include?(name)
      end

      false
    end

    def del(name)
      @props.delete(name)
    end
  end
end
