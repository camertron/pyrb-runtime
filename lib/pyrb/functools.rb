module Pyrb
  module Functools_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['reduce'] = -> (args = [], **kwargs) do
      func, iterable = args.shift(2)
      iter = Utils.unwrap_iter(iterable)

      if args.size > 0
        iter.reduce(args.get(0)) do |accum, args|
          func.call([accum, args])
        end
      else
        iter.reduce do |accum, args|
          func.call([accum, args])
        end
      end
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  Functools = Functools_rb.exports['functools']
end
