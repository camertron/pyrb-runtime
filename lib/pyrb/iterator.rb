module Pyrb
  module Iterator_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    abc, _ = import('abc', from: 'collections')

    exports['iterator'] = Pyrb::PythonClass.new('iterator', [abc.attr('Iterator'), abc.attr('Iterable')], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.ivars['iter'] = args.get(1)
      end,

      '__iter__' => -> (args = [], **kwargs) do
        args.get(0)  # i.e. self
      end,

      '__next__' => -> (args = [], **kwargs) do
        args.get(0).ivars['iter'].next
      end,

      '__reversed__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        self_.prototype.new([self_.ivars['iter'].reverse_each])
      end
    })

    exports['iterator'].instance_module.class_eval do
      include ::Enumerable

      def each
        return to_enum(__method__) unless block_given?
        loop { yield self.next }
      end

      def next
        ivars['iter'].next
      end

      def with_index(*args, &block)
        ivars['iter'].with_index(*args, &block)
      end
    end
  end

  Iterator = Iterator_rb.exports['iterator']
end
