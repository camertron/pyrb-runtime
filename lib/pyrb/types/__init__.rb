module Pyrb
  module Types_init_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['FunctionType'] = Proc
    exports['GeneratorType'] = Enumerable

    exports['ModuleType'] = PythonClass.new('ModuleType', [Pyrb::Object], {
      # name: name of module, doc: module's docstring
      '__init__' => Pyrb.defn(self: nil, name: nil, doc: { default: nil }) do |self_, name, doc|
        Module.new do
          extend Pyrb::Runtime
          using Pyrb::Runtime

          def self.location
            __FILE__
          end

          exports['__name__'] = name
          exports['__doc__'] = doc
        end
      end
    })

    exports['__all__'] = %w(FunctionType GeneratorType ModuleType)

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end

  module Types
    FunctionType = Types_init_rb.exports['FunctionType']
    GeneratorType = Types_init_rb.exports['GeneratorType']
  end
end
