module Pyrb
  module List_iterator_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['list_iterator'] = Pyrb::PythonClass.new('list_iterator', [Pyrb::Iterator])
  end

  ListIterator = List_iterator_rb.exports['list_iterator']
end
