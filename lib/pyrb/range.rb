module Pyrb
  module Range_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['range'] = PythonClass.new('range', [Pyrb::Object], {
      '__init__' => -> (args = [], **kwargs) do
        self_ = args.shift
        self_.attrs['step'] = 1

        if args.size == 1
          self_.attrs['start'] = 0
          self_.attrs['stop'] = args.get(0) || -1
        elsif args.size == 2
          self_.attrs['start'] = args.get(0) || 0
          self_.attrs['stop'] = args.get(1) || -1
        else
          step = args.get(2) || 1

          if step > 0
            self_.attrs['start'] = args.get(0) || 0
            self_.attrs['stop'] = args.get(1) || -1
          else
            self_.attrs['start'] = args.get(0) || -1
            self_.attrs['stop'] = args.get(1) || 0
          end

          self_.attrs['step'] = step
        end
      end,

      '__iter__' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        Pyrb::Iterator.new([
          self_.attr('start').step(
            self_.attr('stop') + (self_.attr('step') < 0 ? 1 : -1),
            self_.attr('step')
          )
        ])
      end,

      '__len__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        start = self_.attr('start')
        stop = self_.attr('stop')
        step = self_.attr('step')

        if (stop > start && step > 0) || (start > stop && step < 0)
          ((stop - start).abs.div(step.abs)).ceil
        else
          0
        end
      end
    })

    exports['range'].klass.class_eval do
      def each(&block)
        fcall('__iter__').each(&block)
      end

      def include?(item)
        item >= attr('start') && item < attr('stop')
      end

      def [](idx)
        val = attr('start') + (idx * attr('step'))
        return val if include?(val)
        raise Pyrb::IndexError.klass, 'range object index out of range'
      end

      def apply_to(seq)
        return to_enum(__method__, seq) unless block_given?

        len = seq.fcall('__len__')
        start = attr('start')
        stop = attr('stop')
        step = attr('step')

        # convert to positive indices
        start += len + 1 if start < 0
        stop += len + 1 if stop < 0

        # Avoid extending past the end of the sequence. Because ranges are
        # always exclusive, the len is correct to use here. For example,
        # "abc"[0:3] returns "abc" and "abc"[0:2] returns "ab".
        start = len if start > len
        stop = len if stop > len

        # A negative step is a special case when the start or stop indices
        # are also at the max and/or min respectively. Because we're iterating
        # "backwards", the first yielded index must be in-bounds, so we
        # subtract 1 from the start. Similarly, we subtract 1 from stop
        # in order to adhere to the exclusivity required by the range
        # algorithm below (we iterate while the value > -1 instead of 0).
        start -= 1 if start == len && step < 0
        stop -= 1 if stop == 0 && step < 0

        i = 0

        loop do
          ri = start + step * i

          if step >= 0
          # For a positive step, the contents of a range r are determined
          # by the formula r[i] = start + step*i where i >= 0 and r[i] < stop.
            break unless ri < stop
          else
            # For a negative step, the contents of the range are still
            # determined by the formula r[i] = start + step*i, but the
            # constraints are i >= 0 and r[i] > stop.
            break unless ri > stop
          end

          yield ri
          i += 1
        end
      end
    end
  end

  Range = Range_rb.exports['range']
end
