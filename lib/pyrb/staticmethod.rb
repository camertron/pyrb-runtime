module Pyrb
  class Staticmethod
    attr_reader :klass, :callable

    def initialize(klass, callable)
      @klass = klass
      @callable = callable
    end

    def call(args = [], **kwargs, &block)
      callable.call(args, **kwargs, &block)
    end
  end
end
