module Pyrb
  module Bool_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['bool'] = PythonClass.new('bool', [Pyrb::Object], {
      '__name__' => 'bool',

      '__eq__' => -> (args = [], **kwargs) do
        val = args.get(0).value
        other_val = args.get(1).value

        (val == true && (other_val == true || other_val == 1)) ||
          (val == false && (other_val == false || other_val == 0))
      end,

      '__ne__' => -> (args = [], **kwargs) do
        !args.get(0).fcall('__eq__', args, **kwargs)
      end
    })

    exports['bool'].klass.class_eval do
      def self.wrap(obj)
        allocate.tap do |inst|
          inst.init
          inst.ivars['value'] = obj
        end
      end

      def value
        ivars['value']
      end
    end

    exports['bool'].instance_eval do
      def call(args = [], **kwargs)
        obj = args.get(0)

        case obj
          when false, nil, 0
            return false
          when ::Hash, ::Array, ::Set
            return obj.size != 0
          else
            if Pyrb.hasattr.call([obj, '__bool__'])
              return obj['__bool__'].call
            elsif Pyrb.hasattr.call([obj, '__len__'])
              return obj['__len__'].call != 0
            end
        end

        true
      end
    end
  end

  Bool = Bool_rb.exports['bool']

  Pyrb::TRUE ||= Bool.wrap(true)
  Pyrb::FALSE ||= Bool.wrap(false)
end
