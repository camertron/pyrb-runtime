module Pyrb
  class PackageAttrs
    def initialize(package)
      @package = package
      @attrs = {}
    end

    def each(&block)
      @attrs.each(&block)
    end

    def [](submodule_or_package)
      if found = @attrs[submodule_or_package]
        return found
      end

      if init_export = init_exports[submodule_or_package]
        @attrs[submodule_or_package] = init_export
        return init_export
      end

      dir = File.join(@package.path, submodule_or_package)
      file = "#{File.join(@package.path, submodule_or_package)}.rb"

      submodule_or_package_name = "#{@package.package_name}.#{submodule_or_package}"

      if File.exist?(dir)
        @attrs[submodule_or_package] ||= PythonPackage.new(dir, submodule_or_package_name)
      elsif File.exist?(file)
        module_stack.push(submodule_or_package_name)
        require file
        module_stack.pop
        @attrs[submodule_or_package] ||= Pyrb::Sys.exports['modules'][file]
      end
    end

    def []=(key, value)
      @attrs[key] = value
    end

    alias_method :get, :[]

    def include?(submodule_or_package)
      @attrs.include?(submodule_or_package)
    end

    def fcall(func_name, args = [], **kwargs, &block)
      if attr = @attrs[func_name]
        attr.call(args, **kwargs, &block)
      else
        raise Pyrb::AttributeError.klass, "module #{File.basename(@package.path).chomp('.rb')} has no attribute '#{func_name}'"
      end
    end

    private

    def module_stack
      Pyrb::Runtime.module_stack
    end

    def init_exports
      if File.exist?(init_file)
        require init_file
        init = Pyrb::Sys.exports['modules'][init_file]

        # we might be in the process of loading this, in which case it won't
        # have been added to loaded modules yet, so bail out
        return {} unless init

        if all = init.exports['__all__']
          all.each_with_object({}) do |export, ret|
            ret[export] = init.imports[export] || init.exports[export]
          end
        else
          {}
        end
      else
        {}
      end
    end

    def init_file
      @init_file ||= File.join(@package.path, '__init__.rb')
    end
  end
end
