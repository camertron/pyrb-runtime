module Pyrb
  module Os_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    environ = PythonClass.new('_Environ', [Pyrb::Object], {
      'get' => Pyrb.defn(self: nil, key: nil) do |self_, key|
        ENV[key]
      end
    })

    # enable bracket [] notation
    environ.attrs['__getitem__'] = environ.attr('get')

    exports['environ'] = environ.new
    exports['path'] = Pyrb::Posixpath

    exports['unlink'] = Pyrb.defn(path: nil, dir_fd: { default: nil }) do |path, dir_fd|
      if ::File.directory?(path)
        raise Pyrb::IsADirectoryError.klass, "'#{path}' is a directory"
      end

      ::File.unlink(path)
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
