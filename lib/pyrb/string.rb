module Pyrb
  module String_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    DIGITS_RE = /\A\d+\z/

    exports['string'] = Pyrb::PythonClass.new('string', [Pyrb::Object], {
      '__name__' => 'string',
      'punctuation' => '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~',
      'ascii_lowercase' => ('a'..'z').to_a,

      '__str__' => -> (args = [], **kwargs) { args.get(0).value },

      '__len__' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        value = args.get(1) || ''
        self_.value.size
      end,

      '__eq__' => -> (args = [], **kwargs) do
        args.get(0).value.eq(args.get(1).value)
      end,

      '__ne__' => -> (args = [], **kwargs) do
        args.get(0).value.neq(args.get(1).value)
      end,

      '__lt__' => -> (args = [], **kwargs) do
        args.get(0).value.lt(args.get(1).value)
      end,

      '__gt__' => -> (args = [], **kwargs) do
        args.get(0).value.gt(args.get(1).value)
      end,

      '__le__' => -> (args = [], **kwargs) do
        args.get(0).value.le(args.get(1).value)
      end,

      '__ge__' => -> (args = [], **kwargs) do
        args.get(0).value.ge(args.get(1).value)
      end,

      'startswith' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        prefix = args.get(1)
        start = args.get(2) || 0
        len = self_.value.length
        stop = args.get(3) || len
        idx = self_.value.index(prefix.value)
        return false unless idx
        idx == start && idx + prefix.value.length <= stop
      end,

      'endswith' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        suffix = args.get(1)
        start = args.get(2) || 0
        len = self_.value.length
        stop = args.get(3) || len
        idx = self_.value.rindex(suffix.value)
        return false unless idx
        idx >= start && idx + suffix.value.length == stop
      end,

      'index' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        to_find = args.get(1)
        start = args.get(2) || 0
        len = self_.value.length
        stop = args.get(3) || len
        idx = self_.value.index(to_find.value)

        if idx.nil? || idx < start || idx > stop
          raise Pyrb::ValueError.klass, 'substring not found'
        end

        idx
      end,

      'append' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        str = args.get(1)
        self_.value.append(str)
      end,

      'lower' => -> (args = [], **kwargs) do
        args.get(0).value.downcase
      end,

      'upper' => -> (args = [], **kwargs) do
        args.get(0).value.upcase
      end,

      # String splitting is so flipping complicated in Python :eyeroll:
      'split' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        sep = kwargs.fetch(:sep) { args.get(1) }.value
        max = kwargs.fetch(:maxsplit) { args.get(2) }.value

        # The simplest split (i.e. on an empty string) returns empty array if
        # no separator was supplied, otherwise for some reason it returns a
        # single-element array containing an empty string. You can't make
        # this stuff up.
        if self_.empty?
          return sep.nil? ? [] : ['']
        end

        result = []

        # the exclusion of a separator triggers special splitting rules
        # wherein runs of whitespace are used as boundaries, because why not
        if sep.nil?
          start = 0
          finish = 0

          while finish < self_.length
            start = self_.index(/[^\s]+/, finish)
            break unless start

            if result.length == max
              result << self_[start..-1]
              break
            end

            finish = self_.index(/\s+/, start) || self_.length
            result << self_[start...finish]
          end
        else
          idx = 0
          last_idx = 0

          while last_idx <= self_.length
            if result.length == max
              result << self_[last_idx..-1]
              break
            end

            idx = self_.index(sep, last_idx) || self_.length
            result << self_[last_idx...idx]
            last_idx = idx + sep.length
          end
        end

        result
      end,

      'join' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        arr = args.get(1)
        Pyrb::List.call([arr]).map(&:value).join(self_.value)
      end,

      'strip' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        chars = args.get(1)
        re = chars.nil? ? '\\s' : Regexp.escape(chars.value)
        self_.value.gsub(/\A[#{re}]*|[#{re}]*\z/, '')
      end,

      'rstrip' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        chars = args.get(1)
        re = chars.nil? ? '\\s' : Regexp.escape(chars.value)
        self_.value.gsub(/[#{re}]*\z/, '')
      end,

      'lstrip' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        chars = args.get(1)
        re = chars.nil? ? '\\s' : Regexp.escape(chars.value)
        self_.value.gsub(/\A[#{re}]*/, '')
      end,

      '__iter__' => -> (args = [], **kwargs) do
        Pyrb::Iterator.new([args.get(0).value.each_char])
      end,

      '__reversed__' => -> (args = [], **kwargs) do
        args.get(0).value.each_char.reverse_each
      end,

      'isdigit' => -> (args = [], **kwargs) do
        (args.get(0).value =~ DIGITS_RE) != nil
      end,

      'capitalize' => -> (args = [], **kwargs) do
        args.get(0).value.sub(/./) { |m| m.upcase }
      end,

      'isupper' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        # there cannot be any lowercase chars and there must be
        # at least one uppercase char
        !(self_.value =~ /\A\p{^Lower}*\z/).nil? &&
          !(self_.value =~ /\p{Upper}/).nil?
      end,

      'islower' => -> (args = [], **kwargs) do
        self_ = args.get(0)

        # there cannot be any uppercase chars and there must be
        # at least one lowercase char
        !(self_.value =~ /\A\p{^Upper}*\z/).nil? &&
          !(self_.value =~ /\p{Lower}/).nil?
      end,

      'zfill' => -> (args = [], **kwargs) do
        self_ = args.get(0)
        len = args.get(1)
        value = self_.value

        if value.start_with?(/[+-]/)
          "#{value[0]}#{value[1..-1].rjust(len - 1, '0')}"
        else
          value.rjust(len, '0')
        end
      end,

      'format' => -> (args = [], **kwargs) do
        self_ = args.shift
        Pyrb::StringFormat.format(self_, args)
      end,

      'isalpha' => -> (args = [], **kwargs) do
        Pyrb::Str.fcall('isalpha', [args.get(0).value])
      end,

      'replace' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        old = args.get(1)
        repl = args.get(2)
        max = args.get(3) || ::Float::INFINITY

        count = 0
        last_idx = 0
        result = ''

        while last_idx < self_.size
          idx = if count >= max
            self_.size
          else
            self_.index(old, last_idx)
          end

          result << self_[last_idx...(idx || self_.size)]
          break if count >= max || idx.nil?
          result << repl
          count += 1
          last_idx = idx + old.size
        end

        result
      end,

      '__getitem__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        idx = args.get(1)
        raise Pyrb::IndexError.klass, 'string index out of range' if idx >= self_.size
        self_.get(idx)
      end,

      'encode' => -> (args = [], **kwargs) do
        begin
          self_ = args.get(0).value
          encoding = args.get(1) || 'UTF-8'
          Pyrb::Bytes.new([self_.encode(encoding)])
        rescue Encoding::UndefinedConversionError => e
          pos = self_.index(e.error_char)
          raise Pyrb::UnicodeEncodeError.klass, "'#{encoding}' codec can't encode character '\\x#{e.error_char.codepoints.first.to_s(16)}' in position #{pos}"
        end
      end,

      'find' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        substr = args.get(1)
        start = args.get(2)
        finish = args.get(3)

        if start
          idx = self_.index(substr, start)
          return -1 if !idx || (finish && idx >= finish)
          idx
        else
          self_.index(substr) || -1
        end
      end,

      'rfind' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        substr = args.get(1)
        start = args.get(2)
        finish = args.get(3)

        if start
          idx = if finish
            self_.rindex(substr, finish)
          else
            self_.rindex(substr)
          end

          return -1 if !idx || (idx < start)
          idx
        else
          self_.rindex(substr)
        end
      end,

      'partition' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        separator = args.get(1).value
        self_.partition(separator)
      end,

      'rpartition' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        separator = args.get(1).value
        self_.rpartition(separator)
      end,

      'count' => -> (args = [], **kwargs) do
        self_ = args.get(0).value
        substr = args.get(1)
        start = args.get(2) || 0
        finish = args.get(3) || self_.size

        idx = start
        count = 0

        while idx < finish
          idx = self_.index(substr, idx)
          break unless idx
          count += 1 if idx < finish
          idx += substr.size
        end

        count
      end,

      '__repr__' => -> (args = [], **kwargs) do
        self_ = args.get(0).value

        if self_.include?('"')
          "'#{self_.gsub(/([^\\])(')/) { "#{$1}\\#{$2}" } }'"
        elsif self_.include?("'")
          "\"#{self_}\""
        else
          "'#{self_}'"
        end
      end,

      '__contains__' => -> (args = [], **kwargs) do
        args.get(0).value.include?(args.get(1).value)
      end
    })
  end

  String = String_rb.exports['string']
end
