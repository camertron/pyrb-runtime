module Pyrb
  module Bytearray_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    exports['bytearray'] = PythonClass.new('bytearray', [Pyrb::Bytes], {
      '__init__' => Pyrb.defn(self: nil, bytes: { default: nil }) do |self_, bytes|
        self_.ivars['value'] = (bytes || ''.b).value
      end
    })

    exports['bytearray'].instance_eval do
      def call(args = [], **kwargs)
        if args.first.is_a?(::Integer)
          new(["\x00".b * args.first])
        elsif args.first.respond_to?(:getbuffer)
          new([args.first.getbuffer.data])
        else
          new([Pyrb::List.call([args.first]).pack('C*')])
        end
      end
    end
  end

  Bytearray = Bytearray_rb.exports['bytearray']
end
