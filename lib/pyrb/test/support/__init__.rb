require 'tempfile'

module Pyrb
  module Test_support_init_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    RUBY_OBJ_BYTES = 40

    struct, _ = import('struct')

    exports['verbose'] = false

    exports['calcobjsize'] = Pyrb.defn(fmt: nil) do |fmt|
      struct.calcsize(fmt) + RUBY_OBJ_BYTES
    end

    exports['calcvobjsize'] = Pyrb.defn(fmt: nil) do |fmt|
      struct.calcsize(fmt) + RUBY_OBJ_BYTES
    end

    exports['run_doctest'] = Pyrb.defn(module: nil, verbosity: { default: nil }, optionflags: { default: 0 }) do |module_, verbosity, optionflags|
      puts 'Doctests are not currently supported'
    end

    exports['EnvironmentVarGuard'] = PythonClass.new('EnvironmentVarGuard', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil) do |self_|
        self_.attrs['_environ'] = ENV.to_hash.dup
        self_.ivars['old_env'] = ENV.to_hash.dup
      end,

      '__getitem__' => Pyrb.defn(self: nil, key: nil) do |self_, key|
        self_.attrs['_environ'][key.value]
      end,

      '__setitem__' => Pyrb.defn(self: nil, key: nil, value: nil) do |self_, key, value|
        self_.attrs['_environ'][key.value] = value.value
        ENV[key.value] = value.value
      end,

      '__contains__' => Pyrb.defn(self: nil, item: nil) do |self_, item|
        self_.attrs['_environ'].include?(item.value)
      end,

      '__enter__' => Pyrb.defn(self: nil, args: { splat: true }) do |self_, *args|
        # do nothing
      end,

      '__exit__' => Pyrb.defn(self: nil, args: { splat: true }) do |self_, *args|
        # restore old env
        ENV.replace(self_.ivars['old_env'])
        self_.attrs['_environ'].replace(self_.ivars['old_env'])
      end
    })

    # prevent from garbage collection by assigning to a local variable
    tempfile = Tempfile.new
    exports['TESTFN'] = tempfile.path

    at_exit do
      tempfile.close
      tempfile.unlink
    end

    exports['__all__'] = %w(
      verbose calcobjsize calcvobjsize run_doctest EnvironmentVarGuard TESTFN
    )

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
