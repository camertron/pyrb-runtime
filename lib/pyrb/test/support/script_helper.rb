require 'open3'

module Pyrb
  module Test_support_script_helper_rb
    def self.location
      __FILE__
    end

    extend Pyrb::Runtime
    using Pyrb::Runtime

    sys, _ = import('sys')
    subprocess, _ = import('subprocess')

    _PythonRunResult = Pyrb::PythonClass.new('_PythonRunResult', [Pyrb::Object], {
      '__init__' => Pyrb.defn(self: nil, rc: nil, out: nil, err: nil) do |self_, rc, out, err|
        self_.attrs['rc'] = rc
        self_.attrs['out'] = out
        self_.attrs['err'] = err
      end
    })

    exports['assert_python_ok'] = Pyrb.defn(args: { splat: true }) do |args|
      exe = sys.attr('executable')
      stdout, stderr, status = Open3.capture3(exe, *args)

      if status != 0
        raise Pyrb::AssertionError.new([
          <<~END
            Process return code is #{status}
            command line: #{[$0, *args].inspect}

            stdout:
            ---
            #{stdout}
            --

            stderr:
            ---
            #{stderr}
            ---
          END
        ])
      end

      _PythonRunResult.new([
        rc, Pyrb::Bytes.call([stdout]), Pyrb::Bytes.call([stderr])
      ])
    end

    exports['spawn_python'] = Pyrb.defn(args: { splat: true }) do |args|
      subprocess.attr('Popen').new(args)
    end

    Pyrb::Sys.exports['modules'][__FILE__] = self
  end
end
