module Pyrb
  include Pyrb::Runtime
  using Pyrb::Runtime

  Object = Pyrb::PythonClass.new('object', [], {
    '__repr__' => -> (args = [], **kwargs) do
      args.get(0).inspect
    end,

    '__str__' => -> (args = [], **kwargs) do
      args.get(0).to_s
    end,

    '__eq__' => -> (args = [], **kwargs) do
      args.get(0).eq(args.get(1))
    end,
  })
end
