require 'pry-byebug' # TODO: remove

module Pyrb
  autoload :Buffer,             'pyrb/buffer'
  autoload :Bytes,              'pyrb/bytes'
  autoload :Bytearray,          'pyrb/bytearray'
  autoload :ClassAttrs,         'pyrb/class_attrs'
  autoload :Classmethod,        'pyrb/classmethod'
  autoload :Date,               'pyrb/date'
  autoload :DeprecationWarning, 'pyrb/warning'
  autoload :Encodings,          'pyrb/encodings'
  autoload :Filter ,            'pyrb/filter'
  autoload :Functools,          'pyrb/functools'
  autoload :Fractions,          'pyrb/fractions'
  autoload :Frozenset,          'pyrb/frozenset'
  autoload :Generator,          'pyrb/generator'
  autoload :Importer,           'pyrb/importer'
  autoload :Iterator,           'pyrb/iterator'
  autoload :InstAttrs,          'pyrb/inst_attrs'
  autoload :ListIterator,       'pyrb/list_iterator'
  autoload :Map,                'pyrb/map'
  autoload :Math,               'pyrb/math'
  autoload :Memoryview,         'pyrb/memoryview'
  autoload :MethodDescriptor,   'pyrb/method_descriptor'
  autoload :NamedModule,        'pyrb/named_module'
  autoload :NativeRange,        'pyrb/native_range'
  autoload :PackageAttrs,       'pyrb/package_attrs'
  autoload :Pattern,            'pyrb/re'
  autoload :Posixpath,          'pyrb/posixpath'
  autoload :Properties,         'pyrb/properties'
  autoload :Property,           'pyrb/property'
  autoload :PythonClass,        'pyrb/python_class'
  autoload :PythonInstance,     'pyrb/python_instance'
  autoload :PythonPackage,      'pyrb/python_package'
  autoload :Range,              'pyrb/range'
  autoload :Re,                 'pyrb/re'
  autoload :Repl,               'pyrb/repl'
  autoload :Runtime,            'pyrb/runtime'
  autoload :Seq,                'pyrb/seq'
  autoload :Set,                'pyrb/set'
  autoload :Staticmethod,       'pyrb/staticmethod'
  autoload :Str,                'pyrb/str'
  autoload :StringFormat,       'pyrb/string_format'
  autoload :Super,              'pyrb/super'
  autoload :TimeDelta,          'pyrb/date'
  autoload :Types,              'pyrb/types/__init__'
  autoload :UserWarning,        'pyrb/warning'
  autoload :Utils,              'pyrb/utils'
  autoload :Warning,            'pyrb/warning'
  autoload :Warnings,           'pyrb/warnings'
  autoload :Zip,                'pyrb/zip'

  module Collections
    autoload :Counter,        'pyrb/collections/counter'
    autoload :Deque,          'pyrb/collections/deque'
    autoload :Sequence,       'pyrb/collections/sequence'
  end

  module Itertools
    autoload :Chain,          'pyrb/itertools/chain'
  end

  class << self
    attr_accessor :__debug__

    def jar_path
      @jar_path ||= File.expand_path('../ext/Pyrb.jar', __dir__)
    end
  end
end

# built-ins (this is necessarily a very specific load order)
require 'pyrb/object'
require 'pyrb/exception'
require 'pyrb/errors'
require 'pyrb/list'
require 'pyrb/dict'
require 'pyrb/string'
require 'pyrb/int'
require 'pyrb/float'
require 'pyrb/bool'
require 'pyrb/none'
require 'pyrb/sys'
require 'pyrb/io/__init__'

Pyrb.__debug__ = true

# do this last
Pyrb::Sys.init
