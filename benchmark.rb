require 'ruby-prof'
require 'benchmark/ips'
require 'pyrb'

module Bench
  extend Pyrb::Runtime
  using Pyrb::Runtime

  def self.location
    __FILE__
  end

  exports[:nth_prime], _ = import('nth_prime', from: 'spec.integration.source.prime')

  exports[:MyClass] = Pyrb::PythonClass.new('MyClass', [Pyrb::Object], {
    'func' => -> (args = [], **kwargs) do
      # do nothing
    end
  })

  # result = RubyProf.profile do
  #   100_000.times { 1.fcall('__eq__', [1]) }
  # end

  # printer = RubyProf::FlatPrinter.new(result)
  # printer.print(STDOUT)
end

# MyClass = Bench.exports[:MyClass]

struct, = Pyrb.import('struct')
args = ['60q', 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, 2147483648, 4294967296, 8589934592, 17179869184, 34359738368, 68719476736, 137438953472, 274877906944, 549755813888, 1099511627776, 2199023255552, 4398046511104, 8796093022208, 17592186044416, 35184372088832, 70368744177664, 140737488355328, 281474976710656, 562949953421312, 1125899906842624, 2251799813685248, 4503599627370496, 9007199254740992, 18014398509481984, 36028797018963968, 72057594037927936, 144115188075855872, 300, 700000]

# result = RubyProf.profile do
#   # Bench.exports[:nth_prime].call([6000])
#   struct.fcall('pack', args)
# end

# printer = RubyProf::FlatPrinter.new(result)
# printer.print(STDOUT)


Benchmark.ips do |x|
  x.report do
    # Bench.exports[:nth_prime].call([50])
    struct.fcall('pack', args)
  end
end
